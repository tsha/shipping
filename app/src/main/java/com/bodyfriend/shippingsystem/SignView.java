package com.bodyfriend.shippingsystem;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.provider.MediaStore.Images;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Sign을 할때 사용되는 뷰이다.
 */
public class SignView extends View {
	Paint mPaint; // 페인트 객체 선언
	ArrayList<Vertex> arVertex; // 꼭짓점들의 모임
	private Bitmap mBitmap;
	private boolean isSetBitmap = false;
	private Canvas mCanvas;

	public SignView(Context context) {
		super(context);
		init(context);
	}

	public SignView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public SignView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	private void init(Context context) {
		// 페인트 객체 생성 후 설정
		mPaint = new Paint();
		mPaint.setColor(Color.BLACK);
		mPaint.setStrokeWidth(12);
		mPaint.setAntiAlias(true); // 안티얼라이싱
		arVertex = new ArrayList<Vertex>();

		// this.setBackgroundColor(Color.TRANSPARENT);
		// this.setZOrderOnTop(true); // necessary
		// getHolder().setFormat(PixelFormat.TRANSPARENT);
		// getHolder().addCallback((Callback) this);
		// mThread = new ViewThread(this);

	}

	/**
	 * 비트맵 인스턴스가 null이 아니면 리턴한다.<br>
	 * 뷰의 사이즈가 가로 또는 세로가 0이면 비트맵 인스턴스를 만들지 않는다.<br>
	 * 뷰의 사이즈만큼 비트맵을 초기화한다.
	 */
	private void initBitmap() {

		if (mBitmap != null) {
			return;
		}
		isSetBitmap = false;

		final int width = getWidth();
		final int height = getHeight();

		if (width == 0 || height == 0) {
			return;
		}

//		mBitmap = Bitmap.createBitmap(width, height, Config.ALPHA_8);
		 mBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		 mBitmap.eraseColor(0);
	}

	public void clearCanvas() {
		arVertex = new ArrayList<Vertex>();
		mBitmap = null;
		invalidate();
	}

	public Uri getImageUri() {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		getBitmap().compress(CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(getContext().getContentResolver(), getBitmap(), "title", null);
		return Uri.parse(path);
	}

	public Uri getImageUri(CompressFormat format) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		getBitmap().compress(format, 100, bytes);
		String path = Images.Media.insertImage(getContext().getContentResolver(), getBitmap(), "title", null);
		return Uri.parse(path);
	}

	public Bitmap loadBitmap(Uri uri) {
		Bitmap bitmap = null;
		try {
			bitmap = Images.Media.getBitmap(getContext().getContentResolver(), uri);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bitmap;
	}

	public class Vertex {
		float x;
		float y;
		boolean draw; // 그리기 여부

		public Vertex(float x, float y, boolean draw) {
			this.x = x;
			this.y = y;
			this.draw = draw;
		}
	}

	/** 터치이벤트를 받는 함수 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			arVertex.add(new Vertex(event.getX(), event.getY(), false));
			break;
		case MotionEvent.ACTION_MOVE:
			arVertex.add(new Vertex(event.getX(), event.getY(), true));
		}

		invalidate(); // onDraw() 호출
		return true;
	}

	/** 화면을 계속 그려주는 함수 */
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		initBitmap();
		if (mBitmap == null) {
			return;
		}
		if (isSetBitmap == false) {
			mCanvas = new Canvas(mBitmap);
			mCanvas.setBitmap(mBitmap);
			isSetBitmap = true;
		}


		// 그리기
		for (int i = 0; i < arVertex.size(); i++) {
			if (arVertex.get(i).draw) { // 이어서 그리고 있는 중이라면
				mCanvas.drawLine(arVertex.get(i - 1).x, arVertex.get(i - 1).y, arVertex.get(i).x, arVertex.get(i).y, mPaint);
				// 이전 좌표에서 다음좌표까지 그린다.
			} else {
				mCanvas.drawPoint(arVertex.get(i).x, arVertex.get(i).y, mPaint);
				// 점만 찍는다.
			}
		}

		canvas.drawBitmap(mBitmap, 0, 0, mPaint);
	}

	/**
	 * @return 현재 그려진 그림을 비트맵으로 가져온다.
	 */
	@SuppressLint("NewApi")
	public Bitmap getBitmap() {
		return mBitmap;
	}
}
