package com.bodyfriend.shippingsystem.main.shipping_list;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;


import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.map.net.NaverGeocoderApi;
import com.bodyfriend.shippingsystem.main.map.net.NaverGeocoderClient;
import com.bodyfriend.shippingsystem.main.map.net.TMapApi;
import com.bodyfriend.shippingsystem.main.map.tmap.FullTextGeoCodingData;
import com.bodyfriend.shippingsystem.main.map.vo.Geocorder;
import com.bodyfriend.shippingsystem.main.map.vo.Item;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MapActivity extends BFActivity {

    private final int PERMISSION_MAP = 10;
    private GoogleMap map;
    private String addr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(onMapReadyCallback);
    }

    @Override
    protected void onParseExtra() {
        super.onParseExtra();

        addr = getIntent().getStringExtra("addr");
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setText(R.id.editText, addr);
        setOnClickListener(R.id.search, onSearchClickListener);

        findViewById(R.id.mapFrontView).setOnTouchListener(onMapTouchListener);
    }

    private View.OnClickListener onSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setMarker(text(R.id.editText));
            hideKeyboard(v);
        }
    };

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapActivity.this.map = googleMap;

            MapActivity.this.map.setOnMarkerClickListener(onMarkerClickListener);

            if (!checkAndRequestPermissions(PERMISSION_MAP, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                map.setMyLocationEnabled(true);
                findViewById(R.id.search).performClick();
            }
        }
    };

    private void changeCamera(CameraUpdate update) {
        changeCamera(update, null);
    }

    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback) {
        map.animateCamera(update, Math.max(1000, 1), callback);
    }

    private void setMarker(String addr) {
        if (map == null) {
            return;
        }
        map.clear();
        TMapApi tMapApi = ServiceGenerator.changeApiBaseUrl(TMapApi.class, NetConst.HOST_TMAP);
        Call<FullTextGeoCodingData> geocoder = tMapApi.fullAddrGeo(addr);
        geocoder.enqueue(new Callback<FullTextGeoCodingData>() {
            @Override
            public void onResponse(Call<FullTextGeoCodingData> call, Response<FullTextGeoCodingData> response) {
                MarkerOptions options = new MarkerOptions();
                Location location = new Location("");
                if (response.body() != null && response.body().coordinateInfo != null && response.body().coordinateInfo.coordinate != null) {
                    FullTextGeoCodingData.CoordinateInfo.Coordinate item = response.body().coordinateInfo.coordinate.get(0);


                    location.setLatitude(Double.parseDouble(!item.lat.isEmpty() ? item.lat : item.newLat));
                    location.setLongitude(Double.parseDouble(!item.lon.isEmpty() ? item.lon : item.newLon));
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    options.position(latLng).title(addr);
                    map.addMarker(options);
                    setMarkerInfo(latLng);
                } else {

                    Geocoder geocoder = new Geocoder(MapActivity.this);
                    List<Address> listAddress;
                    try {
                        listAddress = geocoder.getFromLocationName(addr, 1);
                        if (listAddress.size() > 0) {
                            Address AddrAddress = listAddress.get(0);
                            final double latitude = AddrAddress.getLatitude();
                            final double longitude = AddrAddress.getLongitude();
                            location.setLongitude(latitude);
                            location.setLatitude(longitude);
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                            options.position(latLng).title(addr);
                            map.addMarker(options);
                            setMarkerInfo(latLng);
                        } else {
                            toast("검색하지 못하였습니다. 주소를 확인해 주세요.");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FullTextGeoCodingData> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    private void setMarkerInfo(LatLng latLng) {

        CameraPosition build = new CameraPosition.Builder().target(latLng)
                .zoom(15.5f)
                .bearing(0)
                .tilt(25)
                .build();
        changeCamera(CameraUpdateFactory.newCameraPosition(build));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PERMISSION_MAP) {
            if (resultCode == Activity.RESULT_OK) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                map.setMyLocationEnabled(true);
                findViewById(R.id.search).performClick();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            findViewById(R.id.searchLayout).setVisibility(View.GONE);
            return false;
        }
    };

    private View.OnTouchListener onMapTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                findViewById(R.id.searchLayout).setVisibility(View.VISIBLE);
                hideKeyboard(v);
            }

            return false;
        }
    };
}
