package com.bodyfriend.shippingsystem.main.map


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.databinding.ItemRouteMapBinding
import com.bodyfriend.shippingsystem.main.map.vo.MapShippingListData


class ShippingItemAdapter(val list: List<MapShippingListData.ResultData.ShippingItem>, private val itemClickListener: OnItemClickListener)
    : RecyclerView.Adapter<ShippingItemAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemRouteMapBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size

    interface OnItemClickListener {


        fun onClick(view: View, data: MapShippingListData.ResultData.ShippingItem)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val shippingItem = list[position]
//        Log.d("shippingItem.cardToggle : " + shippingItem.cardToggle)
//        if (shippingItem.cardToggle) {
//            holder.binding.carView.setCardBackgroundColor(Color.parseColor("#fcfca9"))
//        } else {
//            holder.binding.carView.setCardBackgroundColor(Color.parseColor("#ffffff"))
//        }

        holder.binding.shippingItem = shippingItem
        holder.binding.event = itemClickListener
    }


    class ViewHolder(val binding: ItemRouteMapBinding) : RecyclerView.ViewHolder(binding.root)


}
