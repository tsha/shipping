package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemForwardingProductBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData


class ProductListAdapter(val itemClick: (DeliverManStockData.ResultDatum) -> Unit) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {


    private var mealList: MutableList<DeliverManStockData.ResultDatum> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        holder.view.cardView.setOnClickListener { itemClick(mealList[position]) }
    }


    fun updateMealList(postList: MutableList<DeliverManStockData.ResultDatum>) {
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun addUploadItem(item: DeliverManStockData.ResultDatum) {
        mealList.add(0, item)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemForwardingProductBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_forwarding_product, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<DeliverManStockData.ResultDatum> {
        return mealList
    }

    class ViewHolder(var view: ItemForwardingProductBinding) : RecyclerView.ViewHolder(view.root)


}