package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.base.log.Log
import com.bodyfriend.shippingsystem.databinding.ActivityInventoryControlBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.AddedProductAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData
import com.bodyfriend.shippingsystem.main.delivery_order.net.ProductSerialData
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseListData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.DeliveryInventoryViewMode

import com.google.zxing.client.android.CaptureActivity
import kotlinx.android.synthetic.main.activity_inventory_control.*


class DeliveryInventoryActivity : AppCompatActivity(), WarehouseNameListDialog.WarehouseDialogInterface, WarehouseStockListDialog.ProductDialogInterface
        , DeliveryInventoryDialogFragment.DeliveryInventoryDialogInterface {

    override fun selectItem(productData: DeliverManStockData.ResultDatum, giftIndex: Int) {

    }

    override fun warehouseProductSelect(productData: DeliverManStockData.ResultDatum) {
        val deliveryStock = ProductSerialData.ResultData()
        deliveryStock.serialCd = productData.serialCd
        deliveryStock.qcy = "1"
        deliveryStock.slCd = productData.slCd
        deliveryStock.slNm = productData.slNm
        deliveryStock.giftYn = productData.giftYn
        deliveryStock.itemNm = productData.itemNm
        deliveryStock.itemCd = productData.itemCd
//        if (productData.serialCd == "*") {
        viewModel.addOtherItem(deliveryStock)
//        } else {
//            viewModel.onScanRfid(context = this)
//        }

    }


    private lateinit var viewModel: DeliveryInventoryViewMode
    private lateinit var mBinding: ActivityInventoryControlBinding
    private lateinit var addedProductAdapter: AddedProductAdapter

    companion object {
        const val SCAN_RESULT_CODE = 23323
        const val WAREHOUSE_CODE = "1000"
        const val DELIVERY_CODE = "2000"
    }


    override fun selectItem(productData: DeliverManStockData.ResultDatum) {
        val giftProduct = ProductSerialData.ResultData()
        giftProduct.itemCd = productData.itemCd
        giftProduct.itemNm = productData.itemNm
        giftProduct.giftYn = "Y"
        viewModel.addOtherItem(giftProduct)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_inventory_control)
        viewModel = ViewModelProviders.of(this).get(DeliveryInventoryViewMode::class.java)
        mBinding.viewModel = viewModel
        setSupportActionBar(mBinding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mBinding.toolbar.setNavigationOnClickListener { onBackPressed() }

        viewModel.run {
            //            getDelivryManStockList()
            getObserver(this)
        }


        addedProductAdapter = AddedProductAdapter(itemClick = { resultDatum: ProductSerialData.ResultData, position: Int ->
            viewModel.delItem(position, resultDatum)
        }, itemSelected = { index: Int, count: Int ->
            viewModel.setProductCount(index, count)
        })


        val mRecentLayoutManager = LinearLayoutManager(this@DeliveryInventoryActivity)
        mBinding.listDeliveryStock.apply {
            layoutManager = mRecentLayoutManager
            setHasFixedSize(true)
            adapter = addedProductAdapter
        }


        mBinding.editSelectedWarehouse.setOnClickListener {
            val groupCd = if (mBinding.radioWarehouse.isChecked) {
                WAREHOUSE_CODE
            } else {
                DELIVERY_CODE
            }
            val pop = WarehouseNameListDialog.newInstance(groupCd)
            val fm = this@DeliveryInventoryActivity.supportFragmentManager
            pop.show(fm, "name")
        }

        /*mBinding.btnTransmission.setOnClickListener(View.OnClickListener {
            viewModel.setTransmission(addedProductAdapter.getItems(), resources.getStringArray(R.array.returnTypeCode)[mBinding.spinnerReturnTape.selectedItemPosition], mBinding.editSelectedWarehouse.tag as WarehouseListData.ResultData)
        })*/
    }


    private fun getObserver(viewModelDelivery: DeliveryInventoryViewMode) {
        viewModel.isSessionOut.observe(this, Observer {

        })

        viewModelDelivery.resultData.observe(this@DeliveryInventoryActivity, Observer { it ->
            it?.let {
                if (it.resultMsg.isNotBlank()) {
                    viewModelDelivery.getDelivryManStockList()
                }
            }
        })


        viewModelDelivery.selectedInventoryItem.observe(this@DeliveryInventoryActivity, Observer {

            Log.d("TAG", "selectedInventoryItem ~~~~~")
            it?.let { it1 -> addedProductAdapter.updateMealList(it1) }
        })



        viewModelDelivery.otherItem.observe(this@DeliveryInventoryActivity, Observer {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("입고 확인")
                    .setMessage("재고에 등록 되지 않은 제품입니다. 입고 요청 하시겠습니까?")
                    .setNegativeButton("취소") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .setPositiveButton("확인") { dialog, _ ->
                        Log.d("TAG", "addedItem~~~~~ $it")
                        it?.let { it1 -> viewModel.addOtherItem(it1) }
                        dialog.dismiss()
                    }
                    .show()
        })

        viewModelDelivery.otherListError.observe(this@DeliveryInventoryActivity, Observer {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("재고 관리")
                    .setMessage(it)
                    .setPositiveButton("확인") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
            progressBarLoading.visibility = View.GONE
        })

        viewModelDelivery.addedItem.observe(this, Observer {
            Log.d("TAG", "addedItem~~~~~")
            it?.let { it1 -> addedProductAdapter.updateMealList(it1) }
        })

        viewModelDelivery.addedItem.observe(this, Observer {

        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                SCAN_RESULT_CODE -> data?.getStringExtra(CaptureActivity.RESULT.RESULT_SN)?.let {

                    viewModel.addProductItem(it, mBinding.radioDelivery.isChecked)
                }
            }
        }
    }

    override fun selectItem(warehouseData: WarehouseListData.ResultData) {
        Log.d("TAG", "giftSelect $warehouseData")
        mBinding.editSelectedWarehouse.tag = warehouseData
        mBinding.editSelectedWarehouse.text = warehouseData.slNm
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_inventory_control, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.menuInventoryHistory) {
            startActivity(Intent(this@DeliveryInventoryActivity, InventoryRequestHistoryActivity::class.java))
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}
