package com.bodyfriend.shippingsystem.main.delivery_order.net;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSerialValidData {

    @SerializedName("resultData")
    @Expose
    public ResultData data;

    @SerializedName("resultMsg")
    @Expose
    public String resultMsg;
    @SerializedName("resultCode")
    @Expose
    public String resultCode;


    @SerializedName("tSlCd")
    @Expose
    public String tSlCd;

    @SerializedName("tReason")
    @Expose
    public String tReason;

    public static class ResultData {
        public String PROC_STAT_NM;
        public String ISSUE_NO;
        public String MOV_TYPE_NM;
        public String collectYn;
        public String toSlCd;
        public String makerCd;
        public boolean isValid;
        public String issueNo;
        public String item;
        public String lotFlg;


        @SerializedName("BAD_AMOUNT")
        @Expose
        public Integer badAmount;
        @SerializedName("SL_CD")
        @Expose
        public String slCd;
        @SerializedName("SL_NM")
        @Expose
        public String slNm;
        @SerializedName("GIFT_YN")
        @Expose
        public String giftYn;
        @SerializedName("ITEM_NM")
        @Expose
        public String itemNm;
        @SerializedName("ITEM_CD")
        @Expose
        public String itemCd;
        @SerializedName("SERIAL_CD")
        @Expose
        public String serialCd;
        @SerializedName("GOOD_AMOUNT")
        @Expose
        public Integer goodAmount;

        @Override
        public String toString() {
            return "ResultData{" +
                    "PROC_STAT_NM='" + PROC_STAT_NM + '\'' +
                    ", ISSUE_NO='" + ISSUE_NO + '\'' +
                    ", MOV_TYPE_NM='" + MOV_TYPE_NM + '\'' +
                    ", collectYn='" + collectYn + '\'' +
                    ", toSlCd='" + toSlCd + '\'' +
                    ", makerCd='" + makerCd + '\'' +
                    ", isValid=" + isValid +
                    ", issueNo='" + issueNo + '\'' +
                    ", item='" + item + '\'' +
                    ", badAmount=" + badAmount +
                    ", slCd='" + slCd + '\'' +
                    ", slNm='" + slNm + '\'' +
                    ", giftYn='" + giftYn + '\'' +
                    ", itemNm='" + itemNm + '\'' +
                    ", itemCd='" + itemCd + '\'' +
                    ", serialCd='" + serialCd + '\'' +
                    ", goodAmount=" + goodAmount +
                    '}';
        }
    }
}
