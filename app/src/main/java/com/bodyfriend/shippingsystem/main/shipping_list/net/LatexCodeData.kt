package com.bodyfriend.shippingsystem.main.shipping_list.net

import com.google.gson.annotations.SerializedName

data class LatexCodeData(@SerializedName("resultData") val resultData: ArrayList<ResultData>) {
    data class ResultData(val PRODUCT_NAME: String, val PRODUCT_CODE: String)
}