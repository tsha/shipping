package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 관계 데이터
 */
public class searchCodeRelative extends searchCodeList {

	public searchCodeRelative() {
		super();
		setParam("code", 9700);
	}

	@Override
	protected void parse(String json) {
		super.parse(json);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}

//	05-20 16:30:10.041: W/_NETLOG_IN(23559):         {
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_DT": 1432095939077,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD": "01",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DESCRIPTION": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD_NM": "본인",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD_NM": "관계",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_DT": 1432095939077,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "GROUP_CD": "SP",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "SORT_SEQ": "1",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_2": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_3": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_1": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD": "9700"
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         },
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         {
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_DT": 1432095959920,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD": "02",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DESCRIPTION": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD_NM": "부모",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD_NM": "관계",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_DT": 1432096370550,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "GROUP_CD": "SP",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "SORT_SEQ": "2",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_2": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_3": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_1": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD": "9700"
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         },
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         {
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_DT": 1432095979317,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD": "03",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DESCRIPTION": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD_NM": "형제",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD_NM": "관계",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_DT": 1432096378763,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "GROUP_CD": "SP",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "SORT_SEQ": "3",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_2": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_3": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_1": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD": "9700"
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         },
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         {
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_DT": 1432096395160,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD": "04",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DESCRIPTION": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD_NM": "자매",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD_NM": "관계",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_DT": 1432096395160,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "GROUP_CD": "SP",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "SORT_SEQ": "4",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_2": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_3": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_1": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD": "9700"
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         },
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         {
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_DT": 1432096405493,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD": "05",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DESCRIPTION": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "DET_CD_NM": "남매",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD_NM": "관계",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "UPD_DT": 1432096405493,
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "GROUP_CD": "SP",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "INS_ID": "adminsp",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "SORT_SEQ": "5",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_2": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_3": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "REF_1": "",
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):             "COMM_CD": "9700"
//		05-20 16:30:10.041: W/_NETLOG_IN(23559):         }

}
