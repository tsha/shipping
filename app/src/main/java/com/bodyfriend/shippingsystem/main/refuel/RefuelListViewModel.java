package com.bodyfriend.shippingsystem.main.refuel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.bodyfriend.shippingsystem.BR;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.refuel.net.RefuelData;
import com.bodyfriend.shippingsystem.main.refuel.net.Sheet;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RefuelListViewModel extends BaseObservable {

    private final Callback mCallback;

    private String carNo;
    private boolean isShowProgress = true;
    private boolean isNoData;

    public interface Callback {
        void onRefuelData(ArrayList<RefuelData.Record> refuelData);
    }

    @Bindable
    public boolean isShowProgress() {
        return isShowProgress;
    }

    @Bindable
    public String getCarNo() {
        return carNo;
    }

    @Bindable
    public boolean isNoData() {
        return isNoData;
    }

    RefuelListViewModel(Callback callback) {
        this.carNo = PP.carNumber.get();
        this.mCallback = callback;
        Log.d("RefuelListViewModel " + carNo);

        notifyPropertyChanged(BR.carNo);
        getRefuelList(getCarNo());
    }

    /**
     * 차량번호로 구글 시트 정보 가져오기
     *
     * @param carNo 차량 번호
     */
    private void getRefuelList(String carNo) {
//        notifyPropertyChanged(BR.showProgress);
        Disposable disposableObserver = ServiceGenerator.changeApiBaseUrl(Sheet.class, NetConst.GOOGEL_GAS_SCRIPT_URL)
                .getGasList(carNo)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(refuelData -> {
                    if (refuelData.message == null && !refuelData.records.isEmpty()) {
                        mCallback.onRefuelData(refuelData.records);
                    } else {
                        isNoData = true;
                    }
                    isShowProgress = false;
                    notifyPropertyChanged(BR.showProgress);
                    notifyPropertyChanged(BR.noData);
                });
    }
}
