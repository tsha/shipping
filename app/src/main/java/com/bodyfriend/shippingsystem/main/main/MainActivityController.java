package com.bodyfriend.shippingsystem.main.main;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by 이주영 on 2016-12-08.
 */

public interface MainActivityController {
    void changeTitle(int resId);

    void changeFragment(Fragment fragment);

    View getView(int resId);
}
