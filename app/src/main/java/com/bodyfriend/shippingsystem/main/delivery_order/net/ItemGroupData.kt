package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ItemGroupData {

    @SerializedName("resultData")
    @Expose
    var resultData: List<ResultDatum>? = null

    inner class ResultDatum {
        @SerializedName("ITEM_GROUP_NM")
        @Expose
        var itemGroupNm: String? = null
        @SerializedName("ITEM_GROUP_CD")
        @Expose
        var itemGroupCd: String? = null
    }
}
