package com.bodyfriend.shippingsystem.main.sale;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface SaleApi {

    @FormUrlEncoded
    @POST("mobile/api/createBizSale.json")
    Observable<SaleResult> createBizSale(@Field("managerid") String id,
                                         @Field("managername") String deliveryDriverName,
                                         @Field("producttype") String modelType,
                                         @Field("salestype") String saleType,
                                         @Field("ntype") String customerType,
                                         @Field("productname") String modelNameType,
                                         @Field("inusername") String customerName,
                                         @Field("intel") String addr1,
                                         @Field("inhandphone") String addr2,
                                         @Field("etcclausal") String ect);


    @FormUrlEncoded
    @POST("mobile/api/appProductList.json")
    Observable<ProductListData> getProductList(@Field("producttype") String productType);

    @FormUrlEncoded
    @POST("mobile/api/selectBizSales.json")
    Observable<SelectBizSalesData> selectBizSales(@Field("managerid") String managerid);

}
