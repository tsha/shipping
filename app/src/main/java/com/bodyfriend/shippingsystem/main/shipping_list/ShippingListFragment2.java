package com.bodyfriend.shippingsystem.main.shipping_list;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.BFProgressDlg;
import com.bodyfriend.shippingsystem.base.common.BaseFragment;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.CC;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.detail.ShippingMasterDetailActivity2;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingDivList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appCallingLog;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appInsertLog;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appModifyShippingDiv;
import com.bodyfriend.shippingsystem.main.shipping_list.net.modifyProgress;
import com.bodyfriend.shippingsystem.main.shipping_list.net.modifyPromiseCancelShippingDiv;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListArea;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListPromise;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeShippingType;
import com.bodyfriend.shippingsystem.main.shipping_list.net.updateDueDate;
import com.bodyfriend.shippingsystem.main.shipping_list.sms.ShippingSmsManager;
import com.bodyfriend.shippingsystem.main.shipping_list_finish.detail.ShippingDoneMasterDetailActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Set;

/**
 * Created by 이주영 on 2016-08-01.
 */
public class ShippingListFragment2 extends BFFragment2 {
    public static final String TAG = "ShippingListFragment";

    private final int REQUEST_DELIVARYMAN = 1001;

    protected Calendar mStartCalendar = Calendar.getInstance();
    /**
     * 2주 전부터 검색
     */
    protected Calendar mEndCalendar = Calendar.getInstance();
    protected ListView mList;
    private String mInsAddr;

    protected MainActivity.MainActivityControll mainActivityControll;

    private View mSearch;
    private View mFragmentView;
    private BFProgressDlg mProgressDlg;
    private boolean mIsShowProductListTitle;

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 1);

        mFragmentView = inflater.inflate(R.layout.shipping_list_fragment2, container, false);
        showProgress();
        return mFragmentView;
    }

    protected void onLoadOnce() {

        super.onLoadOnce();

        if (mainActivityControll != null)
            mainActivityControll.changeTitle(R.layout.title_layout_shipping_map_list);

        setOnClickListener(R.id.dateStart, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateStart), SDF.mmdd__.format, mStartCalendar));
        setOnClickListener(R.id.dateEnd, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar));
        TextView dateStart = (TextView) findViewById(R.id.dateStart);
        dateStart.addTextChangedListener(watcherDateStart);
        TextView dateEnd = (TextView) findViewById(R.id.dateEnd);
        dateEnd.addTextChangedListener(watcherDateEnd);

        setOnClickListener(R.id.inquiry, onInquiryClickListener);

        mList = (ListView) findViewById(R.id.list);
        mList.setEmptyView(findViewById(R.id.emptyView));

        mSearch = findViewById(R.id.search_layout2);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void show() {
        super.show();

    }

    @Override
    protected void onUpdateUI() {
        super.onUpdateUI();

        setText(R.id.dateStart, SDF.mmdd__.format(mStartCalendar.getTime()));
        setText(R.id.dateEnd, SDF.mmdd__.format(mEndCalendar.getTime()));

    }

    @Override
    protected void onLoad() {
        super.onLoad();

        if (mSearchCodeListArea == null) {
            Net.async(new searchCodeListArea()).setOnNetResponse(onAreaNetResponse);
        } else {
            reqShippingDivList();
        }

        if (mSearchCodeListPromise == null) {
            Net.async(new searchCodeListPromise()).setOnNetResponse(onPromiseCodeNetResponse);
        }

        if (mShippingType == null) {
            Net.async(new searchCodeShippingType()).setOnNetResponse(onShippingTypeCodeNetResponse);
        }
    }

    private searchCodeListPromise mSearchCodeListPromise;
    private Net.OnNetResponse<searchCodeListPromise> onPromiseCodeNetResponse = new Net.OnNetResponse<searchCodeListPromise>() {

        @Override
        public void onResponse(searchCodeListPromise response) {

            mSearchCodeListPromise = response;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    private View.OnClickListener onInquiryClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            reqShippingDivList();
        }
    };

    protected void reqShippingDivList() {
        mProgressDlg = new BFProgressDlg(getActivity(), R.style.Dialog);

        mProgressDlg.show();

        if (!(this instanceof CC.ILOGIN) && !Auth.isLogin()) {
            return;
        }
        final Spinner spinner = (Spinner) mFragmentView.findViewById(R.id.s_datetype);
        int s_datetype = spinner.getSelectedItemPosition();
        if (s_datetype == 0) { // 배정일
            s_datetype = 0; // 배정
        } else if (s_datetype == 1) { // 지정일
            s_datetype = 2; // 지정
        } else if (s_datetype == 2) { // 접수일
            s_datetype = 1; // 접수
        }
        String dateStart = SDF.yyyymmdd_2.format(mStartCalendar.getTime());
        String dateEnd = SDF.yyyymmdd_2.format(mEndCalendar.getTime());
        Spinner mS_area = (Spinner) mFragmentView.findViewById(R.id.s_area);
        String selectedArea = (String) mS_area.getSelectedItem();
        String s_area = "";
        if (mSearchCodeListArea != null)
            for (searchCodeListArea.Data.resultData d : mSearchCodeListArea.data.resultData) {
                if (d.DET_CD_NM.equals(selectedArea)) {
                    s_area = d.DET_CD;
                    break;
                }
            }
//        String s_custname = text(R.id.s_custname);
//        String s_addr = text(R.id.s_addr);

        Log.d("s_area : ", s_area);
        String s_custname = ((TextView) mFragmentView.findViewById(R.id.s_custname)).getText().toString();
        String s_addr = ((TextView) mFragmentView.findViewById(R.id.s_addr)).getText().toString();

        String s_oneself = "N";

//        Net.async(new ShippingDivList(s_datetype, dateStart, dateEnd, s_area
//                , s_custname, s_addr, s_oneself, false)).setOnNetResponse(onNetResponse);
        Net.async(new ShippingDivList(s_datetype, s_area
                , s_custname, s_addr, s_oneself, false)).setOnNetResponse(onNetResponse);

    }

    private ShippingListAdapter mListAdapter;
    protected Net.OnNetResponse<ShippingDivList> onNetResponse = new Net.OnNetResponse<ShippingDivList>() {

        @Override
        public void onResponse(ShippingDivList response) {
            if (getActivity() != null && mProgressDlg != null && mProgressDlg.isShowing()) {
                mProgressDlg.dismiss();
            }
            List<ShippingList.resultData.list> data = response.data.resultData.list;

            final List<ShippingList.resultData.list> list = reOrderShippingList(response);
            if (mListAdapter == null) {
                mListAdapter = new ShippingListAdapter(mContext, list);
                mList.setAdapter(mListAdapter);
            } else {
                mListAdapter.setItemList(list);
                mListAdapter.notifyDataSetChanged();
            }

            if (data.size() > 0) {
                hideSearch();
            } else {
                showSearch();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    /**
     * 받아온 데이터에 대해서 리오더<br>
     * 데이터가 날짜별로 그룹핑 되어야 하며,<br>
     * 날짜가 처음 시작하는 아이템 위에 날짜정보를 출력할 아이템이 필요하다.
     *
     * @return
     */
    private ArrayList<ShippingList.resultData.list> reOrderShippingList(ShippingDivList response) {
        HashMap<String, List<ShippingList.resultData.list>> hashMap = new HashMap<String, List<ShippingList.resultData.list>>();
        List<ShippingList.resultData.list> list = response.data.resultData.list;

        for (ShippingList.resultData.list item : list) { // 날짜별로 hashmap에 리스트를 나눠서 담는다.
            final String due_date = item.DUE_DATE.equals("") ? "999999" : item.DUE_DATE;

            List<ShippingList.resultData.list> list2 = hashMap.get(due_date);
            if (list2 == null) {
                list2 = new ArrayList<>();
            }
            list2.add(item);
            hashMap.put(due_date, list2);
        }

        ArrayList<ShippingList.resultData.list> returnList = new ArrayList<ShippingList.resultData.list>();
        String[] keySet = hashMap.keySet().toArray(new String[hashMap.keySet().size()]);
        Arrays.sort(keySet);
        for (String key : keySet) {
            List<ShippingList.resultData.list> list3 = hashMap.get(key);
            Collections.sort(list3, (o1, o2) -> o1.PROMISE_TIME.compareTo(o2.PROMISE_TIME));
            for (int i = 0; i < list3.size(); i++) {
                list3.get(i).index = i + 1;
            }

            HashMap<String, Integer> deliveryItems = new HashMap<>();
            for (ShippingList.resultData.list titleItem : list3) {
                if (titleItem.SHIPPING_TYPE != null && !TextUtils.isEmpty(titleItem.SHIPPING_TYPE) && titleItem.SHIPPING_TYPE.equals("배송")) {
                    Integer integer = deliveryItems.get(titleItem.PRODUCT_NAME);
                    if (integer != null) {
                        deliveryItems.put(titleItem.PRODUCT_NAME, integer + 1);
                    } else {
                        deliveryItems.put(titleItem.PRODUCT_NAME, 1);
                    }
                }
            }


            ShippingList.resultData.list item = new ShippingList.resultData.list();
            item.DUE_DATE = key.equals("999999") ? "미지정" : key;
            item.setDeliveryItems(deliveryItems);
            item.DUE_DATE_COUNT = String.format("  (%s건)", list3.size());
            item.EXPIRE_FLAG = list3.get(0).EXPIRE_FLAG;
            item.isDate = true;
            // MapShippingListData.resultData.list item = list3.get(0);
            list3.add(0, item);
            returnList.addAll(list3);
        }

        return returnList;
    }

    public String location;
    private ShippingList.resultData.list mItem;

    public class ShippingListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<ShippingList.resultData.list> mItemList;

        public void setItemList(List<ShippingList.resultData.list> list) {
            this.mItemList = (ArrayList<ShippingDivList.Data.resultData.list>) list;
        }

        public ShippingListAdapter(Context context, List<ShippingList.resultData.list> list) {
            this.mItemList = (ArrayList<ShippingDivList.Data.resultData.list>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        DialogInterface.OnClickListener onItemClickN = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        // mItemList.get(position).SHIPPING_SEQ
                        DialogInterface.OnClickListener positiveListener = (dialog1, which1) -> new AlertDialog.Builder(mContext).setItems(new String[]{"불가 내용 보기", "약속 불가 취소"}, onItemClickN).show();
                        showDialog(mItem.PROMISE_FAIL_PS, "확인", positiveListener);

                        break;
                    case 1:

                        DialogInterface.OnClickListener positiveListener2 = (dialog12, which12) -> {
                            Net.async(new modifyPromiseCancelShippingDiv(mItem.SHIPPING_SEQ, "", "", "")).setOnNetResponse(onPromiseNetResponse);
                            Net.async(new appInsertLog(mItem.SHIPPING_SEQ, "", "")).setOnNetResponse(onLogNetResponse);
                        };
                        showDialog("약속 불가를 취소하시겠습니까?", "예", positiveListener2, "아니오", null);
                        break;
                    case 2:
                        if (mItem.DUE_DATE.equals("")) {
                            selectDuedate();
                        } else {
                            unSelectDuedate();
                        }
                        break;
                    default:
                        break;
                }
            }
        };

        /**
         * 약속변경
         *
         * @param pTime 변경시간 HHmm
         */
        private void promiseChange(String pTime) {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTimeInMillis(SDF.hhmm.parse(pTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            new RellTimePicker.Builder(mContext)
                    .setTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
                    .create()
                    .show(getFragmentManager(), (hourOfDay) -> {
//                        Log.d(TAG, "hourOfDay : " + hourOfDay);
                        if (hourOfDay < 1) {
                            Toast.makeText(mContext, "방문 시각을 선택하세요.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        String visitTime = getResources().getStringArray(R.array.timeIntervalCode)[hourOfDay - 1];
                        Net.async(new appModifyShippingDiv(mItem.SHIPPING_SEQ, "Y", visitTime, "", "")).setOnNetResponse(onModifyNetResponse);
                        Net.async(new appInsertLog(mItem.SHIPPING_SEQ, "Y", visitTime)).setOnNetResponse(onLogNetResponse);
                        sendSms(visitTime);

                    });
        }


        private TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() { // 약속변경

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                String promiseTime = SDF.hhmm.format(calendar.getTimeInMillis());
                Net.async(new appModifyShippingDiv(mItem.SHIPPING_SEQ, "Y", promiseTime, "", "")).setOnNetResponse(onModifyNetResponse);
                Net.async(new appInsertLog(mItem.SHIPPING_SEQ, "Y", promiseTime)).setOnNetResponse(onLogNetResponse);
//                sendSms(promiseTime);
            }

        };

        private void sendSms(String m_promisetime) {
            if (m_promisetime == null || m_promisetime.trim().length() == 0) {
                showDialog("약속시간이 정해지지 않았습니다.\n문자를 보내시기전에 약속시간을 정해주세요.", "확인", null);
            } else {
                try {
                    sendMMS(mItem);
                    regnotiSmsNine(m_promisetime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        private void regnotiSmsNine(String m_promisetime) throws ParseException {
            Calendar c = Calendar.getInstance();
            c.setTime(SDF.yyyymmdd_1.parseDate(mItem.DUE_DATE));
            c.set(Calendar.HOUR_OF_DAY, 9);
            c.set(Calendar.MINUTE, 0);

            String date = String.format("%s, %s", SDF.mmdd__.format(SDF.yyyymmdd_1.parse(mItem.DUE_DATE)), m_promisetime);

            ShippingSmsManager.getInstance(mContext).sendSmsNine(mItem, date, c);
        }

        private void sendMMS(ShippingList.resultData.list item) throws ParseException {
            if (item.DUE_DATE.isEmpty()) {
                toast("지정된 날짜가 없습니다.");
                return;
            }
            String date = SDF.mmdd__.format(SDF.yyyymmdd_1.parse(item.DUE_DATE));
            if (item.PROMISE_TIME.isEmpty()) {
                toast("약속시간이 없습니다 .");
                return;
            }
            String visitTime = getVisitTime(SDF.hhmm.parseDate(item.PROMISE_TIME));
//            Log.d("visitTime ~~~ : ", visitTime);
            String msg = String.format("%s, %s", date, visitTime);
            ShippingSmsManager.getInstance(mContext).sendSmsReservation(item, msg);
        }

        private DatePickerDialog.OnDateSetListener onUpdateDueSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String m_duedate = String.format("%s-%s-%s", year, monthOfYear + 1, dayOfMonth);
                Net.async(new updateDueDate(mItem.SHIPPING_SEQ, m_duedate)).setOnNetResponse(onUpdateNetResponse);
            }
        };

        private void selectDuedate() {
            DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Calendar calendar = Calendar.getInstance();
                    showDatePicker(onUpdateDueSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                }
            };
            showDialog("지정일을 지정하시겠습니까?", "예", positiveListener, "아니오", null);
        }

        private Net.OnNetResponse<updateDueDate> onUpdateNetResponse = new Net.OnNetResponse<updateDueDate>() {

            @Override
            public void onResponse(updateDueDate response) {
                toast(response.data.resultMsg);
                reload();
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        private void unSelectDuedate() {
            DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Net.async(new updateDueDate(mItem.SHIPPING_SEQ, "")).setOnNetResponse(onUpdateNetResponse);
                }
            };
            showDialog("지정일 지정을 해제하시겠습니까?", "예", positiveListener, "아니오", null);
        }

        /**
         * 약속취소
         */
        private void promiseCancel(final String shippingSeq) {
            DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Net.async(new appModifyShippingDiv(shippingSeq, "", "", "", "")).setOnNetResponse(onModifyNetResponse); // 약속취소
                    Net.async(new appInsertLog(shippingSeq, "N", "")).setOnNetResponse(onLogNetResponse);

                    // 약속이 취소되니까 등록된 알람도 취소한다
                    ShippingSmsManager.getInstance(mContext).cancelSmsAlarm(mItem.HPHONE_NO); // 알람해제
                }
            };
            showDialog("약속을 취소하시겠습니까?", "예", positiveListener, "아니오", null);
        }

        private View.OnClickListener onPromiseClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mItem = (ShippingList.resultData.list) v.getTag();
                String[] builderItem = null;
                if (mItem.PROMISE.equals("Y")) { // 약속
                    DialogInterface.OnClickListener onItemClick = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:// 약속 시간 변경
                                    promiseChange(mItem.PROMISE_TIME);
                                    break;
                                case 1:// 약속 취소
                                    promiseCancel(mItem.SHIPPING_SEQ);
                                    break;
                                case 2:
                                    if (mItem.DUE_DATE.equals("")) {
                                        selectDuedate();
                                    } else {
                                        unSelectDuedate();
                                    }
                                    ShippingSmsManager.getInstance(mContext).cancelSmsAlarm(mItem.HPHONE_NO); // 알람해제
                                    break;

                                case 3:
                                    ShippingSmsManager.getInstance(mContext).cancelSmsAlarm(mItem.HPHONE_NO); // 알람해제
                                    selectDuedate();
                                    break;
                            }
                        }
                    };

                    if (mItem.DUE_DATE.equals("")) {
                        builderItem = new String[]{"약속 시간 변경", "약속 취소", "지정일 지정"};
                    } else {
                        builderItem = new String[]{"약속 시간 변경", "약속 취소", "지정일 지정 해제", "지정일 변경"};
                    }

                    new AlertDialog.Builder(mContext).setItems(builderItem, onItemClick).show(); // 약속
                } else if (mItem.PROMISE.equals("N")) { // 약속 불가
                    if (mItem.DUE_DATE.equals("")) {
                        builderItem = new String[]{"불가 내용 보기", "약속 불가 취소", "지정일 지정"};
                    } else {
                        builderItem = new String[]{"불가 내용 보기", "약속 불가 취소", "지정일 지정 해제"};
                    }
                    new AlertDialog.Builder(mContext).setItems(builderItem, onItemClickN).show(); // 약속 불가
                } else { // 미 정

                    DialogInterface.OnClickListener onItemClick = new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:// 약속 시간 설정

//                                    new RangeTimePickerDialog(mContext, onTimeSetListener, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true).show();

//                                    showTimePicker(onTimeSetListener, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE));
                                    new RellTimePicker.Builder(mContext)
                                            .setTime(Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE))
                                            .create()
                                            .show(getFragmentManager(), (hourOfDay) -> {
//                                                Log.d(TAG, "hourOfDay : " + hourOfDay);

                                                if (hourOfDay < 1) {
                                                    Toast.makeText(mContext, "방문 시각을 선택하세요.", Toast.LENGTH_SHORT).show();
                                                    return;
                                                }

                                                String visitTime = getResources().getStringArray(R.array.timeIntervalCode)[hourOfDay - 1];
                                                Net.async(new appModifyShippingDiv(mItem.SHIPPING_SEQ, "Y", visitTime, "", "")).setOnNetResponse(onModifyNetResponse);
                                                Net.async(new appInsertLog(mItem.SHIPPING_SEQ, "Y", visitTime)).setOnNetResponse(onLogNetResponse);
//                                                mItem.PROMISE_TIME = visitTime;
                                                sendSms(visitTime);
                                            });


                                    break;
                                case 1:// 약속 취소 사유 입력

                                    selected_shipping_seq = mItem.SHIPPING_SEQ;
                                    startPromiseCancelActivity();

                                    break;
                                case 2:
                                    if (mItem.CUST_UPDATE != null && mItem.CUST_UPDATE.equals("AD")) {
                                        try {
                                            Toast.makeText(mContext, "고객 배송 요청일입니다. " + SDF.mmdd__.format(SDF.yyyymmdd_1.parseDate(mItem.DUE_DATE)), Toast.LENGTH_SHORT).show();
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (mItem.DUE_DATE.equals("")) {
                                        selectDuedate();
                                    } else {
                                        unSelectDuedate();
                                    }
                                    break;
                                case 3:
                                    if (mItem.CUST_UPDATE != null && mItem.CUST_UPDATE.equals("AD")) {
                                        try {
                                            Toast.makeText(mContext, "고객 배송 요청일입니다. " + SDF.mmdd__.format(SDF.yyyymmdd_1.parseDate(mItem.DUE_DATE)), Toast.LENGTH_SHORT).show();
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        selectDuedate();
                                    }
                                    break;
                            }
                        }
                    };

                    if (mItem.DUE_DATE.equals("")) {
                        builderItem = new String[]{"약속 시간 설정", "약속 불가", "지정일 지정"};
                    } else {
                        builderItem = new String[]{"약속 시간 설정", "약속 불가", "지정일 지정 해제", "지정일 변경"};
                    }
                    new AlertDialog.Builder(mContext).setItems(builderItem, onItemClick).show(); // 약속 미정
                }
            }
        };
        private View.OnClickListener onPhoneClickLsitener = new View.OnClickListener() {
            private Net.OnNetResponse<appCallingLog> onAppCallingNetResponse = new Net.OnNetResponse<appCallingLog>() {

                @Override
                public void onResponse(appCallingLog response) {

                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            };

            @Override
            public void onClick(View v) {
                mItem = (ShippingList.resultData.list) v.getTag();

                if (mItem.SHIPPING_TYPE.startsWith("이전") && mItem.PROGRESS_NO.equals("1")) {

                    Net.async(new appCallingLog(mItem.SHIPPING_SEQ, mItem.AGREE_NAME, mItem.HPHONE_NO2)).setOnNetResponse(onAppCallingNetResponse);

                    showDialog(String.format("%s 님과 통화시겠습니까?", mItem.AGREE_NAME), "통화", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", mItem.HPHONE_NO2)));
                            mContext.startActivity(intent);
                        }
                    }, "취소", null);
                } else {
                    Net.async(new appCallingLog(mItem.SHIPPING_SEQ, mItem.CUST_NAME, mItem.HPHONE_NO)).setOnNetResponse(onAppCallingNetResponse);

                    showDialog(String.format("%s 님과 통화시겠습니까?", mItem.CUST_NAME), "통화", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", mItem.HPHONE_NO)));
                            mContext.startActivity(intent);
                        }
                    }, "취소", null);

                }

            }
        };


        public View.OnClickListener onConverViewClickListener = new View.OnClickListener() {
            @SuppressLint({"NewApi", "CommitTransaction"})
            @Override
            public void onClick(View v) {
                mItem = ((ViewHolder) v.getTag()).item;
                if (mItem.PROGRESS_NO.equals("4") || mItem.INS_COMPLETE.equals("ZZ")) {
                    Intent intent = new Intent(mContext, ShippingDoneMasterDetailActivity.class);
                    intent.putExtra(ShippingDoneMasterDetailActivity.EXTRA.SHIPPING_SEQ, mItem.SHIPPING_SEQ);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, ShippingMasterDetailActivity.class);
                    intent.putExtra(ShippingMasterDetailActivity.EXTRA.SHIPPING_SEQ, mItem.SHIPPING_SEQ);
                    startActivity(intent);

//                    startActivity(ShippingMasterDetailActivity2.newInstance(mContext, mItem.SHIPPING_SEQ));
                }
            }
        };

        private View.OnClickListener onSmsClickListener = new View.OnClickListener() {
            ShippingList.resultData.list mItem;

            @Override
            public void onClick(View v) {
                mItem = (ShippingList.resultData.list) v.getTag();
                new AlertDialog.Builder(mContext).setItems(new String[]{"약속 시간 설정 문자"}, onItemClick).show(); // 약속
            }

            DialogInterface.OnClickListener onItemClick = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            try {
                                sendMMS(mItem);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                }
            };
        };

        private View.OnClickListener onMapClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItem = (ShippingList.resultData.list) v.getTag();

                geoCode(mItem.INSADDR);

            }
        };

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.shipping_list_item2, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.insaddr = (TextView) convertView.findViewById(R.id.insaddr);
                viewHolder.cust_name = (TextView) convertView.findViewById(R.id.cust_name);
                viewHolder.phone = convertView.findViewById(R.id.phone);
                viewHolder.map = convertView.findViewById(R.id.map);
                viewHolder.sms = (ImageView) convertView.findViewById(R.id.sms);
                viewHolder.product_name = (TextView) convertView.findViewById(R.id.product_name);
                viewHolder.must = convertView.findViewById(R.id.must);
                viewHolder.status = (TextView) convertView.findViewById(R.id.status);
                viewHolder.freeGift = (TextView) convertView.findViewById(R.id.freeGift);
                viewHolder.freeGiftLabel = (TextView) convertView.findViewById(R.id.freeGiftLabel);
                viewHolder.promise = (TextView) convertView.findViewById(R.id.promise);
                viewHolder.receiveDate = (TextView) convertView.findViewById(R.id.receiveDate);
//                viewHolder.promise_layout = convertView.findViewById(R.id.promise_layout);
//                viewHolder.promiseTime = (TextView) convertView.findViewById(R.id.promise_time);
                viewHolder.product_img = (ImageView) convertView.findViewById(R.id.product_img);
                viewHolder.relocation_check = (TextView) convertView.findViewById(R.id.relocation_check);

                viewHolder.companyCheck = (TextView) convertView.findViewById(R.id.companyCheck);
                viewHolder.dataLayout = (ConstraintLayout) convertView.findViewById(R.id.dataLayout);

                convertView.setTag(viewHolder);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final ShippingList.resultData.list item = mItemList.get(position);
            if (item.CUST_UPDATE != null && item.CUST_UPDATE.equals("AD")) {
                viewHolder.dataLayout.setBackgroundColor(Color.parseColor("#FAF4C0"));
            } else {
                viewHolder.dataLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            if ((item.FREEGIFT_ONE != null && !TextUtils.isEmpty(item.FREEGIFT_ONE.trim())) || (item.FREEGIFT_TWO != null && !TextUtils.isEmpty(item.FREEGIFT_TWO.trim()))) {
                viewHolder.freeGift.setText(item.FREEGIFT_ONE + ", " + item.FREEGIFT_TWO);

                viewHolder.freeGiftLabel.setVisibility(View.VISIBLE);
                viewHolder.freeGift.setVisibility(View.VISIBLE);
            } else {
                viewHolder.freeGift.setVisibility(View.GONE);
                viewHolder.freeGiftLabel.setVisibility(View.GONE);
            }

           /* if (item.FREEGIFT_TWO != null && !TextUtils.isEmpty(item.FREEGIFT_TWO.trim())) {
                viewHolder.freeGift.setText(item.FREEGIFT_ONE + ", " + item.FREEGIFT_TWO);
            } else {
                viewHolder.freeGift.setText(item.FREEGIFT_ONE);
            }*/


            viewHolder.cust_name.setText(item.CUST_NAME);
            viewHolder.insaddr.setText(item.INSADDR);

            if (item.isDate) {
                convertView.findViewById(R.id.dateLayout).setVisibility(View.VISIBLE);
                convertView.findViewById(R.id.dataLayout).setVisibility(View.GONE);

                TextView date = (TextView) convertView.findViewById(R.id.date);
                date.setText(item.DUE_DATE);

                TextView count = (TextView) convertView.findViewById(R.id.count);
                count.setText(item.DUE_DATE_COUNT);

                TextView loadProductsTv = (TextView) convertView.findViewById(R.id.loadProductsTv);

                if (mIsShowProductListTitle && item.getDeliveryItems().size() > 0) {
                    loadProductsTv.setVisibility(View.VISIBLE);
                    HashMap<String, Integer> deliveryItems = item.getDeliveryItems();

                    Set<String> strings = deliveryItems.keySet();
                    StringBuilder deliveryItemInfo = new StringBuilder();
                    deliveryItemInfo.append("\n");
                    for (String key : strings) {
//                        Log.d(TAG, "deliveryItems : " + key);
                        deliveryItemInfo.append(key).append("\t ").append(deliveryItems.get(key)).append("\n");
                    }
                    loadProductsTv.setText(deliveryItemInfo.toString());
                } else {
                    loadProductsTv.setVisibility(View.GONE);
                }

            } else {
                // 이전이면
                if (item.SHIPPING_TYPE.startsWith("이전")) {
                    if (item.PROGRESS_NO.equals("1")) { //회수일경우
                        viewHolder.cust_name.setText(item.AGREE_NAME);
                        viewHolder.relocation_check.setVisibility(View.VISIBLE);
                        viewHolder.relocation_check.setText("회수");
                        viewHolder.insaddr.setText(item.ADDR);
                    } else if (item.PROGRESS_NO.equals("0")) { // 설치일경우

                        if (item.RELOCATION_CHECK.equalsIgnoreCase("Y")) {
                            viewHolder.relocation_check.setVisibility(View.VISIBLE);
                            viewHolder.relocation_check.setText("설치-회수완료");

                            SpannableString ss = new SpannableString("설치-회수완료");
                            ss.setSpan(new ForegroundColorSpan(Color.BLUE), "설치-".length(), "설치-회수완료".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            viewHolder.relocation_check.setText(ss, TextView.BufferType.SPANNABLE);

                        } else if (item.RELOCATION_CHECK.equalsIgnoreCase("N")) {
                            viewHolder.relocation_check.setVisibility(View.VISIBLE);
                            viewHolder.relocation_check.setText("설치-회수미완료");

                            SpannableString ss = new SpannableString("설치-회수미완료");
                            ss.setSpan(new ForegroundColorSpan(Color.RED), "설치-".length(), "설치-회수미완료".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            viewHolder.relocation_check.setText(ss, TextView.BufferType.SPANNABLE);
                        } else {
                            viewHolder.relocation_check.setVisibility(View.GONE);
                        }
                    } else {
                        viewHolder.relocation_check.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.relocation_check.setVisibility(View.GONE);
                }

                convertView.findViewById(R.id.dateLayout).setVisibility(View.GONE);
                convertView.findViewById(R.id.dataLayout).setVisibility(View.VISIBLE);

                viewHolder.setItem(item);
                viewHolder.setShippingType();

                viewHolder.product_name.setText(item.PRODUCT_NAME);


                viewHolder.receiveDate.setText(item.RECEIVE_DATE);


                final String promise = item.PROMISE;
                final String ins_complete = item.INS_COMPLETE;

                final boolean isPromiseAble = !(ins_complete.startsWith("F") || ins_complete.startsWith("X"));

                if (isPromiseAble) {
                    switch (promise) {
                        case "Y":
                            try {
                                viewHolder.promise.setText(SDF.hhmm_.format(SDF.hhmm.parseDate(item.PROMISE_TIME)));
                            } catch (ParseException e) {
                                viewHolder.promise.setText("미정");
                                e.printStackTrace();
                            }
                            break;
                        case "N":
                            viewHolder.promise.setText("약속 불가");
                            break;
                        default:
                            viewHolder.promise.setText("미정");
                            break;
                    }

                    viewHolder.promise.setTag(item);
                    viewHolder.promise.setOnClickListener(onPromiseClickListener);
                }

                String pName = mItemList.get(position).PRODUCT_NAME;

                int resId = 0;
                if (pName.contains("로보")) {
                    resId = R.drawable.chair_irobo;
                } else if (pName.contains("팬텀")) {
                    if (pName.contains("블랙")) {
                        resId = R.drawable.chair_pantom_black;
                    } else if (pName.contains("브라운")) {
                        resId = R.drawable.chair_pantom_brown;
                    } else if (pName.contains("레드")) {
                        resId = R.drawable.chair_pantom_red;
                    } else {
                        resId = R.drawable.chair_pantom_black;
                    }
                } else if (pName.contains("파라오")) {
                    resId = R.drawable.chair_parao;
                    if (pName.contains("S")) {
                        resId = R.drawable.chair_parao_s;
                    }
                } else if (pName.contains("프레지던트")) {
                    resId = R.drawable.chair_president;
                    if (pName.contains("플러스")) {
                        resId = R.drawable.chair_president_plus;
                    }
                } else if (pName.contains("렉스")) {
                    resId = R.drawable.chair_rexl;
                    if (pName.contains("블랙")) {
                        resId = R.drawable.chair_rexl_black;
                    }
                } else if (pName.contains("레지나")) {
                    resId = R.drawable.chair_regina;
                } else if (pName.contains("프레임")) {
                    if (pName.contains("아이보리")) {
                        resId = R.drawable.latex_white_frame;
                    } else if (pName.contains("플로팅")) {
                        resId = R.drawable.latex_floating_frame;
                    } else if (pName.contains("초코")) {
                        resId = R.drawable.latex_black_frame;
                    }
                } else if (pName.contains("공통")) {
                    resId = R.drawable.latex_mat;
                }

                if (resId == 0) {
                    viewHolder.product_img.setImageResource(android.R.color.transparent);
                } else {
                    viewHolder.product_img.setImageResource(resId);
                }

                viewHolder.must.setVisibility(item.MUST_FLAG.equals("Y") ? View.VISIBLE : View.GONE);

                viewHolder.map.setTag(item);
                viewHolder.map.setOnClickListener(onMapClickListener);
                String symptom = "";
                if (!TextUtils.isEmpty(item.SYMPTOM)) {
                    symptom = "<br/> 증상 : " + item.SYMPTOM;
                }
                viewHolder.companyCheck.setText(Html.fromHtml(item.COMPANY_CHECK + symptom));
//                Log.d(TAG, "item.COMPANY_CHECK :" + item.COMPANY_CHECK);
                viewHolder.sms.setTag(item);
                viewHolder.sms.setImageResource(item.HAS_4_MSG ? R.drawable.ic_sms2 : R.drawable.ic_sms);
                viewHolder.sms.setOnClickListener(onSmsClickListener);

                viewHolder.phone.setTag(item);
                viewHolder.phone.setVisibility(item.HPHONE_NO.trim().length() > 0 ? View.VISIBLE : View.GONE);
                viewHolder.phone.setOnClickListener(onPhoneClickLsitener);

                convertView.setOnClickListener(onConverViewClickListener);
            }
            return convertView;
        }
    }

    public class ViewHolder {
        public TextView insaddr; // 제품명
        public TextView cust_name; // 소비자명
        public View phone; //
        public View map; //
        public ImageView sms; //
        public TextView product_name; //
        public ImageView product_img;
        public View must;
        public TextView status;
        public TextView promise;
        //        public TextView promiseTime;
        public View promise_layout;
        public TextView relocation_check;
        public TextView companyCheck;
        public ConstraintLayout dataLayout;
        public TextView freeGift;
        public TextView freeGiftLabel;
        public TextView receiveDate;


        public ShippingList.resultData.list item;

        public void setItem(ShippingList.resultData.list item) {
            this.item = item;
        }

        public void setShippingType() {
            final String sType = item.SHIPPING_TYPE;
            if (sType == null) return;
            status.setText(sType);
            int resImg = 0;
            if (sType.equals("배송")) { // 배송
                status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.calendar_circle_1_3, 0, 0, 0);
            } else if (sType.equals("맞교체")) { // 맞교체
                status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.calendar_circle_2_3, 0, 0, 0);
            } else if (sType.equals("계약철회")) { // 계약철회
                status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.calendar_circle_3_3, 0, 0, 0);
            } else if (sType.equals("이전요청")) { // 이전요청
                status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.calendar_circle_4_3, 0, 0, 0);
            } else if (sType.equals("수리요청")) { // 수리요청
                status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.calendar_circle_5_3, 0, 0, 0);
            } else if (sType.equals("AS")) { // AS
            } else if (sType.equals("탄산수기추가")) { // 탄산수기추가
            } else if (sType.equals("전시장설치")) { // 전시장설치
            } else if (sType.equals("기안건")) { // 기안건
            } else {
            }
        }
    }

    int mWhich;
    String m_shippingseq;

    private void geoCode(String addr) {

        Intent intent = new Intent(mContext, MapActivity.class);

        intent.putExtra("addr", addr);
//        intent.putExtra("addr", "경기 고양시 일산동구 백석동 1313번지");

        startActivity(intent);

    }

    private TextWatcher watcherDateStart = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            setOnClickListener(R.id.dateEnd, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar, mStartCalendar.getTimeInMillis(), -1));
        }
    };

    private TextWatcher watcherDateEnd = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // setOnClickListener(R.id.dateStart, new OnDateClickListener((TextView) findViewById(R.id.dateStart), SDF.mmdd__.format, mStartCalendar));
            // setOnClickListener(R.id.dateEnd, new OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar));
        }
    };

    protected searchCodeListArea mSearchCodeListArea;
    private Net.OnNetResponse<searchCodeListArea> onAreaNetResponse = new Net.OnNetResponse<searchCodeListArea>() {

        @Override
        public void onResponse(searchCodeListArea response) {
//            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            mSearchCodeListArea = response;

            ArrayList<String> arrSpinner = new ArrayList<String>();
            arrSpinner.add("전체");
            for (searchCodeListArea.Data.resultData d : response.data.resultData) {
                arrSpinner.add(d.DET_CD_NM);
            }

            // Spinner s = (Spinner) findViewById(R.id.s_area);
            // ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, arrSpinner);
            // s.setAdapter(adapter);
//            setSpinner(R.id.s_area, arrSpinner);
            if (getActivity() != null) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, android.R.id.text1, arrSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                if (mFragmentView != null)
                    ((Spinner) (mFragmentView.findViewById(R.id.s_area))).setAdapter(adapter);
                reqShippingDivList();
            }
//            reqShippingDivList();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    Net.OnNetResponse<appModifyShippingDiv> onModifyNetResponse = new Net.OnNetResponse<appModifyShippingDiv>() {

        @Override
        public void onResponse(appModifyShippingDiv response) {

//            toast(response.data.resultMsg);
            reload();
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private Net.OnNetResponse<appInsertLog> onLogNetResponse = new Net.OnNetResponse<appInsertLog>() {

        @Override
        public void onResponse(appInsertLog response) {

        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private void startPromiseCancelActivity() {
        ArrayList<String> arrStr = new ArrayList<String>();
        for (searchCodeListPromise.Data.resultData data : mSearchCodeListPromise.data.resultData) {
            arrStr.add(data.DET_CD_NM);
        }
        arrStr.add("직접 입력");
        Intent intent = new Intent(mContext, PromiseCancelActivity.class);
        intent.putExtra(PromiseCancelActivity.EXTRA.M_PROMISEFAILPS, arrStr);
        startActivityForResult(intent, PromiseCancelActivity.REQUEST_CODE);
    }

    private Net.OnNetResponse<modifyPromiseCancelShippingDiv> onPromiseNetResponse = new Net.OnNetResponse<modifyPromiseCancelShippingDiv>() {

        @Override
        public void onResponse(modifyPromiseCancelShippingDiv response) {
            toast(response.data.resultMsg);
            reload();
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private String selected_shipping_seq;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PromiseCancelActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            final int selectedIdx = data.getIntExtra(PromiseCancelActivity.RESULT.SELECTED_IDX, -1);
            final String selectedText = data.getStringExtra(PromiseCancelActivity.RESULT.SELECTED_TXT);

            String m_promisefailcd;
            String m_promisefailps = selectedText;
            final List<searchCodeListPromise.Data.resultData> resultData = mSearchCodeListPromise.data.resultData;
            if (selectedIdx < resultData.size()) {
                searchCodeListPromise.Data.resultData item = resultData.get(selectedIdx);
                m_promisefailcd = item.DET_CD;
            } else {
                m_promisefailcd = "99";
            }

            Net.async(new modifyPromiseCancelShippingDiv(selected_shipping_seq, "N", m_promisefailcd, m_promisefailps)).setOnNetResponse(onPromiseNetResponse);
            Net.async(new appInsertLog(selected_shipping_seq, "N", "")).setOnNetResponse(onLogNetResponse);
        }

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_DELIVARYMAN) {
            reload();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == BackSNActivity.RESULT_CODE) {
            // final String backsn = getIntent().getStringExtra(BackSNActivity.RESULT.BACK_SN);
            // reqModifyProgress(backsn);
        }

    }

    private Net.OnNetResponse<modifyProgress> onMpNetResponse = new Net.OnNetResponse<modifyProgress>() {

        @Override
        public void onResponse(modifyProgress response) {
            toast(response.data.resultMsg);
            hideProgress();
            reload();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    protected searchCodeShippingType mShippingType;
    protected Net.OnNetResponse<searchCodeShippingType> onShippingTypeCodeNetResponse = new Net.OnNetResponse<searchCodeShippingType>() {

        @Override
        public void onResponse(searchCodeShippingType response) {
            mShippingType = response;

            if (mListAdapter != null)
                mListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    protected void hideSearch() {
        if (mSearch != null)
            mSearch.setVisibility(View.GONE);
    }

    protected void showSearch() {
        if (mSearch != null)
            mSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void update(Observable observable, Object data) {
        super.update(observable, data);

        if (OH.c() != observable)
            return;

        if (data instanceof OH.TYPE) {
            OH.TYPE type = (OH.TYPE) data;
            switch (type) {
                case CIRCLE_SHIPPING_LIST:
                    mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 1);
                    mEndCalendar = Calendar.getInstance();
                    updateUI();
                    break;
                case MODIFY_SHIPPING_DETAIL:
                    reload();
                    break;

                case SHOW_PRODUCT_LIST:

                    this.mIsShowProductListTitle = (boolean) ((OH.TYPE) data).obj;
                    if (mListAdapter != null)
                        mListAdapter.notifyDataSetChanged();
                    break;
                case SEARCH:
                    toggle();
                    break;
                default:
                    break;
            }
        }
    }

    protected void toggle() {
        if (mSearch.getVisibility() == View.VISIBLE) {
            hideSearch();
        } else {
            showSearch();
        }
    }

    private String getVisitTime(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
//        if (c.get(Calendar.MINUTE) > 30) {
//            c.set(Calendar.MINUTE, 30);
//        } else {
//            c.set(Calendar.MINUTE, 0);
//        }
//        c.add(Calendar.HOUR_OF_DAY, -2);
        String s = SDF.ahm.format(c.getTime());
        c.add(Calendar.HOUR, 2);
        String s2 = SDF.ahm.format(c.getTime());
        return String.format("%s~\n%s", s, s2);
    }
}