package com.bodyfriend.shippingsystem.main.sale


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.main.login.Auth
import kotlinx.android.synthetic.main.activity_sale.*
import java.util.ArrayList

class SaleActivity : AppCompatActivity() {
    private lateinit var viewModel: SaleViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sale)
        textDeliveryDriverName.text = Auth.d.resultData.ADMIN_NM

        viewModel = ViewModelProviders.of(this).get(SaleViewModel::class.java)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

//        viewModel.getProductLst("M")
        setupEvent()
        getObserve()
    }

    private fun setupEvent() {

        selaBtn.setOnClickListener {
            if (checkInputBox(editTextCustomerName) &&
                    (checkInputBox(editTextContactAddress1) || checkInputBox(editTextContactAddress2))) {
                val modelKind = resources.getStringArray(R.array.modelTypeCode)[spinnerTypeProduct.selectedItemPosition]
                val saleType = resources.getStringArray(R.array.saleType)[spinnerTypeRental.selectedItemPosition]
                val customerType = resources.getStringArray(R.array.customerType)[spinnerTypeNew.selectedItemPosition]

                val modelName = spinnerTypeModelName.selectedItem.toString()


                progressBar.visibility = View.VISIBLE
                viewModel.createBizSale(Auth.getId(), Auth.d.resultData.ADMIN_NM, modelKind,
//                viewModel.createBizSale("hts00", "하태석43543", modelKind,
                        saleType, customerType, modelName, editTextCustomerName.text.toString(), editTextContactAddress1.text.toString()
                        , editTextContactAddress2.text.toString(), editTextEtc.text.toString()

                )
            }

            val view = this.currentFocus
            if (view != null) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        spinnerTypeProduct.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {


            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val modelKind = resources.getStringArray(R.array.modelTypeCode)[position]
                viewModel.getProductLst(modelKind)
            }

        }


    }

    private fun getObserve() {
        viewModel.getProductListData.observe(this, Observer {
            val productNameList = ArrayList<String>()
            for (productInfo in it as ArrayList) {
                productNameList.add(productInfo.PRODUCT_NAME)
            }

            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, android.R.id.text1, productNameList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerTypeModelName.adapter = adapter
        })

        viewModel.getSaleResult.observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it!!.resultCode.equals("200")) {
                Toast.makeText(this, it.resultMsg, Toast.LENGTH_LONG).show()
                finish()
            }

        })
    }

    private fun checkInputBox(input: AutoCompleteTextView): Boolean {

        return if (TextUtils.isEmpty(input.text.trim())) {
            input.error = "입력이 필요합니다."
            false
        } else {
            input.error = null
            true
        }

    }

}
