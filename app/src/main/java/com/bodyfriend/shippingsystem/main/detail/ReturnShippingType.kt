package com.bodyfriend.shippingsystem.main.detail


enum class ReturnShippingType {
    /*    D20	맞교체(전체교체)
        D30	계약철회
        P10	이전요청
        D130	전시장회수
        D140	기안회수건
        D10	배송
        D40	기안건
        D50	전시장설치
        D80	사은품
        D21	맞교체(부분교체)
        P20	수리요청
        D60	탄산수기추가
        D70	AS
        D100	정수기추가
        D120	보류
        D150	러그판매
        D90	재설치*/
    D20,
    D30, P10, D130, D140, D10,
    D40,
    D50,
    D80,

    D21,
    P20,
    D60,
    D70,
    D100,
    D120,
    D150,
    SV10,
    SV11,
    D90
}