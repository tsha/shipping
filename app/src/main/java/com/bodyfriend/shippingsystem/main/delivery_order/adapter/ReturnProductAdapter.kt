package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemReturnProductBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.ReturnProductData

class ReturnProductAdapter(val itemClick: (ReturnProductData.ResultDatum) -> Unit) : RecyclerView.Adapter<ReturnProductAdapter.ViewHolder>() {


    private var mealList: MutableList<ReturnProductData.ResultDatum> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        holder.view.cardView.setOnClickListener { itemClick(mealList[position]) }
    }


    fun updateMealList(postList: MutableList<ReturnProductData.ResultDatum>) {
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun addUploadItem(item: ReturnProductData.ResultDatum) {
        mealList.add(0, item)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemReturnProductBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_return_product, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<ReturnProductData.ResultDatum> {
        return mealList
    }

    class ViewHolder(var view: ItemReturnProductBinding) : RecyclerView.ViewHolder(view.root)


}