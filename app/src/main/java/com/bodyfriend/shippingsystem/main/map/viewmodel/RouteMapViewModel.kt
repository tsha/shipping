package com.bodyfriend.shippingsystem.main.map.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.graphics.Color
import android.text.TextUtils
import com.bodyfriend.shippingsystem.base.NetConst
import com.bodyfriend.shippingsystem.base.log.Log
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator
import com.bodyfriend.shippingsystem.base.util.SDF
import com.bodyfriend.shippingsystem.main.map.net.ShippingApi
import com.bodyfriend.shippingsystem.main.map.net.TMapApi
import com.bodyfriend.shippingsystem.main.map.tmap.RequestRoute
import com.bodyfriend.shippingsystem.main.map.tmap.ViaPoint
import com.bodyfriend.shippingsystem.main.map.vo.MapShippingListData
import com.cocoahero.android.geojson.GeoJSON
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.data.geojson.GeoJsonLayer
import com.google.maps.android.data.geojson.GeoJsonLineString
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONException
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class RouteMapViewModel : ViewModel() {
    private val shippingItemListData = MutableLiveData<List<MapShippingListData.ResultData.ShippingItem>>()
    private val polyline = MutableLiveData<ArrayList<Polyline>>()


    fun getShippingItemList(): LiveData<List<MapShippingListData.ResultData.ShippingItem>> {
        return shippingItemListData
    }

    fun getPolyline(): LiveData<ArrayList<Polyline>> {
        return polyline
    }


    fun getShippingData() {
        val subscribe = ServiceGenerator.createService(ShippingApi::class.java)
                .shippList
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.d("resultCode : " + it.resultCode)
//                    if (it.resultCode == 200) {
//                        Log.d("resultCode : " + it.resultCode)
                    shippingItemListData.postValue(it.resultData.list)
//                    binding.listView.adapter = ShippingItemAdapter(it.resultData.list, this)

//                    }
                }
    }

    fun searchShortestRout(mMarkerList: ArrayList<MapShippingListData.ResultData.ShippingItem>, startEndPoints: ArrayList<LatLng>, googleMap: GoogleMap) {
        if (!startEndPoints.isEmpty() && startEndPoints.size == 2 && !mMarkerList.isEmpty() && mMarkerList.size > 1) {
            getTmapShortestRout(mMarkerList, startEndPoints, googleMap)
        }
    }


    private fun getTmapShortestRout(lists: List<MapShippingListData.ResultData.ShippingItem>, startEndPoints: ArrayList<LatLng>, googleMap: GoogleMap) {

        val requestRoute = RequestRoute()
        requestRoute.carType = "4"
        requestRoute.reqCoordType = "WGS84GEO"
        requestRoute.resCoordType = "WGS84GEO"
        requestRoute.startName = "출발"
        requestRoute.startX = startEndPoints[0].longitude.toString()
        requestRoute.startY = startEndPoints[0].latitude.toString()
        val endCalendar = Calendar.getInstance()
        requestRoute.startTime = SDF.yyyymmddhhmm.format(endCalendar.time)
        requestRoute.endName = "도착"
        requestRoute.endX = startEndPoints[1].longitude.toString()
        requestRoute.endY = startEndPoints[1].latitude.toString()
        requestRoute.endPoiId = ""
        requestRoute.searchOption = "0"

        val viaPoints = ArrayList<ViaPoint>()
        var viaPoint: ViaPoint
        for (list in lists) {
            viaPoint = ViaPoint()
            viaPoint.viaPointId = list.SHIPPING_SEQ
            viaPoint.viaPointName = list.CUST_NAME
            viaPoint.viaDetailAddress = list.INSADDR
            viaPoint.viaX = list.location.longitude.toString()
            viaPoint.viaY = list.location.latitude.toString()
            viaPoint.viaPoiId = ""
            viaPoint.viaTime = 600
            viaPoint.wishStartTime = if (TextUtils.isEmpty(list.tMapWishStartTime)) " " else list.tMapWishStartTime
            viaPoint.wishEndTime = if (TextUtils.isEmpty(list.tMapWishEndTime)) " " else list.tMapWishEndTime
            viaPoints.add(viaPoint)
        }
        requestRoute.viaPoints = viaPoints


        val disposableObserver = ServiceGenerator.changeApiBaseUrl(TMapApi::class.java, NetConst.HOST_TMAP)
                .getRouteOptimization10(requestRoute)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<ResponseBody>() {

                    override fun onNext(tmapRoute: ResponseBody) {


                        Log.d("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                        try {
                            val geoJSON = GeoJSON.parse(tmapRoute.byteStream())
                            val geoJsonLayer = GeoJsonLayer(googleMap, geoJSON.toJSON())
                            val mRoutePolyline: ArrayList<Polyline> = ArrayList()
                            for (feature in geoJsonLayer.features) {

                                // do something to the feature
                                if (feature.geometry.geometryType == "LineString") {
                                    val rnd = Random()
                                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
                                    val coordinates = (feature.geometry as GeoJsonLineString).coordinates
                                    mRoutePolyline.add(googleMap.addPolyline(PolylineOptions().color(color).addAll(coordinates)))

                                }
                            }
                            polyline.postValue(mRoutePolyline)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        Log.d("@@@@@@@@@@@@@@@@@")
                    }


                    override fun onError(e: Throwable) {

                        Log.d("onError : " + e.toString())
                    }

                    override fun onComplete() {
                        Log.d("onComplete ~~~")
                    }
                })
    }


}