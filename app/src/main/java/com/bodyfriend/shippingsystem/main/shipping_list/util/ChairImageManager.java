package com.bodyfriend.shippingsystem.main.shipping_list.util;

import com.bodyfriend.shippingsystem.R;

/**
 * Created by 이주영 on 2016-12-08.
 */
public class ChairImageManager {
    private static ChairImageManager ourInstance = new ChairImageManager();

    public static ChairImageManager getInstance() {
        return ourInstance;
    }

    private ChairImageManager() {
    }

    /**
     * 제품의 이미지를 리턴한다.
     *
     * @param pName 제품명
     * @return resId 이미지 리소스 번호
     */
    public int getProductImage(String pName) {
        int resId = R.drawable.latex_mat;
        if (pName.contains("아이로보")) {
            resId = R.drawable.chair_irobo;
        } else if (pName.contains("팬텀")) {
            if (pName.contains("블랙")) {
                resId = R.drawable.chair_pantom_black;
            } else if (pName.contains("브라운")) {
                resId = R.drawable.chair_pantom_brown;
            } else if (pName.contains("레드")) {
                resId = R.drawable.chair_pantom_red;
            } else {
                resId = R.drawable.chair_pantom_black;
            }
        } else if (pName.contains("파라오")) {
            resId = R.drawable.chair_parao;
            if (pName.contains("S")) {
                resId = R.drawable.chair_parao_s;
            }
        } else if (pName.contains("프레지던트")) {
            resId = R.drawable.chair_president;
            if (pName.contains("플러스")) {
                resId = R.drawable.chair_president_plus;
            }
        } else if (pName.contains("렉스")) {
            resId = R.drawable.chair_rexl;
            if (pName.contains("블랙")) {
                resId = R.drawable.chair_rexl_black;
            }
        } else if (pName.contains("레지나")) {
            resId = R.drawable.chair_regina;
        } else if (pName.contains("프레임")) {
            if (pName.contains("아이보리")) {
                resId = R.drawable.latex_white_frame;
            } else if (pName.contains("플로팅")) {
                resId = R.drawable.latex_floating_frame;
            } else if (pName.contains("초코")) {
                resId = R.drawable.latex_black_frame;
            }
        } else if (pName.contains("공통")) {
            resId = R.drawable.latex_mat;
        }
        return resId;
    }
}
