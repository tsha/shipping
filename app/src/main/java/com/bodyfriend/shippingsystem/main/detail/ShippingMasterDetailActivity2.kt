package com.bodyfriend.shippingsystem.main.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.databinding.ObservableInt
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.TextView
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.base.util.Util
import com.bodyfriend.shippingsystem.databinding.ActivityShippingMasterDetail2Binding
import com.bodyfriend.shippingsystem.main.delivery_order.DeliveryInventoryDialogFragment
import com.bodyfriend.shippingsystem.main.delivery_order.GiftDialogFragment
import com.bodyfriend.shippingsystem.main.delivery_order.ReturnProductDialogFragment
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData
import com.bodyfriend.shippingsystem.main.delivery_order.net.GiftListData
import com.bodyfriend.shippingsystem.main.detail.ui.fragment.ResponseFragment
import com.bodyfriend.shippingsystem.main.detail.ui.fragment.SectionsPagerAdapter
import com.bodyfriend.shippingsystem.main.detail.ui.fragment.ServiceFragment
import com.bodyfriend.shippingsystem.main.detail.viewmode.DetailViewModel
import com.bodyfriend.shippingsystem.main.detail.viewmode.Presenter
import com.bodyfriend.shippingsystem.main.shipping_list.data.IntervalTimerViewModelFactory

class ShippingMasterDetailActivity2 : AppCompatActivity(), DeliveryInventoryDialogFragment.DeliveryInventoryDialogInterface
        , GiftDialogFragment.GiftDialogInterface, ReturnProductDialogFragment.ReturnProductDialogInterface, Presenter {


    private lateinit var serviceFragment: ServiceFragment
    private lateinit var responseFragment: ResponseFragment

    private lateinit var binding: ActivityShippingMasterDetail2Binding
    private val detailViewModel: DetailViewModel
            by lazy {
                ViewModelProviders.of(this, IntervalTimerViewModelFactory)
                        .get(DetailViewModel::class.java)
            }

    companion object {
        const val SHIPPING_SEQ = "SHIPPING_SEQ"
        @JvmStatic
        fun newInstance(context: Context, shippingSeq: String): Intent {
            val nextIntent = Intent(context, ShippingMasterDetailActivity2::class.java)
            nextIntent.putExtra(SHIPPING_SEQ, shippingSeq)
            return nextIntent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.setContentView(
                this, R.layout.activity_shipping_master_detail2)
        binding.viewmodel = detailViewModel
        setupViewModel()

        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener { v -> onBackPressed() }



        responseFragment = ResponseFragment.newInstance()
        serviceFragment = ServiceFragment.newInstance()

        val sectionsPagerAdapter = SectionsPagerAdapter(this@ShippingMasterDetailActivity2, supportFragmentManager)
        sectionsPagerAdapter.addFragment(responseFragment)
        sectionsPagerAdapter.addFragment(serviceFragment)
        binding.viewPager.adapter = sectionsPagerAdapter
        binding.tabs.setupWithViewPager(binding.viewPager)
        onReload()
        println("ProgressNo.getValue(0) ${ProgressNo.getProgressString(0)}")
    }


    private fun setupViewModel() {
        detailViewModel.receiveData.observe(this, Observer {
            it?.let {
                responseFragment.setViewData(it)
                serviceFragment.setViewData(it)
            }
        })

        detailViewModel.isShowPopupWindow.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {

                detailViewModel.isShowPopupWindow.get()?.let {
                    if (TextUtils.isEmpty(it)) {
                        hideReferNamePopupWindow()
                    } else {
                        showReferNamePopupWindow(detailViewModel.isShowPopupWindow.get())
                    }
                }
            }
        })

        detailViewModel.quantity.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(p0: Observable?, p1: Int) {
                println("p0.get() : ${(p0 as ObservableInt).get()}")
            }
        })
    }

    private lateinit var mReferNamePopupWindow: PopupWindow


    private fun hideReferNamePopupWindow() {
        if (::mReferNamePopupWindow.isInitialized && mReferNamePopupWindow.isShowing) {
            mReferNamePopupWindow.dismiss()
        }

    }

    private fun showReferNamePopupWindow(get: String?) {
        hideReferNamePopupWindow()
        val form = layoutInflater.inflate(R.layout.refer_name_popup_window, null)
        val referName = form.findViewById<TextView>(R.id.textView1)
        referName.text = get
        mReferNamePopupWindow = PopupWindow(form, RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        mReferNamePopupWindow.showAsDropDown(findViewById(R.id.product_name), 0, (-Util.convertDpToPixel(52 * 1.4f, this@ShippingMasterDetailActivity2)).toInt())
    }


    fun onReload() {
        detailViewModel.getDetailInfo(intent.getStringExtra(SHIPPING_SEQ))
    }


    override fun selectItem(productData: DeliverManStockData.ResultDatum) {
    }

    override fun selectItem(productData: DeliverManStockData.ResultDatum, giftIndex: Int) {
    }

    override fun giftSelect(giftData: GiftListData.ResultData, giftIndex: Int) {
    }

    override fun returnProductItem(returnProductItem: DeliverManStockData.ResultDatum, callbackView: Int) {
    }

    override fun onItemQuantityChange(selectedFreeGiftIndex: Int) {
        println("latexCodeList.get()?.get(selectedFreeGiftIndex) $selectedFreeGiftIndex")
    }
}