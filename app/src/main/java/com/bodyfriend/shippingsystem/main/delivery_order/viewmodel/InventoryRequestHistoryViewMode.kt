package com.bodyfriend.shippingsystem.main.delivery_order.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator
import com.bodyfriend.shippingsystem.base.util.OH
import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData
import com.bodyfriend.shippingsystem.main.delivery_order.net.LoiData
import com.bodyfriend.shippingsystem.main.delivery_order.net.LoiGroupByIssueNoData
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseApi
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*


class InventoryRequestHistoryViewMode : ViewModel() {


    var errorResult: MutableLiveData<String> = MutableLiveData()
    var loiGetList: MutableLiveData<MutableList<LoiGroupByIssueNoData.ForwardingItemData>> = MutableLiveData()
    var loiResponseList: MutableLiveData<MutableList<LoiGroupByIssueNoData.ForwardingItemData>> = MutableLiveData()
    var isClear: MutableLiveData<Boolean> = MutableLiveData()
    var isResponseClear: MutableLiveData<Boolean> = MutableLiveData()


    fun getInInLoiList() {
        val subscribeWith = ServiceGenerator.createService(WarehouseApi::class.java)
                .selectInLoiList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : Observer<LoiData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: LoiData) {
                        println("!it.isNullOrEmpty()")
                        t.resultData.let {
                            if (it.isNullOrEmpty()) {
                                errorResult.postValue("erro")
                            } else {
                                println("!it.isNullOrEmpty()")
                                loiGetList.postValue(groupByIssueNo(it))
                            }
                        }
                        if (t.resultData.isNullOrEmpty()) {
                            println("!it.isNullOrEmpty()")
                            isClear.postValue(true)
                        }
                    }

                    override fun onError(e: Throwable) {
//                        isClear.postValue(true)
                        errorResult.postValue("erro")
                    }
                })
    }


    private fun groupByIssueNo(list: List<LoiData.ResultDatum>): ArrayList<LoiGroupByIssueNoData.ForwardingItemData> {
        val hashMap = HashMap<String, List<LoiData.ResultDatum>>()
        for (item in list) { // issueNo 로 hashmap에 리스트를 나눠서 담는다.
            var list2: MutableList<LoiData.ResultDatum>? = hashMap[item.issueNo] as MutableList<LoiData.ResultDatum>?
            if (list2 == null) {
                list2 = ArrayList()
            }
            list2.add(item)
            item.issueNo.let { it.let { it1 -> hashMap.put(it1, list2) } }
        }

        val returnList = ArrayList<LoiGroupByIssueNoData.ForwardingItemData>()
        val keySet = hashMap.keys.toTypedArray()
        Arrays.sort(keySet)
        for (key in keySet) {
            val list3 = hashMap[key]
            val forwardingItemData = LoiGroupByIssueNoData.ForwardingItemData()

            if (list3 != null) {
                for (item in list3) {
                    val itemNm = item.itemNm ?: ""
                    val makerLotNo = item.makerLotNo ?: ""
                    val serialCd = item.serialCd ?: ""
                    forwardingItemData.modelInfoArrayList.add(LoiGroupByIssueNoData.ForwardingItemData.ModelInfo(makerLotNo, serialCd, itemNm))
                }
            }
            forwardingItemData.issueNo = list3?.get(0)?.issueNo ?: "0"
            forwardingItemData.toSlNm = list3?.get(0)?.toslNm ?: "0"
            forwardingItemData.movTypeNm = list3?.get(0)?.movTypeNm ?: "0"
            forwardingItemData.reqDt = list3?.get(0)?.reqDt ?: "0"
            forwardingItemData.modelTotal = list3?.size ?: 0
            forwardingItemData.fromSlNm = list3?.get(0)?.fromSlNm.toString()

            returnList.add(forwardingItemData)
        }
        returnList.sortWith(Comparator { o1, o2 -> o2.reqDt.compareTo(o1.reqDt) })
        return returnList
    }


    fun getInLoiReceiveList() {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectInLoiReceiveList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<LoiData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: LoiData) {


                        t.resultData.let {
                            if (it.isNullOrEmpty()) {
                                errorResult.postValue("erro")
                            } else {
                                loiResponseList.postValue(groupByIssueNo(it))
                            }
                        }
                        if (t.resultData.isNullOrEmpty()) {
                            println("getInLoiReceiveList !it.isNullOrEmpty()")
                            isResponseClear.postValue(true)
                        }

                    }

                    override fun onError(e: Throwable) {
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }
                })
    }

    /**
    기사간 제품 인수인계 확인 처리
     * */
    fun setConfirmTransferReq(issueNo: String) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .confirmTransferReq(issueNo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<BfData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: BfData) {
                        getInLoiReceiveList()
                    }

                    override fun onError(e: Throwable) {
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }
                })
    }

    /**
    신청취소
     * */
    fun delExportLoi(issueNo: String) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .delExportLoi(issueNo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<BfData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: BfData) {
                        getInInLoiList()
                    }

                    override fun onError(e: Throwable) {
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }
                })
    }
}