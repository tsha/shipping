package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.WarehouseFragmentBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.GiftAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.GiftListData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.WarehouseViewModel


class GiftDialogFragment : AppCompatDialogFragment() {
    private lateinit var mBinding: WarehouseFragmentBinding
    private var content: Int = 1
    private lateinit var giftAdapter: GiftAdapter

    companion object {
        const val EXTRA_GIFT_INDEX = "EXTRA_GIFT_INDEX"
        @JvmStatic
        fun newInstance(giftIndex: Int): GiftDialogFragment {
            val f = GiftDialogFragment()
            val args = Bundle()
            args.putInt(EXTRA_GIFT_INDEX, giftIndex)
            f.arguments = args
            return f
        }
    }


    interface GiftDialogInterface {
        fun giftSelect(giftData: GiftListData.ResultData, giftIndex: Int)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        content = arguments?.getInt(EXTRA_GIFT_INDEX) ?: 1
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.warehouse_fragment, null, false)
        val viewModel = ViewModelProviders.of(this).get(WarehouseViewModel::class.java)
        content.let { viewModel.selectGiftList() }
        mBinding.data = viewModel
        getObserver(viewModel)
        giftAdapter = GiftAdapter(itemClick = {
            (activity as (GiftDialogInterface)).giftSelect(it, content)
            dismiss()
        })


        val mRecentLayoutManager = LinearLayoutManager(activity)
        mBinding.listWarehouse.apply {
            layoutManager = mRecentLayoutManager
            setHasFixedSize(true)
            adapter = giftAdapter

            addItemDecoration(DividerItemDecoration(activity, 1))

        }
        val alert = AlertDialog.Builder(activity)
        alert.apply {
            setView(mBinding.root)
        }

        return alert.create()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val style = STYLE_NO_FRAME
        setStyle(style, theme)

    }

    private fun getObserver(viewModel: WarehouseViewModel) {
        viewModel.giftListData.observe(this, Observer { it ->
            it.let {
                if (it != null) {
                    giftAdapter.updateMealList(it)
                }
            }

        })
    }
}
