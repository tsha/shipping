package com.bodyfriend.shippingsystem.main.delivery_order.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.databinding.ItemReqMaterialBinding;
import com.bodyfriend.shippingsystem.main.delivery_order.net.ProductItemListData;

import java.util.ArrayList;

/**
 * Created by Taeseok on 2017-10-12.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private ArrayList<ProductItemListData.list> mReqMaterialList = new ArrayList<>();
    private ViewHolder mViewHolder;
//    HashSet<ProductItemListData.list> listHashSet = new HashSet<>();

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mViewHolder = new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_req_material, parent, false));
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
        holder.setData(mReqMaterialList.get(position));

    }

    @Override
    public int getItemCount() {
        return mReqMaterialList.size();
    }

    public void addItem(ProductItemListData.list result) {


        if (!mReqMaterialList.isEmpty()) {
            Log.d(" gdsLvlCd : " + result.toString());
            for (ProductItemListData.list item : mReqMaterialList) {
                Log.d("mReqMaterialList gdsLvlCd : " + item.toString());
            }
            if (!mReqMaterialList.contains(result)) {
                Log.d("!mReqMaterialList.contains(result)");
                mReqMaterialList.add(new ProductItemListData.list(result));
                notifyItemRangeChanged(mReqMaterialList.size() - 1, mReqMaterialList.size());
            } else {
                Toast.makeText(mViewHolder.mBinding.getRoot().getContext(), "동일 제품이 존재합니다.", Toast.LENGTH_SHORT).show();
            }
        } else if (mReqMaterialList.isEmpty()) {
            Log.d("mReqMaterialList.isEmpty()");
            Log.d(" gdsLvlCd : " + result.gdsLvlCd);
            mReqMaterialList.add(new ProductItemListData.list(result));
            for (ProductItemListData.list item : mReqMaterialList) {
                Log.d("mReqMaterialList gdsLvlCd : " + item.gdsLvlCd);
            }
            notifyDataSetChanged();
        }

    }

    public ArrayList<ProductItemListData.list> getItems() {
        return mReqMaterialList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemReqMaterialBinding mBinding;


        public ViewHolder(ItemReqMaterialBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;

        }

        public void setData(ProductItemListData.list result) {
            String info = String.format("%s %s", result.itemNmKr, result.clrtnNm);
            mBinding.itemMaterialName.setText(info);
            mBinding.itemMaterialCount.setText(String.valueOf(result.count));
        }


    }


}
