package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.SerializedName

data class ReturnProductData(@SerializedName("resultData")

                             var resultData: List<ResultDatum>) {
    data class ResultDatum(
            @SerializedName("ITEM_NM")
            var itemNm: String
            , @SerializedName("ITEM_CD")
            var itemCd: String
            , var serialCd: String

    )

}
