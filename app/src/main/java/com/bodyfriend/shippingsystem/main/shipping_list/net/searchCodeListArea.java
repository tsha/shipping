package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 지역 코드리스트
 */
public class searchCodeListArea extends searchCodeList {

	public searchCodeListArea() {
		super();
//		setEnableProgress();
		setParam("code", 400);
	}
	
	@Override
	protected void parse(String json) {
		super.parse(json);
	}

	public Data data;

	public static class Data {
		public String resultMsg;
		public int resultCode;
		
		public resultData resultData1;
		public List<resultData> resultData;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}

	// "AREA": "406",

	// 05-04 10:18:26.916: W/(18469): "INS_DT": 1419387850977,
	// 05-04 10:18:26.916: W/(18469): "DET_CD": "405",
	// 05-04 10:18:26.916: W/(18469): "DESCRIPTION": "",
	// 05-04 10:18:26.916: W/(18469): "DET_CD_NM": "충북",
	// 05-04 10:18:26.916: W/(18469): "COMM_CD_NM": "지역",
	// 05-04 10:18:26.916: W/(18469): "UPD_ID": "admin",
	// 05-04 10:18:26.916: W/(18469): "UPD_DT": 1419387850977,
	// 05-04 10:18:26.916: W/(18469): "GROUP_CD": "SP",
	// 05-04 10:18:26.916: W/(18469): "INS_ID": "admin",
	// 05-04 10:18:26.916: W/(18469): "SORT_SEQ": "5",
	// 05-04 10:18:26.916: W/(18469): "REF_2": "",
	// 05-04 10:18:26.916: W/(18469): "REF_3": "",
	// 05-04 10:18:26.916: W/(18469): "REF_1": "",

}
