package com.bodyfriend.shippingsystem.main.sale

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.bodyfriend.shippingsystem.base.NetConst
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SaleViewModel : ViewModel() {
    private val mProductListData = MutableLiveData<List<ProductListData.ResultData.list>>()
    private val mSaleResult = MutableLiveData<SaleResult>()
    val getProductListData: LiveData<List<ProductListData.ResultData.list>> get() = mProductListData
    val getSaleResult: LiveData<SaleResult> get() = mSaleResult


    fun createBizSale(id: String, name: String, modelKind: String, saleType: String, customerType: String, modelName: String,
                      customerName: String, intel: String, inhandphone: String, etcclausal: String) {
        val onSaleCommit = ServiceGenerator.changeApiBaseUrl(SaleApi::class.java, NetConst.HOST_SALE)
                .createBizSale(id, name, modelKind, saleType, customerType, modelName, customerName, intel, inhandphone, etcclausal)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : Observer<SaleResult> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(saleResult: SaleResult) {
                        mSaleResult.value = saleResult
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {

                    }
                })

    }

    fun getProductLst(kindCode: String) {
        val onSaleCommit = ServiceGenerator.changeApiBaseUrl(SaleApi::class.java, NetConst.HOST_SALE)
                .getProductList(kindCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : Observer<ProductListData> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(productListData: ProductListData) {
                        mProductListData.value = productListData.resultData.list
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {

                    }
                })

    }

}