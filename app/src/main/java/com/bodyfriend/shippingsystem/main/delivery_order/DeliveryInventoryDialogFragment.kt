package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.DialogDeliveryInventoryBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.DeliveryInventoryAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.DeliveryInventoryViewMode
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingMasterDetailActivity


class DeliveryInventoryDialogFragment : AppCompatDialogFragment() {
    private lateinit var mBinding: DialogDeliveryInventoryBinding
    private lateinit var _productAdapter: DeliveryInventoryAdapter


    enum class DeliveryInventoryType {
        PRODUCT, GIFT
    }

    interface DeliveryInventoryDialogInterface {
        fun selectItem(productData: DeliverManStockData.ResultDatum)

        fun selectItem(productData: DeliverManStockData.ResultDatum, giftIndex: Int)
    }

    companion object {
        private const val EXTRA_INVENTORY_TYPE: String = "EXTRA_INVENTORY_TYPE"
        private const val EXTRA_GIFT_INDEX: String = "EXTRA_GIFT_INDEX"

        @JvmStatic
        fun newInstance(): DeliveryInventoryDialogFragment {

            val f = DeliveryInventoryDialogFragment()
            // Supply num input as an argument.
            val args = Bundle()
//            args.putString(EXTRA_INVENTORY_TYPE, inventoryType.name)
            f.arguments = args
            return f
        }

        @JvmStatic
        fun newInstance(inventoryType: DeliveryInventoryType, giftIndex: Int): DeliveryInventoryDialogFragment {

            val f = DeliveryInventoryDialogFragment()
            // Supply num input as an argument.
            val args = Bundle()
            args.putString(EXTRA_INVENTORY_TYPE, inventoryType.name)
            args.putInt(EXTRA_GIFT_INDEX, giftIndex)
            Log.d("TAG", "giftIndex $giftIndex \tinventoryType.name ${inventoryType.name}")
            f.arguments = args
            return f
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_delivery_inventory, null, false)
        val viewModel = ViewModelProviders.of(this).get(DeliveryInventoryViewMode::class.java)
        viewModel.getDelivryManStockList()
        mBinding.data = viewModel
        getObserver(viewModel)
        _productAdapter = DeliveryInventoryAdapter(
                itemClick = { it2 ->
                    when (activity) {
                        is DeliveryInventoryActivity -> {
                            (activity as DeliveryInventoryActivity).warehouseProductSelect(it2)
                        }
                        is ShippingMasterDetailActivity -> arguments?.getInt(EXTRA_GIFT_INDEX)?.let { it1 -> (activity as ShippingMasterDetailActivity).selectItem(it2, it1) }
                    }
                    dismiss()
                }
        )


        val mRecentLayoutManager = LinearLayoutManager(activity)
        mBinding.listWarehouse.apply {
            layoutManager = mRecentLayoutManager
            setHasFixedSize(true)
            adapter = _productAdapter
            addItemDecoration(DividerItemDecoration(activity, 1))
        }

        val alert = AlertDialog.Builder(activity)
        alert.apply {
            setView(mBinding.root)
        }
        return alert.create()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setStyle(STYLE_NO_FRAME, theme)
    }

    private fun getObserver(viewModel: DeliveryInventoryViewMode) {
        viewModel.deliverManStock.observe(this, Observer { it ->
            it?.let { it1 ->
                val inventoryType = arguments?.getString(EXTRA_INVENTORY_TYPE)
                Log.d("TAG", "getObserver inventoryType $inventoryType")

                val filterList = ArrayList<DeliverManStockData.ResultDatum>()
                when (inventoryType) {
                    DeliveryInventoryType.PRODUCT.name -> {
                        it1.forEach {
                            if (it.giftYn == "N" || it.giftYn == "S") {
                                filterList.add(it)
                            }
                        }
                        if (filterList.isEmpty()) {
                            Toast.makeText(activity, "사용 가능한 제품이 없습니다.", Toast.LENGTH_LONG).show()
                            dismiss()
                        } else {
                            _productAdapter.updateInventoryList(filterList)
                        }

                    }
                    DeliveryInventoryType.GIFT.name -> {

                        it1.forEach {
                            if (it.giftYn == "S" || it.giftYn == "Y") {
                                filterList.add(it)
                            }
                        }
                        if (filterList.isEmpty()) {
                            Toast.makeText(activity, "사용 가능한 사은품이 없습니다.", Toast.LENGTH_LONG).show()
                            dismiss()
                        } else {
                            _productAdapter.updateInventoryList(filterList)
                        }
                    }
                    else -> _productAdapter.updateInventoryList(it)
                }
            }
        })

        viewModel.isSessionOut.observe(this, Observer {
            if (it!!)
                dismiss()
        })


        viewModel.errorMessage.observe(this, Observer { it ->
            it?.let {
                dismiss()
                Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
            }

        })
    }
}
