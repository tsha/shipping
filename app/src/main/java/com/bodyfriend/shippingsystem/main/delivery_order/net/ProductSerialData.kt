package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProductSerialData(

        @SerializedName("resultData")
        @Expose
        var data: ResultData? = null

        , @SerializedName("resultMsg")
        @Expose
        var resultMsg: String? = null
        , @SerializedName("resultCode")
        @Expose
        var resultCode: String? = null) {

    data class ResultData(
            @SerializedName("BAD_AMOUNT")
            @Expose
            var badAmount: Int? = null
            , @SerializedName("SL_CD")
            @Expose
            var slCd: String? = null
            , @SerializedName("SL_NM")
            @Expose
            var slNm: String? = null
            , @SerializedName("GIFT_YN")
            @Expose
            var giftYn: String? = null
            , @SerializedName("ITEM_NM")
            @Expose
            var itemNm: String? = null
            , @SerializedName("ITEM_CD")
            @Expose
            var itemCd: String? = null
            , @SerializedName("SERIAL_CD")
            @Expose
            var serialCd: String? = null
            , @SerializedName("GOOD_AMOUNT")
            @Expose
            var goodAmount: Int? = null
            , @SerializedName("qcy")
            @Expose
            var qcy: String? = null
    )


}
