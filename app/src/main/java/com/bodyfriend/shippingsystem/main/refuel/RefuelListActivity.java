package com.bodyfriend.shippingsystem.main.refuel;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.databinding.ActivityRefuelListBinding;
import com.bodyfriend.shippingsystem.main.refuel.net.RefuelData;

import java.util.ArrayList;
import java.util.Objects;

public class RefuelListActivity extends AppCompatActivity implements RefuelListViewModel.Callback {

    private ActivityRefuelListBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_refuel_list);

        setSupportActionBar(mBinding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mBinding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        RefuelListViewModel refuelListViewModel = new RefuelListViewModel(this);
        mBinding.setViewModel(refuelListViewModel);
        setupRecyclerView();
    }


    private void setupRecyclerView() {
        RefuelAdapter mAdapter = new RefuelAdapter();
        mBinding.recyclerView.setAdapter(mAdapter);
        mBinding.recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL));
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    @Override
    public void onRefuelData(ArrayList<RefuelData.Record> refuelData) {
        Log.d("refuelData : " + refuelData.size());
        mBinding.setRefuelData(refuelData);
    }

    @BindingAdapter("item")
    public static void bindItem(RecyclerView recyclerView, ArrayList<RefuelData.Record> movie) {
        Log.d("bindItem : ");
        RefuelAdapter adapter = (RefuelAdapter) recyclerView.getAdapter();
        if (adapter != null) {
//            adapter.setItem(movie);
            if (movie != null && !movie.isEmpty()) {
//                for (RefuelData.records records : movie) {
//                    Log.d("movie : " + movie.toString());
//                }

                adapter.setData(movie);
            }
        }
    }
}
