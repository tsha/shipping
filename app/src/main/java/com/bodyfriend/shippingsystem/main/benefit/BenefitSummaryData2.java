package com.bodyfriend.shippingsystem.main.benefit;

import java.util.List;

public class BenefitSummaryData2 {

    public List<ItemBenefit> listHashMap;

    public static class ItemBenefit {
        public String day;
        public String total;
        public String benefitSummaryInfo;

    }
}
