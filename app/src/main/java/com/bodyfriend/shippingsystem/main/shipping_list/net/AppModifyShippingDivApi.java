package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AppModifyShippingDivApi {
    @FormUrlEncoded
    @POST("mobile/api/appUpdateGiftAddr.json")
    Observable<BfData> updateGiftAddr(
            @Field("m_freegiftAddr") String addr
            , @Field("BODY_NO") String mBodyNo
    );

    @FormUrlEncoded
    @POST("mobile/api/appShippingMasterDetail.json?deleteFlag=N")
    Observable<ShippingMasterDetailData> getShippingMasterDetailData(
            @Field("shipping_seq") String shipping_seq
    );


    @FormUrlEncoded
    @POST("mobile/api/appSearchCodeList.json")
    Observable<ShippingCodeData> getShippingCodeData(
            @Field("code") int code
    );

    @FormUrlEncoded
    @POST("mobile/api/modifyAddress.json")
    Observable<BfData> modifyAddress(
            @Field("m_shippingseq") String shippingSeq
            , @Field("m_address") String address
    );


    @POST("mobile/api/appSelectLatexCode.json")
    Observable<LatexCodeData> getLatexCode();
}
