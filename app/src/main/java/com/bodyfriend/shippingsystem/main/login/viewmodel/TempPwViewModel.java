package com.bodyfriend.shippingsystem.main.login.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;
import com.bodyfriend.shippingsystem.main.login.net.LoginApi;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TempPwViewModel extends ViewModel {

    private MutableLiveData<BfData> kakaoKeyData = new MutableLiveData<>();
    private MutableLiveData<BfData> authKeyData = new MutableLiveData<>();
    private MutableLiveData<BfData> updatePwData = new MutableLiveData<>();

    public MutableLiveData<BfData> getKakaoKeyData() {
        return kakaoKeyData;
    }

    public MutableLiveData<BfData> getAuthKeyData() {
        return authKeyData;
    }

    public MutableLiveData<BfData> getUpdatePwData() {
        return updatePwData;
    }

    public void getKakaoKey(String id, String phoneNum) {
        ServiceGenerator.createService(LoginApi.class)
                .reqTempKey(id, phoneNum)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BfData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BfData bfData) {
                        kakaoKeyData.postValue(bfData);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void checkauthKey(String id, String authKey) {
        ServiceGenerator.createService(LoginApi.class)
                .authKeyCheck(id, authKey)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BfData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BfData bfData) {
                        authKeyData.postValue(bfData);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void updateAdminPwd(String id, String authKey) {
        ServiceGenerator.createService(LoginApi.class)
                .updateAdminPwd(id, authKey)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BfData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BfData bfData) {
                        updatePwData.postValue(bfData);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
