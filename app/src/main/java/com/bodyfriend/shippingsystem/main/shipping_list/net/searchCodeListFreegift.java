package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 사은품
 */
public class searchCodeListFreegift extends searchCodeList {

    public searchCodeListFreegift() {
        super();
        setParam("code", 9600);
        showLog(false);
    }

    @Override
    protected void parse(String json) {
        super.parse(json);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
        public List<resultData> resultData;

        public class resultData {
            public long INS_DT;
            public String DET_CD;
            public String DESCRIPTION;
            public String DET_CD_NM;
            public String COMM_CD_NM;
            public String UPD_ID;
            public long UPD_DT;
            public String GROUP_CD;
            public String INS_ID;
            public String SORT_SEQ;
            public String REF_2;
            public String REF_3;
            public String REF_1;
            public String COMM_CD;
        }
    }


}
