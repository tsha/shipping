package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 제품 구분
 */
public class searchCodeListProductGb extends searchCodeList {
	public searchCodeListProductGb() {
		super();
		setParam("code", 9200);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public String resultMsg;
		public int resultCode;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public int SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "INS_DT": 1429003080103,
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DET_CD": "L",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DESCRIPTION": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DET_CD_NM": "라텍스",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "COMM_CD_NM": "제품구분",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "UPD_ID": "adminsp",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "UPD_DT": 1429003080103,
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "GROUP_CD": "SP",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "INS_ID": "adminsp",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "SORT_SEQ": "2",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_2": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_3": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_1": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "COMM_CD": "9200"
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):         },
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):         {
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "INS_DT": 1429003071903,
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DET_CD": "M",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DESCRIPTION": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DET_CD_NM": "안마의자",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "COMM_CD_NM": "제품구분",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "UPD_ID": "adminsp",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "UPD_DT": 1429003071903,
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "GROUP_CD": "SP",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "INS_ID": "adminsp",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "SORT_SEQ": "1",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_2": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_3": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_1": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "COMM_CD": "9200"
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):         },
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):         {
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "INS_DT": 1429003091200,
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DET_CD": "W",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DESCRIPTION": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "DET_CD_NM": "정수기",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "COMM_CD_NM": "제품구분",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "UPD_ID": "adminsp",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "UPD_DT": 1429003091200,
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "GROUP_CD": "SP",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "INS_ID": "adminsp",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "SORT_SEQ": "3",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_2": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_3": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "REF_1": "",
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):             "COMM_CD": "9200"
//	05-07 10:07:41.203: W/_NETLOG_IN(32203):         }

}
