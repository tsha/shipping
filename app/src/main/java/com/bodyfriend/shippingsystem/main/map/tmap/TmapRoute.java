
package com.bodyfriend.shippingsystem.main.map.tmap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TmapRoute {

    private String type;
    private Properties properties;
    private List<Feature> features = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "TmapRoute{" +
                "type='" + type + '\'' +
                ", properties=" + properties +
                ", features=" + features +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
