package com.bodyfriend.shippingsystem.main.map.presenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.base.util.CC;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.map.net.NaverGeocoderApi;
import com.bodyfriend.shippingsystem.main.map.net.NaverGeocoderClient;
import com.bodyfriend.shippingsystem.main.map.net.TMapApi;
import com.bodyfriend.shippingsystem.main.map.tmap.FullTextGeoCodingData;
import com.bodyfriend.shippingsystem.main.map.vo.Geocorder;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingMasterDetailActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingDivList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;
import com.kakao.kakaonavi.KakaoNaviParams;
import com.kakao.kakaonavi.KakaoNaviService;
import com.kakao.kakaonavi.NaviOptions;
import com.kakao.kakaonavi.options.CoordType;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ts.ha on 2017-04-28.
 */

public class ShippingMapPresenterImpl implements ShippingMapPresenter {
    private static final String TAG = ShippingMapPresenterImpl.class.getSimpleName();
    private Activity mActivity;
    private ShippingMapPresenter.View view;
    protected Net.OnNetResponse<ShippingDivList> onNetResponse = new Net.OnNetResponse<ShippingDivList>() {

        @Override
        public void onResponse(ShippingDivList response) {

            List<ShippingList.resultData.list> data = response.data.resultData.list;
            view.sendResponseShippingDivList(data);
//            for (MapShippingListData.resultData.list deliveryItem :
//                    data) {
//                Log.d(TAG, "onResponse: " + deliveryItem.INSADDR);
//            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
//            hideProgress();
            view.sendError(error);
        }
    };
    private Calendar mStartCalendar = Calendar.getInstance();
    private ShippingList.resultData.list mDeliveryItem;
    private String mDueDate;

    public ShippingMapPresenterImpl(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public void setView(ShippingMapPresenter.View view) {

        this.view = view;

        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 1);
    }

    public void reqShippingDivList(String dueDate) {
        if (!(this instanceof CC.ILOGIN) && !Auth.isLogin()) {
            return;
        }
        this.mDueDate = dueDate;
        int s_datetype = 0;
        String dateStart = SDF.yyyymmdd_2.format(mStartCalendar.getTime());
        String dateEnd = SDF.yyyymmdd_2.format(new Date());

        String s_oneself = "Y";

        Net.async(new ShippingDivList(s_datetype, dateStart, dateEnd, "", "", "", s_oneself, false)).setOnNetResponse(onNetResponse);

    }

    @Override
    public void setModel(ShippingList.resultData.list deliveryItem) {
        mDeliveryItem = deliveryItem;
    }

    @Override
    public void getGeocoder(List<ShippingList.resultData.list> data) {

        Retrofit naverGeocoderClient = NaverGeocoderClient.getClient();
        NaverGeocoderApi retrofitApi = naverGeocoderClient.create(NaverGeocoderApi.class);


        TMapApi tMapApi = ServiceGenerator.changeApiBaseUrl(TMapApi.class, NetConst.HOST_TMAP);

        for (ShippingList.resultData.list deliveryItem : data) {
            if (mDueDate.equals(deliveryItem.DUE_DATE)) {
                Call<FullTextGeoCodingData> geocoder = tMapApi.fullAddrGeo(deliveryItem.INSADDR);
                Log.d(TAG, "deliveryItem DUE_DATE:  " + deliveryItem.DUE_DATE);

                geocoder.enqueue(new Callback<FullTextGeoCodingData>() {
                    @Override
                    public void onResponse(Call<FullTextGeoCodingData> call, Response<FullTextGeoCodingData> response) {
                        if (response.body() != null && response.body().coordinateInfo != null && response.body().coordinateInfo.coordinate != null) {
                            FullTextGeoCodingData.CoordinateInfo.Coordinate item = response.body().coordinateInfo.coordinate.get(0);
                            Location location = new Location("");


                            location.setLongitude(Double.parseDouble(!item.lat.isEmpty() ? item.lat : item.newLat));
                            location.setLatitude(Double.parseDouble(!item.lon.isEmpty() ? item.lon : item.newLon));
                            deliveryItem.setLocation(location);
                            view.addMarker(deliveryItem);
                        } else {
                            Log.d(TAG, "실패 CUST_NAME:  " + deliveryItem.CUST_NAME + "\t INSADDR:  " + deliveryItem.INSADDR);
                            Geocoder geocoder = new Geocoder(mActivity.getApplicationContext());
                            List<Address> listAddress;
                            try {
                                listAddress = geocoder.getFromLocationName(deliveryItem.INSADDR, 1);
                                if (listAddress.size() > 0) {
                                    Address AddrAddress = listAddress.get(0);
                                    final double latitude = AddrAddress.getLatitude();
                                    final double longitude = AddrAddress.getLongitude();
                                    Location location = new Location("");
                                    location.setLongitude(latitude);
                                    location.setLatitude(longitude);
                                    deliveryItem.setLocation(location);
                                    view.addMarker(deliveryItem);
                                } else {
                                    Toast.makeText(mActivity, deliveryItem.INSADDR + " 주소 변환에 실패 하였습니다.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<FullTextGeoCodingData> call, Throwable t) {
                        t.printStackTrace();
                        Log.d(TAG, "onFailure: ");
                    }
                });
            }
        }
    }

    @Override
    public void onCall(String hphone_no) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + hphone_no));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mActivity.startActivity(intent);
    }

    @Override
    public void onDetailView(String shipping_seq) {
        Intent intent = new Intent(mActivity, ShippingMasterDetailActivity.class);
        intent.putExtra(ShippingMasterDetailActivity.EXTRA.SHIPPING_SEQ, shipping_seq);
        mActivity.startActivity(intent);
    }

    @Override
    public void connectNavi(ShippingList.resultData.list deliveryItem, Location mLastLocation) {
        Intent startLink = mActivity.getPackageManager().getLaunchIntentForPackage("com.locnall.KimGiSa");
        if (startLink == null) {
            AlertDialog.Builder alert_confirm = new AlertDialog.Builder(mActivity);
            alert_confirm.setMessage("카카오내비가 설치 되어 있지 않습니다.");
            alert_confirm.setPositiveButton("설치하기", (dialog, which) -> {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("market://details?id=com.locnall.KimGiSa"));
                mActivity.startActivity(sendIntent);
            });

            alert_confirm.setNegativeButton("취소", (dialog, which) -> {

            });

            AlertDialog alert = alert_confirm.create();
            alert.setIcon(R.drawable.ic_launcher);
            alert.setTitle("내비게이션 연결");
            alert.show();
        } else {
            KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(com.kakao.kakaonavi.Location.newBuilder
                    (deliveryItem.INSADDR, deliveryItem.location.getLatitude(), deliveryItem.location.getLongitude()).build())
                    .setNaviOptions(NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setStartX(mLastLocation.getLatitude()).setStartY(mLastLocation.getLongitude()).build());
            KakaoNaviService.navigate(mActivity, builder.build());
        }
    }

    private class NetworkCall extends AsyncTask<Call, Void, String> {
        @Override
        protected String doInBackground(Call... params) {
            try {
                Call<List<Geocorder>> call = params[0];
                Response<List<Geocorder>> response = call.execute();
                Log.d(TAG, "onResponse: " + response.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }


}
