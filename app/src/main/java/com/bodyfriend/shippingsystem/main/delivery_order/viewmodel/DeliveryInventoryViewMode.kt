package com.bodyfriend.shippingsystem.main.delivery_order.viewmodel


import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator
import com.bodyfriend.shippingsystem.base.util.OH
import com.bodyfriend.shippingsystem.main.delivery_order.DeliveryInventoryActivity
import com.bodyfriend.shippingsystem.main.delivery_order.DeliveryInventoryDialogFragment
import com.bodyfriend.shippingsystem.main.delivery_order.net.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.zxing.client.android.CaptureActivity
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import util.SoundSearcher


class DeliveryInventoryViewMode : ViewModel() {
    private val _deliverManStock: MutableLiveData<MutableList<DeliverManStockData.ResultDatum>> = MutableLiveData()
    val deliverManStock: LiveData<MutableList<DeliverManStockData.ResultDatum>> get() = _deliverManStock
    var resultData: MutableLiveData<BfData> = MutableLiveData()
    private val _addedItem: MutableLiveData<MutableList<ProductSerialData.ResultData>> = MutableLiveData()
    val addedItem: LiveData<MutableList<ProductSerialData.ResultData>> = _addedItem
    val otherList = MutableLiveData<MutableList<ProductSerialData.ResultData>>()
    val otherItem = MutableLiveData<ProductSerialData.ResultData>()
    var otherListError: MutableLiveData<String> = MutableLiveData()
    var selectedInventoryItem: MutableLiveData<ProductSerialData.ResultData> = MutableLiveData()
    private val selectedItmes: MutableList<ProductSerialData.ResultData> = ArrayList()
    //    var returnTypeString: MutableLiveData<Array<String>> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()

    var deliverManStockData: List<DeliverManStockData.ResultDatum> = listOf()

    var isSessionOut: MutableLiveData<Boolean> = MutableLiveData()

    fun onScanRfid(v: View) {
        val intent = Intent(v.context, CaptureActivity::class.java)
        (v.context as Activity).startActivityForResult(intent, DeliveryInventoryActivity.SCAN_RESULT_CODE)
    }

    fun onScanRfid(context: Context) {
        val intent = Intent(context, CaptureActivity::class.java)
        (context as Activity).startActivityForResult(intent, DeliveryInventoryActivity.SCAN_RESULT_CODE)
    }


    fun showStockList(view: View) {
        Log.d("TAG", "showStockList")
        val z11001 = DeliveryInventoryDialogFragment.newInstance()
        z11001.show((view.context as DeliveryInventoryActivity).supportFragmentManager, "stock")
    }


    fun onWarehouseRadio(group: RadioGroup, checkedId: Int, returnType: Spinner) {

        val checkedRadioButton = group.findViewById(checkedId) as RadioButton
        val isChecked = checkedRadioButton.isChecked
        if (checkedId == R.id.radioWarehouse) {
            if (isChecked) {
//                returnTypeString.postValue(returnType.context.resources.getStringArray(R.array.returnTypeName))

                val adapter = ArrayAdapter<String>(returnType.context,
                        android.R.layout.simple_list_item_1, returnType.context.resources.getStringArray(R.array.returnTypeName));
                returnType.adapter = adapter

            }
        } else if (checkedId == R.id.radioDelivery) {
            if (isChecked) {
//                returnTypeString.postValue(returnType.context.resources.getStringArray(R.array.returnDeliveryTypeName))
                val adapter = ArrayAdapter<String>(returnType.context,
                        android.R.layout.simple_list_item_1, returnType.context.resources.getStringArray(R.array.returnDeliveryTypeName))
                returnType.adapter = adapter
                returnType.invalidate()
            }
        }
    }


    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        checkSearch(s.toString())
    }

    /**
     * 검색 창에 텍스트가 입력될때에 리스트를 재구성한다.
     */
    private fun checkSearch(search: String) {
        val newDatas: ArrayList<DeliverManStockData.ResultDatum>

        if (search.isEmpty()) {

            _deliverManStock.postValue(deliverManStockData as MutableList<DeliverManStockData.ResultDatum>?)
        } else {
            newDatas = ArrayList()
            var value: String
            val value1 = deliverManStock.value
            if (value1 != null) {
                for (data in deliverManStockData) {
                    value = data.itemNm
                    if (SoundSearcher.matchString(value, search)) {
                        newDatas.add(data)
                    }
                }
                _deliverManStock.postValue(newDatas as MutableList<DeliverManStockData.ResultDatum>?)
            }
        }
    }


    fun getDelivryManStockList() {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectDeliveryManStockList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<DeliverManStockData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: DeliverManStockData) {
                        t.resultData.let {
                            println("getDelivryManStockList")
                            if (!it.isNullOrEmpty() && it.isNotEmpty()) {
                                println(" isNotEmpty getDelivryManStockList")
                                _deliverManStock.postValue(it as MutableList<DeliverManStockData.ResultDatum>?)
                                deliverManStockData = it
                            } else {
                                println("재고가 없습니다.")
                                errorMessage.postValue("재고가 없습니다.")
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        isSessionOut.postValue(true)
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }
                })
    }


    fun onTransmisson(editSelectedWarehouse: TextView, spinnerReturnType: Spinner, radioGroupSelectWarehouse: RadioGroup, progressBarLoading: ProgressBar) {
        progressBarLoading.visibility = View.VISIBLE
        when {
            editSelectedWarehouse.tag == null -> {
                progressBarLoading.visibility = View.GONE
                otherListError.value = "창고를 선택하세요"

                return
            }
            selectedItmes.isEmpty() -> {
                progressBarLoading.visibility = View.GONE
                otherListError.value = "입고할 제품을 선택하세요."
                return
            }
            else -> setTransmission(editSelectedWarehouse, spinnerReturnType, radioGroupSelectWarehouse)
        }
    }

    fun setTransmission(editSelectedWarehouse: TextView, spinnerReturnTape: Spinner, radioGroupSelectWarehouse: RadioGroup) {

        val tag = editSelectedWarehouse.tag as WarehouseListData.ResultData

        val jsonArray = JsonArray()
        val json = JsonObject()
        val isChecked = radioGroupSelectWarehouse.checkedRadioButtonId
        val returnType = if (isChecked == R.id.radioWarehouse) {
            spinnerReturnTape.context.resources.getStringArray(R.array.returnTypeCode)[spinnerReturnTape.selectedItemPosition]
        } else {
            spinnerReturnTape.context.resources.getStringArray(R.array.returnDeliveryTypeCode)[spinnerReturnTape.selectedItemPosition]
        }

        selectedItmes.forEach {
            val serialCdJson = JsonObject()
            serialCdJson.addProperty("serialCd", it.serialCd ?: "")
            serialCdJson.addProperty("itemCd", it.itemCd ?: "")
            serialCdJson.addProperty("qty", it.qcy ?: "1")

            jsonArray.add(serialCdJson)

        }
        json.addProperty("toSlCd", tag.slCd)
        json.addProperty("plantCd", tag.plantCd)
        json.addProperty("movType", returnType)
        json.add("itemList", jsonArray)


        val disposableObserver = ServiceGenerator.createService(WarehouseApi::class.java)
                .createInLoi(json.toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<BfData>() {
                    override fun onNext(bfData: BfData) {
//                        resultData.postValue(bfData)
                        selectedItmes.clear()
                        _addedItem.postValue(selectedItmes)
                        otherListError.postValue("등록 완료하였습니다.")
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {

                    }
                })
    }


    fun addOtherItem(otherItem: ProductSerialData.ResultData) {
        otherItem.qcy = "1"
        if (!selectedItmes.contains(otherItem)) {
            selectedItmes.add(otherItem)
            _addedItem.postValue(selectedItmes)
        }
    }

    fun delItem(position: Int, product: ProductSerialData.ResultData) {
        selectedItmes.removeAt(position)
        _addedItem.postValue(selectedItmes)
    }

    fun setProductCount(index: Int, count: Int) {
        selectedItmes[index].qcy = (count + 1).toString()
        _addedItem.postValue(selectedItmes)
    }


    fun addProductItem(serialCd: String, isDeliveryCheck: Boolean) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .checkProductSerialValid(serialCd)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : DisposableObserver<ProductSerialData>() {
                    override fun onNext(productSerialValidData: ProductSerialData) {
                        when {
                            productSerialValidData.resultCode == "200" -> {

                                productSerialValidData.data?.let { selectedItmes.add(it) }
                                _addedItem.postValue(selectedItmes)
                            }
                            /*   productSerialValidData.resultCode == "930" -> {
                                   if (isDeliveryCheck) {
                                       otherListError.postValue("기사 이동에서는 제품을 추가 할수 없습니다.")
                                       return
                                   }
                                   val product = ProductSerialData.ResultData()
                                   product.serialCd = serialCd
                                   otherItem.value = product
                               }*/
                            else -> {
                                otherListError.postValue("등록된 제품이 아닙니다.")
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }

                    override fun onComplete() {

                    }
                })


    }

    fun onSelectProduct(it: DeliverManStockData.ResultDatum) {

        val deliveryStock = ProductSerialData.ResultData()
        deliveryStock.serialCd = it.serialCd
        deliveryStock.qcy = "1"
        deliveryStock.slCd = it.slCd
        deliveryStock.slNm = it.slNm
        deliveryStock.giftYn = "N"
        deliveryStock.itemNm = it.itemNm
        deliveryStock.itemCd = it.itemCd
        Log.d("TAG", "deliveryStock ${deliveryStock.toString()}")
        selectedInventoryItem.postValue(deliveryStock)
//        selectedInventoryItem.value(deliveryStock)
//        selectedInventoryItem.value
    }


}