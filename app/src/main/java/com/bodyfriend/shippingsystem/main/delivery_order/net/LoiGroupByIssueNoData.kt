package com.bodyfriend.shippingsystem.main.delivery_order.net

import java.util.ArrayList

/**
 * Created by Taeseok on 2018-03-05.
 */

class LoiGroupByIssueNoData {

    var forwardingItemData: ForwardingItemData? = null

    class ForwardingItemData {
        var movTypeNm: String? = null
        var reqDt: String =""
        var loiKnd: String? = null
        var issueNo: String? = null
        var toSlNm: String? = null
        var modelTotal: Int = 0
        var fromSlNm: String = ""
        var modelInfoArrayList = ArrayList<ModelInfo>()

        class ModelInfo(var makerLotNo: String = "", var serialCd: String = "", var itemNm: String = "") {
            var contractNo: String? = null
        }
    }
}
