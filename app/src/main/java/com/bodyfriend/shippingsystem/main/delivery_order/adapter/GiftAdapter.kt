package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemGiftBinding
import com.bodyfriend.shippingsystem.databinding.ItemWarehouseBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.GiftListData
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseListData

class GiftAdapter(val itemClick: (GiftListData.ResultData) -> Unit) : RecyclerView.Adapter<GiftAdapter.ViewHolder>() {


    private var mealList: MutableList<GiftListData.ResultData> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        holder.view.cardView.setOnClickListener { itemClick(mealList[position]) }
    }


    fun updateMealList(postList: MutableList<GiftListData.ResultData>) {
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun addUploadItem(item: GiftListData.ResultData) {
        mealList.add(0, item)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemGiftBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_gift, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<GiftListData.ResultData> {
        return mealList
    }

    class ViewHolder(var view: ItemGiftBinding) : RecyclerView.ViewHolder(view.root)


}