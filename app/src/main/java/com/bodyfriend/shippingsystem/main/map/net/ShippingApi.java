package com.bodyfriend.shippingsystem.main.map.net;

import com.bodyfriend.shippingsystem.main.map.vo.MapShippingListData;

import io.reactivex.Observable;
import retrofit2.http.POST;

public interface ShippingApi {


    /**
     * route 지도 배송 리스트 요청
     */
    @POST("mobile/api/appShippingDivList.json?s_producttype=M&s_oneself=N")
    Observable<MapShippingListData> getShippList();

}
