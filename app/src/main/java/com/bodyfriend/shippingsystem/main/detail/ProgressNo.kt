package com.bodyfriend.shippingsystem.main.detail

enum class ProgressNo(val type: String) {
    INSTALLATION("설치"),
    COLLECTION("회수"),
    MOVE("이동");

    companion object {
        fun getProgressString(progressNo: Int): String {
            return values()[progressNo].type
        }
    }
}