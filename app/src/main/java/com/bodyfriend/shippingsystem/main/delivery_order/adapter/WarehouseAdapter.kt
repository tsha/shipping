package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemWarehouseBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseListData

class WarehouseAdapter(val itemClick: (WarehouseListData.ResultData) -> Unit) : RecyclerView.Adapter<WarehouseAdapter.ViewHolder>() {


    private var mealList: MutableList<WarehouseListData.ResultData> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        holder.view.cardView.setOnClickListener { itemClick(mealList[position]) }
    }


    fun updateMealList(postList: MutableList<WarehouseListData.ResultData>) {
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun addUploadItem(item: WarehouseListData.ResultData) {
        mealList.add(0, item)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemWarehouseBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_warehouse, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<WarehouseListData.ResultData> {
        return mealList
    }

    class ViewHolder(var view: ItemWarehouseBinding) : RecyclerView.ViewHolder(view.root)


}