package com.bodyfriend.shippingsystem.main.map

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.view.animation.BounceInterpolator
import android.widget.Toast
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.base.log.Log
import com.bodyfriend.shippingsystem.base.util.SDF
import com.bodyfriend.shippingsystem.databinding.ActivityRouteMapBinding
import com.bodyfriend.shippingsystem.main.map.viewmodel.RouteMapViewModel
import com.bodyfriend.shippingsystem.main.map.vo.MapShippingListData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*

class RouteMapActivity : AppCompatActivity(), OnMapReadyCallback, ShippingItemAdapter.OnItemClickListener {

    private lateinit var binding: ActivityRouteMapBinding

    private lateinit var mMap: GoogleMap
    private lateinit var viewModel: RouteMapViewModel
    private lateinit var mBounds: LatLngBounds.Builder
    private lateinit var mMarkerList: ArrayList<MapShippingListData.ResultData.ShippingItem>
    private lateinit var shippingListData: List<MapShippingListData.ResultData.ShippingItem>

    private val startEndPoints = java.util.ArrayList<LatLng>(2)
    private lateinit var startMarker: Marker
    private lateinit var endMarker: Marker

    private lateinit var mPolylines: ArrayList<Polyline>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_route_map)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.listView.layoutManager = layoutManager
        mBounds = LatLngBounds.Builder()
        viewModel = ViewModelProviders.of(this).get(RouteMapViewModel::class.java)


        mMarkerList = ArrayList()
        setEvent()
        viewModel.getPolyline().observe(this, Observer { it ->
            mPolylines = it!!
            binding.searchShortestRoutBtn.visibility = View.GONE
        })
    }


    private fun setEvent() {
        // 시작 마커 생성
        binding.addMarkerBtn.setOnClickListener {
            Log.d("addMarkerBtn")
            val latLng = mMap.cameraPosition.target
//            binding.clearMarkerBtn.visibility = View.VISIBLE
//            binding.searchShortestRoutBtn.visibility = View.VISIBLE
            val position = MarkerOptions().position(latLng)
            val iconFactory = IconGenerator(this)
            iconFactory.setStyle(IconGenerator.STYLE_DEIVIRY)
            if (startEndPoints.size == 0) {
                startEndPoints.add(latLng)
                position.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("시작점"))).anchor(iconFactory.anchorU, iconFactory.anchorV)
                startMarker = mMap.addMarker(position)
                binding.addMarkerBtn.text = "종료점 생성"

            } else {
                if (startEndPoints.size == 1) {
                    startEndPoints.add(latLng)
                    position.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("종료좀"))).anchor(iconFactory.anchorU, iconFactory.anchorV)
                    endMarker = mMap.addMarker(position)
                }
            }
        }

        // 최적경로 검색 시작
        binding.searchShortestRoutBtn.setOnClickListener { viewModel.searchShortestRout(mMarkerList, startEndPoints, mMap) }

        // 시작 종료 마컷 삭제
        binding.clearMarkerBtn.setOnClickListener { clearStartEndMarker() }

        viewModel.getShippingItemList().observe(this, Observer { it ->
            shippingListData = it!!

            binding.listView.adapter = ShippingItemAdapter(shippingListData, this)
        })
    }

    private fun clearStartEndMarker() {
        binding.addMarkerBtn.text = "시작점 생성"
        startMarker.remove()
        endMarker.remove()
        startEndPoints.clear()

        if (!mPolylines.isEmpty()) {
            for (polyline in mPolylines) {
                Log.d("mRoutePolyline Not null")
                polyline.remove()
            }
            mPolylines.clear()
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        viewModel.getShippingData()
    }


    override fun onClick(view: View, data: MapShippingListData.ResultData.ShippingItem) {

        val geocoder = Geocoder(this)
        val fromLocationName = geocoder.getFromLocationName(data.INSADDR, 1)

        if (!fromLocationName.isEmpty()) {
            val position = setPosition(fromLocationName)
            val marker = setMarkerOption(data, position)


            val location = Location("")
            location.latitude = position.latitude
            location.longitude = position.longitude
            data.setLocation(location)
            val indexOf = shippingListData.indexOf(data)

            if (!mMarkerList.contains(data)) {

                mBounds.include(position)
                val mCameraUpdateFactory = CameraUpdateFactory.newLatLngBounds(mBounds.build(), 100)
                mMap.moveCamera(mCameraUpdateFactory)
                setBounceMarker(marker)
                mMarkerList.add(data)
                shippingListData[indexOf].isCardToggle = true
                data.isCardToggle = true
                view.background = ActivityCompat.getDrawable(this, R.drawable.shape_rect_map)

            } else {
                shippingListData[indexOf].isCardToggle = false
                mMarkerList.remove(data)
                addMarker(mMarkerList)
                data.isCardToggle = false
                view.background = ActivityCompat.getDrawable(this, R.drawable.shape_rect_white)
            }
            binding.listView.adapter.notifyItemChanged(indexOf, data)

        } else {
            Toast.makeText(this, "주소변경에 실패 했습니다.", Toast.LENGTH_LONG).show()

        }
    }

    private fun setMarkerOption(data: MapShippingListData.ResultData.ShippingItem, position: LatLng): Marker {
        val iconFactory = IconGenerator(this)
        val options = MarkerOptions()
        val customerInfo = setInfoMarker(data)

        setColorMarker(data, iconFactory)

        options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(customerInfo)))
                .position(position)
                .zIndex(1f)
                .anchor(iconFactory.anchorU, iconFactory.anchorV)
        val addMarker = mMap.addMarker(options)
        addMarker.tag = data
        return addMarker
    }

    private fun addMarker(data: ArrayList<MapShippingListData.ResultData.ShippingItem>) {
        mBounds = LatLngBounds.Builder()
        mMap.clear()

        for (shippingItem in data) {
            val latLng = LatLng(shippingItem.location.latitude, shippingItem.location.longitude)
            mBounds.include(latLng)
            val mCameraUpdateFactory = CameraUpdateFactory.newLatLngBounds(mBounds.build(), 100)
            mMap.moveCamera(mCameraUpdateFactory)
            setMarkerOption(shippingItem, latLng)
        }
    }

    private fun setPosition(fromLocationName: MutableList<Address>): LatLng {
        return LatLng(fromLocationName[0].latitude, fromLocationName[0].longitude)
    }


    private fun setBounceMarker(marker: Marker) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val duration: Long = 1500
        val interpolator = BounceInterpolator()
        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = Math.max(
                        1 - interpolator.getInterpolation(elapsed.toFloat() / duration), 0f)
                marker.setAnchor(0.5f, 1.0f + 2 * t)
                if (t > 0.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }


    private fun setColorMarker(shippingData: MapShippingListData.ResultData.ShippingItem, iconFactory: IconGenerator) {
        when {
            shippingData.SHIPPING_TYPE == ShippingType.D10.typeName -> iconFactory.setStyle(IconGenerator.STYLE_DEIVIRY)
            shippingData.SHIPPING_TYPE == ShippingType.D20.typeName -> iconFactory.setStyle(IconGenerator.STYLE_CHAGNGE)
            shippingData.SHIPPING_TYPE == ShippingType.D30.typeName -> iconFactory.setStyle(IconGenerator.STYLE_CANCEL)
            shippingData.SHIPPING_TYPE == ShippingType.P10.typeName -> iconFactory.setStyle(IconGenerator.STYLE_RELOCATION)
            shippingData.SHIPPING_TYPE == ShippingType.D90.typeName -> iconFactory.setStyle(IconGenerator.STYLE_AS)
            else -> iconFactory.setStyle(IconGenerator.STYLE_DEIVIRY)
        }
    }

    private fun setInfoMarker(shippingData: MapShippingListData.ResultData.ShippingItem): StringBuffer {
        val customerInfo = StringBuffer()
        if (shippingData.MUST_FLAG == "Y") {
            customerInfo.append("지정건\n")
        }
        if (!TextUtils.isEmpty(shippingData.PROMISE_TIME)) {
            customerInfo.append(SDF.hhmm_.format(SDF.hhmm.parseDate(shippingData.PROMISE_TIME))).append("\n")
        } else {
            customerInfo.append("미지정").append("\n")
        }
        customerInfo.append(shippingData.PRODUCT_NAME).append("\n")
        customerInfo.append(shippingData.CUST_NAME)
        return customerInfo
    }

}
