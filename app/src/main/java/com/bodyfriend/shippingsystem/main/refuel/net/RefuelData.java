package com.bodyfriend.shippingsystem.main.refuel.net;

import java.util.ArrayList;

/**
 * 주유 내역 등록
 */
public class RefuelData {
    public String message;
    public String code;
    public ArrayList<Record> records = new ArrayList<>();

    public class Record {
        public String date;
        public String name;
        public String carNumber;
        public float gasLiter;
        public int gasPrice;
        public int dashboard;
        public int dieselExhaustFluidLiter;
        public int dieselExhaustFluid;
        public int ectPrice;
        public String carChang;
        public String ownerCard;
    }
}
