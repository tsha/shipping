package com.bodyfriend.shippingsystem.main.shipping_list_back.detail;

import android.os.Bundle;
import android.os.PersistableBundle;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingMasterDetailActivity;

/**
 * Created by 이주영 on 2016-08-05.
 */
public class ShippingBackMasterDetailActivity extends ShippingMasterDetailActivity {
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.shipping_back_master_detail2);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        setText(R.id.title, "회수내역");
    }
}
