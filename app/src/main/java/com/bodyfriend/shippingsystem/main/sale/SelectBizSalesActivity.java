package com.bodyfriend.shippingsystem.main.sale;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;


import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.common.BaseActivity;
import com.bodyfriend.shippingsystem.databinding.ActivitySelectBizSalesBinding;
import com.bodyfriend.shippingsystem.main.login.Auth;

import java.util.Objects;

public class SelectBizSalesActivity extends BaseActivity {

    private ActivitySelectBizSalesBinding mBinding;
    private SelectBizSalesViewModel viewModel;
    private SaleProductAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_select_biz_sales);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_biz_sales);

        viewModel = ViewModelProviders.of(this).get(SelectBizSalesViewModel.class);
//        mBinding.setModel = viewModel
        viewModel.getBizSaleList(Auth.getId());

        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mBinding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        setupRecyclerView();
        viewModel.getSalesData().observe(this, lists -> {
            mBinding.swipeLayout.setRefreshing(false);
            mBinding.loadingProgressBar.setVisibility(View.GONE);
            mAdapter.setBenefitDataList(lists);

            if (lists == null || lists.isEmpty()) {
                mBinding.emptyText.setVisibility(View.VISIBLE);
            }

        });
        mBinding.swipeLayout.setOnRefreshListener(() -> {
            mBinding.loadingProgressBar.setVisibility(View.VISIBLE);
            viewModel.getBizSaleList(Auth.getId());
        });
    }

    private void setupRecyclerView() {
        mAdapter = new SaleProductAdapter();
        mBinding.recyclerView.setAdapter(mAdapter);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this, R.drawable.divider)));
        mBinding.recyclerView.addItemDecoration(itemDecorator);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

}
