package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoiData(@SerializedName("resultData")
                   @Expose
                   var resultData: List<ResultDatum>) {


    data class ResultDatum(@SerializedName("PLANT_CD")
                           @Expose
                           var plantCd: String? = null
                           , @SerializedName("MAKER_LOT_NO")
                           @Expose
                           var makerLotNo: String? = null
                           , @SerializedName("ISSUE_NO")
                           @Expose
                           var issueNo: String
                           , @SerializedName("TO_SL_NM")
                           @Expose
                           var toslNm: String? = null
                           , @SerializedName("GI_QTY")
                           @Expose
                           var giQty: Int? = null
                           , @SerializedName("TO_SL_CD")
                           @Expose
                           var toSlCd: String? = null
                           , @SerializedName("ORDER_NO")
                           @Expose
                           var orderNo: String? = null
                           , @SerializedName("REQ_DT")
                           @Expose
                           var reqDt: String? = null
                           , @SerializedName("SERIAL_CD")
                           @Expose
                           var serialCd: String? = null
                           , @SerializedName("MOV_TYPE_NM")
                           @Expose
                           var movTypeNm: String? = null
                           , @SerializedName("MOV_TYPE")
                           @Expose
                           var movType: String? = null
                           , @SerializedName("CONTRACT_NO")
                           @Expose
                           var contractNo: String? = null
                           , @SerializedName("GIFT_YN")
                           @Expose
                           var giftYn: String? = null
                           , @SerializedName("ITEM_CD")
                           @Expose
                           var itemCd: String? = null
                           , @SerializedName("ITEM_NM")
                           @Expose
                           var itemNm: String? = null

                           , @SerializedName("FROM_SL_NM")
                           @Expose
                           var fromSlNm: String? = null

                           , @SerializedName("FROM_SL_CD")
                           @Expose
                           var fromSlCd: String? = null

    )
}