
package com.bodyfriend.shippingsystem.main.map.tmap;

import java.util.HashMap;
import java.util.Map;

public class Properties_ {

    private String index;
    private String viaPointId;
    private String viaPointName;
    private String arriveTime;
    private String completeTime;
    private String distance;
    private String deliveryTime;
    private String waitTime;
    private String pointType;
    private String viaDetailAddress;
    private String groupKey;
    private String time;
    private String fare;
    private String poiId;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getViaPointId() {
        return viaPointId;
    }

    public void setViaPointId(String viaPointId) {
        this.viaPointId = viaPointId;
    }

    public String getViaPointName() {
        return viaPointName;
    }

    public void setViaPointName(String viaPointName) {
        this.viaPointName = viaPointName;
    }

    public String getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(String arriveTime) {
        this.arriveTime = arriveTime;
    }

    public String getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(String completeTime) {
        this.completeTime = completeTime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(String waitTime) {
        this.waitTime = waitTime;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getViaDetailAddress() {
        return viaDetailAddress;
    }

    public void setViaDetailAddress(String viaDetailAddress) {
        this.viaDetailAddress = viaDetailAddress;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
