package com.bodyfriend.shippingsystem.main.shipping_list_finish;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.common.BaseFragment;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.MapActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListArea;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListPromise;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeShippingType;
import com.bodyfriend.shippingsystem.main.shipping_list_finish.detail.ShippingDoneMasterDetailActivity;
import com.bodyfriend.shippingsystem.main.shipping_list_finish.net.ShippingDoneList;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

/**
 * Created by 이주영 on 2016-08-01.
 */
public class ShippingFinishListFragment extends BFFragment2 {
    protected MainActivity.MainActivityControll mainActivityControll;

    protected Calendar mStartCalendar = Calendar.getInstance();
    protected Calendar mEndCalendar = Calendar.getInstance();

    private View mSearch;
    private View mFragmentView;

    public static class EXTRA {
        public static final String MODE = "MODE";
        public static final int MODE_THIS_MONTH = 1;
        public static final int MODE_TODAY = 2;
    }

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    public void setMainActivityControll() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("ShippingFinishListFragment onCreateView");
        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 3);
        mFragmentView = inflater.inflate(R.layout.fragment_shpping_finish, container, false);
        return mFragmentView;
    }

    @Override
    protected void onParseExtra() {
        super.onParseExtra();

        Bundle bundle = this.getArguments();
        int mode = bundle.getInt(EXTRA.MODE, 0);
        Calendar currentCalendar = Calendar.getInstance();
        switch (mode) {
            case EXTRA.MODE_TODAY:
                mStartCalendar = currentCalendar;
                mEndCalendar = currentCalendar;
                break;
            case EXTRA.MODE_THIS_MONTH:
                mStartCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), 1);
                mEndCalendar = currentCalendar;
                break;

        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setSwipeRefresh();
        if (mainActivityControll == null) {
            mainActivityControll = ((MainActivity) getActivity()).getMainActivityControll();
            mainActivityControll.changeTitle(R.layout.title_finsh_layout);

        } else {
            mainActivityControll.changeTitle(R.layout.title_finsh_layout);
        }

        String[] arrSpinner = {"설치일", "분배일", "접수일", "지정일"};
        setSpinner(R.id.s_datetype, arrSpinner);

        setOnClickListener(R.id.dateStart, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateStart), SDF.mmdd__.format, mStartCalendar));
        setOnClickListener(R.id.dateEnd, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar));
        TextView dateStart = (TextView) findViewById(R.id.dateStart);
        dateStart.addTextChangedListener(watcherDateStart);
        TextView dateEnd = (TextView) findViewById(R.id.dateEnd);
        dateEnd.addTextChangedListener(watcherDateEnd);

        setOnClickListener(R.id.inquiry, onInquiryClickListener);

        mList = (ListView) findViewById(R.id.list);
        mList.setEmptyView(findViewById(R.id.emptyView));

        mSearch = findViewById(R.id.search_layout);
    }

    @Override
    protected void onUpdateUI() {
        super.onUpdateUI();

        setText(R.id.dateStart, SDF.mmdd__.format(mStartCalendar.getTime()));
        setText(R.id.dateEnd, SDF.mmdd__.format(mEndCalendar.getTime()));

    }

    @Override
    protected void onLoad() {
        super.onLoad();

        if (mSearchCodeListArea == null) {
            Net.async(new searchCodeListArea()).setOnNetResponse(onAreaNetResponse);
        } else {
            reqShippingDivList();
        }

        if (mSearchCodeListPromise == null) {
            Net.async(new searchCodeListPromise()).setOnNetResponse(onPromiseCodeNetResponse);
        }

        if (mShippingType == null) {
            Net.async(new searchCodeShippingType()).setOnNetResponse(onShippingTypeCodeNetResponse);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        super.update(observable, data);

        if (OH.c() != observable)
            return;

        if (data instanceof OH.TYPE) {
            OH.TYPE type = (OH.TYPE) data;
            switch (type) {
                case SEARCH:
                    toggle();
                    break;
                default:
                    break;
            }
        }
    }

    private void toggle() {
        if (mSearch.getVisibility() == View.VISIBLE) {
            hideSearch();
        } else {
            showSearch();
        }
    }

    private void hideSearch() {
        if (mSearch != null)
            mSearch.setVisibility(View.GONE);
    }

    private void showSearch() {
        if (mSearch != null)
            mSearch.setVisibility(View.VISIBLE);
    }

    private TextWatcher watcherDateStart = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            setOnClickListener(R.id.dateEnd, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar, mStartCalendar.getTimeInMillis(), -1));
        }
    };

    private TextWatcher watcherDateEnd = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // setOnClickListener(R.id.dateStart, new OnDateClickListener((TextView) findViewById(R.id.dateStart), SDF.mmdd__.format, mStartCalendar));
            // setOnClickListener(R.id.dateEnd, new OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar));
        }
    };

    private View.OnClickListener onInquiryClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            hideKeyboard(v);
            reqShippingDivList();
        }
    };

    protected void reqShippingDivList() {

        Log.d("reqShippingDivList~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        final Spinner spinner = (Spinner) mFragmentView.findViewById(R.id.s_datetype);
        final int spinnerPosition = spinner.getSelectedItemPosition();
//        final int spinnerPosition = 0;
        int s_datetype = 0;
        switch (spinnerPosition) {
            case 0: // 설치
                s_datetype = 3;
                break;
            case 1: // 배정
                s_datetype = 0;
                break;
            case 2: // 접수
                s_datetype = 1;
                break;
            case 3: // 지정
                s_datetype = 2;
                break;

            default:
                break;
        }

        String dateStart = SDF.yyyymmdd_2.format(mStartCalendar.getTime());
        String dateEnd = SDF.yyyymmdd_2.format(mEndCalendar.getTime());
        Spinner mS_area = (Spinner) mFragmentView.findViewById(R.id.s_area);
        String selectedArea = (String) mS_area.getSelectedItem();
        String s_area = null;
        for (searchCodeListArea.Data.resultData d : mSearchCodeListArea.data.resultData) {
            if (d.DET_CD_NM.equals(selectedArea)) {
                s_area = d.DET_CD;
                break;
            }
        }
        String s_custname = ((TextView) mFragmentView.findViewById(R.id.s_custname)).getText().toString();
        String s_addr = ((TextView) mFragmentView.findViewById(R.id.s_addr)).getText().toString();
        Net.async(new ShippingDoneList(s_datetype, dateStart, dateEnd, s_area, s_custname, s_addr)).setOnNetResponse(onNetResponse);

    }

    protected searchCodeListArea mSearchCodeListArea;
    private Net.OnNetResponse<searchCodeListArea> onAreaNetResponse = new Net.OnNetResponse<searchCodeListArea>() {

        @Override
        public void onResponse(searchCodeListArea response) {
            mSearchCodeListArea = response;

            ArrayList<String> arrSpinner = new ArrayList<String>();
            arrSpinner.add("전체");
            for (searchCodeListArea.Data.resultData d : response.data.resultData) {
                arrSpinner.add(d.DET_CD_NM);
            }

//                setSpinner(R.id.s_area, arrSpinner);
            try {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, android.R.id.text1, arrSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                ((Spinner) (mFragmentView.findViewById(R.id.s_area))).setAdapter(adapter);
                reqShippingDivList();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    private searchCodeListPromise mSearchCodeListPromise;
    private Net.OnNetResponse<searchCodeListPromise> onPromiseCodeNetResponse = new Net.OnNetResponse<searchCodeListPromise>() {

        @Override
        public void onResponse(searchCodeListPromise response) {

            mSearchCodeListPromise = response;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    protected searchCodeShippingType mShippingType;
    protected Net.OnNetResponse<searchCodeShippingType> onShippingTypeCodeNetResponse = new Net.OnNetResponse<searchCodeShippingType>() {

        @Override
        public void onResponse(searchCodeShippingType response) {
            mShippingType = response;

//            if (mListAdapter != null)
//                mListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    protected ListView mList;

    protected Net.OnNetResponse<ShippingDoneList> onNetResponse = new Net.OnNetResponse<ShippingDoneList>() {

        @Override
        public void onResponse(ShippingDoneList response) {
            List<ShippingDoneList.Data.resultData.list> mData = response.data.resultData.list;


            HashMap<String, List<ShippingDoneList.Data.resultData.list>> hashMap = new HashMap<>();
            List<ShippingDoneList.Data.resultData.list> list = response.data.resultData.list;

            for (ShippingDoneList.Data.resultData.list item : list) { // 날짜별로 hashmap에 리스트를 나눠서 담는다.
                String due_date = item.DUE_DATE;
//                Log.d("due_date item.DUE_DATE : " + due_date);
                List<ShippingDoneList.Data.resultData.list> list2 = hashMap.get(due_date);
                if (list2 == null) {
                    list2 = new ArrayList<>();
                }
                list2.add(item);
                hashMap.put(due_date, list2);
            }
            String[] keySet = hashMap.keySet().toArray(new String[hashMap.keySet().size()]);
            Arrays.sort(keySet, Collections.reverseOrder());
            ArrayList<ShippingDoneList.Data.resultData.list> returnList = new ArrayList<>();
            for (String key : keySet) {
                List<ShippingDoneList.Data.resultData.list> list3 = hashMap.get(key);
                for (int i = 0; i < list3.size(); i++) {
                    list3.get(i).index = i + 1;
                }
                ShippingDoneList.Data.resultData.list item = new ShippingDoneList.Data.resultData.list();
                item.DUE_DATE = key;
                item.DUE_DATE_COUNT = String.format("  (%s건)", list3.size());
                item.isDate = true;

                list3.add(0, item);
                returnList.addAll(list3);
            }


            ShippingListAdapter listAdapter = new ShippingListAdapter(mContext, returnList);
            mList.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();

            if (!returnList.isEmpty()) {
                hideSearch();
            } else {
                showSearch();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    public class ShippingListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<ShippingDoneList.Data.resultData.list> mItemList;

        public ShippingListAdapter(Context context, List<ShippingDoneList.Data.resultData.list> list) {
            this.mItemList = (ArrayList<ShippingDoneList.Data.resultData.list>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_finish_shipping, parent, false);

                viewHolder = new ViewHolder();

                viewHolder.cust_name = (TextView) convertView.findViewById(R.id.cust_name);
                viewHolder.product_name = (TextView) convertView.findViewById(R.id.product_name);
                viewHolder.insaddr = (TextView) convertView.findViewById(R.id.insaddr);
                viewHolder.install = (TextView) convertView.findViewById(R.id.install);
                viewHolder.product_img = (ImageView) convertView.findViewById(R.id.product_img);
                viewHolder.phone = (ImageView) convertView.findViewById(R.id.phone);
                viewHolder.satisfactionRating = (ImageView) convertView.findViewById(R.id.satisfactionRating);

                convertView.setTag(viewHolder);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final ShippingDoneList.Data.resultData.list item = mItemList.get(position);

            if (item.isDate) {
                convertView.findViewById(R.id.dateLayout).setVisibility(View.VISIBLE);
                convertView.findViewById(R.id.item_finish_shipping_content).setVisibility(View.GONE);
                Log.d("item.DUE_DATE: : " + item.DUE_DATE);

                TextView date = (TextView) convertView.findViewById(R.id.date);
                date.setText(item.DUE_DATE);

                TextView count = (TextView) convertView.findViewById(R.id.count);
                count.setText(item.DUE_DATE_COUNT);

            } else {
                convertView.findViewById(R.id.dateLayout).setVisibility(View.GONE);
                convertView.findViewById(R.id.item_finish_shipping_content).setVisibility(View.VISIBLE);

                viewHolder.product_name.setText(item.PRODUCT_NAME);
                viewHolder.insaddr.setText(item.INSADDR);
                if (item.EVAL.equals("Y")) {
                    viewHolder.satisfactionRating.setImageResource(R.drawable.ic_satisfaction_rating_p);
                } else {
                    viewHolder.satisfactionRating.setImageResource(R.drawable.ic_satisfaction_rating_n);
                }
                viewHolder.cust_name.setText(item.CUST_NAME);

                if (item.PROGRESS_NO_NM.equals("회수")) {
                    viewHolder.cust_name.setText(item.AGREE_NAME);
                    viewHolder.insaddr.setText(item.ADDR);
                    viewHolder.phone.setOnClickListener(v -> {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", item.PHONE_NO2)));
                        v.getContext().startActivity(intent);
                    });
                } else if (item.PROGRESS_NO_NM.equals("설치")) {
                    viewHolder.cust_name.setText(item.CUST_NAME);
                    viewHolder.insaddr.setText(item.ADDR2);
                    viewHolder.phone.setOnClickListener(v -> {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", item.PHONE_NO)));
                        v.getContext().startActivity(intent);
                    });
                }


                if (!TextUtils.isEmpty(item.PHONE_NO)) {
                    viewHolder.phone.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.phone.setVisibility(View.GONE);
                }

                String str = "";
                if (!TextUtils.isEmpty(item.SHIPPING_TYPE)) {
                    str = item.SHIPPING_TYPE + "완료";
                } else {
                    str = "설치완료";
                }

            /*    if (item.PROGRESS_NO_NM.equals("회수")) { // 회수
                    str = "회수완료";
                } else if (item.SHIPPING_TYPE.contains("맞교체")) { // 설치완료
                    str = "맞교체완료";
                } else if (item.SHIPPING_TYPE.contains("재설치")) { // 설치완료
                    str = "재설치완료";
                } else if (item.SHIPPING_TYPE.contains("이전요청")) { // 설치완료
                    str = "이전요청완료";
                } else if ("ZZ".equals(item.INS_COMPLETE)) { // 설치완료
                    str = "설치완료";
                } else if ("F2".equals(item.INS_COMPLETE)) { // 초도불량
                    str = "초도불량";
                } else if ("X1".equals(item.INS_COMPLETE)) { // 설치불가
                    str = "설치불가";
                } else if ("X2".equals(item.INS_COMPLETE)) { // 수취거부
                    str = "수취거부";
                } else if ("X3".equals(item.INS_COMPLETE)) { // 취소
                    str = "취소";
                }*/

                String format = "";
                try {
                    format = SDF.mmddE__.format(SDF.yyyymmdd_1.parse(item.DUE_DATE));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SpannableString ss = new SpannableString(String.format("%s %s", format, str));
                ss.setSpan(new ForegroundColorSpan(Color.parseColor("#244766")), 0, item.DUE_DATE.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(Color.parseColor("#80a399")), item.DUE_DATE.length() + 1, ss.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                viewHolder.install.setText(ss, TextView.BufferType.SPANNABLE);


                String pName = item.PRODUCT_NAME;

                int resId = 0;
                if (pName.contains("로보")) {
                    resId = R.drawable.chair_irobo;
                } else if (pName.contains("팬텀")) {
                    if (pName.contains("블랙")) {
                        resId = R.drawable.chair_pantom_black;
                    } else if (pName.contains("브라운")) {
                        resId = R.drawable.chair_pantom_brown;
                    } else if (pName.contains("레드")) {
                        resId = R.drawable.chair_pantom_red;
                    } else {
                        resId = R.drawable.chair_pantom_black;
                    }
                } else if (pName.contains("파라오")) {
                    resId = R.drawable.chair_parao;
                    if (pName.contains("S")) {
                        resId = R.drawable.chair_parao_s;
                    }
                } else if (pName.contains("프레지던트")) {
                    resId = R.drawable.chair_president;
                    if (pName.contains("플러스")) {
                        resId = R.drawable.chair_president_plus;
                    }
                } else if (pName.contains("렉스")) {
                    resId = R.drawable.chair_rexl;
                    if (pName.contains("블랙")) {
                        resId = R.drawable.chair_rexl_black;
                    }
                } else if (pName.contains("레지나")) {
                    resId = R.drawable.chair_regina;
                } else if (pName.contains("프레임")) {
                    if (pName.contains("아이보리")) {
                        resId = R.drawable.latex_white_frame;
                    } else if (pName.contains("플로팅")) {
                        resId = R.drawable.latex_floating_frame;
                    } else if (pName.contains("초코")) {
                        resId = R.drawable.latex_black_frame;
                    }
                } else if (pName.contains("공통")) {
                    resId = R.drawable.latex_mat;
                }

                if (resId == 0) {
                    viewHolder.product_img.setImageResource(android.R.color.transparent);
                } else {
                    viewHolder.product_img.setImageResource(resId);
                }

                convertView.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, ShippingDoneMasterDetailActivity.class);
                    intent.putExtra("SHIPPING_SEQ", item.SHIPPING_SEQ);
                    startActivity(intent);
                });
            }
            return convertView;
        }

        private void geoCode(String addr) {
            Intent intent = new Intent(mContext, MapActivity.class);
            intent.putExtra("addr", addr);
            startActivity(intent);
        }

        public class ViewHolder {
            public TextView cust_name;
            public TextView product_name; //
            public TextView insaddr; //
            public TextView install;
            public ImageView product_img;
            public ImageView phone;
            public ImageView satisfactionRating;
        }
    }

}