package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.WarehouseFragmentBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.WarehouseAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseListData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.WarehouseViewModel


class WarehouseNameListDialog : AppCompatDialogFragment() {
    private lateinit var mBinding: WarehouseFragmentBinding
    private var content: String? = null
    private lateinit var warehouseAdapter: WarehouseAdapter

    companion object {
        const val EXTRA_GROUP_CD = "groupCd"
        fun newInstance(type: String): WarehouseNameListDialog {
            val f = WarehouseNameListDialog()
            // Supply num input as an argument.
            val args = Bundle()
            args.putString(EXTRA_GROUP_CD, type)
            f.arguments = args
            return f
        }
    }


    interface WarehouseDialogInterface {
        fun selectItem(warehouseData: WarehouseListData.ResultData)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        content = arguments?.getString(EXTRA_GROUP_CD)
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.warehouse_fragment, null, false)
        val viewModel = ViewModelProviders.of(this).get(WarehouseViewModel::class.java)
        content?.let { viewModel.getWarehouseList(it) }
        mBinding.data = viewModel
        getObserver(viewModel)
        warehouseAdapter = WarehouseAdapter(itemClick = {

            (activity as (WarehouseDialogInterface)).selectItem(it)
            dismiss()
        })


        val mRecentLayoutManager = LinearLayoutManager(activity)
        mBinding.listWarehouse.apply {
            layoutManager = mRecentLayoutManager as RecyclerView.LayoutManager?
            setHasFixedSize(true)
            adapter = warehouseAdapter

        }
        val alert = AlertDialog.Builder(activity)
        alert.apply {
            setView(mBinding.root)
        }

        return alert.create()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val style = STYLE_NO_FRAME
        setStyle(style, theme)

    }

    private fun getObserver(viewModel: WarehouseViewModel) {
        viewModel.warehouseList.observe(this, Observer {
            if (it != null) {
                warehouseAdapter.updateMealList(it)
            }
        })

        viewModel.isSessionOut.observe(this, Observer {
            it.let {
                if (it!!) {
                    dismiss()
                }
            }

        })
    }
}
