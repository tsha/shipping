package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 배송구분 코드리스트
 * 
 */
public class searchCodeListDeliveryGb extends searchCodeList {

	public searchCodeListDeliveryGb() {
		super();
		setParam("code", 9100);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public String resultMsg;
		public int resultCode;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}

	
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):     "ResultData": [
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         {
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "GROUP_CD": "SP",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_DT": 1431430338987,
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_ID": "shipadmin",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD": "D10",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DESCRIPTION": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "SORT_SEQ": "1",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD_NM": "배송",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_2": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_3": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD_NM": "배송구분",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_1": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD": "9100",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "UPD_DT": 1429002223063
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         },
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         {
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "GROUP_CD": "SP",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_DT": 1431430338993,
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_ID": "shipadmin",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD": "D20",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DESCRIPTION": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "SORT_SEQ": "2",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD_NM": "맞교체",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_2": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_3": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD_NM": "배송구분",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_1": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD": "9100",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "UPD_DT": 1429002181230
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         },
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         {
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "GROUP_CD": "SP",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_DT": 1431430339003,
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_ID": "shipadmin",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD": "D30",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DESCRIPTION": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "SORT_SEQ": "3",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD_NM": "계약철회",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_2": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_3": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD_NM": "배송구분",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_1": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD": "9100",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "UPD_DT": 1429002204213
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         },
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):         {
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "GROUP_CD": "SP",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_DT": 1431430339010,
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "INS_ID": "shipadmin",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD": "P10",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DESCRIPTION": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "SORT_SEQ": "4",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "DET_CD_NM": "이전요청",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_2": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_3": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD_NM": "배송구분",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "REF_1": "",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "COMM_CD": "9100",
//	05-29 15:49:44.691: W/_NETLOG_IN(19938):             "UPD_DT": 1429002215557

}
