package com.bodyfriend.shippingsystem.main.delivery_order.net;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Taeseok on 2018-03-05.
 */

public interface WarehouseApi {


    @FormUrlEncoded
    @POST("linkTms/selectOutLoiList.do")
    Observable<OutLoiListData> getOutLoiList(
            @Field("deliveryManId") String admin_id
    );


    @FormUrlEncoded
    @POST("mobile/api/wms/selectOutLoiList.json")
    Observable<OutLoiListData> selectOutLoiList(
            @Field("fromDate") String fromDate
    );

    @FormUrlEncoded
    @POST("linkTms/selectItemList.do")
    Observable<ProductItemListData> getItemList(
            @Field("clsCd") String clsCd,
            @Field("itemNmKr") String itemNmKr,
            @Field("itemNmEn") String itemNmEn
    );

    @FormUrlEncoded
    @POST("mobile/api/wms/selectGiftStockList.json")
    Observable<DeliverManStockData> selectItemList(
            @Field("slCd") String slCd
    );


    @POST("mobile/api/wms/selectItemStockList.json")
    Observable<DeliverManStockData> selectItemList();

    @FormUrlEncoded
    @POST("mobile/api/wms/selectItemStockList.json")
    Observable<DeliverManStockData> selectItemStockList(
            @Field("itemGroup") String itemGroup
            , @Field("slCd") String slCd
    );


    @Multipart
    @POST("mobile/api/wms/createOutLoi.json")
    Observable<DeliveryResponseData> addReserveInsData(
            @Part("jsonData") RequestBody list
    );

    @FormUrlEncoded
    @POST("mobile/api/wms/delOutLoi.json")
    Observable<BfData> delExportLoi(
            @Field("issueNo") String loiNo
    );

    @FormUrlEncoded
    @POST("mobile/api/wms/delOutLoi.json")
    Observable<BfData> delExportLoi(
            @Field("issueNo") String issueNo
            , @Field("ifNo") int loiNo
    );


    @POST("mobile/api/wms/selectWarehouseList.json")
    Observable<WarehouseListData> selectWarehouseList();

    @FormUrlEncoded
    @POST("mobile/api/wms/selectWarehouseList.json")
    Observable<WarehouseListData> selectWarehouseList(
            @Field("groupCd") String groupCd
    );

    @FormUrlEncoded
    @POST("mobile/api/wms/selectItemList.json")
    Observable<ReturnProductData> selectReturnItemList(
            @Field("itemGroup") String itemGroup);

    @FormUrlEncoded
    @POST("mobile/api/wms/selectItemGroupList.json")
    Observable<ItemGroupData> selectItemGroupList(
            @Field("itemGroup") String itemGroup
            , @Field("itemDepth") String itemDepth
    );


    @Multipart
    @POST("linkTms/addExportLoi.do")
    Observable<DeliveryResponseData> addReserveInsData(
            @Query("loiGioTp") String loiGioTp
            , @Query("procSystCd") String procSystCd
            , @Query("whsCd") String whsCd
            , @Query("isMove") boolean isMove
            , @Query("ownrHrId") String ownrHrId
            , @Query("ownrHrNm") String ownrHrNm
            , @Query("vhclNo") String vhclNo
            , @Query("rmrk") String rmrk
            , @Part("listData") RequestBody list
    );


    @FormUrlEncoded
    @POST("mobile/api/wms/createOutLoi.json")
    Observable<BfData> createOutLoi(
            @Field("jsonData") String jsonData
    );


    @FormUrlEncoded
    @POST("mobile/api/wms/checkProductSerialValid.json")
    Observable<ProductSerialValidData> checkProductSerialValid(
            @Field("serialCd") String serialCd
            , @Field("giftYn") String giftFlag
            , @Field("itemType") String itemType
    );
    @FormUrlEncoded
    @POST("mobile/api/wms/checkProductSerialType.json")
    Observable<BfData> checkProductSerialType(
            @Field("serialCd") String serialCd
            , @Field("itemCd") String itemCd
    );

    @FormUrlEncoded
    @POST("mobile/api/wms/checkProductSerialValid.json")
    Observable<ProductSerialValidData> checkProductSerialValid2(
            @Field("serialCd") String serialCd
    );


    @FormUrlEncoded
    @POST("mobile/api/wms/checkProductSerialValid.json")
    Observable<ProductSerialValidData> checkProductSerialValid(
            @Field("serialCd") String serialCd
            , @Field("giftYn") String giftFlag

    );


    @FormUrlEncoded
    @POST("mobile/api/wms/checkProductSerialValid.json")
    Observable<ProductSerialData> checkProductSerialValid(
            @Field("serialCd") String serialCd
    );


    @FormUrlEncoded
    @POST("mobile/api/wms/createInLoi.json")
    Observable<BfData> createInLoi(
            @Field("jsonData") String jsonData
    );


    //입고요청 리스트(인수확인목록)
    @POST("mobile/api/wms/selectInLoiReceiveList.json")
    Observable<LoiData> selectInLoiReceiveList(

    );

    //입고요청 리스트(인계확인목록)
    @POST("mobile/api/wms/selectInLoiList.json")
    Observable<LoiData> selectInLoiList();


    //기사 재고 리스트
    @POST("mobile/api/wms/selectDelivManStockList.json")
    Observable<DeliverManStockData> selectDeliveryManStockList();

    @FormUrlEncoded
    @POST("mobile/api/wms/confirmTransferReq.json")
    Observable<BfData> confirmTransferReq(
            @Field("issueNo") String serialCd
    );


    @POST("mobile/api/wms/selectGiftList.json")
    Observable<GiftListData> selectGiftList();

    @FormUrlEncoded
    @POST("mobile/api/wms/createBarcode.json")
    Observable<CreatedBarcodeData> createBarcode(
            @Field("itemCd") String itemCd
    );


}
