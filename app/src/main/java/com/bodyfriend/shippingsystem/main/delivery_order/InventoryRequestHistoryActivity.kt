package com.bodyfriend.shippingsystem.main.delivery_order


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ActivityInventoryRequestHistoryBinding
import com.bodyfriend.shippingsystem.databinding.FragmentInventoryRequestHistoryBinding
import com.bodyfriend.shippingsystem.databinding.FragmentInventoryResponseHistoryBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.HistoryRequestLoiListAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.HistoryResponseLoiListAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.InventoryRequestHistoryViewMode
import kotlinx.android.synthetic.main.activity_inventory_request_history.*

class InventoryRequestHistoryActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityInventoryRequestHistoryBinding
    private lateinit var viewModel: InventoryRequestHistoryViewMode
    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v13.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_inventory_request_history)
        viewModel = ViewModelProviders.of(this).get(InventoryRequestHistoryViewMode::class.java)

        setSupportActionBar(mBinding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mBinding.toolbar.setNavigationOnClickListener { v -> onBackPressed() }


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter
        mBinding.layoutTab.setupWithViewPager(mBinding.container)
        getObserver(viewModel)
    }

    private fun getObserver(viewModel: InventoryRequestHistoryViewMode) {

    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: android.support.v4.app.FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): android.support.v4.app.Fragment? {
            // getItem is called to instantiate the fragment for the given page.
            // Return a LoiRequestListFragment (defined as a static inner class below).
            return when (position) {
                0 -> LoiRequestListFragment()
                1 -> LoiResponseFragment()
                else -> LoiRequestListFragment()
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return resources.getString(R.string.request_inventory)
                1 -> return resources.getString(R.string.respons_inventory)
            }
            return null
        }


    }


    class LoiRequestListFragment : android.support.v4.app.Fragment() {

        private lateinit var mBinding: FragmentInventoryRequestHistoryBinding
        lateinit var historyLoiAdapter: HistoryRequestLoiListAdapter
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {

            mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_inventory_request_history, null, false)
            val viewModel = ViewModelProviders.of(this).get(InventoryRequestHistoryViewMode::class.java)
            viewModel.getInInLoiList()
            getObserver(viewModel)

            historyLoiAdapter = HistoryRequestLoiListAdapter(itemClick = {
                it.issueNo?.let { it1 -> viewModel.delExportLoi(it1) }
            })

            val mRecentLayoutManager = LinearLayoutManager(context)
            mBinding.listViewRequestLoi.apply {
                layoutManager = mRecentLayoutManager
                setHasFixedSize(true)
                adapter = historyLoiAdapter
            }


            return mBinding.root
        }

        fun getObserver(viewModel: InventoryRequestHistoryViewMode) {
            viewModel.loiGetList.observe(this, Observer { it ->
                println("loiGetList~~~")

                it?.let {
                    historyLoiAdapter.updateMealList(it)
                }
            })
            viewModel.isClear.observe(this, Observer {
                historyLoiAdapter.clear()
            })


        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    class LoiResponseFragment : android.support.v4.app.Fragment() {

        private lateinit var mBinding: FragmentInventoryResponseHistoryBinding
        lateinit var historyResponseLoiAdapter: HistoryResponseLoiListAdapter

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {

            mBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_inventory_response_history, null, false)
            val viewModel = ViewModelProviders.of(this).get(InventoryRequestHistoryViewMode::class.java)
            viewModel.getInLoiReceiveList()
            getObserver(viewModel)


            historyResponseLoiAdapter = HistoryResponseLoiListAdapter(itemClick = {
                it.issueNo?.let { it1 -> viewModel.setConfirmTransferReq(it1) }
            })


            val mRecentLayoutManager = LinearLayoutManager(context)
            mBinding.listViewResponseLoi.apply {
                layoutManager = mRecentLayoutManager
                setHasFixedSize(true)
                adapter = historyResponseLoiAdapter
            }


            return mBinding.root
        }

        fun getObserver(viewModel: InventoryRequestHistoryViewMode) {
            viewModel.loiResponseList.observe(this, Observer { it ->

                it?.let {
                    //                    historyResponseLoiAdapter.clearList()
                    historyResponseLoiAdapter.updateMealList(it)
                }

            })
            viewModel.errorResult.observe(this, Observer {
                Toast.makeText(activity, it, Toast.LENGTH_LONG);
            })

            viewModel.isResponseClear.observe(this, Observer {
                historyResponseLoiAdapter.clear()
            })

        }


    }
}
