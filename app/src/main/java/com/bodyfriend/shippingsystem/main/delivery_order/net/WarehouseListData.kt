package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WarehouseListData(@SerializedName("resultData")
                             @Expose
                             var resultDataList: List<ResultData>) {


    data class ResultData(@SerializedName("PLANT_CD")
                     var plantCd: String? = null,
                     @SerializedName("SL_CD")
                     var slCd: String? = null,
                     @SerializedName("SL_NM")
                     var slNm: String? = null,
                     @SerializedName("RENTAL_NM")
                     var rentalNm: String? = null)
}
