package com.bodyfriend.shippingsystem.main.detail.ui.fragment

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.bodyfriend.shippingsystem.R
import java.util.*


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager)
    : FragmentPagerAdapter(fm) {
    private val mList: ArrayList<Fragment> = ArrayList()
    private val TAB_TITLES = arrayOf(
            R.string.tab_text_1,
            R.string.tab_text_2
    )

    override fun getItem(position: Int): Fragment {
        println("position : $position")
            return mList[position]

    }

    fun addFragment(f: Fragment) {
        mList.add(f)
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 2
    }
}