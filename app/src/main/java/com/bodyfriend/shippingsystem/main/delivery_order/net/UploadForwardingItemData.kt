package com.bodyfriend.shippingsystem.main.delivery_order.net

data class UploadForwardingItemData(var itemName: String?, var item: String?, var qty: String) {
    var giftYn: String? = null
    var invYn: String? = null
}
