package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.DialogProducetBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.ProductListAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.ForwardingOrderViewModel
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingMasterDetailActivity

/** 창고 재고 및 외주기사 창고  **/
class WarehouseStockListDialog : AppCompatDialogFragment() {
    private lateinit var mBinding: DialogProducetBinding
    private lateinit var _productAdapter: ProductListAdapter

    companion object {
        const val EXTRA_SL_CD = "EXTRA_SL_CD"
        const val EXTRA_ITEM_GROUP = "EXTRA_ITEM_GROUP"
        const val EXTRA_OUT_DELIVER: String = "EXTRA_OUT_DELIVER"
        const val EXTRA_IS_GIFT: String = "EXTRA_GIFT_INDEX"


        fun newInstance(slCd: String, itemGroup: String?): WarehouseStockListDialog {
            val f = WarehouseStockListDialog()
            val args = Bundle()
            args.putString(slCd, itemGroup)
            args.putString(EXTRA_SL_CD, slCd)
            args.putString(EXTRA_ITEM_GROUP, itemGroup)
            f.arguments = args
            return f
        }

        @JvmStatic
        fun newInstance(giftIndex: Int): WarehouseStockListDialog {
            val f = WarehouseStockListDialog()
            val args = Bundle()

            args.putInt(EXTRA_OUT_DELIVER, giftIndex)

            f.arguments = args
            return f
        }

        @JvmStatic
        fun newInstance(isGift: Boolean, callbackCode: Int): WarehouseStockListDialog {
            val f = WarehouseStockListDialog()
            val args = Bundle()

            args.putBoolean(EXTRA_IS_GIFT, isGift)
            args.putInt(EXTRA_OUT_DELIVER, callbackCode)
            f.arguments = args
            return f
        }
    }


    interface ProductDialogInterface {
        fun warehouseProductSelect(productData: DeliverManStockData.ResultDatum)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val slCd = arguments?.getString(EXTRA_SL_CD)
        val itemGroup = arguments?.getString(EXTRA_ITEM_GROUP)
        val outDeliver = arguments?.getInt(EXTRA_OUT_DELIVER)
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_producet, null, false)
        val viewModel = ViewModelProviders.of(this).get(ForwardingOrderViewModel::class.java)
        when {
            itemGroup != null -> slCd?.let { viewModel.getItemStockList(itemGroup, it) }
            outDeliver != 0 -> {
                viewModel.getItemStockList()
            }
            else -> slCd?.let { viewModel.getItemStockList(null, it) }
        }
        mBinding.data = viewModel
        getObserver(viewModel)
        _productAdapter = ProductListAdapter(itemClick = { it2 ->


            when (activity) {
                is ForwardingOrderActivity -> {
                    (activity as ForwardingOrderActivity).warehouseProductSelect(it2)
                }

                is ShippingMasterDetailActivity -> {
//                    it2.lotFlg = "*"
//                    it2.serialCd = "*"
                    it2.isValidCheck = false
                    it2.isValid = false
                    arguments?.getInt(EXTRA_OUT_DELIVER)?.let { it1 -> (activity as ShippingMasterDetailActivity).selectItem(it2, it1) }
                }
            }

            dismiss()
        })


        val mRecentLayoutManager = LinearLayoutManager(activity)
        mBinding.listWarehouse.apply {
            layoutManager = mRecentLayoutManager
            setHasFixedSize(true)
            adapter = _productAdapter
            addItemDecoration(DividerItemDecoration(activity, 1))
        }

        val alert = AlertDialog.Builder(activity)
        alert.apply {
            setView(mBinding.root)
        }

        return alert.create()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val style = STYLE_NO_FRAME
        setStyle(style, theme)

    }

    private fun getObserver(viewModel: ForwardingOrderViewModel) {
        viewModel.productData.observe(this, Observer { it1 ->
            if (it1 != null) {
                arguments?.getBoolean(EXTRA_IS_GIFT)?.let { it ->
                    val filterList = ArrayList<DeliverManStockData.ResultDatum>()
                    if (it) {
                        it1.forEach {
                            if (it.giftYn == "S" || it.giftYn == "Y") {
                                filterList.add(it)
                            }
                        }
                        _productAdapter.updateMealList(filterList)
                    } else {
                        it1.forEach {
                            if (it.giftYn != "Y") {
                                filterList.add(it)
                            }
                        }
                        _productAdapter.updateMealList(it1)
                    }
                }
            }
        })
    }
}
