package com.bodyfriend.shippingsystem.main.map.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ts.ha on 2017-05-12.
 */

public class NaverGeocoderClient {
    final static String BASE_URL = "https://openapi.naver.com/v1/map/";
    final static String TAG = "TmapApiClient";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        Interceptor interceptor = chain -> {
            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("X-Naver-Client-Id", "nsWp3uH_N7RKAL4EI8Oa")
                    .addHeader("X-Naver-Client-Secret", "AA1h48EQOL");

            return chain.proceed(builder.build());
        };

        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
                .build();


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
//                .connectionSpecs(Collections.singletonList(spec))
                .build();

        if (retrofit == null) {
            Gson gson = new GsonBuilder().setLenient().create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .callFactory(okHttpClient)
                    .build();
        }
        return retrofit;
    }

}
