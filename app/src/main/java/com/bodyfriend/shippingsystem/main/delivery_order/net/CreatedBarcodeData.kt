package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.SerializedName

data class CreatedBarcodeData(@SerializedName("resultCode")
                              var resultCode: String
                              , @SerializedName("resultData")
                              var resultData: ResultDatum?) {


    data class ResultDatum(@SerializedName("TEMP_LOT")
                           var tempLot: String
    )
}