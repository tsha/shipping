package com.bodyfriend.shippingsystem.main.schedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.benefit.BenefitApi;
import com.bodyfriend.shippingsystem.main.benefit.BenefitSummaryData;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.schedule.list.ScheduleListFragment;
import com.bodyfriend.shippingsystem.main.schedule.net.appShippingDivCalendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by 이주영 on 2016-08-01.
 */
public class ScheduleFragment1 extends BFFragment2 {
    private MainActivity.MainActivityControll mainActivityControll;

    private Calendar todayCalendar = Calendar.getInstance();
    private ArrayList<Calendar> calendarArrayList = new ArrayList<Calendar>();

    private final int maxYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
    private final int minYear = maxYear - 3;
    private ViewPager calendarViewPager;

    private CalendarPagerAdapter calendarPagerAdapter;
    private HashMap<String, List<BenefitSummaryData.ResultDatum>> mBenefitSummaryDataHashMap = new HashMap<>();

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        mainActivityControll.changeTitle(R.layout.title_layout_schedule);

        initCalendarArray();
        initCalendarViewPager();
        moveToMonth(Calendar.getInstance());

        findViewById(R.id.left).setOnClickListener(onSideArrowListener);
        findViewById(R.id.right).setOnClickListener(onSideArrowListener);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        loadMonthPage(calendarViewPager.getCurrentItem());

        ((Switch) mainActivityControll.getView(R.id.switchSchedule)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.d("calendarViewPager.getCurrentItem() : " + calendarViewPager.getCurrentItem());
            Log.d("mPosition : " + mPosition);
//            loadMonthPage(calendarViewPager.getCurrentItem());
            loadMonthPage(mPosition);
        });
    }

    private void moveToMonth(Calendar calendar) {
        Calendar currentCalendar = calendar;
        Calendar targetCalendar;
        int position = 0;
        for (int i = 0; i < calendarArrayList.size(); i++) {
            targetCalendar = calendarArrayList.get(i);
            final int curYear = currentCalendar.get(Calendar.YEAR);
            final int curMonth = currentCalendar.get(Calendar.MONTH);
            final int tarYear = targetCalendar.get(Calendar.YEAR);
            final int tarMonth = targetCalendar.get(Calendar.MONTH);

            if (curYear == tarYear && curMonth == tarMonth) {
                position = i;
                break;
            }
        }

        calendarViewPager.setCurrentItem(position);
    }

    /**
     * 뷰 페이저에 표기할 정보인 Calendar 컬렉션을 초기화 한다.
     */
    private ArrayList<Calendar> initCalendarArray() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(minYear, 0, 1);
        for (int year = minYear; year < maxYear; year++) {
            for (int month = 1; month <= 12; month++) {
                calendarArrayList.add(calendar);
                calendar = (Calendar) calendar.clone();
                calendar.add(Calendar.MONTH, 1);
            }
        }

        return calendarArrayList;
    }

    private void initCalendarViewPager() {

        calendarPagerAdapter = new CalendarPagerAdapter(getContext());
        calendarViewPager = (ViewPager) findViewById(R.id.viewPager);
        calendarViewPager.setAdapter(calendarPagerAdapter);
        calendarViewPager.addOnPageChangeListener(onPageChangeListener);
    }

    private void loadMonthPage(final int position) {
        if (position == -1) {
            return;
        }
        final Calendar cal = calendarArrayList.get(position);
        final String text = SDF.yyyymm5.format(cal.getTime());
        TextView tv = (TextView) findViewById(R.id.date);
        tv.setText(text);

        reqShippingDivCalendar(cal);

    }

    private void reqShippingDivCalendar(Calendar currentCalendar) {

        Calendar beforeCalendar = (Calendar) currentCalendar.clone(); // 전달
        Calendar testCalendar = (Calendar) currentCalendar.clone(); // 전달
        beforeCalendar.add(Calendar.MONTH, -1);
        Calendar afterCalendar = (Calendar) currentCalendar.clone(); // 다음달
        afterCalendar.add(Calendar.MONTH, 1);

        final int minOfMonth = currentCalendar.get(Calendar.DAY_OF_WEEK) - 1; // 1일의 요일
        final int maxOfMonth = currentCalendar.getActualMaximum(Calendar.DAY_OF_MONTH); // 마지막 일수
        final int maxDayOfBeforeMonth = beforeCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        int dateStart = maxDayOfBeforeMonth - minOfMonth + 1; // 이전달 요청 시작 부분
        beforeCalendar.set(Calendar.DAY_OF_MONTH, dateStart);
        String sDateStart = SDF.yyyymmdd_1.format(beforeCalendar.getTime());

        int dateEnd = 42 - maxOfMonth - minOfMonth; // 다음달 요청 끝 부분
        afterCalendar.set(Calendar.DAY_OF_MONTH, dateEnd);
        String sDateEnd = SDF.yyyymmdd_1.format(afterCalendar.getTime());

        String endDate = SDF.yyyymmdd_1.format(testCalendar.getTime());
        String startDate;
        int maximum = testCalendar.getMaximum(Calendar.DAY_OF_MONTH);
        int i = testCalendar.get(Calendar.DAY_OF_MONTH);
        if (maximum != i) {
            testCalendar.add(Calendar.DATE, (maximum - i));
            endDate = SDF.yyyymmdd_1.format(testCalendar.getTime());
            testCalendar.add(Calendar.DATE, -(maximum - i));
            startDate = SDF.yyyymmdd_1.format(testCalendar.getTime());
            Log.d("startDate currentCalendar : " + startDate);
            Log.d("currentCalendar : " + endDate);
        } else {
            Log.d("currentCalendar : " + endDate);
            testCalendar.add(Calendar.DATE, -(testCalendar.getMaximum(Calendar.DAY_OF_MONTH) - 1));
            startDate = SDF.yyyymmdd_1.format(testCalendar.getTime());
            Log.d("startDate currentCalendar : " + startDate);
        }
        Log.d("sDateEnd L : " + sDateEnd);
//        Net.async(new appShippingDivCalendar(startDate, endDate)).setOnNetResponse(onNetResponse);

        String scheduleType = ((Switch) mainActivityControll.getView(R.id.switchSchedule)).isChecked() ? "Y" : "N";
        getSummaryBenefit( Auth.getId(), sDateStart, sDateEnd);
        Net.async(new appShippingDivCalendar(sDateStart, sDateEnd, scheduleType)).setOnNetResponse(onNetResponse);

    }


    public void getSummaryBenefit( String employeeId, String dateStart, String dateEnd) {
//        isEmptData.set(View.VISIBLE);
        DisposableObserver<BenefitSummaryData> benefitDataDisposableObserver = ServiceGenerator.createService(BenefitApi.class)
                .getSummaryBenefit(employeeId, dateStart, dateEnd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BenefitSummaryData>() {
                    @Override
                    public void onNext(BenefitSummaryData benefitData) {
//                        Log.d("benefitData.ResultData : " + benefitData.ResultData.get(0).toString());
                        if (benefitData.resultData != null && !benefitData.resultData.isEmpty()) {
                            setDayPerData(benefitData);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void setDayPerData(BenefitSummaryData benefitData) {
        Log.d("setDayPerData~~~~~~~~~~~~");
        mBenefitSummaryDataHashMap.clear();
        List<BenefitSummaryData.ResultDatum> list = benefitData.resultData;

        for (BenefitSummaryData.ResultDatum resultData : list) {
            Log.d("ResultData : " + resultData.toString());
            List<BenefitSummaryData.ResultDatum> list2 = mBenefitSummaryDataHashMap.get(resultData.iNDATE);
            if (list2 == null) {
                list2 = new ArrayList<>();
            }
            list2.add(resultData);
            mBenefitSummaryDataHashMap.put(resultData.iNDATE, list2);
        }
        calendarPagerAdapter.notifyDataSetChanged();
    }

    private Net.OnNetResponse<appShippingDivCalendar> onNetResponse = new Net.OnNetResponse<appShippingDivCalendar>() {

        @Override
        public void onResponse(appShippingDivCalendar response) {
            hashMap.clear();
            for (appShippingDivCalendar.Data.resultData.list item : response.data.resultData.list) {
                hashMap.put(item.DUE_DATE, item);
            }

            calendarPagerAdapter.notifyDataSetChanged();
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private HashMap<String, appShippingDivCalendar.Data.resultData.list> hashMap = new HashMap<>();

    private class CalendarPagerAdapter extends PagerAdapter {

        private LayoutInflater mInflater;
        private SparseArray<View> views = new SparseArray<View>();

        public CalendarPagerAdapter(Context c) {
            super();
            mInflater = LayoutInflater.from(c);
            // TextView textView = (TextView) mInflater.inflate(R.layout.cal_cell_d10, null);
        }

        @Override
        public int getCount() {
            return calendarArrayList.size();
        }

        @SuppressLint("InflateParams")
        @Override
        public Object instantiateItem(ViewGroup pager, int position) {
            Log.d("instantiateItem position " + position);
            View root = mInflater.inflate(R.layout.calendar_layout4, null);

            pager.addView(root, 0);
            views.put(position, root);

            final Calendar calendar = calendarArrayList.get(position);

            initCalendar(calendar, root);
            return root;
        }

        @Override
        public void notifyDataSetChanged() {

            final int curPosition = calendarViewPager.getCurrentItem();
            View root = views.get(curPosition);
            Log.d("notifyDataSetChanged : " + curPosition);
            if (root != null) {
                final Calendar calendar = calendarArrayList.get(curPosition);
                initCalendar(calendar, root);
            }

            super.notifyDataSetChanged();
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
            views.remove(position);
            view = null;
        }

        @Override
        public boolean isViewFromObject(View pager, Object obj) {
            return pager == obj;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

        @Override
        public void finishUpdate(View arg0) {
        }

        private void initCalendar(Calendar curMonthCalendar, View parent) {

            Calendar beforeCalendar = (Calendar) curMonthCalendar.clone(); // 전달
            beforeCalendar.add(Calendar.MONTH, -1);
            Calendar afterCalendar = (Calendar) curMonthCalendar.clone(); // 다음달
            afterCalendar.add(Calendar.MONTH, 1);

            curMonthCalendar.setFirstDayOfWeek(Calendar.SUNDAY);// 일요일을 주의 시작일로 지정
            curMonthCalendar.set(curMonthCalendar.get(Calendar.YEAR), curMonthCalendar.get(Calendar.MONTH), 1);
            final int minOfMonth = curMonthCalendar.get(Calendar.DAY_OF_WEEK) - 1; // 1일의 요일
            final int maxOfMonth = curMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH); // 마지막 일수
            final int maxDayOfBeforeMonth = beforeCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

            ViewGroup calendar_layout = (ViewGroup) parent.findViewById(R.id.calendar_layout); // 캘랜더 레이아웃
            final int rowCount = calendar_layout.getChildCount(); // 칼렌더의 열 갯수
            int count = 0;
            int day;
            String date; // yyyy-MM-dd
            for (int i = 0; i < rowCount; i++) {
                ViewGroup rowLayout = (ViewGroup) calendar_layout.getChildAt(i);
                final int columnCount = rowLayout.getChildCount();

                int k = 0;

                for (; k < columnCount; k++) {

                    View rowCellLayout = rowLayout.getChildAt(k);
                    rowCellLayout.setOnClickListener(onCellClickListener);
                    TextView dayTextView = (TextView) rowCellLayout.findViewById(R.id.dayTextView);
                    int textColor;
                    if (count < minOfMonth) { // 이전달 끝부분
                        textColor = Color.parseColor("#acacac");
                        day = maxDayOfBeforeMonth - minOfMonth + count + 1;
                        beforeCalendar.set(Calendar.DAY_OF_MONTH, day);
                        date = SDF.yyyymmdd_1.format(beforeCalendar.getTime());
                    } else if (minOfMonth <= count + minOfMonth && count < maxOfMonth + minOfMonth) { // 이번달
                        day = count - minOfMonth + 1;

                        textColor = Color.parseColor("#393a3a");

                        curMonthCalendar.set(Calendar.DAY_OF_MONTH, day);
                        date = SDF.yyyymmdd_1.format(curMonthCalendar.getTime());
                        final String toDate = SDF.yyyymmdd_1.format(todayCalendar.getTime());
                    } else { // 다음달 시작부분
                        textColor = Color.GRAY;
                        day = count - (maxOfMonth + minOfMonth) + 1;
                        afterCalendar.set(Calendar.DAY_OF_MONTH, day);
                        date = SDF.yyyymmdd_1.format(afterCalendar.getTime());
                    }
                    dayTextView.setText(String.valueOf(day));
                    dayTextView.setTextColor(textColor);

                    rowCellLayout.setTag(date);
                    setSmallData(date, rowCellLayout);
                    count++;
                }
            }
        }

        @SuppressLint("ResourceAsColor")
        private void setSmallData(String date, View rowCellLayout) {
            appShippingDivCalendar.Data.resultData.list item = hashMap.get(date);
            boolean isVisible = false;
            if (item != null) {

                if (item.TOTAL > 0) {
                    ((TextView) rowCellLayout.findViewById(R.id.text1)).setText(String.format("%s건", item.TOTAL));
                    rowCellLayout.findViewById(R.id.text1).setVisibility(View.VISIBLE);
                    isVisible = true;
                } else {
                    rowCellLayout.findViewById(R.id.text1).setVisibility(View.GONE);
                }
                if (item.AD_CNT > 0) {
                    ((TextView) rowCellLayout.findViewById(R.id.text2)).setText(String.format("변경%s건", item.AD_CNT));
                    rowCellLayout.findViewById(R.id.text2).setVisibility(View.VISIBLE);
                    isVisible = true;
                } else {
                    rowCellLayout.findViewById(R.id.text2).setVisibility(View.GONE);
                }


                if (isVisible) {
                    rowCellLayout.findViewById(R.id.small_text_layout).setVisibility(View.VISIBLE);
                    ((TextView) rowCellLayout.findViewById(R.id.dayTextView)).setTextColor(Color.WHITE);
                    rowCellLayout.findViewById(R.id.dayTextView).setBackgroundResource(R.drawable.shape_rect_green);
                }
            } else {
                rowCellLayout.findViewById(R.id.small_text_layout).setVisibility(View.GONE);
                rowCellLayout.findViewById(R.id.dayTextView).setBackgroundResource(android.R.color.transparent);
            }


            List<BenefitSummaryData.ResultDatum> resultData = mBenefitSummaryDataHashMap.get(date);
            Log.d("ResultData  : " + date);
            if (resultData != null) {
                int totalBenefit = 0;
                for (BenefitSummaryData.ResultDatum resultDatum : resultData) {
                    Log.d(" resultDatum.tOTAL : " + resultDatum.tOTAL);
                    totalBenefit += resultDatum.tOTAL;
                }
                if (totalBenefit > 0) {
                    ((TextView) rowCellLayout.findViewById(R.id.benefitText)).setText(String.format("%,d", totalBenefit));
                    rowCellLayout.findViewById(R.id.benefitText).setVisibility(View.VISIBLE);
//                    rowCellLayout.findViewById(R.id.small_text_layout).setVisibility(View.VISIBLE);
                } else {
                    if (!isVisible) {
                        rowCellLayout.findViewById(R.id.benefitText).setVisibility(View.GONE);
                        rowCellLayout.findViewById(R.id.small_text_layout).setVisibility(View.GONE);
                        rowCellLayout.findViewById(R.id.dayTextView).setBackgroundResource(android.R.color.transparent);
                    }
                }
            }

            if (SDF.yyyymmdd_1.now().equals(date)) {
                ((TextView) rowCellLayout.findViewById(R.id.dayTextView)).setTextColor(Color.WHITE);
                rowCellLayout.findViewById(R.id.dayTextView).setBackgroundResource(R.drawable.shape_rect_blue2);
                rowCellLayout.findViewById(R.id.helloDot).setVisibility(View.VISIBLE);
            }
        }

        private View.OnClickListener onCellClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String date = (String) v.getTag();
                Fragment fragment = new ScheduleListFragment();
                ((ScheduleListFragment) fragment).setMainActivityControll(mainActivityControll);
                Bundle bundle = new Bundle();
                bundle.putString("date", date);
                String scheduleType = ((Switch) mainActivityControll.getView(R.id.switchSchedule)).isChecked() ? "Y" : "N";
                bundle.putString("scheduleType", scheduleType);
                fragment.setArguments(bundle);
                mainActivityControll.changeFragment(fragment);
            }
        };
    }

    private int mPosition;
    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            loadMonthPage(position);
            mPosition = position;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * 달력 좌우의 버튼을 눌렀을때 이벤트를 받는 리스너
     */
    private View.OnClickListener onSideArrowListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int cur = calendarViewPager.getCurrentItem();
            int position = cur - 1;
            int id = v.getId();
            if (id == R.id.left) {
                position = cur - 1;
                position = position >= 0 ? position : cur;
            } else if (id == R.id.right) {
                position = cur + 1;
                position = position < calendarArrayList.size() ? position : cur;
            }

            calendarViewPager.setCurrentItem(position);
        }
    };
}