package com.bodyfriend.shippingsystem.main.map;

public enum ShippingType {
    D10("배송"), D20("맞교체"), D30("계약철회"), P10("이전요청"), P20("수리요청"), D70("AS"), D140("기안회수"), D90("재설치");
    final private String name;

    ShippingType(String name) { //enum에서 생성자 같은 역할
        this.name = name;
    }

    public String getTypeName() { // 문자를 받아오는 함수
        return name;
    }

}
