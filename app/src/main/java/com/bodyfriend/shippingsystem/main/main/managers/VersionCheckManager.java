package com.bodyfriend.shippingsystem.main.main.managers;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.net.VersionCheck;

/**
 * Created by 이주영 on 2016-12-20.
 */

public class VersionCheckManager {

    private BFActivity mBFActivity;

    public static VersionCheckManager newInstance() {
        return new VersionCheckManager();
    }

    /**
     * 버전체크
     */
    public void reqVersionCheck(BFActivity bfActivity) {
        if (!Auth.isLogin()) return;
        this.mBFActivity = bfActivity;

        boolean b = false;
        if (versionCheckTime == 0) {
            b = true;
            versionCheckTime = System.currentTimeMillis();
        } else {
            long gapTime = System.currentTimeMillis() - versionCheckTime;

            long time = 1000; // 1초
            time *= 60; // 1분
            time *= 60; // 1시간
            time *= 6; // 6시간

            if (gapTime > time) {
                b = true; // 마지막 버전 체크 시간보다 6시간이 지났으면 버전을 체크한다.
                versionCheckTime = System.currentTimeMillis();
            }
        }
//        if (b)
        Net.async(new VersionCheck()).setOnNetResponse(onVersionCheckListener);

    }

    private static long versionCheckTime = 0;

    private Net.OnNetResponse<VersionCheck> onVersionCheckListener = new Net.OnNetResponse<VersionCheck>() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(VersionCheck response) {

            assert response != null;
            Log.d("version ~~~~~~~~~~~~~~~~~~~~~~~~~~ : " + BuildConfig.VERSION_CODE);
            if (response.data != null && response.data.resultCode != 200) {
                return;
            }

            Log.d("version ~~~~~~~~~~~~~~~~~~~~~~~~~~ : " + BuildConfig.VERSION_CODE);

            if (BuildConfig.VERSION_CODE < response.data.resultData.version) {
                if (response.data.resultData.updateType.equals("Y")) {
                    mBFActivity.showDialog("업데이트 안내", response.data.resultData.msg, "업데이트", positiveListener, "종료", finishListener).setCancelable(false);
                    versionCheckTime = 0;
                } else {
                    mBFActivity.showDialog("업데이트 안내", response.data.resultData.msg, "업데이트", positiveListener, "취소", null);
                }
            }
        }

        DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.bfservice.co.kr/login/login.view"));
                mBFActivity.startActivity(intent);
            }
        };

        DialogInterface.OnClickListener finishListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OH.c().notifyObservers(OH.TYPE.EXIT);
            }
        };
    };

}
