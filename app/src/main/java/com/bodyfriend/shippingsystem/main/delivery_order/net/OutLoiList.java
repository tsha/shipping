package com.bodyfriend.shippingsystem.main.delivery_order.net;

import java.util.ArrayList;

/**
 * Created by Taeseok on 2018-03-05.
 */

public class OutLoiList {

    public ForwardingItemData forwardingItemData;

    public static class ForwardingItemData {
        public String deliveryManId;
        public String deliveryManNm;
        public String loiKnd;
        public String loiNo;
        public String procStatCd;
        public String regDt;
        public String vhclNo;
        public String whsCd;
        public int modelTotal;
        public String procStatNm;
        public String erpFlag;
        public String fromSlCd;


        public ArrayList<models> models = new ArrayList<>();

        public static class models {
            public final int ifNo;
            public final String erpFlag;
            public final String serialCd;


            public String qty;
            public String itemExpln;
            public String gdsStatNm;

            public models(String qty, String itemNm, int ifNo, String erpFlag, String serialCd) {
                this.qty = qty;
                this.itemExpln = itemNm;
                this.ifNo = ifNo;
                this.erpFlag = erpFlag;
                this.serialCd = serialCd;
            }


        }
    }
}
