package com.bodyfriend.shippingsystem.main.sale;



import java.io.Serializable;
import java.util.List;

/**
 * Created by ts.ha on 2017-06-01.
 */
@SuppressWarnings("serial")
public class ProductListData {
    public ResultData resultData;
    public String resultMsg;
    public int resultCode;
    @SuppressWarnings("serial")
    public class ResultData {
        public String resultMsg;
        public int resultCode;
        public int listCount;
        public List<list> list;

        @SuppressWarnings("serial")
        public class list implements Serializable {
            public String PRODUCT_IMAGE_S; // 이미지주소
            public String PRODUCT_TYPE; // 제품 타입
            public String PRODUCT_NAME; // 제품명
            //				public String PRODUCT_CAUTION; // 설치주의사항
            public String PRODUCT_SPEC; // 제품스펙
            public String PRODUCT_CODE; // 제품 코드
            public String PRODUCT_DETAIL; // 제품설명
            public String imageone;
            public String imagetwo;
            public String PRODUCT_IMAGE_L;
            public String PRODUCT_INTRO;

            @Override
            public String toString() {
                return "list{" +
                        "PRODUCT_IMAGE_S='" + PRODUCT_IMAGE_S + '\'' +
                        ", PRODUCT_TYPE='" + PRODUCT_TYPE + '\'' +
                        ", PRODUCT_NAME='" + PRODUCT_NAME + '\'' +
                        ", PRODUCT_SPEC='" + PRODUCT_SPEC + '\'' +
                        ", PRODUCT_CODE='" + PRODUCT_CODE + '\'' +
                        ", PRODUCT_DETAIL='" + PRODUCT_DETAIL + '\'' +
                        ", imageone='" + imageone + '\'' +
                        ", imagetwo='" + imagetwo + '\'' +
                        ", PRODUCT_IMAGE_L='" + PRODUCT_IMAGE_L + '\'' +
                        ", PRODUCT_INTRO='" + PRODUCT_INTRO + '\'' +
                        '}';
            }
        }
    }
}
