package com.bodyfriend.shippingsystem.main.widget.net;

/**
 * Created by Taeseok on 2017-11-20.
 */

class NET_API {
    public static final String LOGIN_APP_LOGIN_JSON = "login/appLogin.json";
    public static final String APP_RECEIVE_LIST_JSON = "mobile/api/appShippingDivList.json";
}
