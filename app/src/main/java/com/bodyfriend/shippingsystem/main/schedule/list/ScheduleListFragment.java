package com.bodyfriend.shippingsystem.main.schedule.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.CC;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.schedule.ScheduleFragment;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingListFragment2;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingDivList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeShippingType;

/**
 * Created by 이주영 on 2016-08-09.
 */
public class ScheduleListFragment extends ShippingListFragment2 {


    private String mScheduleType;

    public static final class EXTRA {
        public static final String DATE = "date";
        public static final String SCHEDULE_TYPE = "scheduleType";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.calendar_shipping_list_fragment2, container, false);
    }

    private String mDate;

    @Override
    protected void onParseExtra() {
        setSwipeRefresh();
        Bundle bundle = this.getArguments();
        mDate = bundle.getString(EXTRA.DATE);
        mScheduleType = bundle.getString(EXTRA.SCHEDULE_TYPE);
    }

    @Override
    protected void onLoadOnce() {
        if (mainActivityControll != null) {
            mainActivityControll.changeTitle(R.layout.title_layout_schedule_list);

            TextView title = (TextView) mainActivityControll.getView(R.id.title);
            title.setText(mDate);

            mList = (ListView) findViewById(R.id.list);
            mList.setEmptyView(findViewById(R.id.emptyView));

            mainActivityControll.getView(R.id.back).setOnClickListener(onBackClickListener);
        } else {
            mainActivityControll = ((MainActivity) getActivity()).getMainActivityControll();
            mainActivityControll.changeTitle(R.layout.title_layout_schedule_list);
        }
    }

    @Override
    protected void onReload() {
        reqShippingDivList();
    }

    protected void reqShippingDivList() {

        if (!(this instanceof CC.ILOGIN) && !Auth.isLogin()) {
            return;
        }
        final int s_datetype = 0;
        String dateStart = mDate;
        String dateEnd = mDate;
        String s_area = "";
        String s_custname = "";
        String s_addr = "";
        String s_oneself = "N";

        showProgress();

        if (mShippingType == null) {
            Net.async(new searchCodeShippingType()).setOnNetResponse(onShippingTypeCodeNetResponse);
        }
        Net.async(new ShippingDivList(s_datetype, dateStart, dateEnd, s_area, s_custname, s_addr, s_oneself, mScheduleType)).setOnNetResponse(onNetResponse);

    }

    private View.OnClickListener onBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ScheduleFragment scheduleFragment = new ScheduleFragment();
            scheduleFragment.setMainActivityControll(mainActivityControll);
            mainActivityControll.changeFragment(scheduleFragment);
        }
    };


}
