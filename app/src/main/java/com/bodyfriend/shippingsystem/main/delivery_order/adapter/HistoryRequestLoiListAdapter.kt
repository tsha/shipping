package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.annotation.TargetApi
import android.databinding.DataBindingUtil
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemHistoryLoiBinding
import com.bodyfriend.shippingsystem.databinding.LayoutLoiItemBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.LoiData
import com.bodyfriend.shippingsystem.main.delivery_order.net.LoiGroupByIssueNoData
import java.util.*


class HistoryRequestLoiListAdapter(val itemClick: (LoiGroupByIssueNoData.ForwardingItemData) -> Unit) : RecyclerView.Adapter<HistoryRequestLoiListAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        holder.setModelInf(mealList[position].modelInfoArrayList)
        holder.view.delProductGroup.setOnClickListener {
            itemClick(mealList[position])
        }

        /*holder.delProductGroup.setOnClickListener {
            itemClick(mealList[position])
        }*/

    }


    private var mealList: MutableList<LoiGroupByIssueNoData.ForwardingItemData> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }


    fun updateMealList(postList: MutableList<LoiGroupByIssueNoData.ForwardingItemData>) {
        mealList.clear()
        this.mealList = postList

        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemHistoryLoiBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_history_loi, parent, false)
        return ViewHolder(binding)
    }

    fun getItems(): MutableList<LoiGroupByIssueNoData.ForwardingItemData> {
        return mealList
    }

    fun clear() {
        mealList.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(var view: ItemHistoryLoiBinding) : RecyclerView.ViewHolder(view.root) {
        @TargetApi(Build.VERSION_CODES.N)
        fun setModelInf(modelInfoArrayList: ArrayList<LoiGroupByIssueNoData.ForwardingItemData.ModelInfo>) {
            view.layoutItemList.removeAllViews()
            modelInfoArrayList.forEach {
                val rootView = LayoutInflater.from(view.root.context).inflate(R.layout.layout_loi_item, null)
                val bind = DataBindingUtil.bind<LayoutLoiItemBinding>(rootView)
                bind?.viewData = it
                view.layoutItemList.addView(rootView)

            }
        }
    }
}