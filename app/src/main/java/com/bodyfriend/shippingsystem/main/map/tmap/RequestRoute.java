
package com.bodyfriend.shippingsystem.main.map.tmap;

import java.util.List;

public class RequestRoute {

    public String reqCoordType;
    public String resCoordType;
    public String startName;
    public String startX;
    public String startY;
    public String startTime;
    public String endName;
    public String endX;
    public String endY;
    public String endPoiId;
    public String searchOption;
    public String carType;
    public List<ViaPoint> viaPoints = null;
//    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
//
//    public Map<String, Object> getAdditionalProperties() {
//        return this.additionalProperties;
//    }
//
//    public void setAdditionalProperty(String name, Object value) {
//        this.additionalProperties.put(name, value);
//    }

}
