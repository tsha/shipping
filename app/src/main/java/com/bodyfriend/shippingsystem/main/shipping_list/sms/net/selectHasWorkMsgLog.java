package com.bodyfriend.shippingsystem.main.shipping_list.sms.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * Created by 이주영 on 2017-02-28.
 */
public class selectHasWorkMsgLog extends BFEnty {
    public selectHasWorkMsgLog() {
        setUrl("mobile/api/selectHasWorkMsgLog.json");
        setParam(
                "s_datetype", 2
                , "msg_gb", 4

        );
    }

    public Data data;

    public static class Data {
        public List<resultData> resultData;

        public class resultData {
            public String HPHONE_NO;
            public String CUST_NAME;
            public String SHIPPING_SEQ;
            public String SESS_NM;
        }
    }
}
