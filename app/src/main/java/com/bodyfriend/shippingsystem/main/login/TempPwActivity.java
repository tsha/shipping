package com.bodyfriend.shippingsystem.main.login;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.databinding.ActivityTempPwBinding;
import com.bodyfriend.shippingsystem.main.login.viewmodel.TempPwViewModel;

public class TempPwActivity extends AppCompatActivity {

    private ActivityTempPwBinding mBinding;
    private TempPwViewModel mViewModel;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_temp_pw);
        mViewModel = ViewModelProviders.of(this).get(TempPwViewModel.class);
        setEvent();
        observeViewModel(mViewModel);

    }


    private void observeViewModel(TempPwViewModel mViewModel) {


        mViewModel.getKakaoKeyData().observe(this, bfData -> {
            assert bfData != null;
            if (bfData.resultCode == 200) {
                mBinding.phoneNumCsl.setVisibility(View.GONE);
                mBinding.kakaoKeyCsl.setVisibility(View.VISIBLE);
            } else {
                mBinding.phoneNumTil.setError(bfData.resultMsg);
            }
        });

        mViewModel.getAuthKeyData().observe(this, bfData -> {
            assert bfData != null;
            if (bfData.resultCode == 200) {
                mBinding.kakaoKeyCsl.setVisibility(View.GONE);
                mBinding.pwCsl.setVisibility(View.VISIBLE);
            } else {
                mBinding.textInputLayout4.setError(bfData.resultMsg);
            }
        });


        mViewModel.getUpdatePwData().observe(this, bfData -> {
            assert bfData != null;
            if (bfData.resultCode == 200) {
                mBinding.loginPassword1.setText("");
                mBinding.loginPassword2.setText("");
                Toast.makeText(this, "비밀번호 변경에 성공하였습니다.", Toast.LENGTH_SHORT).show();
                onBackPressed();
            } else {
                mBinding.textInputLayout6.setError(bfData.resultMsg);
            }
        });

    }

    private void setEvent() {
        // 카카오톡 인증 번호 요청
        mBinding.kakaoKeyRequestBtn.setOnClickListener(v -> {
            id = mBinding.loginId.getText().toString();
            String phoneNum = mBinding.userPhone.getText().toString();
            if (TextUtils.isEmpty(id)) {
                mBinding.textInputLayout2.setError("아이디를 입력하세요.");
                return;
            } else {
                mBinding.phoneNumTil.setError(null);
            }
            if (TextUtils.isEmpty(phoneNum)) {
                mBinding.phoneNumTil.setError("전화번호를 입력하세요.");
                return;
            } else {
                mBinding.phoneNumTil.setError(null);
            }
            mViewModel.getKakaoKey(id, phoneNum);
        });

        mBinding.button3.setOnClickListener(v -> {
            String authKey = mBinding.loginAuthCode.getText().toString();
            mViewModel.checkauthKey(id, authKey);
        });

        // 비밀번호 변경
        mBinding.button5.setOnClickListener(v -> {
            String password = mBinding.loginPassword1.getText().toString();
            String password2 = mBinding.loginPassword2.getText().toString();
            if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(password2) && password.equals(password2)) {

                mViewModel.updateAdminPwd(id, password);
            } else {
                mBinding.textInputLayout6.setError("비밀번호가 동일하지 않습니다.");
            }
        });

        // 카카오 인증키 확인
        mBinding.button3.setOnClickListener(v -> {
            String authCod = mBinding.loginAuthCode.getText().toString();

            if (!TextUtils.isEmpty(authCod)) {
                mViewModel.checkauthKey(id, authCod);
            } else {
                mBinding.textInputLayout6.setError("카카오 인증키를 입력하세요.");
            }
        });

    }

}
