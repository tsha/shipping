package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GiftListData(@SerializedName("resultData")
                        @Expose
                        val resultData: List<ResultData>) {
    data class ResultData(
            @SerializedName("GIFT_YN")
            @Expose
            val giftYn: String
            , @SerializedName("LOT_FLG")
            @Expose
            val lotFlg: String
            , @SerializedName("ITEM_NM")
            @Expose
            val itemNm: String
            , @SerializedName("ITEM_CD")
            @Expose
            val itemCd: String
    )
}