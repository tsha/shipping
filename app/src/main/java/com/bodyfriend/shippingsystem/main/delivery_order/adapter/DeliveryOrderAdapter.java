package com.bodyfriend.shippingsystem.main.delivery_order.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.main.delivery_order.net.OutLoiList;

import java.util.ArrayList;

/**
 * Created by 이주영 on 2016-12-08.
 */

public class DeliveryOrderAdapter extends RecyclerView.Adapter<DeliveryViewHolder> {
    private ArrayList<OutLoiList.ForwardingItemData> data;
    private LoiRecyclerViewClickListener mLoiRecyclerViewClickListener;

    public DeliveryOrderAdapter(LoiRecyclerViewClickListener loiRecyclerViewClickListener) {
        mLoiRecyclerViewClickListener = loiRecyclerViewClickListener;
    }

    /**
     * 뷰를 만든다
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public DeliveryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        DeliveryViewHolder holder = new DeliveryViewHolder(DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.item_delivery_order2,
                parent, false), mLoiRecyclerViewClickListener);
        return holder;
    }

    /**
     * 아이템의 갯수
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    /**
     * 바인드뷰홀더는 데이터를 입력하는곳이다.
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(DeliveryViewHolder holder, int position) {
        holder.setupData(data.get(position));
    }

    /**
     * 데이터를 입력한다.
     *
     * @param data
     */
    public void setData(ArrayList<OutLoiList.ForwardingItemData> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * 데이터를 입력한다.
     *
     * @param data
     */
    public void addData(ArrayList<OutLoiList.ForwardingItemData> data) {
        this.data.addAll(data);
        Log.d("data: " + data.size());
        notifyDataSetChanged();
    }
}
