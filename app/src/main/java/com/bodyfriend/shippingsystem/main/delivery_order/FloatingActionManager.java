package com.bodyfriend.shippingsystem.main.delivery_order;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

/**
 * Created by 이주영 on 2016-12-13.
 */

public class FloatingActionManager {
    private FloatingActionButton[] buttons;

    private static final int ANIMATION_DURATION = 300;
    private static final float COLLAPSED_PLUS_ROTATION = 0f;
    private static final float EXPANDED_PLUS_ROTATION = 90f + 45f;
    private AnimatorSet mExpandAnimation = new AnimatorSet().setDuration(ANIMATION_DURATION);
    private AnimatorSet mCollapseAnimation = new AnimatorSet().setDuration(ANIMATION_DURATION);
    private static Interpolator sExpandInterpolator = new OvershootInterpolator();
    private static Interpolator sCollapseInterpolator = new DecelerateInterpolator(3f);
    private static Interpolator sAlphaExpandInterpolator = new DecelerateInterpolator();

    private boolean isExpanded = false;

    private Context mContext;

    private boolean isVisible = true;

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public static FloatingActionManager newInstance(Context context, FloatingActionButton... buttons) {
        FloatingActionManager fragment = new FloatingActionManager(context, buttons);
        return fragment;
    }

    public FloatingActionManager(Context context, FloatingActionButton... buttons) {
        mContext = context;
        this.buttons = buttons;

        setupFloatingAnimation();

        setupChildAnimation();
    }

    private void setupChildAnimation() {
        for (int i = 0; i < buttons.length; i++) {
            if (i == 0) {

            } else {
                buttons[i].setVisibility(View.GONE);
                AnimHolder animHolder = new AnimHolder(buttons[i], (int) (i * (56 * 1.3f)));
                mExpandAnimation.play(animHolder.mExpandAlpha);
                mExpandAnimation.play(animHolder.mExpandDir);
                mCollapseAnimation.play(animHolder.mCollapseAlpha);
                mCollapseAnimation.play(animHolder.mCollapseDir);
            }
        }
    }

    /**
     * 플로팅 버튼의 애니메이션 세팅한다.
     */
    private void setupFloatingAnimation() {
        final ObjectAnimator collapseAnimator = ObjectAnimator.ofFloat(buttons[0], "rotation", EXPANDED_PLUS_ROTATION, COLLAPSED_PLUS_ROTATION);
        final ObjectAnimator expandAnimator = ObjectAnimator.ofFloat(buttons[0], "rotation", COLLAPSED_PLUS_ROTATION, EXPANDED_PLUS_ROTATION);

        final OvershootInterpolator interpolator = new OvershootInterpolator();
        collapseAnimator.setInterpolator(interpolator);
        expandAnimator.setInterpolator(interpolator);

        mExpandAnimation.addListener(onExpandAnimationListener);
        mCollapseAnimation.addListener(onCollapseAnimationListener);

        mExpandAnimation.play(expandAnimator);
        mCollapseAnimation.play(collapseAnimator);

        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
            }
        });
    }

    /**
     * 토글
     */
    public void toggle() {
        if (isExpanded) {
            // 무너짐 애니메이션
            startCollapse();
        } else {
            // 확장애니메이션
            startExpanded();
        }
    }

    /**
     * 확장애니메이션
     */
    public void startExpanded() {
        if (isExpanded) return;
        isExpanded = true;
        mCollapseAnimation.cancel();
        mExpandAnimation.start();
    }

    /**
     * 무너짐 애니메이션
     */
    public void startCollapse() {
        if (!isExpanded) return;
        isExpanded = false;
        mCollapseAnimation.start();
        mExpandAnimation.cancel();
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!isVisible) {
                    return;
                }

                if (dy > 0) {
                    buttons[0].hide();
                    if (isExpanded) {
                        startCollapse();
                    }
                } else {
                    buttons[0].show();
                }
            }
        });
    }

    class AnimHolder {
        public ObjectAnimator mExpandDir = new ObjectAnimator();
        public ObjectAnimator mExpandAlpha;
        public ObjectAnimator mCollapseDir = new ObjectAnimator();
        public ObjectAnimator mCollapseAlpha;

        public AnimHolder(View view, int value) {
            mExpandAlpha = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            mExpandAlpha.setInterpolator(sAlphaExpandInterpolator); // 확장애니메이션

            mCollapseAlpha = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
            mCollapseAlpha.setInterpolator(sCollapseInterpolator); // 축소애니메이션

            mExpandDir.setInterpolator(sExpandInterpolator); // 확장애니메이션
            mExpandDir.setProperty(View.TRANSLATION_Y);
            mExpandDir.setFloatValues(0, -convertDpToPixel(value, mContext));
            mExpandDir.setTarget(view);

            mCollapseDir.setInterpolator(sCollapseInterpolator); // 축소애니메이션
            mCollapseDir.setProperty(View.TRANSLATION_Y);
            mCollapseDir.setFloatValues(-convertDpToPixel(value, mContext), 0);
            mCollapseDir.setTarget(view);

        }

    }

    public float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    private Animator.AnimatorListener onExpandAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {
            for (int i = 0; i < buttons.length; i++) {
                if (i != 0) {
                    buttons[i].setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onAnimationEnd(Animator animation) {

        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    private Animator.AnimatorListener onCollapseAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            for (int i = 0; i < buttons.length; i++) {
                if (i != 0) {
                    buttons[i].setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
}
