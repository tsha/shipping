package com.bodyfriend.shippingsystem.main.sale;



public class SaleResult {
    public Result resultData;
    public String resultMsg;
    public String resultCode;

    public class Result {
        public String resultMsg;
        public int resultCode;
    }

}
