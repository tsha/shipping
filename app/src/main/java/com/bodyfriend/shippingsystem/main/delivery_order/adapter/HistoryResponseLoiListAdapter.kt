package com.bodyfriend.shippingsystem.main.delivery_order.adapter


import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemResponsHistoryLoiBinding
import com.bodyfriend.shippingsystem.databinding.LayoutResponseLoiItemBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.LoiGroupByIssueNoData
import java.util.*


class HistoryResponseLoiListAdapter(val itemClick: (LoiGroupByIssueNoData.ForwardingItemData) -> Unit) : RecyclerView.Adapter<HistoryResponseLoiListAdapter.ViewHolder>() {


    private var mealList: MutableList<LoiGroupByIssueNoData.ForwardingItemData> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        holder.setModelInf(mealList[position].modelInfoArrayList)
        holder.view.btnReceipt.setOnClickListener {
            itemClick(mealList[position])
        }
    }


    fun updateMealList(postList: MutableList<LoiGroupByIssueNoData.ForwardingItemData>) {
        mealList.clear()
        this.mealList = postList
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemResponsHistoryLoiBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_respons_history_loi, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<LoiGroupByIssueNoData.ForwardingItemData> {
        return mealList
    }

    fun clear() {
        mealList.clear()
        notifyDataSetChanged()
    }


    class ViewHolder(var view: ItemResponsHistoryLoiBinding) : RecyclerView.ViewHolder(view.root) {
        fun setModelInf(modelInfoArrayList: ArrayList<LoiGroupByIssueNoData.ForwardingItemData.ModelInfo>) {
            view.layoutItemList.removeAllViews()
            modelInfoArrayList.forEach {
                val rootView = LayoutInflater.from(view.root.context).inflate(R.layout.layout_response_loi_item, null)
                val bind = DataBindingUtil.bind<LayoutResponseLoiItemBinding>(rootView)
                bind?.viewData = it
                view.layoutItemList.addView(rootView)

            }
        }
    }
}