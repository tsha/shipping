package com.bodyfriend.shippingsystem.main.detail.ui.fragment


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.FragmentResponsBinding
import com.bodyfriend.shippingsystem.main.detail.viewmode.DetailViewModel
import com.bodyfriend.shippingsystem.main.shipping_list.data.IntervalTimerViewModelFactory
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingMasterDetailData


/**
 * A simple [Fragment] subclass.
 *
 */
class ResponseFragment : Fragment() {
    private lateinit var binding: FragmentResponsBinding
    private val detailViewModel: DetailViewModel
            by lazy {
                activity?.let {
                    ViewModelProviders.of(it, IntervalTimerViewModelFactory)
                            .get(DetailViewModel::class.java)
                }!!
            }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_respons, container, false)
        binding.viewmodel = detailViewModel

    /*    val viewData = arguments?.getParcelable(VIEW_DATA) as ShippingMasterDetailData.ResultData
        println("tset : ${viewData}")*/

//        binding.viewData = viewData
        setupViewModel()
        return binding.root
    }

    fun setViewData(viewData: ShippingMasterDetailData.ResultData) {
        binding.viewData = viewData
    }


    private fun setupViewModel() {


    }

    companion object {
        private const val VIEW_DATA = "VIEW_DATA"
        @JvmStatic
        fun newInstance(viewData: ShippingMasterDetailData.ResultData): ResponseFragment {
            return ResponseFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(VIEW_DATA, viewData)
                }
            }
        }
        @JvmStatic
        fun newInstance(): ResponseFragment {
            return ResponseFragment().apply {
                arguments = Bundle().apply {

                }
            }
        }
    }


}
