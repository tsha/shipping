package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemAddedProductBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.ProductSerialData


class AddedProductAdapter(val itemSelected: (index: Int, position: Int) -> Unit, val itemClick: (ProductSerialData.ResultData, position: Int) -> Unit) : RecyclerView.Adapter<AddedProductAdapter.ViewHolder>() {

    private var mealList: MutableList<ProductSerialData.ResultData> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) {
        holder.view.item = mealList[index]
        Log.d("TAG", "mealList[index].toString() :  ${mealList[index]}")
        holder.view.btnDelItem.setOnClickListener {
            itemClick(mealList[index], index)
        }

        holder.view.spinnerItemCount.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                itemSelected(index, position)
            }
        }

    }


    fun updateMealList(postList: MutableList<ProductSerialData.ResultData>) {
        Log.d("TAG", "updateStockList:  ")
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun updateMealList(postList: ProductSerialData.ResultData) {
        Log.d("TAG", "updateStockList:  ")
        this.mealList.add(postList)
        notifyDataSetChanged()

    }

    fun addUploadItem(isValid: Boolean, position: Int) {
        Log.d("TAG", "mealList[position].toString() :  ${mealList[position]}")
        notifyItemChanged(position)
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemAddedProductBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_added_product, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<ProductSerialData.ResultData> {
        return mealList
    }

    class ViewHolder(var view: ItemAddedProductBinding) : RecyclerView.ViewHolder(view.root)


}