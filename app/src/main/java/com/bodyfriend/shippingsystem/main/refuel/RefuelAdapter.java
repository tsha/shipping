package com.bodyfriend.shippingsystem.main.refuel;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.databinding.ItemRefuelBinding;
import com.bodyfriend.shippingsystem.main.refuel.net.RefuelData;

import java.util.ArrayList;

public class RefuelAdapter extends RecyclerView.Adapter<RefuelAdapter.RefuelViewHolder> {

    private ArrayList<RefuelData.Record> data;

    @NonNull
    @Override
    public RefuelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context mContext = parent.getContext();
        if (mContext != null) {
            return new RefuelViewHolder(DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.item_refuel,
                    parent, false));
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RefuelViewHolder holder, int position) {
        RefuelData.Record itemBenefit = data.get(position);
        holder.mBinding.setRefuelData(itemBenefit);
    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.size();
    }

    public void setData(ArrayList<RefuelData.Record> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class RefuelViewHolder extends RecyclerView.ViewHolder {
        public ItemRefuelBinding mBinding;

        RefuelViewHolder(ItemRefuelBinding itemView) {
            super(itemView.getRoot());
            mBinding = itemView;
        }
    }
}
