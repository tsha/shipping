package com.bodyfriend.shippingsystem.main.detail.viewmode

import android.databinding.BindingAdapter
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import com.bodyfriend.shippingsystem.main.detail.ui.RadioGridGroup
import com.bodyfriend.shippingsystem.main.detail.viewmode.SpinnerExtensions.setSpinnerEntries
import com.bodyfriend.shippingsystem.main.detail.viewmode.SpinnerExtensions.setSpinnerItemSelectedListener
import com.bodyfriend.shippingsystem.main.detail.viewmode.SpinnerExtensions.setSpinnerValue

object BindingModel {
    @JvmStatic
    @BindingAdapter("touchListener")
    fun setTouchListener(self: ImageView, productNameView: TextView) {
        self.setOnTouchListener { _, event ->
            println(event.action)
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    productNameView.visibility = View.GONE
                }
                MotionEvent.ACTION_DOWN -> {
                    productNameView.visibility = View.VISIBLE
                }
            }
            true
        }
    }

    @JvmStatic
    @BindingAdapter("entries")
    fun Spinner.setEntries(entries: List<Any>?) {
        setSpinnerEntries(entries)
    }


    @JvmStatic
    @BindingAdapter("onCustomEventListener")
    fun RadioGridGroup.setOnCustomEventListener(listener: RadioGridGroup.OnCustomEventListener) {
        println("~~~~~~~~~~~~~~~~~~~~~~~~~~~ onCustomEventListener ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        setOnCustomEventListener(listener)
    }

    @JvmStatic
    @BindingAdapter("onItemSelected")
    fun Spinner.setItemSelectedListener(itemSelectedListener: SpinnerExtensions.ItemSelectedListener?) {
        setSpinnerItemSelectedListener(itemSelectedListener)
    }

    @JvmStatic
    @BindingAdapter("newValue")
    fun Spinner.setNewValue(newValue: Any?) {
        println("setNewValue ")
        setSpinnerValue(newValue)
    }

    @JvmStatic
    @BindingAdapter("selectedValue")
    fun Spinner.setSelectedValue(selectedValue: Any?) {
        setSpinnerValue(selectedValue)
    }
}