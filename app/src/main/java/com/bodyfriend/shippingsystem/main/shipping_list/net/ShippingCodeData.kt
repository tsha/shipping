package com.bodyfriend.shippingsystem.main.shipping_list.net

import com.google.gson.annotations.SerializedName

data class ShippingCodeData(var resultMsg: String, var resultCode: String, @SerializedName("resultData") var resultData: ArrayList<ResultData>) {
    data class ResultData(var USE_YN: String
                          , var INS_DT: String
                          , var DET_CD: String
                          , var DESCRIPTION: String
                          , var DET_CD_NM: String
                          , var COMM_CD_NM: String
                          , var UPD_DT: String
                          , var GROUP_CD: String
                          , var INS_ID: String
                          , var SORT_SEQ: String
                          , var REF_2: String
                          , var REF_3: String
                          , var REF_1: String
                          , var COMM_CD: String

    )
}
