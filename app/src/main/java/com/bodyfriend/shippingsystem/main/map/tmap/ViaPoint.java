
package com.bodyfriend.shippingsystem.main.map.tmap;

public class ViaPoint {

    public String viaPointId;
    public String viaPointName;
    public String viaDetailAddress;
    public String viaX;
    public String viaY;
    public String viaPoiId;
    public Integer viaTime;
    public String wishStartTime;
    public String wishEndTime;
   /* private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }*/

}
