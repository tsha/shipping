package com.bodyfriend.shippingsystem.main.delivery_order;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.databinding.FragmentDeliveryOrder2Binding;
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.DeliveryOrderAdapter;
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.LoiRecyclerViewClickListener;
import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.OutLoiList;
import com.bodyfriend.shippingsystem.main.delivery_order.net.OutLoiListData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseApi;
import com.bodyfriend.shippingsystem.main.main.MainActivity;

import org.jetbrains.annotations.NotNull;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by 이주영 on 2016-12-08.
 * 출고지시서 페이지이다.
 */

public class DeliveryOrderFragment2 extends BFFragment2 {

    public static final String TAG = DeliveryOrderFragment2.class.getSimpleName();
    // 상세화면 갈때 리절트 코드
    private final int RESULTCODE_SHIPDETAIL = 1234;
    private MainActivity.MainActivityControll mMainActivityControll;
    public static int ADD_PRODUCT_ITEM = 29;
    private FragmentDeliveryOrder2Binding mBinding;
    private DeliveryOrderAdapter mRecyclerAdapter;
    private ArrayList<OutLoiList.ForwardingItemData> mOutLoiList;


    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mMainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_delivery_order2, container, false);
        return mBinding.getRoot();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getOutLoiList();
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        setupTitle();
        setupRecyclerView();
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        getOutLoiList();
    }


    /**
     * 리사이클러뷰 설정
     */
    private void setupRecyclerView() {
        mBinding.recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.recyclerView.setLayoutManager(layoutManager);

        LoiRecyclerViewClickListener loiRecyclerViewClickListener = new LoiRecyclerViewClickListener() {
            @Override
            public void delLoiGroup(@NotNull View view, @NotNull String issueNo) {
                new AlertDialog.Builder(mContext).setMessage("선택항목 삭제 하시겠습니까?").setPositiveButton("확인", (dialog, which) -> {
                    dialog.cancel();
                    delExportLoi(issueNo);
                }).setNegativeButton("아니오", (dialog, which) -> {
                    dialog.cancel();
                }).show();
            }

            @Override
            public void delLoiItem(@NotNull View view, String issueNo, int ifNo) {
                new AlertDialog.Builder(mContext).setMessage("선택항목 삭제 하시겠습니까?").setPositiveButton("확인", (dialog, which) -> {
                    dialog.cancel();
                    delExportLoiItem(issueNo, ifNo);
                }).setNegativeButton("아니오", (dialog, which) -> {
                    dialog.cancel();
                }).show();

            }

            @Override
            public void addLoiItem(@NotNull View view, @NotNull String issueNo, @NotNull String fromSlCd) {
                Log.d(TAG, "addLoiItem : " + issueNo);
                startActivityForResult(ForwardingOrderActivity.newIntent(Objects.requireNonNull(getActivity()), issueNo, fromSlCd), ADD_PRODUCT_ITEM);
            }
        };
        mRecyclerAdapter = new DeliveryOrderAdapter(loiRecyclerViewClickListener);
        mBinding.recyclerView.setAdapter(mRecyclerAdapter);
        mBinding.recyclerView.setItemAnimator(new CustomItemAnimator());
    }


    /**
     * 개별 아이템 삭제
     *
     * @param ifNo
     */
    private void delExportLoiItem(String issueNo, int ifNo) {

        mBinding.progressBar.setVisibility(View.VISIBLE);
        DisposableObserver<BfData> disposableObserver = ServiceGenerator.createService(WarehouseApi.class)
                .delExportLoi(issueNo, ifNo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BfData>() {
                    @Override
                    public void onNext(BfData deliveryResponseData) {
                        if (deliveryResponseData.resultCode == 200) {
                            mBinding.progressBar.setVisibility(View.GONE);
                            getOutLoiList();
                        } else {
                            new AlertDialog.Builder(getContext())//
                                    .setMessage("삭제 실패하였습니다. ")
                                    .setTitle("출고지시서")
                                    .show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBinding.progressBar.setVisibility(View.GONE);
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT);
                    }

                    @Override
                    public void onComplete() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });

    }


    /**
     * 출고지시 번호로 그룹으로 삭제
     *
     * @param issueNo
     */
    private void delExportLoi(String issueNo) {

        mBinding.progressBar.setVisibility(View.VISIBLE);
        DisposableObserver<BfData> disposableObserver = ServiceGenerator.createService(WarehouseApi.class)
                .delExportLoi(issueNo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BfData>() {
                    @Override
                    public void onNext(BfData deliveryResponseData) {
                        if (deliveryResponseData.resultCode == 200) {
                            mBinding.progressBar.setVisibility(View.GONE);
                            getOutLoiList();
                        } else {
                            new AlertDialog.Builder(getContext())//
                                    .setMessage("삭제 실패하였습니다. ")
                                    .setTitle("출고지시서")
                                    .show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBinding.progressBar.setVisibility(View.GONE);
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT);
                    }

                    @Override
                    public void onComplete() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });

    }

    /**
     * 출고지시서 리트스 요청
     */
    @SuppressLint("CheckResult")
    private void getOutLoiList() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MONTH, -1);
        ServiceGenerator.createService(WarehouseApi.class)
                .selectOutLoiList(SDF.yyyymmdd_1.format(instance.getTime()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<OutLoiListData>() {
                    @Override
                    public void onNext(OutLoiListData outLoiListData) {
                        if (mOutLoiList != null) {
                            mOutLoiList.clear();
                        }
                        mBinding.progressBar.setVisibility(View.GONE);
                        if (outLoiListData.resultData != null) {
                            mRecyclerAdapter.setData(groupByIssueNo(outLoiListData.resultData));
                            mBinding.emptyView.setVisibility(View.GONE);
                            mOutLoiList = groupByIssueNo(outLoiListData.resultData);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBinding.progressBar.setVisibility(View.GONE);

//                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT);
                    }

                    @Override
                    public void onComplete() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });
    }


    private ArrayList<OutLoiList.ForwardingItemData> groupByIssueNo(List<OutLoiListData.ResultDatum> list) {
//        HashMap<>
        HashMap<String, List<OutLoiListData.ResultDatum>> hashMap = new HashMap<>();
        List<OutLoiListData.ResultDatum> sortList = list;

        for (OutLoiListData.ResultDatum item : list) { // issueNo로 hashmap에 리스트를 나눠서 담는다.
            List<OutLoiListData.ResultDatum> list2 = hashMap.get(item.issueNo);
            if (list2 == null) {
                list2 = new ArrayList<>();
            }
            list2.add(item);
            hashMap.put(item.issueNo, list2);
        }

        ArrayList<OutLoiList.ForwardingItemData> returnList = new ArrayList<OutLoiList.ForwardingItemData>();


        for (String key : hashMap.keySet()) {
            List<OutLoiListData.ResultDatum> list3 = hashMap.get(key);
//            Collections.sort(list3, (o1, o2) -> o2.reqDt.compareTo(o1.reqDt));
            int count = list3.size();
            OutLoiList.ForwardingItemData forwardingItemData = new OutLoiList.ForwardingItemData();
            for (OutLoiListData.ResultDatum item : list3) {
                String itemNm = item.itemNm;
                String serialCd = item.serialCd;
                String itemCd = item.itemCd;
                String erpFlag = item.erpFlag;
                int ifNo = item.ifNo;
                forwardingItemData.models.add(new OutLoiList.ForwardingItemData.models("1", itemNm, ifNo, erpFlag, serialCd));
            }
            forwardingItemData.loiNo = list3.get(0).issueNo;
            forwardingItemData.regDt = list3.get(0).reqDt;
            forwardingItemData.erpFlag = list3.get(0).erpFlag;
            forwardingItemData.fromSlCd = list3.get(0).fromSlCd;

            forwardingItemData.procStatNm = list3.get(0).procStatNm;
            forwardingItemData.modelTotal = count;
            returnList.add(forwardingItemData);
        }

        Collections.sort(returnList, (o1, o2) -> o2.regDt.compareTo(o1.regDt));

        return returnList;
    }


    /**
     * 타이틀 설정
     */
    private void setupTitle() {
        if (mMainActivityControll != null) {
            mMainActivityControll.changeTitle(R.layout.title_layout_delivery_order);
            mMainActivityControll.getView(R.id.date).setOnClickListener(v -> {
                startActivityForResult(new Intent(getContext(), ForwardingOrderActivity.class), ADD_PRODUCT_ITEM);
            });
        }
    }

}