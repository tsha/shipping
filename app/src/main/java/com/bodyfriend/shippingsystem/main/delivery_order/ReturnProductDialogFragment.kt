package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ReturnProductDialogBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.ReturnProductAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.WarehouseViewModel


class ReturnProductDialogFragment : AppCompatDialogFragment() {
    private lateinit var mBinding: ReturnProductDialogBinding
    private var itemGroup: String? = null
    private lateinit var returnProductAdapter: ReturnProductAdapter

    companion object {
        const val EXTRA_ITEM_GROUP = "EXTRA_ITEM_GROUP"
        const val EXTRA_CALLBACK_VIEW = "EXTRA_CALLBACK_VIEW"
        @JvmStatic
        fun newInstance(type: String, callback: Int): ReturnProductDialogFragment {
            val f = ReturnProductDialogFragment()
            // Supply num input as an argument.
            val args = Bundle()
            args.putString(EXTRA_ITEM_GROUP, type)
            args.putInt(EXTRA_CALLBACK_VIEW, callback)
            f.arguments = args
            return f
        }
    }


    interface ReturnProductDialogInterface {
        fun returnProductItem(returnProductItem: DeliverManStockData.ResultDatum, callbackView: Int)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        itemGroup = arguments?.getString(EXTRA_ITEM_GROUP)
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.return_product_dialog, null, false)
        val viewModel = ViewModelProviders.of(this).get(WarehouseViewModel::class.java)
        itemGroup?.let { viewModel.getReturnProductList(it) }
        mBinding.data = viewModel
        getObserver(viewModel)
        returnProductAdapter = ReturnProductAdapter(itemClick = {
            //            if (it.itemCd == "N/A") {
            arguments?.getInt(EXTRA_CALLBACK_VIEW)?.let { it1 ->
                val stockData: DeliverManStockData.ResultDatum = DeliverManStockData.ResultDatum(itemCd = it.itemCd,
                        serialCd = "", itemNm = it.itemNm, isValidCheck = true)
                (activity as (ReturnProductDialogInterface)).returnProductItem(stockData, it1)
                dismiss()
            }
            /*} else {
                viewModel.createBarcode(it)
            }*/
        })


        val mRecentLayoutManager = LinearLayoutManager(activity)
        mBinding.listWarehouse.apply {
            layoutManager = mRecentLayoutManager
            setHasFixedSize(true)
            adapter = returnProductAdapter
            addItemDecoration(DividerItemDecoration(activity, 1))
        }
        val alert = AlertDialog.Builder(activity)
        alert.apply {
            setView(mBinding.root)
        }

        return alert.create()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val style = STYLE_NO_FRAME
        setStyle(style, theme)

    }

    private fun getObserver(viewModel: WarehouseViewModel) {
        viewModel.returnProductDataList.observe(this, Observer {
            if (it != null) {
                returnProductAdapter.updateMealList(it)
            }
        })
        viewModel.isSessionOut.observe(this, Observer {
            if (it!!) {
                dismiss()
            }
        })
        /* viewModel.createBarcodeData.observe(this, Observer {
             arguments?.getInt(EXTRA_CALLBACK_VIEW)?.let { it1 ->
                 if (it != null) {
                     (activity as (ReturnProductDialogInterface)).returnProductItem(it, it1)
                 }
             }
             dismiss()
         })*/
    }
}
