package com.bodyfriend.shippingsystem.main.map.vo;

import android.location.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class MapShippingListData {
    public ResultData resultData;
    public String resultMsg;
    public int resultCode;

    public  class ResultData {
        public int listCount;
        public List<ShippingItem> list;

        public  class ShippingItem {

            public int index; // 이건 아답터에 표기될 순서
            public String DUE_DATE;
            public String DUE_DATE_COUNT;
            public boolean isDate; // 이건 아답터에 표기될 날짜( 날짜이면 아래의 값들은 없다.)
            public String CUST_UPDATE;
            public String DIV_DATE;
            public String PROMISE;
            public String PROMISE_TIME;
            public String HPHONE_NO;
            public String AREA;
            public String PRODUCT_NAME;
            public String FREEGIFT_ONE;
            public String FREEGIFT_TWO;
            public String TEL_NO;
            public String PURCHASING_OFFICE;
            public String SHIPPING_SEQ;
            public String SERVICE_MONTH;
            public String SHIPPING_TYPE;
            public String INSADDR;
            public String CUST_NAME;
            public String SYMPTOM;
            public String DELIVERYMAN;
            public String MUST_FLAG;
            public int QTY;
            public String SHIPPING_PS;
            public String PROMISE_FAIL_PS;
            public String INS_COMPLETE;
            public String INS_COMPLATE;
            public String PROGRESS_NO;
            public String EXPIRE_FLAG;
            public String RELOCATION_CHECK;
            public boolean HAS_4_MSG;
            public String COMPANY_CHECK;
            public String RECEIVE_DATE;
            public String tMapWishStartTime;
            public String tMapWishEndTime;

            public Location location;

            public boolean cardToggle;

            public void setCardToggle(boolean cardToggle) {
                this.cardToggle = cardToggle;
            }



            public boolean isCardToggle() {
                return cardToggle;
            }

            public void setLocation(Location location) {
                this.location = location;
            }

            @Override
            public String toString() {
                return "list{" +
                        "index=" + index +
                        ", DUE_DATE='" + DUE_DATE + '\'' +
                        ", DUE_DATE_COUNT='" + DUE_DATE_COUNT + '\'' +
                        ", isDate=" + isDate +
                        ", CUST_UPDATE='" + CUST_UPDATE + '\'' +
                        ", DIV_DATE='" + DIV_DATE + '\'' +
                        ", PROMISE='" + PROMISE + '\'' +
                        ", PROMISE_TIME='" + PROMISE_TIME + '\'' +
                        ", HPHONE_NO='" + HPHONE_NO + '\'' +
                        ", AREA='" + AREA + '\'' +
                        ", PRODUCT_NAME='" + PRODUCT_NAME + '\'' +
                        ", FREEGIFT_ONE='" + FREEGIFT_ONE + '\'' +
                        ", TEL_NO='" + TEL_NO + '\'' +
                        ", PURCHASING_OFFICE='" + PURCHASING_OFFICE + '\'' +
                        ", SHIPPING_SEQ='" + SHIPPING_SEQ + '\'' +
                        ", SERVICE_MONTH='" + SERVICE_MONTH + '\'' +
                        ", SHIPPING_TYPE='" + SHIPPING_TYPE + '\'' +
                        ", INSADDR='" + INSADDR + '\'' +
                        ", CUST_NAME='" + CUST_NAME + '\'' +
                        ", DELIVERYMAN='" + DELIVERYMAN + '\'' +
                        ", MUST_FLAG='" + MUST_FLAG + '\'' +
                        ", QTY=" + QTY +
                        ", SHIPPING_PS='" + SHIPPING_PS + '\'' +
                        ", PROMISE_FAIL_PS='" + PROMISE_FAIL_PS + '\'' +
                        ", INS_COMPLETE='" + INS_COMPLETE + '\'' +
                        ", INS_COMPLATE='" + INS_COMPLATE + '\'' +
                        ", PROGRESS_NO='" + PROGRESS_NO + '\'' +
                        ", EXPIRE_FLAG='" + EXPIRE_FLAG + '\'' +
                        ", RELOCATION_CHECK='" + RELOCATION_CHECK + '\'' +
                        ", HAS_4_MSG=" + HAS_4_MSG +
                        ", COMPANY_CHECK='" + COMPANY_CHECK + '\'' +
                        ", location=" + location +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                ShippingItem that = (ShippingItem) o;
                return index == that.index &&
                        isDate == that.isDate &&
                        QTY == that.QTY &&
                        HAS_4_MSG == that.HAS_4_MSG &&
                        Objects.equals(DUE_DATE, that.DUE_DATE) &&
                        Objects.equals(DUE_DATE_COUNT, that.DUE_DATE_COUNT) &&
                        Objects.equals(CUST_UPDATE, that.CUST_UPDATE) &&
                        Objects.equals(DIV_DATE, that.DIV_DATE) &&
                        Objects.equals(PROMISE, that.PROMISE) &&
                        Objects.equals(PROMISE_TIME, that.PROMISE_TIME) &&
                        Objects.equals(HPHONE_NO, that.HPHONE_NO) &&
                        Objects.equals(AREA, that.AREA) &&
                        Objects.equals(PRODUCT_NAME, that.PRODUCT_NAME) &&
                        Objects.equals(FREEGIFT_ONE, that.FREEGIFT_ONE) &&
                        Objects.equals(FREEGIFT_TWO, that.FREEGIFT_TWO) &&
                        Objects.equals(TEL_NO, that.TEL_NO) &&
                        Objects.equals(PURCHASING_OFFICE, that.PURCHASING_OFFICE) &&
                        Objects.equals(SHIPPING_SEQ, that.SHIPPING_SEQ) &&
                        Objects.equals(SERVICE_MONTH, that.SERVICE_MONTH) &&
                        Objects.equals(SHIPPING_TYPE, that.SHIPPING_TYPE) &&
                        Objects.equals(INSADDR, that.INSADDR) &&
                        Objects.equals(CUST_NAME, that.CUST_NAME) &&
                        Objects.equals(SYMPTOM, that.SYMPTOM) &&
                        Objects.equals(DELIVERYMAN, that.DELIVERYMAN) &&
                        Objects.equals(MUST_FLAG, that.MUST_FLAG) &&
                        Objects.equals(SHIPPING_PS, that.SHIPPING_PS) &&
                        Objects.equals(PROMISE_FAIL_PS, that.PROMISE_FAIL_PS) &&
                        Objects.equals(INS_COMPLETE, that.INS_COMPLETE) &&
                        Objects.equals(INS_COMPLATE, that.INS_COMPLATE) &&
                        Objects.equals(PROGRESS_NO, that.PROGRESS_NO) &&
                        Objects.equals(EXPIRE_FLAG, that.EXPIRE_FLAG) &&
                        Objects.equals(RELOCATION_CHECK, that.RELOCATION_CHECK) &&
                        Objects.equals(COMPANY_CHECK, that.COMPANY_CHECK) &&
                        Objects.equals(RECEIVE_DATE, that.RECEIVE_DATE) &&
                        Objects.equals(location, that.location) &&
                        Objects.equals(deliveryItems, that.deliveryItems);
            }

            @Override
            public int hashCode() {

                return Objects.hash(index, DUE_DATE, DUE_DATE_COUNT, isDate, CUST_UPDATE, DIV_DATE, PROMISE, PROMISE_TIME, HPHONE_NO, AREA, PRODUCT_NAME, FREEGIFT_ONE, FREEGIFT_TWO, TEL_NO, PURCHASING_OFFICE, SHIPPING_SEQ, SERVICE_MONTH, SHIPPING_TYPE, INSADDR, CUST_NAME, SYMPTOM, DELIVERYMAN, MUST_FLAG, QTY, SHIPPING_PS, PROMISE_FAIL_PS, INS_COMPLETE, INS_COMPLATE, PROGRESS_NO, EXPIRE_FLAG, RELOCATION_CHECK, HAS_4_MSG, COMPANY_CHECK, RECEIVE_DATE, location, deliveryItems);
            }

            private HashMap<String, Integer> deliveryItems = new HashMap<>();

            public HashMap<String, Integer> getDeliveryItems() {
                return deliveryItems;
            }

            public void setDeliveryItems(HashMap<String, Integer> deliveryItems) {
                this.deliveryItems = deliveryItems;
            }
        }

        @Override
        public String toString() {
            return "resultData{" +
                    "listCount=" + listCount +
                    ", list=" + list +
                    '}';
        }
    }
}
