package com.bodyfriend.shippingsystem.main.shipping_list.net

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ShippingMasterDetailData(var resultCode: Int = 200, var resultMsg: String = "", @SerializedName("resultData") var resultData: ResultData) {
    data class ResultData(var INS_COMPLETE_NM: String = "",
                          var PICTURE_PATH_THREE: String = "",
                          var TEL_NO2: String = "",
                          var FREEGIFT_ONE: String = "",
                          var INSADDR2: String = "",
                          var PURCHASING_OFFICE: String = "",
                          var imagesign: String = "",
                          var COLORTYPE: String = "",
                          var PICTURE_PATH_SIX: String = "",
                          var PRODUCT_SN: String = "",
                          var AREA2: String = "",
                          var imagefour: String = "",
                          var PICTURE_PATH_TWO: String = "",
                          var COMPANY_CHECK: String = "",
                          var MUST_FLAG: String = "",
                          var FREEGIFT_CHECK_TWO: String = "",
                          var PICTURE_PATH_FIVE: String = "",
                          var REFER_NAME: String = "",
                          var AGREE_NAME: String = "",
                          var CASH_RECEIPT_CARD_NUM: String = "",
                          var PROMISE: String = "",
                          var BACK_SN: String = "",
                          var WATER_COOKING: String = "",
                          var BACK_PICTURE_TWO: String = "",
                          var DUE_DATE: String = "",
                          var DELIVERYMAN1: String = "",
                          var SERVER_TIME: String = "",
                          var PROMISE_FAIL_CD_NM: String = "",
                          var SERVICE_MONTH: String = "",
                          var DELIVERYMAN3: String = "",
                          var DELIVERYMAN2: String = "",
                          var DIV_STATUS: String = "",
                          var backimageone: String = "",
                          var imagetwo: String = "",
                          var CUST_NAME: String = "",
                          var backimagethree: String = "",
                          var WATER_ADAPTOR: String = "",
                          var PRODUCT_TYPE: String = "",
                          var FREEGIFT_TWO: String = "",
                          var PROMISE_FAIL_PS: String = "",
                          var FAX_NO: String = "",
                          var imagethree: String = "",
                          var PICTURE_PATH_SIGN: String = "",
                          var BACK_PICTURE_ONE: String = "",
                          var FREEGIFT_CHECK_ONE: String = "",
                          var PRODUCT_NAME: String = "",
                          var PRODUCT_CODE: String = "",
                          var PROMISE_FAIL_CD: String = "",
                          var COST_AMOUNT: String = "",
                          var INS_COMPLETE: String = "",
                          var backimagetwo: String = "",
                          var PICTURE_PATH_ONE: String = "",
                          var HPHONE_NO2: String = "",
                          var BODY_NO: String = "",
                          var COSTNCOST: String = "",
                          var PAYMENT_TYPE: String = "",
                          var QTY: String = "",
                          var PRODUCT_TYPE_NM: String = "",
                          var HPHONE_NO: String = "",
                          var imagesix: String = "",
                          var imageone: String = "",
                          var SYMPTOM: String = "",
                          var PROMISE_TIME: String = "",
                          var DELIVERYMAN_NM2: String = "",
                          var AREA: String = "",
                          var BACK_PICTURE_THREE: String = "",
                          var DELIVERYMAN_NM3: String = "",
                          var PROGRESS_NO: String = "",
                          var DELIVERYMAN_NM1: String = "",
                          var TEL_NO: String = "",
                          var PICTURE_PATH_FOUR: String = "",
                          var SHIPPING_SEQ: String = "",
                          var SHIPPING_TYPE: String = "",
                          var BACK_DATE: String = "",
                          var INSADDR: String = "",
                          var imagefive: String = "",
                          var PROGRESS_NO_NM: String = "",
                          var IN_DATE: String = "",
                          var DELYN: String = "",
                          var RECEIVE_DATE: String = "",
                          var CUST_UPDATE: String = "",
                          var SHIPPING_PS: String = "",
                          var CASH_RECEIPT_TYPE: String = "",
                          var L_FREEGIFT: String = "", // 선택한 라텍스 사은품 코드,
                          var L_FREEGIFT_NM: String = "", // 선택한 라텍스 사은품 이름,
                          var IS_LADDER: Boolean = false, // 사다리차 사용여부,
                          var LADDER_PRICE: String = "", // 사다리차 금액,
                          var ladder_image_one: String = "", // 사다리차 사진1,
                          var ladder_image_two: String = "", // 사다리차 사진2,
                          var ladder_image_three: String = "", //사다리차 사진3,
                          var LADDER_PAYMENT_TYPE: Int = 0,// 페이먼트 타입,
                          var free_gift_warn: Boolean = false, // 사은품 여부,
                          var IS_SOCKET: Boolean = false, // 여부,
                          var socket_image_one: String = "", // 양중1,
                          var socket_image_two: String = "", // 양중 2,
                          var socket_image_three: String = "", // 양중 3,
                          var SOCKET_PICTURE_ONE: String = "", // 완료 양중1,
                          var SOCKET_PICTURE_TWO: String = "", // 완료 양중 2,
                          var SOCKET_PICTURE_THREE: String = "", // 완료 양중 3,
                          var ADDITIONAL_LADDER_PRICE: String = "", // 사다리 추가 요금,
                          var m_extra_cost_memo: String = "", // 사다리 비용 비고,
                          var FREEGIFT_ADDR: String = "", // 사은품 주소,
                          var collect_image_one: String = "", // 회수 사진,
                          var collect_image_two: String = "", // 회수 사진,
                          var service_image_one: String = "", // 내림서비스1,
                          var service_image_two: String = "", // 내림서비스2
                          var CARNO: String = "" // 차량번호
    ) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readByte() != 0.toByte(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readInt(),
                parcel.readByte() != 0.toByte(),
                parcel.readByte() != 0.toByte(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString()) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(INS_COMPLETE_NM)
            parcel.writeString(PICTURE_PATH_THREE)
            parcel.writeString(TEL_NO2)
            parcel.writeString(FREEGIFT_ONE)
            parcel.writeString(INSADDR2)
            parcel.writeString(PURCHASING_OFFICE)
            parcel.writeString(imagesign)
            parcel.writeString(COLORTYPE)
            parcel.writeString(PICTURE_PATH_SIX)
            parcel.writeString(PRODUCT_SN)
            parcel.writeString(AREA2)
            parcel.writeString(imagefour)
            parcel.writeString(PICTURE_PATH_TWO)
            parcel.writeString(COMPANY_CHECK)
            parcel.writeString(MUST_FLAG)
            parcel.writeString(FREEGIFT_CHECK_TWO)
            parcel.writeString(PICTURE_PATH_FIVE)
            parcel.writeString(REFER_NAME)
            parcel.writeString(AGREE_NAME)
            parcel.writeString(CASH_RECEIPT_CARD_NUM)
            parcel.writeString(PROMISE)
            parcel.writeString(BACK_SN)
            parcel.writeString(WATER_COOKING)
            parcel.writeString(BACK_PICTURE_TWO)
            parcel.writeString(DUE_DATE)
            parcel.writeString(DELIVERYMAN1)
            parcel.writeString(SERVER_TIME)
            parcel.writeString(PROMISE_FAIL_CD_NM)
            parcel.writeString(SERVICE_MONTH)
            parcel.writeString(DELIVERYMAN3)
            parcel.writeString(DELIVERYMAN2)
            parcel.writeString(DIV_STATUS)
            parcel.writeString(backimageone)
            parcel.writeString(imagetwo)
            parcel.writeString(CUST_NAME)
            parcel.writeString(backimagethree)
            parcel.writeString(WATER_ADAPTOR)
            parcel.writeString(PRODUCT_TYPE)
            parcel.writeString(FREEGIFT_TWO)
            parcel.writeString(PROMISE_FAIL_PS)
            parcel.writeString(FAX_NO)
            parcel.writeString(imagethree)
            parcel.writeString(PICTURE_PATH_SIGN)
            parcel.writeString(BACK_PICTURE_ONE)
            parcel.writeString(FREEGIFT_CHECK_ONE)
            parcel.writeString(PRODUCT_NAME)
            parcel.writeString(PRODUCT_CODE)
            parcel.writeString(PROMISE_FAIL_CD)
            parcel.writeString(COST_AMOUNT)
            parcel.writeString(INS_COMPLETE)
            parcel.writeString(backimagetwo)
            parcel.writeString(PICTURE_PATH_ONE)
            parcel.writeString(HPHONE_NO2)
            parcel.writeString(BODY_NO)
            parcel.writeString(COSTNCOST)
            parcel.writeString(PAYMENT_TYPE)
            parcel.writeString(QTY)
            parcel.writeString(PRODUCT_TYPE_NM)
            parcel.writeString(HPHONE_NO)
            parcel.writeString(imagesix)
            parcel.writeString(imageone)
            parcel.writeString(SYMPTOM)
            parcel.writeString(PROMISE_TIME)
            parcel.writeString(DELIVERYMAN_NM2)
            parcel.writeString(AREA)
            parcel.writeString(BACK_PICTURE_THREE)
            parcel.writeString(DELIVERYMAN_NM3)
            parcel.writeString(PROGRESS_NO)
            parcel.writeString(DELIVERYMAN_NM1)
            parcel.writeString(TEL_NO)
            parcel.writeString(PICTURE_PATH_FOUR)
            parcel.writeString(SHIPPING_SEQ)
            parcel.writeString(SHIPPING_TYPE)
            parcel.writeString(BACK_DATE)
            parcel.writeString(INSADDR)
            parcel.writeString(imagefive)
            parcel.writeString(PROGRESS_NO_NM)
            parcel.writeString(IN_DATE)
            parcel.writeString(DELYN)
            parcel.writeString(RECEIVE_DATE)
            parcel.writeString(CUST_UPDATE)
            parcel.writeString(SHIPPING_PS)
            parcel.writeString(CASH_RECEIPT_TYPE)
            parcel.writeString(L_FREEGIFT)
            parcel.writeString(L_FREEGIFT_NM)
            parcel.writeByte(if (IS_LADDER) 1 else 0)
            parcel.writeString(LADDER_PRICE)
            parcel.writeString(ladder_image_one)
            parcel.writeString(ladder_image_two)
            parcel.writeString(ladder_image_three)
            parcel.writeInt(LADDER_PAYMENT_TYPE)
            parcel.writeByte(if (free_gift_warn) 1 else 0)
            parcel.writeByte(if (IS_SOCKET) 1 else 0)
            parcel.writeString(socket_image_one)
            parcel.writeString(socket_image_two)
            parcel.writeString(socket_image_three)
            parcel.writeString(SOCKET_PICTURE_ONE)
            parcel.writeString(SOCKET_PICTURE_TWO)
            parcel.writeString(SOCKET_PICTURE_THREE)
            parcel.writeString(ADDITIONAL_LADDER_PRICE)
            parcel.writeString(m_extra_cost_memo)
            parcel.writeString(FREEGIFT_ADDR)
            parcel.writeString(collect_image_one)
            parcel.writeString(collect_image_two)
            parcel.writeString(service_image_one)
            parcel.writeString(service_image_two)
            parcel.writeString(CARNO)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<ResultData> {
            override fun createFromParcel(parcel: Parcel): ResultData {
                return ResultData(parcel)
            }

            override fun newArray(size: Int): Array<ResultData?> {
                return arrayOfNulls(size)
            }
        }
    }

}