package com.bodyfriend.shippingsystem.main.refuel;

import android.databinding.BaseObservable;

import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.refuel.net.Sheet;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RefuelViewModel extends BaseObservable {

    private final Callback mCallback;

    public interface Callback {
        void onSuccess();
    }

    public RefuelViewModel(Callback callback) {
        mCallback = callback;
    }


    /**
     * 주유비 업데이트
     */
    public void updateGas(String carNumber, String name, String gasLiter
            , String gasPrice, String dashboard
            , String dieselExhaustFluid, String ectPrice
            , String carChang, String ownerCard, String dieselExhaustFluidLiter) {

        Disposable subscribe = ServiceGenerator.changeApiBaseUrl(Sheet.class, NetConst.GOOGEL_GAS_SCRIPT_URL)
                .updateGas(SDF.yyyymmddhhmm.format(System.currentTimeMillis()), name
                        , carNumber, gasLiter
                        , gasPrice, dashboard
                        , dieselExhaustFluid, ectPrice
                        , carChang, ownerCard
                        , dieselExhaustFluidLiter)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(voidResponse -> {

                    if (voidResponse.code() == 200) {
                        mCallback.onSuccess();
                    }
                });
    }
}
