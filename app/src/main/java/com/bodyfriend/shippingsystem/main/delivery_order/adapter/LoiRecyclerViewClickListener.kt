package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.view.View

interface LoiRecyclerViewClickListener {
    fun delLoiGroup(view: View, issueNo: String)
    fun delLoiItem(view: View, issueNo: String, ifNo: Int)
    fun addLoiItem(view: View, issueNo: String, fromSlCd: String)


}