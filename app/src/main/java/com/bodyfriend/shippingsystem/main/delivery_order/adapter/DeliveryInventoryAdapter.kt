package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemInventroyBinding
import com.bodyfriend.shippingsystem.main.delivery_order.DeliveryInventoryDialogFragment
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData


class DeliveryInventoryAdapter(val itemClick: (DeliverManStockData.ResultDatum) -> Unit) : RecyclerView.Adapter<DeliveryInventoryAdapter.ViewHolder>() {


    private var mealList: MutableList<DeliverManStockData.ResultDatum> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val resultDatum = mealList[position]
        holder.view.item = resultDatum
        holder.view.cardView.setOnClickListener { itemClick(resultDatum) }

    }


    fun updateInventoryList(postList: MutableList<DeliverManStockData.ResultDatum>) {
        this.mealList = postList
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemInventroyBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_inventroy, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<DeliverManStockData.ResultDatum> {
        return mealList
    }

    class ViewHolder(var view: ItemInventroyBinding) : RecyclerView.ViewHolder(view.root)


}