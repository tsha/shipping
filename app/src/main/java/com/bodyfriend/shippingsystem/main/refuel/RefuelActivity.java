package com.bodyfriend.shippingsystem.main.refuel;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.databinding.ActivityRefuelBinding;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list.SelectCarActivity;

public class RefuelActivity extends AppCompatActivity implements RefuelViewModel.Callback {

    private ActivityRefuelBinding mBinding;
    private RefuelViewModel mViewModel;
    private final int REQUEST_CHANGE_CAR = 1003;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_refuel);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_refuel);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mBinding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        mViewModel = new RefuelViewModel(this);
        mBinding.setViewModel(mViewModel);
        mBinding.carNoTv.setText(PP.carNumber.get());
        setEvent();
    }

    private void setEvent() {
        mBinding.commitBtn.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mBinding.dashboardIdt.getText().toString().trim()) ) {
                showRefuelDialog();
            } else {
                Toast.makeText(this, "주행거리 입력하세요!!!", Toast.LENGTH_SHORT).show();
            }
        });
        mBinding.carNoChangeBt.setOnClickListener(v -> {
            Intent intent = new Intent(RefuelActivity.this, SelectCarActivity.class);
            startActivityForResult(intent, REQUEST_CHANGE_CAR);
        });
    }

    private void showRefuelDialog() {
        String gasLiter = TextUtils.isEmpty(mBinding.gasLiterIdt.getText().toString()) ? "0" : mBinding.gasLiterIdt.getText().toString();
        String gasPrice = TextUtils.isEmpty(mBinding.gasPriceIdt.getText().toString()) ? "0" : mBinding.gasPriceIdt.getText().toString();
        String dieselExhaustFluid = TextUtils.isEmpty(mBinding.dieselExhaustFluidIdt.getText().toString()) ? "0" : mBinding.dieselExhaustFluidIdt.getText().toString();
        String ectPriceIdt = TextUtils.isEmpty(mBinding.ectPriceIdt.getText().toString()) ? "0" : mBinding.ectPriceIdt.getText().toString();
        String dieselExhaustFluidLiter = TextUtils.isEmpty(mBinding.dieselExhaustFluidLiteIdt.getText().toString()) ? "0" : mBinding.dieselExhaustFluidLiteIdt.getText().toString();
        String dashboard = TextUtils.isEmpty(mBinding.dashboardIdt.getText().toString()) ? "0" : mBinding.dashboardIdt.getText().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("주유비 등록")
                .setMessage(" 주유 리터 : " + gasLiter
                        + "\n 주유 금액 : " + gasPrice
                        + "\n 주행 누계 : " + dashboard
                        + "\n 요소수 리터 : " + dieselExhaustFluidLiter
                        + "\n 요소수 금액 : " + dieselExhaustFluid
                        + "\n 기타 금액 : " + ectPriceIdt
                )
                .setPositiveButton("확인", (dialog, which) -> {
                            // 주유비 업데이트 버튼
                            mBinding.progressBar3.setVisibility(View.VISIBLE);
                            mViewModel.updateGas(
                                    PP.carNumber.get(), Auth.d.resultData.ADMIN_NM
                                    , mBinding.gasLiterIdt.getText().toString()
                                    , mBinding.gasPriceIdt.getText().toString()
                                    , mBinding.dashboardIdt.getText().toString()
                                    , mBinding.dieselExhaustFluidIdt.getText().toString()
                                    , mBinding.ectPriceIdt.getText().toString()
                                    , mBinding.carChangCb.isChecked() ? "O" : ""
                                    , mBinding.ownerCardIdt.getText().toString()
                                    , mBinding.dieselExhaustFluidLiteIdt.getText().toString()
                            );
                        }
                )
                .setNegativeButton("취소", (dialog, which) -> {
                });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case REQUEST_CHANGE_CAR:
                    resultCarNumber(data);
                    break;
            }
        }
    }

    private void resultCarNumber(Intent data) {
        String carNumber = data.getStringExtra(SelectCarActivity.EXTRA.CAR_NUMBER);
        if (!TextUtils.isEmpty(carNumber)) {
            mBinding.carNoTv.setText(carNumber);
            PP.carNumber.set(carNumber);
        }
    }

    @Override
    public void onSuccess() {
        if (!isFinishing()) {
            mBinding.progressBar3.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("주유비 등록")
                    .setMessage("등록 완료")
                    .setPositiveButton("확인", (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    });

            builder.show();
        }
    }
}
