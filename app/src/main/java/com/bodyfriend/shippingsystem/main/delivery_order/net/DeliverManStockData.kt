package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DeliverManStockData(@SerializedName("resultData")
                               @Expose
                               var resultData: List<ResultDatum>) {


    data class ResultDatum(@SerializedName("BAD_AMOUNT")
                           @Expose
                           var badAmount: Int = -1,
                           @SerializedName("SL_CD")
                           @Expose
                           var slCd: String = "",
                           @SerializedName("SL_NM")
                           @Expose
                           var slNm: String = "",
                           @SerializedName("ITEM_NM")
                           @Expose
                           var itemNm: String,
                           @SerializedName("ITEM_CD")
                           @Expose
                           var itemCd: String,
                           @SerializedName("SERIAL_CD")
                           @Expose
                           var serialCd: String,
                           @SerializedName("GOOD_AMOUNT")
                           @Expose
                           var goodAmount: Int = -1,
                           @SerializedName("GIFT_YN")
                           @Expose
                           var giftYn: String = "",
                           @SerializedName("RENTAL_NM")
                           @Expose
                           var rentalNm: String = "",


                           @SerializedName("QTY")
                           @Expose
                           var qty: Int = 1,


                           @SerializedName("LOT_FLG")
                           @Expose
                           var lotFlg: String = "",
                           var isValidCheck: Boolean = false
                           , var isValid: Boolean = false
    )


}
