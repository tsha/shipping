package com.bodyfriend.shippingsystem.main.sale;


public interface SaleProductEvent {
    void onEventClicked(SelectBizSalesData.ResultData.List list);
}
