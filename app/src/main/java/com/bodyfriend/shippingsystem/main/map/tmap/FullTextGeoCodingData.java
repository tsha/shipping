package com.bodyfriend.shippingsystem.main.map.tmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FullTextGeoCodingData {
    @SerializedName("coordinateInfo")
    @Expose
    public CoordinateInfo coordinateInfo;

    public class CoordinateInfo {

        @SerializedName("coordType")
        @Expose
        public String coordType;
        @SerializedName("addressFlag")
        @Expose
        public String addressFlag;
        @SerializedName("page")
        @Expose
        public String page;
        @SerializedName("count")
        @Expose
        public String count;
        @SerializedName("totalCount")
        @Expose
        public String totalCount;
        @SerializedName("coordinate")
        @Expose
        public List<Coordinate> coordinate = null;

        public class Coordinate {

            @SerializedName("matchFlag")
            @Expose
            public String matchFlag;
            @SerializedName("lat")
            @Expose
            public String lat;
            @SerializedName("lon")
            @Expose
            public String lon;
            @SerializedName("latEntr")
            @Expose
            public String latEntr;
            @SerializedName("lonEntr")
            @Expose
            public String lonEntr;
            @SerializedName("city_do")
            @Expose
            public String cityDo;
            @SerializedName("gu_gun")
            @Expose
            public String guGun;
            @SerializedName("eup_myun")
            @Expose
            public String eupMyun;
            @SerializedName("legalDong")
            @Expose
            public String legalDong;
            @SerializedName("adminDong")
            @Expose
            public String adminDong;
            @SerializedName("ri")
            @Expose
            public String ri;
            @SerializedName("bunji")
            @Expose
            public String bunji;
            @SerializedName("buildingName")
            @Expose
            public String buildingName;
            @SerializedName("buildingDong")
            @Expose
            public String buildingDong;
            @SerializedName("newMatchFlag")
            @Expose
            public String newMatchFlag;
            @SerializedName("newLat")
            @Expose
            public String newLat;
            @SerializedName("newLon")
            @Expose
            public String newLon;
            @SerializedName("newLatEntr")
            @Expose
            public String newLatEntr;
            @SerializedName("newLonEntr")
            @Expose
            public String newLonEntr;
            @SerializedName("newRoadName")
            @Expose
            public String newRoadName;
            @SerializedName("newBuildingIndex")
            @Expose
            public String newBuildingIndex;
            @SerializedName("newBuildingName")
            @Expose
            public String newBuildingName;
            @SerializedName("newBuildingCateName")
            @Expose
            public String newBuildingCateName;
            @SerializedName("newBuildingDong")
            @Expose
            public String newBuildingDong;
            @SerializedName("zipcode")
            @Expose
            public String zipcode;
            @SerializedName("remainder")
            @Expose
            public String remainder;

        }
    }

}
