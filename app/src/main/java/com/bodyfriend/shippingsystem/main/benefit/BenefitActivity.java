package com.bodyfriend.shippingsystem.main.benefit;


import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.databinding.ActivityBenefitBinding;

import java.util.Objects;

public class BenefitActivity extends AppCompatActivity implements BenefitViewModel.BenefitViewModelCallback {

    private ActivityBenefitBinding mBinding;
    public final ObservableArrayList<BenefitSummaryData2.ItemBenefit> mBenefitList = new ObservableArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_benefit);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mBinding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        BenefitViewModel benefitViewModel = new BenefitViewModel(this, this);
        mBinding.setViewModel(benefitViewModel);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        BenefitAdapter mAdapter = new BenefitAdapter();
        mBinding.recyclerView.setAdapter(mAdapter);
        mBinding.recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL));
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    @Override
    public void onSummaryBenefit(BenefitSummaryData2 benefitData) {

        mBenefitList.clear();
        mBenefitList.addAll(benefitData.listHashMap);

        mBinding.setBenefitList(mBenefitList);
    }

    @Override
    public void onEmptyBenefit() {
        Log.d("onEmptyBenefit");
    }
}
