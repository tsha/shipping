package com.bodyfriend.shippingsystem.main.sale;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class SelectBizSalesViewModel extends ViewModel {
    MutableLiveData<ArrayList<SelectBizSalesData.ResultData.List>> salesData = new MutableLiveData<>();

    public LiveData<ArrayList<SelectBizSalesData.ResultData.List>> getSalesData() {
        return salesData;
    }

    public void getBizSaleList(String employeeId) {
        DisposableObserver<SelectBizSalesData> benefitDataDisposableObserver = ServiceGenerator.changeApiBaseUrl(SaleApi.class, NetConst.HOST_SALE)
                .selectBizSales(employeeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<SelectBizSalesData>() {

                    @Override
                    public void onNext(SelectBizSalesData benefitData) {
                        ArrayList<SelectBizSalesData.ResultData.List> list = benefitData.resultData.list;
                        if (list != null) {
                            salesData.postValue(list);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

}
