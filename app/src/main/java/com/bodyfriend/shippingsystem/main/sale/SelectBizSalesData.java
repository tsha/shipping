package com.bodyfriend.shippingsystem.main.sale;



import java.util.ArrayList;

public class SelectBizSalesData {
    public ResultData resultData;

    public class ResultData {
        public int listCount;
        public ArrayList<List> list;

        public class List {
            public String IN_USER_NAME;//": "김순자",2
            public String IN_TEL;//": "01083326928",
            public String SALES_TYPE;//": "렌탈",
            public String PRODUCT_NAME;//": "안마의자",
            public String IN_HANDPHONE;//": "",
            public String MANAGER_NAME;//": "이재원",
            public String N_TYPE;//": "기존",
            public int SALES_SEQ;//": 224,
            public String MANAGER_ID;//": "j1body",
            public String REG_DATE;//": "2019-01-28",
            public String ETCCLAUSAL;//": "구매의사 있으니 전화달라 말씀하심",
            public String PRODUCT_TYPE;//": "M",
            public String SALES_CONFIRM;//": "",
            public String RECEIVE_PS;//": "",
            public String PRODUCT_TYPE_NM;//": "안마의자",
            public String IN_USER_NO;//": ""
        }
    }
}
