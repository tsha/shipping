package com.bodyfriend.shippingsystem.main.product;

import android.app.ProgressDialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFProgressDlg;
import com.bodyfriend.shippingsystem.base.image.AUIL;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.product.youtubu.DeveloperKey;
import com.bodyfriend.shippingsystem.main.product.youtubu.YouTubeFailureRecoveryActivity;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.product.net.appProductDetail;
import com.bodyfriend.shippingsystem.main.product.net.appProductDetail.Data.resultData;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ProductDescriptionDetailActivity extends YouTubeFailureRecoveryActivity {

	public static final String TAG = "ProductDescriptionDetail";

	public static class EXTRA {
		public static final String PRODUCTCODE = "PRODUCTCODE";

	}

	private YouTubePlayerView youTubeView;

	@Override
	protected void onCreate(Bundle arg0) {

		super.onCreate(arg0);
		setContentView(R.layout.product_discription_detail_activity2);

		youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
//		youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);


		ApplicationInfo appInfo = null;
		try {
			appInfo = getPackageManager().getApplicationInfo(this.getPackageName(), PackageManager.GET_META_DATA);
			Bundle aBundle = appInfo.metaData;
			String aValue = aBundle.getString("youtubeKey");
			youTubeView.initialize(aValue, this);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		
		String productCode = getIntent().getStringExtra(EXTRA.PRODUCTCODE);
		Net.async(new appProductDetail(productCode), onProductDetailNetResponse);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
//		loadVideo(product_intro);
	}

	private void loadVideo(String id) {

		if (player != null && id != null && id.length() > 0) {
			try {
				player.loadVideo(id);
				player.setFullscreen(false);
				youTubeView.setVisibility(View.VISIBLE);
				findViewById(R.id.product_image_l).setVisibility(View.GONE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected Provider getYouTubePlayerProvider() {
		return null;
	}

	YouTubePlayer player;

	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer player, boolean wasRestored) {
		
		if (!wasRestored) {
			this.player = player;
			loadVideo(product_intro);
		}
	}

	public void onClick(View view) {
		int id = view.getId();
		if (id == R.id.btn_back) {
			finish();
		} else {
		}
	}

	/** AUIL */
	protected void setImage(int resid, String image) {
		setImage(resid, image, AUIL.options);
	}

	protected void setImage(int resid, String image, DisplayImageOptions options) {
		ImageLoader.getInstance().displayImage(image, (ImageView) findViewById(resid), options);
	}

	protected static void setImage(ImageView imageview, String image, DisplayImageOptions options) {
		ImageLoader.getInstance().displayImage(image, imageview, options);
	}

	protected static void setImage(ImageView imageview, String image) {
		ImageLoader.getInstance().displayImage(image, imageview, AUIL.options);
	}

	private String product_intro;
	private Net.OnNetResponse<appProductDetail> onProductDetailNetResponse = new Net.OnNetResponse<appProductDetail>() {

		@Override
		public void onResponse(appProductDetail response) {

			if (Auth.checkSession(response.data.resultCode)) {
				Auth.startLogin(ProductDescriptionDetailActivity.this);
				return;
			} else if (response.data.resultCode == -1) {
				toast(response.data.resultMsg);
				return;
			}

			final resultData r = response.data.resultData;
			setImage(R.id.product_image_l, NetConst.host + r.imagetwo);
			setText(R.id.product_name, r.PRODUCT_NAME);
			setText(R.id.product_caution, r.PRODUCT_CAUTION);
			setText(R.id.product_spec, r.PRODUCT_SPEC);
			setText(R.id.product_detail, r.PRODUCT_DETAIL);

			setText(R.id.product_caution1, r.PRODUCT_CAUTION1);
			setText(R.id.product_caution2, r.PRODUCT_CAUTION2);
			setText(R.id.product_caution3, r.PRODUCT_CAUTION3);
			setText(R.id.product_caution4, r.PRODUCT_CAUTION4);
			setText(R.id.product_caution5, r.PRODUCT_CAUTION5);

			product_intro = r.PRODUCT_INTRO;
			if (product_intro.length() != 0) {
				int tmp1 = product_intro.indexOf("v=");
				product_intro = product_intro.substring(tmp1 + 2);
			}

			if (product_intro.length() != 0 && player != null && player.isPlaying() == false) {
				loadVideo(product_intro);
			}

			setCautionMove(r.PRODUCT_CAUTION1_MOV, R.id.product_caution1_mov);
			setCautionMove(r.PRODUCT_CAUTION2_MOV, R.id.product_caution2_mov);
			setCautionMove(r.PRODUCT_CAUTION3_MOV, R.id.product_caution3_mov);
			setCautionMove(r.PRODUCT_CAUTION4_MOV, R.id.product_caution4_mov);
			setCautionMove(r.PRODUCT_CAUTION5_MOV, R.id.product_caution5_mov);

		}

		@Override
		public void onErrorResponse(VolleyError error) {
//			hideProgress();
		}
	};

	private void setCautionMove(final String cuation_move, int product_caution_mov) {
		if (cuation_move.length() != 0) {
			findViewById(product_caution_mov).setVisibility(View.VISIBLE);
			findViewById(product_caution_mov).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (cuation_move.length() != 0) {
						int tmp1 = cuation_move.indexOf("v=");
						loadVideo(cuation_move.substring(tmp1 + 2));
					}
				}
			});
		}
	}

	protected void setText(int resid, String format, Object... args) {
		setText(resid, String.format(format, args));
	}

	protected void setText(int resid, CharSequence text) {
		setText(resid, text, BufferType.NORMAL);
	}

	protected void setText(int resid, SpannableString text) {
		setText(resid, text, BufferType.SPANNABLE);
	}

	protected void setText(int resid, CharSequence text, BufferType bufferType) {
		setText(findViewById(resid), text, bufferType);
	}

	protected void setText(View v, CharSequence text, BufferType bufferType) {
		if (v == null)
			throw new IllegalArgumentException("뭐야이건");

		if (!(v instanceof TextView))
			throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

		final TextView tv = (TextView) v;

		tv.setText(text, bufferType);
	}

	protected void toastL(Object foramt, Object... args) {
		String text;
		if (foramt instanceof Integer)
			text = ProductDescriptionDetailActivity.this.getString((Integer) foramt, args);
		else if (foramt instanceof String)
			text = String.format((String) foramt, args);
		else
			text = foramt.toString();

		Toast.makeText(ProductDescriptionDetailActivity.this, text, Toast.LENGTH_LONG).show();
	}

	public void toast(Object foramt, Object... args) {
		String text;
		if (foramt instanceof Integer)
			text = ProductDescriptionDetailActivity.this.getString((Integer) foramt, args);
		else if (foramt instanceof String)
			text = String.format((String) foramt, args);
		else
			text = foramt.toString();

		Toast.makeText(ProductDescriptionDetailActivity.this, text, Toast.LENGTH_SHORT).show();
	}

	protected void createProgressDialog() {
		mProgressDlg = new BFProgressDlg(ProductDescriptionDetailActivity.this, R.style.Dialog);
	}

	protected ProgressDialog mProgressDlg;
	protected int mProgressDlgRef = 0;

	public void showProgress() {
		if (mProgressDlg != null && mProgressDlg.isShowing()) {
			mProgressDlgRef++;
			return;
		}
		createProgressDialog();
		mProgressDlg.show();
	}

	public void hideProgress() {
		if (mProgressDlg != null && mProgressDlg.isShowing())
			mProgressDlgRef--;

		if (mProgressDlgRef <= 0) {
			removeProgressDialog();
		}
	}

	protected void removeProgressDialog() {
		mProgressDlgRef = 0;
		if (mProgressDlg != null)
			mProgressDlg.cancel();
	}
}
