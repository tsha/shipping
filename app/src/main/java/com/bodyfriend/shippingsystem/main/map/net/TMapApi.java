package com.bodyfriend.shippingsystem.main.map.net;


import com.bodyfriend.shippingsystem.main.map.tmap.FullTextGeoCodingData;
import com.bodyfriend.shippingsystem.main.map.tmap.RequestRoute;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TMapApi {

    @Headers({
            "appKey: 26254f50-a966-4e7c-a808-cee3df5c9405"
            , "Content-Type: application/json"
            , "Accept: application/json"
    })
    @POST("tmap/routes/routeOptimization10?version=1")
    Observable<ResponseBody> getRouteOptimization10(
            @Body RequestRoute body
    );


    @Headers({
            "appKey: 26254f50-a966-4e7c-a808-cee3df5c9405"
            , "Content-Type: application/json"
            , "Accept: application/json"
    })
    @POST("tmap/routes/routeOptimization10?version=1&format=xml")
    Observable<String> getRouteOptimization10Tmap(
            @Body RequestRoute body
    );

    @Headers({
            "appKey: 26254f50-a966-4e7c-a808-cee3df5c9405"
            , "Content-Type: application/json"
            , "Accept: application/json"
    })
    @GET("tmap/geo/fullAddrGeo?version=1&format=json")
    Call<FullTextGeoCodingData> fullAddrGeo(
            @Query("fullAddr") String fullAddr
    );
}
