package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemUploadForwardingProductBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.UploadForwardingItemData

class ForwardingOrderAdapter : RecyclerView.Adapter<ForwardingOrderAdapter.ViewHolder>() {
    private var mealList: MutableList<UploadForwardingItemData> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
    }


    fun updateMealList(postList: MutableList<UploadForwardingItemData>) {
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun addUploadItem(item: UploadForwardingItemData) {
        if (!mealList.contains(item)) {
            mealList.add(0, item)
            notifyDataSetChanged()
        }
    }


    fun clear() {
        mealList.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemUploadForwardingProductBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_upload_forwarding_product, parent, false)
        return ViewHolder(binding)
    }

    fun getItems(): MutableList<UploadForwardingItemData> {
        return mealList
    }

    class ViewHolder(var view: ItemUploadForwardingProductBinding) : RecyclerView.ViewHolder(view.root)


}