package com.bodyfriend.shippingsystem.main.delivery_order.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ItemDeliveryStockBinding
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData


class DeliveryManStockAdapter(val itemClick: (DeliverManStockData.ResultDatum, position: Int) -> Unit) : RecyclerView.Adapter<DeliveryManStockAdapter.ViewHolder>() {
    private lateinit var viewModel: Any
    private var mealList: MutableList<DeliverManStockData.ResultDatum> = mutableListOf()

    override fun getItemCount(): Int {
        return mealList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = mealList[position]
        Log.d("TAG", "mealList[position].toString() :  ${mealList[position]}")
        holder.view.btnScan.setOnClickListener {
            itemClick(mealList[position], position)
        }
    }


    fun updateMealList(postList: MutableList<DeliverManStockData.ResultDatum>) {
        this.mealList = postList
        notifyDataSetChanged()
    }

    fun addUploadItem(isValid: Boolean, position: Int) {
        mealList[position].isValidCheck = isValid
        Log.d("TAG", "mealList[position].toString() :  ${mealList[position]}")
        notifyItemChanged(position)
    }


    fun isValidItem(rfid: String) {
        println("isValidItem : $rfid")
        run loop@{
            mealList.forEach {
                if (it.serialCd == rfid && !it.isValidCheck) {

                    println("isValidItem : ${it.isValidCheck}")
                    it.isValidCheck = true
                    return@loop
                }
            }
        }
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemDeliveryStockBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_delivery_stock, parent, false)

        return ViewHolder(binding)
    }

    fun getItems(): MutableList<DeliverManStockData.ResultDatum> {
        return mealList
    }

    class ViewHolder(var view: ItemDeliveryStockBinding) : RecyclerView.ViewHolder(view.root)


}