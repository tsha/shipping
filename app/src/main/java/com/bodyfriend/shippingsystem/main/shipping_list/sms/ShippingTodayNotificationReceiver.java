package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bodyfriend.shippingsystem.base.log.Log;

/**
 * Created by 이주영 on 2017-02-28.
 */
public class ShippingTodayNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(":: onReceive");

        Intent intent2 = new Intent(context, TodayMessageSenderActivity.class);
        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent2);
    }
}
