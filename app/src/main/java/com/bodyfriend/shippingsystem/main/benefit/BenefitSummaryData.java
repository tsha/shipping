package com.bodyfriend.shippingsystem.main.benefit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BenefitSummaryData {


    @SerializedName("resultData")
    @Expose
    public List<ResultDatum> resultData = null;

    @Override
    public String toString() {
        return "BenefitSummaryData{" +
                "ResultData=" + resultData +
                '}';
    }

    public class ResultDatum {

        @SerializedName("TOTAL")
        @Expose
        public Integer tOTAL;

        @SerializedName("DELIVERYMAN")
        @Expose
        public String dELIVERYMAN;
        @SerializedName("IN_DATE")
        @Expose
        public String iNDATE;
        @SerializedName("DELIVERYMAN_NM")
        @Expose
        public String dELIVERYMANNM;
        @SerializedName("DAILY_COUNT")
        @Expose
        public Float dAILYCOUNT;
        @SerializedName("TYPE")
        @Expose
        public String tYPE;

        @Override
        public String toString() {
            return "ResultDatum{" +
                    "tOTAL=" + tOTAL +
                    ", dELIVERYMAN='" + dELIVERYMAN + '\'' +
                    ", iNDATE='" + iNDATE + '\'' +
                    ", dELIVERYMANNM='" + dELIVERYMANNM + '\'' +
                    ", dAILYCOUNT=" + dAILYCOUNT +
                    ", tYPE='" + tYPE + '\'' +
                    '}';
        }
    }
}
