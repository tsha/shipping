package com.bodyfriend.shippingsystem.main.login.data;

public class AuthenticationRequest {
    String phoneNumber;
    String userId;

    public AuthenticationRequest(String phoneNumber, String userId) {
        this.phoneNumber = phoneNumber;
        this.userId = userId;
    }
}
