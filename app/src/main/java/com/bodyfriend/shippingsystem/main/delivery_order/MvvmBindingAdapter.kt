package com.bodyfriend.shippingsystem.main.delivery_order

import android.annotation.TargetApi
import android.app.Activity
import android.databinding.BindingAdapter
import android.os.Build
import android.widget.Toolbar

object MvvmBindingAdapter {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @JvmStatic
    @BindingAdapter("onNavigationBackClick")
    fun onNavigationBackClick(toolbar: Toolbar, b: Int) {
        toolbar.setNavigationOnClickListener { v ->
            val host = v.context as Activity
            host.onBackPressed()
        }
    }
}

