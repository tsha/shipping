package com.bodyfriend.shippingsystem.main.delivery_order.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.databinding.ItemDeliveryOrder2Binding;
import com.bodyfriend.shippingsystem.main.delivery_order.net.OutLoiList;

/**
 * Created by 이주영 on 2016-12-08.
 */

class DeliveryViewHolder extends RecyclerView.ViewHolder {

    private final ItemDeliveryOrder2Binding mBinding;
    private LoiRecyclerViewClickListener mLoiRecyclerViewClickListener;

    DeliveryViewHolder(ItemDeliveryOrder2Binding itemView, LoiRecyclerViewClickListener loiRecyclerViewClickListener) {
        super(itemView.getRoot());
        mBinding = itemView;
        this.mLoiRecyclerViewClickListener = loiRecyclerViewClickListener;
    }

    /**
     * @param data
     */
    void setupData(OutLoiList.ForwardingItemData data) {
        mBinding.itemDate.setText(data.regDt);
        mBinding.productName.setText(data.loiNo);
        mBinding.qty.setText(String.valueOf(data.modelTotal));
        mBinding.shippingType.setText(data.procStatNm);
        mBinding.addProductItems.setOnClickListener(v ->
                mLoiRecyclerViewClickListener.addLoiItem(v, data.loiNo, data.fromSlCd));

        if (data.erpFlag.equals("Y")) {
            mBinding.delProductGroup.setVisibility(View.GONE);
        } else {
            mBinding.delProductGroup.setVisibility(View.VISIBLE);
            mBinding.delProductGroup.setOnClickListener(v ->
                    mLoiRecyclerViewClickListener.delLoiGroup(v, data.loiNo));
        }

        mBinding.layoutItemList.removeAllViews();
        for (OutLoiList.ForwardingItemData.models models : data.models) {
            View view = LayoutInflater.from(mBinding.getRoot().getContext()).inflate(R.layout.layout_forwrad_item, null);

            mBinding.layoutItemList.addView(view);
            ((TextView) view.findViewById(R.id.tvItemName)).setText(models.itemExpln);
            ((TextView) view.findViewById(R.id.tvItemSerialCd)).setText(models.serialCd);
            if (models.erpFlag.equals("Y")) {
                view.findViewById(R.id.imageBtnItemDel).setVisibility(View.GONE);
            } else {
                view.findViewById(R.id.imageBtnItemDel).setVisibility(View.VISIBLE);
                view.findViewById(R.id.imageBtnItemDel).setOnClickListener(v ->
                        mLoiRecyclerViewClickListener.delLoiItem(v, data.loiNo, models.ifNo));
            }
        }
    }
}
