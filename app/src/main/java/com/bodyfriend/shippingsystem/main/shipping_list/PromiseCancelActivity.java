package com.bodyfriend.shippingsystem.main.shipping_list;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;

/**
 * 약속 불가 사유입력.
 * 
 */
public class PromiseCancelActivity extends BFActivity {

	public final static int REQUEST_CODE = 1003;
	
	public static class EXTRA {
		/**
		 * 스피너에 들어갈 목록들의 리스트
		 */
		public static final String M_PROMISEFAILPS = "M_PROMISEFAILPS";
	}

	public static class RESULT {
		/**
		 * 선택된 스피너의 인덱스를 넘긴다.
		 */
		public static final String SELECTED_IDX = "SELECTED_IDX";

		/**
		 * 선택된 스피너의 내용을 넘긴다.
		 */
		public static final String SELECTED_TXT = "SELECTED_TXT";
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.promise_cancel_activity);
	}

	@Override
	protected void onLoadOnce() {
		super.onLoadOnce();

		setFinish(R.id.close);
		setOnClickListener(R.id.ok, onOkClickListener);
	}

	@Override
	protected void onParseExtra() {
		super.onParseExtra();

		final Intent intent = getIntent();

		ArrayList<String> arrStr = intent.getStringArrayListExtra(EXTRA.M_PROMISEFAILPS);
		setSpinner(R.id.spinner, arrStr);
		setOnItemSelectedListener(R.id.spinner, onSpinnerItemSelectedListener);
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	private OnClickListener onOkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent data = new Intent();
			final int selectedPosition = getSelectedItemPosition(R.id.spinner);
			data.putExtra(RESULT.SELECTED_IDX, selectedPosition);
			data.putExtra(RESULT.SELECTED_TXT, text(R.id.text));
			setResult(RESULT_OK, data);
			finish();
		}
	};
	
	private OnItemSelectedListener onSpinnerItemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			final String formatter = "[%s] ";
			setText(R.id.text, String.format(formatter, getSelectedItem(R.id.spinner)));
			EditText tv = (EditText) findViewById(R.id.text);
			tv.setSelection(tv.getText().length());
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			
		}
	};
}
