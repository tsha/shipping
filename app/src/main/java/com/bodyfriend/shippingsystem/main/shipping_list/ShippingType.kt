package com.bodyfriend.shippingsystem.main.shipping_list

enum class ShippingType {
    D10,
    D40,
    D50,
    D80

}