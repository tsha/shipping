package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.io.File;

public class modifyProgress extends BFEnty {

	public modifyProgress(String m_shippingseq, String m_progressno, String backsn, File screen1, File screen2, File screen3) {
		setUrl("mobile/api/modifyProgress.json");
		setParam("m_shippingseq", m_shippingseq, "m_progressno", m_progressno, "backsn", backsn, "screen1", screen1, "screen2", screen2, "screen3", screen3);
	}

	public Data data;

	public static class Data {
		public int resultCode;
		public String resultMsg;
	}

}
