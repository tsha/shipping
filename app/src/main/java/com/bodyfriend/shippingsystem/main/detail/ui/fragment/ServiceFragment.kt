package com.bodyfriend.shippingsystem.main.detail.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.databinding.ShippingMasterDetailBody23Binding
import com.bodyfriend.shippingsystem.main.detail.viewmode.DetailViewModel
import com.bodyfriend.shippingsystem.main.shipping_list.data.IntervalTimerViewModelFactory
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingMasterDetailData


/**
 * A placeholder fragment containing a simple view.
 */
class ServiceFragment : Fragment() {

    private lateinit var binding: ShippingMasterDetailBody23Binding
    private val detailViewModel: DetailViewModel
            by lazy {
                activity?.let {
                    ViewModelProviders.of(it, IntervalTimerViewModelFactory)
                            .get(DetailViewModel::class.java)
                }!!
            }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.shipping_master_detail_body2_3, container, false)
        binding.viewmodel = detailViewModel


        return binding.root
    }


    fun setViewData(viewData: ShippingMasterDetailData.ResultData) {
        binding.viewData = viewData
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val VIEW_DATA = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: ShippingMasterDetailData.ResultData): ServiceFragment {
            return ServiceFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(VIEW_DATA, sectionNumber)
                }
            }
        }

        @JvmStatic
        fun newInstance(): ServiceFragment {
            return ServiceFragment().apply {
                arguments = Bundle().apply {
                }
            }
        }
    }
}