package com.bodyfriend.shippingsystem.main.map.presenter;

import android.location.Location;
import android.os.Message;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

/**
 * Created by ts.ha on 2017-04-28.
 */

public interface ShippingMapPresenter  {
    void setView(ShippingMapPresenter.View view);

    void reqShippingDivList(String dueDate);

    void setModel(ShippingList.resultData.list deliveryItem);

    void getGeocoder(List<ShippingList.resultData.list> data);

    void onCall(String hphone_no);

    void onDetailView(String shipping_seq);

    void connectNavi(ShippingList.resultData.list deliveryItem, Location mLastLocation);


    interface View {
        void sendError(VolleyError error);

        void sendResponseShippingDivList(List<ShippingList.resultData.list> data);

        void addMarker(ShippingList.resultData.list deliveryItem);

    }
}
