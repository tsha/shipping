package com.bodyfriend.shippingsystem.main.delivery_order

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.base.util.PP
import com.bodyfriend.shippingsystem.databinding.ActivityForwardingOrderBinding
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.ForwardingOrderAdapter
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData
import com.bodyfriend.shippingsystem.main.delivery_order.viewmodel.ForwardingOrderViewModel
import com.bodyfriend.shippingsystem.main.login.Auth


class ForwardingOrderActivity : AppCompatActivity(), WarehouseStockListDialog.ProductDialogInterface {

    companion object {
        private const val EXTRA_ISSUE_NO = "EXTRA_ISSUE_NO"
        private const val EXTRA_FROM_SL_CD = "EXTRA_FROM_SL_CD"
        @JvmStatic
        fun newIntent(context: Context, issueNo: String, fromSlCd: String): Intent {
            val intent = Intent(context, ForwardingOrderActivity::class.java)
            intent.putExtra(EXTRA_ISSUE_NO, issueNo)
            intent.putExtra(EXTRA_FROM_SL_CD, fromSlCd)
            return intent
        }
    }


    override fun warehouseProductSelect(productData: DeliverManStockData.ResultDatum) {
        mBinding.textProductName.tag = productData
        mBinding.textProductName.text = productData.itemNm
    }


    private lateinit var mBinding: ActivityForwardingOrderBinding
    private lateinit var forwardingItemAdapter: ForwardingOrderAdapter
    private lateinit var viewModel: ForwardingOrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!Auth.isLogin() || TextUtils.isEmpty(PP.MSG_ID.get())) {
            Auth.startLogin(this)
        }


        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forwarding_order)

        viewModel = ViewModelProviders.of(this).get(ForwardingOrderViewModel::class.java)
        mBinding.viewMode = viewModel
        setSupportActionBar(mBinding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mBinding.toolbar.setNavigationOnClickListener { v -> onBackPressed() }

        viewModel.getWarehouseList("1000")
        mBinding.isShowProgressBar = true
        mBinding.isShowGift = false
        getObserver(viewModel)


        val mRecentLayoutManager = LinearLayoutManager(this)
        forwardingItemAdapter = ForwardingOrderAdapter()
        mBinding.itemList.apply {

            layoutManager = mRecentLayoutManager
            setHasFixedSize(true)
            adapter = forwardingItemAdapter
            addItemDecoration(DividerItemDecoration(context, 1))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.item_add) {
            val items = forwardingItemAdapter.getItems()
            if (items.isNotEmpty()) {
                val issueNo = intent.getStringExtra(EXTRA_ISSUE_NO) ?: ""
                mBinding.viewMode?.uploadItems(items, mBinding.spinnerWarehouse.selectedItemPosition, issueNo)

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.product, menu)
        return true
    }

    private fun getObserver(viewModel: ForwardingOrderViewModel) {
        viewModel.warehouseList.observe(this, Observer {
            mBinding.isShowProgressBar = false
            val warehouseNames = mutableListOf<String>()
            val warehouseCode = mutableListOf<String>()
            if (it != null) {
                val issueNo = intent.getStringExtra(EXTRA_FROM_SL_CD) ?: ""
                for (warehouseInfo in it) {
                    warehouseInfo.slNm?.let { it1 -> warehouseNames.add(it1) }
                    warehouseInfo.slCd?.let { it1 -> warehouseCode.add(it1) }
                }
                val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, warehouseNames)
                mBinding.spinnerWarehouse.adapter = arrayAdapter
                if (issueNo.isNotEmpty()) {
                    mBinding.spinnerWarehouse.setSelection(warehouseCode.indexOf(issueNo))
                    mBinding.spinnerWarehouse.isEnabled = false
                }

            }
        })


        viewModel.itemGroupData.observe(this, Observer {
            mBinding.isShowProgressBar = false
            it?.let { it ->
                val itemGroupNames = mutableListOf<String>()
                for (itemGroup in it) {
                    itemGroup.itemGroupNm?.let { it1 -> itemGroupNames.add(it1) }
                }
                Log.d("TAG", "itemGroupData")
                val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemGroupNames)
                mBinding.spinnerGroupCd.adapter = arrayAdapter
                mBinding.textProductName.text = "출고 물품을 선택해주세요."
                mBinding.productCount.setText("")
            }
        })

        // 제품 스피너
        viewModel.productGroupData.observe(this, Observer {
            mBinding.isShowProgressBar = false
            it?.let { it ->
                val itemGroupNames = mutableListOf<String>()
                for (itemGroup in it) {
                    itemGroup.itemGroupNm?.let { it1 -> itemGroupNames.add(it1) }
                }
                Log.d("TAG", "productGroupData")
                val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemGroupNames)
                mBinding.spinnerProductCd.adapter = arrayAdapter
                mBinding.textProductName.text = "출고 물품을 선택해주세요."
                mBinding.productCount.setText("")
            }
        })

        /* viewModel.productData.observe(this, Observer {
             mBinding.isShowProgressBar = false
             it?.let { it ->
                 val itemGroupNames = mutableListOf<String>()
                 for (productInfo in it) {
                     productInfo.itemNm?.let { it1 -> itemGroupNames.add(it1 + " " + productInfo.itemCd) }

                 }
                 val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, itemGroupNames)
 //                mBinding.textProductName.setAdapter(arrayAdapter)
             }
         })*/

        viewModel.uploadForwardingItemData.observe(this, Observer { it ->
            Log.d("TAG", "it ${it.toString()}")
            it?.let {
                forwardingItemAdapter.addUploadItem(it)
            }
        })

        viewModel.isShowGift.observe(this, Observer {
            mBinding.isShowGift = it
        })

        viewModel.registerSuccess.observe(this, Observer { it ->
            it?.let {
                if (it) {
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    if (!isFinishing)
                        AlertDialog.Builder(this@ForwardingOrderActivity)
                                .setMessage("등록에 실패하였습니다. ")
                                .setTitle("출고지시서")
                                .show()
                }
            }
        })

        /* viewModel.productDialogType.observe(this, Observer {
             val split = it?.split(" ")
             val pop = if (split?.size!! > 1) {
                 WarehouseStockListDialog.newInstance(split[0], split[1])
             } else {
                 WarehouseStockListDialog.newInstance(split[0])
             }
             val fm = this@ForwardingOrderActivity.supportFragmentManager
             pop.show(fm, "name")
         })*/
    }

}
