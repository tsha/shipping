package com.bodyfriend.shippingsystem.main.detail.viewmode

interface Presenter {
    fun onItemQuantityChange(selectedFreeGiftIndex: Int)
}