package com.bodyfriend.shippingsystem.main.detail.viewmode


import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.Intent
import android.databinding.Bindable
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.os.Build
import android.text.Html
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.bodyfriend.shippingsystem.BR
import com.bodyfriend.shippingsystem.base.BFApplication
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator
import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseApi
import com.bodyfriend.shippingsystem.main.detail.ProgressNo
import com.bodyfriend.shippingsystem.main.detail.ReturnShippingType
import com.bodyfriend.shippingsystem.main.detail.ui.RadioGridGroup
import com.bodyfriend.shippingsystem.main.login.LoginActivity
import com.bodyfriend.shippingsystem.main.shipping_list.net.AppModifyShippingDivApi
import com.bodyfriend.shippingsystem.main.shipping_list.net.LatexCodeData
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingCodeData
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingMasterDetailData
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


open class DetailViewModel : ObservableViewModel(), RadioGridGroup.OnCustomEventListener {


    private val REQUEST_CODE_SHIPPING_TYPE = 9100
    private val REQUEST_CODE_SHIPPING_PROGRESS = 9110
    private val REQUEST_CODE_SHIPPING_AREA = 400
    private val REQUEST_CODE_SHIPPING_PRODUCT_TYPE = 9200
    private val REQUEST_CODE_SHIPPING_IN_OUT = 9300

    private var viewData = MutableLiveData<ShippingMasterDetailData.ResultData>()
    val receiveData: LiveData<ShippingMasterDetailData.ResultData> get() = viewData
    val quantity = ObservableInt()

    var isShowPopupWindow = ObservableField<String>()


    @get:Bindable
    var isShowLatex: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showLatex)
        }

    @get:Bindable
    var isCollection: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.collection)
        }
    @get:Bindable
    var isShowDefault: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showDefault)
        }
    @get:Bindable
    var isShowD70: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showD70)
        }
    @get:Bindable
    var isShowMove: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showMove)
        }
    @get:Bindable
    var isShowD20: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showD20)
        }
    @get:Bindable
    var isShowD30: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showD30)
        }
    @get:Bindable
    var isShowProgress: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showProgress)
        }

    @get:Bindable
    var isShowP10: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showP10)
        }

    @get:Bindable
    var shippingInOut: String = "본사"
        set(value) {
            field = value
            notifyPropertyChanged(BR.shippingInOut)
        }

    @get:Bindable
    var shippingTypeLabel: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.shippingTypeLabel)
        }


    @get:Bindable
    var customerTypeLabel: String = "고객명"
        set(value) {
            field = value
            notifyPropertyChanged(BR.customerTypeLabel)
        }

    @get:Bindable
    var addressTypeLabel: String = "주소"
        set(value) {
            field = value
            notifyPropertyChanged(BR.addressTypeLabel)
        }

    @get:Bindable
    var homePhoneNumberTypeLabel: String = "집전화"
        set(value) {
            field = value
            notifyPropertyChanged(BR.homePhoneNumberTypeLabel)
        }

    @get:Bindable
    var handPhoneNumberTypeLabel: String = "핸드폰"
        set(value) {
            field = value
            notifyPropertyChanged(BR.handPhoneNumberTypeLabel)
        }
    @get:Bindable
    var recoverySurface: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.recoverySurface)
        }

    @get:Bindable
    var installationArea: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.installationArea)
        }

    @get:Bindable
    var productType: String = "안마의자"
        set(value) {
            field = value
            notifyPropertyChanged(BR.productType)
        }


    @get:Bindable
    var companyCheck: CharSequence = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.companyCheck)
        }


    @get:Bindable
    var shippingTypeByText: String = "설치"
        set(value) {
            field = value
            notifyPropertyChanged(BR.shippingTypeByText)
        }

    @get:Bindable
    var depositDetailHistory: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.depositDetailHistory)
        }


    var latexCodeList: ObservableField<ArrayList<String>> = ObservableField()


    @get:Bindable
    var isShowLadderView: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showLadderView)
        }

    @get:Bindable
    var isShowConcentView: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.showConcentView)
        }

    @get:Bindable
    var isShowCollectView: Boolean = true
        set(value) {
            field = value
            notifyPropertyChanged(BR.showCollectView)
        }


    fun onItemSelectedNumber(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    @SuppressLint("ClickableViewAccessibility")
    fun showReferNamePopupWindow(self: ImageView, productNameView: TextView) {
        println("viewData.value?.SHIPPING_TYPE ${viewData.value?.SHIPPING_TYPE}")
        self.setOnTouchListener { _, event ->
            println(event.action)
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    isShowPopupWindow.set("")
                }
                MotionEvent.ACTION_DOWN -> {
                    isShowPopupWindow.set(viewData.value?.REFER_NAME)
//                    isShowPopupWindow.set("리버이름")
                }
            }
            true
        }
    }

    fun setViewByShippingType(shippingType: String, progressNo: String) {
        when (ReturnShippingType.valueOf(shippingType)) {

            ReturnShippingType.P20 -> {

            }
            ReturnShippingType.D70 -> {
                isShowD70 = true
            }
            ReturnShippingType.D20 -> {
                isShowD20 = true

            }
            ReturnShippingType.D30 -> {
                shippingTypeByText = "회수"
                isShowDefault = true

            }
            ReturnShippingType.P10 -> {

                getShippingCode(REQUEST_CODE_SHIPPING_AREA)

                customerTypeLabel = BFApplication.instance().context().resources.getString(com.bodyfriend.shippingsystem.R.string.recaller)
                addressTypeLabel = "회수주소"
                homePhoneNumberTypeLabel = "회수자\n전화번호"
                handPhoneNumberTypeLabel = "회수자\n핸드폰번호"
                shippingTypeByText = "회수"
                isShowDefault = true
                if (progressNo == ProgressNo.COLLECTION.ordinal.toString()) {

                    isCollection = true
                } else if (progressNo == ProgressNo.MOVE.ordinal.toString()) {
                    isShowMove = true
                    isShowDefault = true
                }
            }
            else -> {
                isShowDefault = true
            }
        }
    }

    override fun onEvent(radioButton: RadioButton) {
        gonePhotoView()
        println("radioButton : ${radioButton.text}")
        if (radioButton.tag != null)
            when (radioButton.tag.toString()) {
                "ZZ" -> {

                }
                "F1" -> {

                }
                "F2" -> {

                }
                "X1" -> {

                }

            }

    }

    fun gonePhotoView() {
        isShowP10 = false
        isShowD20 = false
        isShowDefault = false
        isShowD70 = false
    }

    fun modifyAddress(address: EditText) {

        ServiceGenerator.createService(AppModifyShippingDivApi::class.java)
                .modifyAddress(viewData.value?.SHIPPING_SEQ, address.text.toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<BfData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                        isShowProgress = true
                    }

                    override fun onNext(t: BfData) {
                        if (t.resultCode == 200) {
                            viewData.value?.SHIPPING_SEQ?.let { getDetailInfo(it) }
                        } else {
                            Toast.makeText(BFApplication.instance().context(), t.resultMsg, Toast.LENGTH_SHORT).show()
                        }
                        isShowProgress = false
                    }

                    override fun onError(e: Throwable) {
                    }

                })
    }

    fun getDetailInfo(shippingSeq: String) {

        var test = ServiceGenerator.createService(WarehouseApi::class.java)
                .selectWarehouseList(shippingSeq)
        var test2 = ServiceGenerator.createService(WarehouseApi::class.java)
                .selectReturnItemList(shippingSeq)


        /*  Observable.zip(test, test2, BiFunction
          { t1: WarehouseListData, t2: ReturnProductData ->
              t1.resultDataList[0].plantCd + t2.resultData[1].serialCd
          }).subscribeOn(Schedulers.newThread())
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(
                          object : Observer<String> {
                              override fun onComplete() {
                                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                              }

                              override fun onSubscribe(d: Disposable) {
                                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                              }

                              override fun onNext(t: String) {
                                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                              }

                              override fun onError(e: Throwable) {
                                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                              }
                          })
  */
        isShowProgress = true
        ServiceGenerator.createService(AppModifyShippingDivApi::class.java)
                .getShippingMasterDetailData(shippingSeq)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe(object : Observer<ShippingMasterDetailData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(shippingmaster: ShippingMasterDetailData) {
                        println("onNext")
                        isShowProgress = false
                        shippingmaster.resultData.let {
                            viewData.postValue(it)
                            setViewByShippingType(it.SHIPPING_TYPE, it.PROGRESS_NO)
                            getShippingCode(REQUEST_CODE_SHIPPING_TYPE)
                            getShippingCode(REQUEST_CODE_SHIPPING_PRODUCT_TYPE)
                            getShippingCode(REQUEST_CODE_SHIPPING_IN_OUT)


                            companyCheck = if (!TextUtils.isEmpty(it.SYMPTOM)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    Html.fromHtml(it.COMPANY_CHECK + "<br/> 증상" + it.SYMPTOM, Html.FROM_HTML_MODE_LEGACY)
                                } else {
                                    Html.fromHtml(it.COMPANY_CHECK + "<br/> 증상" + it.SYMPTOM)
                                }

                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    Html.fromHtml(it.COMPANY_CHECK, Html.FROM_HTML_MODE_LEGACY)
                                } else {
                                    Html.fromHtml(it.COMPANY_CHECK)
                                }
                            }
                            println("companyCheck : $companyCheck")

                            if (it.PRODUCT_TYPE == "L") {
                                isShowLatex = true
                                getLatexCode()
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        isShowProgress = false
                        println("onError")
                    }
                })
    }

    fun getLatexCode() {
        ServiceGenerator.createService(AppModifyShippingDivApi::class.java)
                .latexCode
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<LatexCodeData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                        isShowProgress = true
                    }

                    override fun onNext(t: LatexCodeData) {
//                        latexCodeList = t.resultData
                        val nameList = ArrayList<String>()
                        t.resultData.forEach {
                            nameList.add(it.PRODUCT_NAME)
                        }
                        latexCodeList.set(nameList)
                        isShowProgress = false
                    }

                    override fun onError(e: Throwable) {
                        isShowProgress = false
                    }

                })

    }


    fun getShippingCode(code: Int) {
        ServiceGenerator.createService(AppModifyShippingDivApi::class.java)
                .getShippingCodeData(code)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ShippingCodeData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                        isShowProgress = true
                    }

                    override fun onNext(shippingCodeData: ShippingCodeData) {
                        println("onNext")
                        when (code) {
                            REQUEST_CODE_SHIPPING_TYPE -> shippingCodeData.resultData.forEach {
                                if (viewData.value?.SHIPPING_TYPE.equals(it.DET_CD)) {
                                    shippingTypeLabel = it.DET_CD_NM
                                    if (viewData.value!!.SHIPPING_TYPE.startsWith("P")) {
                                        getShippingCode(REQUEST_CODE_SHIPPING_PROGRESS)
                                    }
                                }
                            }
                            REQUEST_CODE_SHIPPING_PROGRESS -> shippingCodeData.resultData.forEach {
                                if (viewData.value?.PROGRESS_NO == it.DET_CD) {
                                    shippingTypeLabel = String.format("%s - %s", shippingTypeLabel, it.DET_CD_NM)
                                }
                            }
                            REQUEST_CODE_SHIPPING_AREA -> shippingCodeData.resultData.forEach {
                                if (viewData.value?.AREA == it.DET_CD) {
                                    recoverySurface = it.DET_CD_NM // 회수지역
                                }

                                if (viewData.value?.AREA2 == it.DET_CD) {
                                    installationArea = it.DET_CD_NM // 회수지역
                                }
                            }
                            REQUEST_CODE_SHIPPING_PRODUCT_TYPE -> shippingCodeData.resultData.forEach {
                                if (viewData.value?.PRODUCT_TYPE == it.DET_CD) {
                                    productType = it.DET_CD_NM
                                }
                            }
                            REQUEST_CODE_SHIPPING_IN_OUT -> shippingCodeData.resultData.forEach {
                                if (viewData.value?.PRODUCT_TYPE == it.DET_CD) {
                                    shippingInOut = it.DET_CD_NM
                                }
                            }

                        }
                        isShowProgress = false
                    }

                    override fun onError(e: Throwable) {
                        isShowProgress = false
                    }
                })
    }


    fun startLogin(mContext: Context) {
        val intent = Intent(mContext, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        mContext.startActivity(intent)

    }


}





