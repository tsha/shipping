package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.os.Bundle;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingDivList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListArea;
import com.bodyfriend.shippingsystem.main.shipping_list.sms.net.selectHasWorkMsgLog;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 이 액티비티는
 * <p>
 * /mobile/api/appShippingDivList.json
 * 를 호출하여 문자를 보내야하는 고객들의 정보를 필터하여
 * {@link SmsActivity} 호출한다.
 */
public class TodayMessageSenderActivity extends BFActivity {

    protected Calendar mStartCalendar = Calendar.getInstance();
    protected Calendar mEndCalendar = Calendar.getInstance();

    protected searchCodeListArea mSearchCodeListArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(":: onCreate");
        setContentView(R.layout.activity_today_message_sender);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        Log.d(":: onLoadOnce");
        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 1);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        Log.d(":: onLoad");

        if (mSearchCodeListArea == null) {
            Net.async(new searchCodeListArea()).setOnNetResponse(onAreaNetResponse);
        } else {
            Net.async(new selectHasWorkMsgLog()).setOnNetResponse(onSelectHashWorkMsgLog);
        }
    }

    /**
     * appShippingDivList를 호출한다.
     */
    protected void reqShippingDivList() {
        Log.d(":: reqShippingDivList");

        int s_datetype = 2;
        String dateStart = SDF.yyyymmdd_2.format(mStartCalendar.getTime());
        String dateEnd = SDF.yyyymmdd_2.format(mEndCalendar.getTime());
        String selectedArea = "전체";
        String s_area = "";
        if (mSearchCodeListArea != null) {
            for (searchCodeListArea.Data.resultData d : mSearchCodeListArea.data.resultData) {
                if (d.DET_CD_NM.equals(selectedArea)) {
                    s_area = d.DET_CD;
                    break;
                }
            }
        }

        String s_custname = "";
        String s_addr = "";
        String s_oneself = "N";

        Net.async(new ShippingDivList(s_datetype, dateStart, dateEnd, s_area, s_custname, s_addr, s_oneself, false)).setOnNetResponse(onNetResponse);

    }

    /**
     * searchCodeListArea api의 리스너이다.
     */
    private Net.OnNetResponse<searchCodeListArea> onAreaNetResponse = new Net.OnNetResponse<searchCodeListArea>() {

        @Override
        public void onResponse(searchCodeListArea response) {
            mSearchCodeListArea = response;

            Net.async(new selectHasWorkMsgLog()).setOnNetResponse(onSelectHashWorkMsgLog);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    /**
     * ShippingDivList api의 리스너이다.
     */
    protected Net.OnNetResponse<ShippingDivList> onNetResponse = new Net.OnNetResponse<ShippingDivList>() {

        @Override
        public void onResponse(ShippingDivList response) {

            List<ShippingList.resultData.list> data = response.data.resultData.list;

            ArrayList<ShippingList.resultData.list> lists = new ArrayList<>();

            for (ShippingList.resultData.list d : data) {
                // 받은 배송 리스트에서 보내야할 selectHasWorkMsgLog 에 있는 아이템에 대해서
                // 문자를 보낸다.
                for (selectHasWorkMsgLog.Data.resultData d2 : mHasWorkMsgLog.data.resultData)
                    if (d2.SHIPPING_SEQ.equals(d.SHIPPING_SEQ)) lists.add(d);
            }

            // 테스트이면 모든 문자를 보낼 수 있도록 추가한다.
            if (!NetConst.isReal()) lists.addAll(data);

            if (lists.size() == 0) {
                toast("송신할 문자가 없습니다.");
                finish();
                return;
            }
            createAndSendMessages(lists);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    private void createAndSendMessages(ArrayList<ShippingList.resultData.list> lists) {
        ArrayList<SmsModel> smsModels = new ArrayList<>();

        for (ShippingList.resultData.list l : lists) {
            SmsModel smsModel = new SmsModel();
            smsModel.HPHONE_NO = l.HPHONE_NO;
            smsModel.SHIPPING_SEQ = l.SHIPPING_SEQ;
            smsModel.ADMIN_ID = Auth.d.resultData.ADMIN_ID;
            smsModel.ADMIN_NM = Auth.d.resultData.ADMIN_NM;
            smsModel.TEL_NO = l.TEL_NO;
            smsModel.GROUP_CD = Auth.d.resultData.GROUP_CD;
            smsModel.PER_CD = Auth.d.resultData.PER_CD;
            smsModel.MSG_GB = 4;
            smsModel.PRODUCT_TYPE = "";
            smsModel.CUST_NAME = l.CUST_NAME;
            smsModel.MSG = createMsg(l);
            // 메세지가 있으면 모델에 넣는다.
            if (!smsModel.MSG.isEmpty()) smsModels.add(smsModel);
        }

        ShippingSmsManager.getInstance(this).startSmsActivity(smsModels);
        finish();
    }

    private String createMsg(ShippingList.resultData.list item) {
        String msg = "";
        try {
            Calendar c = Calendar.getInstance();

            c.setTime(SDF.yyyymmdd_1.parseDate(item.DUE_DATE));

            c.set(Calendar.HOUR_OF_DAY, 9);
            c.set(Calendar.MINUTE, 0);

            if (item.PROMISE_TIME.trim().isEmpty()) return msg;

            String date = String.format("%s, %s", SDF.mmdd__.format(SDF.yyyymmdd_1.parse(item.DUE_DATE)), getVisitTime(SDF.hhmm.parseDate(item.PROMISE_TIME)));

            msg = String.format(mContext.getString(R.string.shipping_sms)
                    , item.CUST_NAME
                    , item.DELIVERYMAN
                    , date
            );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return msg;
    }

    private String getVisitTime(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        if (c.get(Calendar.MINUTE) > 30) {
            c.set(Calendar.MINUTE, 30);
        } else {
            c.set(Calendar.MINUTE, 0);
        }
        c.add(Calendar.MINUTE, -30);
        String s = SDF.ahm.format(c.getTime());
        c.add(Calendar.HOUR, 1);
        String s2 = SDF.ahm.format(c.getTime());
        return String.format("%s~\n%s", s, s2);
    }

    private selectHasWorkMsgLog mHasWorkMsgLog;
    private Net.OnNetResponse<selectHasWorkMsgLog> onSelectHashWorkMsgLog = new Net.OnNetResponse<selectHasWorkMsgLog>() {
        @Override
        public void onErrorResponse(VolleyError error) {
            finish();
        }

        @Override
        public void onResponse(selectHasWorkMsgLog response) {

            reqShippingDivList();
            mHasWorkMsgLog = response;

        }
    };
}
