package com.bodyfriend.shippingsystem.main.delivery_order.net;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Taeseok on 2018-03-05.
 */

public class OutLoiListData {
    @SerializedName("resultData")
    @Expose
    public List<ResultDatum> resultData = null;

    public class ResultDatum {

        @SerializedName("FROM_SL_NM")
        @Expose
        public String fromSlNm;

        @SerializedName("GI_QTY")
        @Expose
        public Double giQty;

        @SerializedName("GIFT_YN")
        @Expose
        public String giftYn;

        @SerializedName("ORDER_NO")
        @Expose
        public String orderNo;

        @SerializedName("TO_SL_CD")
        @Expose
        public String toSlCd;

        @SerializedName("ITEM_CD")
        @Expose
        public String itemCd;

        @SerializedName("PLANT_CD")
        @Expose
        public String plantCd;

        @SerializedName("PROC_STAT_NM")
        @Expose
        public String procStatNm;

        @SerializedName("ISSUE_NO")
        @Expose
        public String issueNo;


        @SerializedName("TO_SL_NM")
        @Expose
        public String toSlNm;


        @SerializedName("FROM_SL_CD")
        @Expose
        public String fromSlCd;


        @SerializedName("REQ_DT")
        @Expose
        public String reqDt;
        @SerializedName("ITEM_NM")
        @Expose
        public String itemNm;
        @SerializedName("CONTRACT_NO")
        @Expose
        public String contractNo;
        @SerializedName("MOV_TYPE")
        @Expose
        public String movType;
        @SerializedName("MOV_TYPE_NM")
        @Expose
        public String movTypeNm;


        @SerializedName("IF_NO")
        @Expose
        public int ifNo;

        @SerializedName("ERP_FLAG")
        @Expose
        public String erpFlag;


        @SerializedName("SERIAL_CD")
        @Expose
        public String serialCd;


    }


}

