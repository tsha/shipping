package com.bodyfriend.shippingsystem.main.delivery_order.net

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForwardingItemData {

    @SerializedName("resultData")
    @Expose
    var resultData: List<ResultDatum>? = null


    inner class ResultDatum {

        @SerializedName("ITEM_NM")
        @Expose
        var itemNm: String? = null
        @SerializedName("ITEM_CD")
        @Expose
        var itemCd: String? = null

        @SerializedName("QTY")
        @Expose
        var qty: Int = 1


        @SerializedName("GIFT_YN")
        @Expose
        var giftYn: String? = null

        @SerializedName("RENTAL_NM")
        @Expose
        var rentalNm: String? = null
    }
}
