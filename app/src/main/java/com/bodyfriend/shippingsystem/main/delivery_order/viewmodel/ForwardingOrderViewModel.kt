package com.bodyfriend.shippingsystem.main.delivery_order.viewmodel


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import android.view.View
import android.widget.*
import com.bodyfriend.shippingsystem.R
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator
import com.bodyfriend.shippingsystem.base.util.OH
import com.bodyfriend.shippingsystem.main.delivery_order.ForwardingOrderActivity
import com.bodyfriend.shippingsystem.main.delivery_order.WarehouseStockListDialog
import com.bodyfriend.shippingsystem.main.delivery_order.net.*
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject
import util.SoundSearcher


class ForwardingOrderViewModel : ViewModel() {
    private val FREEBIES_CODE = "Z11001"
    private val GRIFT_CODE = "33"
    private val _warehouseList: MutableLiveData<MutableList<WarehouseListData.ResultData>> = MutableLiveData()
    val warehouseList: LiveData<MutableList<WarehouseListData.ResultData>> get() = _warehouseList

    private val _itemGroupData: MutableLiveData<MutableList<ItemGroupData.ResultDatum>> = MutableLiveData()
    val itemGroupData: LiveData<MutableList<ItemGroupData.ResultDatum>> get() = _itemGroupData


    private val _productGroupData: MutableLiveData<MutableList<ItemGroupData.ResultDatum>> = MutableLiveData()
    val productGroupData: LiveData<MutableList<ItemGroupData.ResultDatum>> get() = _productGroupData

    private val _productData: MutableLiveData<MutableList<DeliverManStockData.ResultDatum>> = MutableLiveData()
    val productData: LiveData<MutableList<DeliverManStockData.ResultDatum>> get() = _productData

    private val _uploadForwardingItemData: MutableLiveData<UploadForwardingItemData> = MutableLiveData()
    val uploadForwardingItemData: LiveData<UploadForwardingItemData> get() = _uploadForwardingItemData


    var registerSuccess: MutableLiveData<Boolean> = MutableLiveData()
    var isShowGift: MutableLiveData<Boolean> = MutableLiveData()


    var productList: List<DeliverManStockData.ResultDatum> = listOf()

    private val _productDialogType: MutableLiveData<String> = MutableLiveData()
    val productDialogType: LiveData<String> get() = _productDialogType


    fun selectedSpinner(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {

        when (parent?.id) {
            // 타입
            R.id.spinnerProductType -> {
                val itemGroup = view?.context?.resources?.getStringArray(R.array.productTypeCode)?.get(pos)
                if (itemGroup == FREEBIES_CODE) {
//                    getItemItemList(GRIFT_CODE)
                    isShowGift.value = true
                } else {
                    itemGroup?.let { getItemGroupList(it, false) }
                    isShowGift.value = false
                }
            }

            // 그룹
            R.id.spinnerGroupCd -> {
                itemGroupData.value?.let {
                    val resultDatum = it[pos]
                    resultDatum.itemGroupCd?.let { it1 ->
                        getItemGroupList(it1, true)
                    }
                }
            }

        }

    }


    fun selectedItemSpinner(parent: AdapterView<*>, view: View, pos: Int, id: Long, warehouse: Spinner, textProductName: TextView) {
        // 제품
//        Log.d("TAG", "selectedItemSpinner : $pos")
        textProductName.tag = null
        textProductName.text = "출고 물품을 선택해주세요."
        productGroupData.value?.let {
            val resultDatum = it[pos]

            warehouseList.value?.let {
                resultDatum.itemGroupCd?.let { it1 -> it[warehouse.selectedItemPosition].slCd?.let { it2 -> getItemStockList(it1, it2) } }
            }
        }
    }


    fun getProductDialog(warehosusePosition: Spinner, groupPosition: Spinner, spinnerProductCd: Spinner) {

        val itemGroup = groupPosition.context.resources.getStringArray(R.array.productTypeCode)[groupPosition.selectedItemPosition]
        if (itemGroup == FREEBIES_CODE) {
            // 사은품 선택
//            println("사은품 선택 ")
            warehouseList.value?.let {
                it[warehosusePosition.selectedItemPosition].slCd?.let { it2 ->
                    val z11001 = WarehouseStockListDialog.newInstance(it2, null)
                    z11001.show((warehosusePosition.context as ForwardingOrderActivity).supportFragmentManager, "gift")
                }
            }

        } else {
            if (spinnerProductCd.selectedItemPosition != -1) {
                productGroupData.value?.let {
                    val resultDatum = it[spinnerProductCd.selectedItemPosition]
                    warehouseList.value?.let {
                        resultDatum.itemGroupCd?.let { it1 ->
                            it[warehosusePosition.selectedItemPosition].slCd?.let { it2 ->
                                val z11001 = WarehouseStockListDialog.newInstance(it2, it1)
                                z11001.show((warehosusePosition.context as ForwardingOrderActivity).supportFragmentManager, "gift")
                            }
                        }
                    }
                }
            }
        }
    }


    fun updateForwardingItem(productText: TextView, count: EditText
                             , type: Spinner, group: Spinner, product: Spinner) {
        val tag = productText.tag
        if (tag != null) {
            val productNameTag = tag as DeliverManStockData.ResultDatum
            if (count.text.toString().isNotEmpty() && productNameTag.qty >= (count.text.toString()).toInt()) {
                val productItem = UploadForwardingItemData(productNameTag.itemNm, productNameTag.itemCd, count.text.toString())
                if (type.selectedItemPosition == 2) {
                    productItem.invYn = "Y"
                    productItem.giftYn = "Y"
                } else {
                    productItem.invYn = "N"
                    productItem.giftYn = "N"
                }
                productText.tag = null
                productText.text = "출고 물품을 선택해주세요."
                _uploadForwardingItemData.postValue(productItem)
                count.text.clear()
            } else {
                Toast.makeText(productText.context, "제품명, 수량을 입력하세요.", Toast.LENGTH_LONG).show()
            }

            /*   if (itemGroup == FREEBIES_CODE && productNameTag.itemNm != null && count.text.toString().isNotEmpty()) {
                   val productItem = UploadForwardingItemData(productNameTag.itemNm, productNameTag.itemCd, count.text.toString())
                   productItem.invYn = "Y"
                   productItem.giftYn = "Y"
                   _uploadForwardingItemData.postValue(productItem)
                   productText.tag = null
                   productText.text = "출고 물품을 선택해주세요."
                   count.text.clear()
               } else if (productNameTag.itemNm != null && count.text.toString().isNotEmpty() && productNameTag.qty >= (count.text.toString()).toInt()) {
                   val productItem = UploadForwardingItemData(productNameTag.itemNm, productNameTag.itemCd, count.text.toString())
                   productItem.invYn = "N"
                   productItem.giftYn = "N"
                   _uploadForwardingItemData.postValue(productItem)
                   productText.tag = null
                   productText.text = "출고 물품을 선택해주세요."
                   count.text.clear()
               } else {
                   Toast.makeText(productText.context, "제품명, 수량을 입력하세요.", Toast.LENGTH_LONG).show()
               }*/
        } else {
            Toast.makeText(productText.context, "제품명, 수량을 입력하세요.", Toast.LENGTH_LONG).show()
        }
    }

    private fun checkProduct(item: String): DeliverManStockData.ResultDatum? {
        val selectedProduct: DeliverManStockData.ResultDatum? = null
        _productData.value?.let { it ->
            val split = item.split(" ")
            val itemCode = split[split.size - 1]
            for (product in it) {
                product.itemCd.let {
                    if (itemCode.contains(it)) {
                        Log.d("TAG", "getWarehouseList  ${product.itemNm}")
                        return product
                    }
                }

            }
        }
        return selectedProduct
    }


    fun getWarehouseList(groupCd: String) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectWarehouseList(groupCd)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
//                .doOnError { print("getItemGroupList") }
                .subscribe(object : Observer<WarehouseListData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                        Log.d("TAG", "onSubscribe")
                    }

                    override fun onNext(t: WarehouseListData) {
                        t.resultDataList?.let {
                            _warehouseList.postValue(it as MutableList<WarehouseListData.ResultData>?)
                        }
                        isShowGift.value = false
                    }

                    override fun onError(e: Throwable) {
                        Log.d("TAG", " onError getWarehouseList onSubscribe")
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                        e.printStackTrace()
                    }
                })
    }


    private fun getItemGroupList(itemGroup: String, isGrade: Boolean) {
        val grad = if (isGrade) "2" else "1"

        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectItemGroupList(itemGroup, grad)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe(object : Observer<ItemGroupData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: ItemGroupData) {
                        Log.d("TAG", "getWarehouseList")
                        if (!isGrade) {
                            t.resultData?.let {
                                _itemGroupData.postValue(it as MutableList<ItemGroupData.ResultDatum>?)
                            }
                        } else {
                            t.resultData?.let {
                                _productGroupData.postValue(it as MutableList<ItemGroupData.ResultDatum>?)
                            }
                        }
                        isShowGift.value = false
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
//                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }

                })
    }


    fun getItemStockList() {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectItemList()
                .subscribeOn(Schedulers.newThread())

                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<DeliverManStockData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: DeliverManStockData) {
                        t.resultData.let {
                            if (!it.isNullOrEmpty()) {
                                _productData.postValue(it as MutableList<DeliverManStockData.ResultDatum>?)
                                productList = it
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
    }


    fun getItemStockList(itemGroup: String?, slCd: String) {
        val selectItemStockList: Observable<DeliverManStockData> = if (itemGroup != null) {
            // 일반 제품
            ServiceGenerator.createService(WarehouseApi::class.java)
                    .selectItemStockList(itemGroup, slCd)
        } else {
            // 사은품
            ServiceGenerator.createService(WarehouseApi::class.java)
                    .selectItemList(slCd)
        }

        selectItemStockList
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<DeliverManStockData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: DeliverManStockData) {
                        t.resultData.let {
                            //                            if (!it.isNullOrEmpty()) {
                            _productData.postValue(it as MutableList<DeliverManStockData.ResultDatum>?)
                            productList = it
//                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
    }


    fun uploadItems(items: MutableList<UploadForwardingItemData>, selectWarehousePosition: Int, issueNo: String) {
        val uploadItem = JSONObject()
        uploadItem.put("plantCd", _warehouseList.value?.get(selectWarehousePosition)?.plantCd)
        uploadItem.put("fromSlCd", _warehouseList.value?.get(selectWarehousePosition)?.slCd)
        uploadItem.put("issueNo", issueNo)
        val jsonArray = JSONArray()
        for (item in items) {
            val gson = Gson()
            val toJson = gson.toJson(item)
            jsonArray.put(toJson)
        }

        uploadItem.put("itemList", jsonArray)

        createOutLoi(uploadItem)
    }

    private fun createOutLoi(itemGroup: JSONObject) {
//        val repairhistory = RequestBody.create(MediaType.parse("json/plain"), itemGroup.toString())
        ServiceGenerator.createService(WarehouseApi::class.java)
                .createOutLoi(itemGroup.toString())
                .subscribeOn(Schedulers.newThread())

                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<BfData> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: BfData) {
                        Log.d("TAG", "getWarehouseList")
                        registerSuccess.value = t.resultCode == 200
                    }

                    override fun onError(e: Throwable) {
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }

                })
    }


    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        checkSearch(s.toString())
    }

    /**
     * 검색 창에 텍스트가 입력될때에 리스트를 재구성한다.
     */
    private fun checkSearch(search: String) {
        val newDatas: ArrayList<DeliverManStockData.ResultDatum> = ArrayList()
        if (search.isEmpty()) {
            _productData.postValue(productList as MutableList<DeliverManStockData.ResultDatum>?)
        } else {
//            newDatas = ArrayList()
            var value: String
            val value1 = productData.value
            if (value1 != null) {
                for (data in productList) {
                    value = data.itemNm
                    if (SoundSearcher.matchString(value, search)) {
                        newDatas.add(data)
                    }
                }
                if (newDatas.isNotEmpty())
                    _productData.postValue(newDatas as MutableList<DeliverManStockData.ResultDatum>?)
            }
        }
    }

}