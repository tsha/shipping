package com.bodyfriend.shippingsystem.main.shipping_list;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.BFDialog;
import com.bodyfriend.shippingsystem.base.BFMultiNet;
import com.bodyfriend.shippingsystem.base.BaseConst;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.image.ImageUtils;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.MultipartUploader;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.base.util.FU;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.base.util.TextUtil;
import com.bodyfriend.shippingsystem.base.util.Util;
import com.bodyfriend.shippingsystem.main.delivery_order.DeliveryInventoryDialogFragment;
import com.bodyfriend.shippingsystem.main.delivery_order.GiftDialogFragment;
import com.bodyfriend.shippingsystem.main.delivery_order.ReturnProductDialogFragment;
import com.bodyfriend.shippingsystem.main.delivery_order.WarehouseStockListDialog;
import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.CreatedBarcodeData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliverManStockData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.GiftListData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.ProductSerialValidData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.UpdateProductSerialData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseApi;
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseListData;
import com.bodyfriend.shippingsystem.main.detail.ReturnShippingType;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list.net.AppModifyShippingDivApi;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ModifyAddress;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ModifyAddressConfirm;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingMasterDetail;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingMasterDetail.Data.resultData;
import com.bodyfriend.shippingsystem.main.shipping_list.net.UpdateShippnigCimpcheck;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appInsertLog;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appSelectLatexCode;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appUpdateLFreegift;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeAdapter;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeJoli;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListArea;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListDeliveryGb;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListFreegift;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListProductBonsa;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListProductGb;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListProductName;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeShippingType;
import com.bodyfriend.shippingsystem.main.shipping_list.net.selectDeliveryManList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.zxing.client.android.CaptureActivity;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import gun0912.tedbottompicker.TedBottomPicker;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * 배송 상세화면
 *
 * @author 이주영
 */
public class ShippingMasterDetailActivity extends BFActivity implements DeliveryInventoryDialogFragment.DeliveryInventoryDialogInterface
        , GiftDialogFragment.GiftDialogInterface, ReturnProductDialogFragment.ReturnProductDialogInterface {
    private static final int PER_CD = 104;
    private final int REQUEST_DELIVARYMAN = 1002;

    private final int REQUEST_CHANGE_CAR = 1003;
    private final int RESULT_CODE_BARCODE_1 = 1004;
    private final int RESULT_CODE_BARCODE_2 = 1005;
    private final int REQUEST_CODE_SIGN = 1006;
    private static final int REQUEST_CAPTURE_IMAGE = 1007;
    private final int REQUEST_DELIVARYMAN2 = REQUEST_CAPTURE_IMAGE + 1;

    private final int RESULT_CODE_BARCODE_PRODUCT = REQUEST_DELIVARYMAN2 + 1; // 제품 코드
    private final int RESULT_CODE_BARCODE_MANUFACTURER = RESULT_CODE_BARCODE_PRODUCT + 1; // 제조사

    private final int RESULT_CODE_BARCODE_PRODUCT_2 = RESULT_CODE_BARCODE_MANUFACTURER + 1; // 제품 코드 2
    private final int RESULT_CODE_BARCODE_MANUFACTURER_2 = RESULT_CODE_BARCODE_PRODUCT_2 + 1; // 제조사 2

    private final int RESULT_CODE_BARCODE_RETURN_PRODUCT = RESULT_CODE_BARCODE_MANUFACTURER_2 + 1; //  회수 제품
    private final int RESULT_CODE_BARCODE_RETURN_MANUFACTURER = RESULT_CODE_BARCODE_RETURN_PRODUCT + 1; //  회수 제품

    private final int RESULT_CODE_BARCODE_RETURN_PRODUCT_2 = RESULT_CODE_BARCODE_RETURN_MANUFACTURER + 1; //  회수 제품
    private final int RESULT_CODE_BARCODE_RETURN_MANUFACTURER_2 = RESULT_CODE_BARCODE_RETURN_PRODUCT_2 + 1; //  회수 제품

    private final int RESULT_CODE_BARCODE_GIFT_ITEM = RESULT_CODE_BARCODE_RETURN_MANUFACTURER_2 + 1; // 사은품 1
    private final int RESULT_CODE_BARCODE_GIFT_MANUFACTURER = RESULT_CODE_BARCODE_GIFT_ITEM + 1; // 사은품 1

    private final int RESULT_CODE_BARCODE_GIFT_ITEM_2 = RESULT_CODE_BARCODE_GIFT_MANUFACTURER + 1; // 사은품 2
    private final int RESULT_CODE_BARCODE_GIFT_MANUFACTURER_2 = RESULT_CODE_BARCODE_GIFT_ITEM_2 + 1; // 사은품 2

    private final int RESULT_CODE_BARCODE_RETURN_GIFT_ITEM = RESULT_CODE_BARCODE_GIFT_MANUFACTURER_2 + 1; // 호수 사은품 1
    private final int RESULT_CODE_BARCODE_RETURN_GIFT_MANUFACTURER = RESULT_CODE_BARCODE_RETURN_GIFT_ITEM + 1; // 회수 사은품 1


    private final int RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2 = RESULT_CODE_BARCODE_RETURN_GIFT_MANUFACTURER + 1; // 사은품 2
    private final int RESULT_CODE_BARCODE_RETURN_GIFT_MANUFACTURER_2 = RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2 + 1; // 사은품 2
    private final int RESULT_CODE_BARCODE_GIFT_ITEM_3 = RESULT_CODE_BARCODE_RETURN_GIFT_MANUFACTURER_2 + 1; // 사은품 3
    private final int RESULT_CODE_BARCODE_GIFT_ITEM_4 = RESULT_CODE_BARCODE_GIFT_ITEM_3 + 1; // 사은품 4
    private final int RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3 = RESULT_CODE_BARCODE_GIFT_ITEM_4 + 1; // 회수 사은품 4
    private final int RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4 = RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3 + 1; // 회수 사은품 4
    private final int RESULT_CODE_BARCODE_RETURN_PRODUCT_3 = RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4 + 1; // 회수 사은품 4


    private HashMap<Integer, ProductSerialValidData.ResultData> mCompleteProductMap = new HashMap<>();
//    private HashMap<Integer, String> mManufacturerBarcodeMap = new HashMap<>();

    private static final int INTERVAL_TIME = 7200000; // 2시간 = 7200초

    private PopupWindow mReferNamePopupWindow;

    // 라텍스 사은품 리스트
    private List<appSelectLatexCode.Data.resultData> mAppSelectLatexCode;
    private ArrayList<ImageHolder> mPhotoList = new ArrayList<>();

    //    public RequestManager mGlideRequestManager;
    private File mPickImageFile;
    private Spinner mGiftSpinnerFirst;
    private Spinner mGiftSpinnerSecond;
    private String m_extra_cost_memo;
    private String KEY_DATA = "PHOTO";
    private final int PRODUCT_CODE_CHECK = 500;
    private ArrayList<View> mAddyViewList;
    // 회수 창고 코드
    private String mReturnSlCd = "";
    // 회수 사유 입력
    private String mReturnReason = "";
    private String mHouseType;


    @Override
    public void selectItem(@NotNull DeliverManStockData.ResultDatum productData) {

    }


    /**
     * 재고에서 선택한 제품 설치완료를 위한 정보 변환
     *
     * @param productData
     * @param isGift
     * @return
     */
    private ProductSerialValidData.ResultData setProductInfo(DeliverManStockData.ResultDatum productData, boolean isGift) {
        ProductSerialValidData.ResultData productInfo = new ProductSerialValidData.ResultData();
        if (isGift) {
            productInfo.giftYn = "Y";
        } else {
            productInfo.giftYn = "N";
        }
        productInfo.isValid = productData.isValid();
        productInfo.collectYn = "N";
        productInfo.serialCd = productData.getSerialCd();
        productInfo.item = productData.getItemCd();
        productInfo.makerCd = "";
        productInfo.lotFlg = productData.getLotFlg();
        productInfo.itemNm = productData.getItemNm();
        productInfo.itemCd = productData.getItemCd();
        productInfo.slCd = productData.getSerialCd();

        return productInfo;
    }

    /**
     * 재고 다이얼로그에서 받은 데이터 구분하여 바코드 촬영 여부
     *
     * @param productData
     * @param resultCode
     */
    private void setBarcodeStart(ProductSerialValidData.ResultData productData, int resultCode) {
        Log.d("TAG", "productData : " + productData.toString());

        if (productData.lotFlg.equals("Y")) {
            mCompleteProductMap.put(resultCode, productData);
            Intent intent = new Intent(mContext, CaptureActivity.class);
            startActivityForResult(intent, resultCode);
        } else {
            productData.isValid = true;
            setProductSerialValidInfo(productData, productData.serialCd != null ? productData.serialCd : "", resultCode);
        }
    }

    /**
     * 외주 기사 , 본사기사
     * 보유 재고  선택
     * getLotFlg = Y 이면 validation check 진행
     * mCompleteProductMap 에 저장
     *
     * @param productData 선택한 재고 정보
     * @param resultCode  사은품 항목 1, 2
     */
    @Override
    public void selectItem(@NotNull DeliverManStockData.ResultDatum productData, int resultCode) {

        ProductSerialValidData.ResultData resultData;
        Log.d("TAG", "warehouseProductSelect : " + productData.toString());
        switch (resultCode) {
            case RESULT_CODE_BARCODE_GIFT_ITEM:
                resultData = setProductInfo(productData, true);
                setBarcodeStart(resultData, RESULT_CODE_BARCODE_GIFT_ITEM);
                break;
            case RESULT_CODE_BARCODE_GIFT_ITEM_2:
                resultData = setProductInfo(productData, true);
                setBarcodeStart(resultData, RESULT_CODE_BARCODE_GIFT_ITEM_2);
                break;
            case RESULT_CODE_BARCODE_GIFT_ITEM_3:
                resultData = setProductInfo(productData, true);
                setBarcodeStart(resultData, RESULT_CODE_BARCODE_GIFT_ITEM_3);
                break;
            case RESULT_CODE_BARCODE_GIFT_ITEM_4:
                resultData = setProductInfo(productData, true);
                setBarcodeStart(resultData, RESULT_CODE_BARCODE_GIFT_ITEM_4);
                break;
            case RESULT_CODE_BARCODE_PRODUCT:
                resultData = setProductInfo(productData, false);
                setBarcodeStart(resultData, RESULT_CODE_BARCODE_PRODUCT);
                break;
            case RESULT_CODE_BARCODE_PRODUCT_2:
                resultData = setProductInfo(productData, false);
                setBarcodeStart(resultData, RESULT_CODE_BARCODE_PRODUCT_2);
                break;

            case RESULT_CODE_BARCODE_RETURN_PRODUCT_3:
                resultData = setProductInfo(productData, false);
                resultData.isValid = true;
                setProductSerialValidInfo(resultData, resultData.serialCd != null ? resultData.serialCd : "", resultCode);
            default:
                break;
        }




        /*if (giftIndex == RESULT_CODE_BARCODE_PRODUCT) {
            layoutViewGroup = findViewById(R.id.layoutCheckItemCode);
            ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(productData.getItemNm());

        } else if (giftIndex == RESULT_CODE_BARCODE_PRODUCT_2) {
            layoutViewGroup = findViewById(R.id.layoutCheckItemCode2);


        } else if (giftIndex == RESULT_CODE_BARCODE_GIFT_ITEM) {
            layoutViewGroup = findViewById(R.id.layoutCheckGiftCode1);
            ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(productData.getItemNm());

        } else {
            layoutViewGroup = findViewById(R.id.layoutCheckGiftCode2);
            ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(productData.getItemNm());

            if (productData.getLotFlg().equals("Y")) {
                layoutViewGroup.findViewById(R.id.btnBarcode).setVisibility(View.VISIBLE);
                Intent intent = new Intent(mContext, CaptureActivity.class);
                startActivityForResult(intent, giftIndex);
            } else {
                layoutViewGroup.findViewById(R.id.btnBarcode).setVisibility(View.GONE);
            }
        }*/
    }

    /**
     * 회수 사은품 선택
     * getLotFlg = Y 이면  바코드 필수
     *
     * @param giftData   선택한 사은품 정보
     * @param resultCode 회수 사은품 1, 2 구분
     */
    @Override
    public void giftSelect(@NotNull GiftListData.ResultData giftData, int resultCode) {
        View layoutViewGroup;
        ProductSerialValidData.ResultData resultData = new ProductSerialValidData.ResultData();
        resultData.giftYn = "Y";
        resultData.collectYn = "Y";
        resultData.makerCd = "";
        resultData.serialCd = giftData.getLotFlg();
        resultData.lotFlg = "";
        resultData.itemNm = giftData.getItemNm();
        resultData.item = giftData.getItemCd();

        switch (resultCode) {
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM:
                layoutViewGroup = findViewById(R.id.layoutCheckReturnGiftCode1);
                ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(giftData.getItemNm());
                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2:
                layoutViewGroup = findViewById(R.id.layoutReturnCheckGiftCode2);
                ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(giftData.getItemNm());
                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3:
                layoutViewGroup = findViewById(R.id.layoutReturnCheckGiftCode3);
                ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(giftData.getItemNm());
                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4:
                layoutViewGroup = findViewById(R.id.layoutReturnCheckGiftCode4);
                ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(giftData.getItemNm());
                break;
        }
        setReturnGiftValidInfo(giftData, resultCode, resultData);
       /* if (resultCode == RESULT_CODE_BARCODE_RETURN_GIFT_ITEM) {
            layoutViewGroup = findViewById(R.id.layoutCheckReturnGiftCode1);
            ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(giftData.getItemNm());


        } else if (resultCode == RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2) {
            layoutViewGroup = findViewById(R.id.layoutReturnCheckGiftCode2);
            ((TextView) layoutViewGroup.findViewById(R.id.tvItemName)).setText(giftData.getItemNm());
            setReturnGiftValidInfo(giftData, resultCode, resultData);
        }*/
    }

    private void setReturnGiftValidInfo(@NotNull GiftListData.ResultData giftData, int resultCode, ProductSerialValidData.ResultData resultData) {
        if (giftData.getLotFlg().equals("Y")) {
            setProductSerialValidInfo(resultData, "", resultCode);
            Intent intent = new Intent(mContext, CaptureActivity.class);
            startActivityForResult(intent, resultCode);
        } else {
            resultData.isValid = true; // 체크여부 해제
            setProductSerialValidInfo(resultData, "", resultCode);
        }
    }

    /**
     * 회수 상품 제품 다이얼로그 선택
     *
     * @param productData
     * @param resultCode
     */
    @Override
    public void returnProductItem(@NotNull DeliverManStockData.ResultDatum productData, int resultCode) {
        Log.d("returnProductItem : " + productData.toString());
//        View layoutViewGroup;
        ProductSerialValidData.ResultData resultData;
        resultData = setProductInfo(productData, productData.getGiftYn().equals("Y"));

//        if (resultData.lotFlg.equals("Y")) {
        showReturnItemSetBarcode(resultData, resultCode);
//        } else {
//            setBarcodeStart(resultData, resultCode);
//            createBarcode(resultData, resultCode);
//        }
    }


    /**
     * 회수 제품 바코드 촬여 여부 확인
     */
    private void showReturnItemSetBarcode(ProductSerialValidData.ResultData data, int resultCode) {
        new AlertDialog.Builder(mContext)
                .setTitle("회수제품 바코드 여부 확인")
                .setMessage("제품바코드가 존재하면 촬영해주세요.")
                .setPositiveButton("존재하지 않음", (dialog, which) -> createBarcode(data, resultCode))
                .setNegativeButton("촬영", (dialog, which) -> {
                    mCompleteProductMap.put(resultCode, data);
                    Intent intent = new Intent(mContext, CaptureActivity.class);
                    startActivityForResult(intent, resultCode);
                })
                .setCancelable(false)
                .create()
                .show();
    }

    /**
     * 임시 바코드 생성
     *
     * @param resultDatum
     * @param resultCode
     */
    private void createBarcode(ProductSerialValidData.ResultData resultDatum, int resultCode) {
        ServiceGenerator.createService(WarehouseApi.class)
                .createBarcode(resultDatum.itemCd)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CreatedBarcodeData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CreatedBarcodeData createdBarcodeData) {
                        if (createdBarcodeData.getResultData() != null) {
                            resultDatum.serialCd = createdBarcodeData.getResultData().getTempLot();
                            setBarcodeStart(resultDatum, resultCode);
                        } else {
                            Toast.makeText(ShippingMasterDetailActivity.this, "임시바코드 생성에 실패했습니다. 다시 시도 부탁드립니다,", Toast.LENGTH_LONG).show();
                            OH.c().notifyObservers(OH.TYPE.SESSION_OUT);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(ShippingMasterDetailActivity.this, "임시바코드 생성에 실패했습니다. 다시 시도 부탁드립니다,", Toast.LENGTH_LONG).show();
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public static class EXTRA {
        public static String SHIPPING_SEQ = "SHIPPING_SEQ";
    }

    private static String 배송 = "D10"; // 배송
    private static String 맞교 = "D20"; // 맞교체
    private static String 계철 = "D30"; // 계약철회
    private static String 이전 = "P10"; // 이전요청
    private static String 수리 = "P20"; // 수리요청
    private static String AS = "D70"; // AS

    private static String 설치 = "0"; // 설치
    private static String 회수 = "1"; // 회수
    private static String 이동 = "2"; // 이동


    private static int 라디오_설치완료 = R.id.m_completedtZ; // 설치완료
    private static int 라디오_초도불량 = R.id.m_completedtF2; // 초도불량
    private static int 라디오_설치불가 = R.id.m_completedtX1; // 설치불가
    private static int 라디오_수취거부 = R.id.m_completedtX2; // 수취거부
    private static int 라디오_취소 = R.id.m_completedtX3; // 취소
    private static int 라디오_설치_후_반품 = R.id.m_completedtX4; // 설치 후 반품

    protected String shipping_seq;

    protected resultData mResultData;
    private JSONArray mPhotoJsonArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shipping_master_detail2);
//        mGlideRequestManager = Glide.with(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Log.d("mPhotoJsonArray.toString() : " + mPhotoJsonArray.toString());
        PP.PHOTO.set(mPhotoJsonArray.toString());
    }


    @Override
    protected void onParseExtra() {
        super.onParseExtra();
        shipping_seq = getIntent().getStringExtra(EXTRA.SHIPPING_SEQ);
        Log.d("SHIPPING_SEQ : " + shipping_seq);
    }

    private void setTitle() {
        setFinish(R.id.title_layout_leftmenu);
    }

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setTitle();

        // 뷰와 이벤트에 대해 셋업한다
        setupViewAndEvent();

        if (fileHashMap == null) fileHashMap = new HashMap<>();

    }


    protected InputFilter filter = (source, start, end, dest, dstart, dend) -> {

        Pattern ps = Pattern.compile("^[a-zA-Z0-9\\s-]+$");

        if (!ps.matcher(source).matches()) {
            return "";
        }
        return null;
    };


    private void setupViewAndEvent() {
        setRadioButton(라디오_설치완료, R.id.m_completedtF1, 라디오_초도불량, 라디오_설치불가, 라디오_수취거부, 라디오_취소, 라디오_설치_후_반품);

        setOnClickListener(R.id.imageone, onSnImageClickListener);
        setOnClickListener(R.id.imagetwo, onImageClickListener2);
        setOnClickListener(R.id.imagethree, onImageClickListener2);
        setOnClickListener(R.id.imagefour, onImageClickListener2);
        setOnClickListener(R.id.imagefive, onImageClickListener2);
        setOnClickListener(R.id.imagesix, onImageClickListener2);

        setOnClickListener(R.id.servicePicture1, onImageClickListener2);
        setOnClickListener(R.id.servicePicture2, onImageClickListener2);

        setOnClickListener(R.id.service, onServiceClickListener); // 서비스 접수
        setOnClickListener(R.id.modify, onModifyClickListener);
        setOnClickListener(R.id.sign, onSign2ClickListener); // 서명
        setOnClickListener(R.id.btn1, onBtn1ClickListener);
        setOnClickListener(R.id.btn2, onBtn2ClickListener);

        setOnClickListener(R.id.ladder_image_one, onImageClickListener2);
        setOnClickListener(R.id.ladder_image_two, onImageClickListener2);
        setOnClickListener(R.id.ladder_image_three, onImageClickListener2);

        setOnClickListener(R.id.concent_image_one, onImageClickListener2);
        setOnClickListener(R.id.concent_image_two, onImageClickListener2);
        setOnClickListener(R.id.concent_image_three, onImageClickListener2);


        // 사다리 사용 여부 라디오 버튼이다.
        RadioButton ladder_y = (RadioButton) findViewById(R.id.ladder_y);
        ladder_y.setChecked(true);

        EditText mCollectionSn = (EditText) findViewById(R.id.m_collection_sn);
        mCollectionSn.setFilters(new InputFilter[]{filter});
        EditText mProductSn = (EditText) findViewById(R.id.m_productsn);
        mProductSn.setFilters(new InputFilter[]{filter});

        // 내림서비스
        RadioButton additional_radio_add = (RadioButton) findViewById(R.id.additional_radio_add);
        additional_radio_add.setChecked(true);

        ladder_y.setOnCheckedChangeListener((buttonView, isChecked) -> {
            setVisibility(R.id.concent_image_layout, false);
            setChecked(R.id.concent_n, true);
            setVisibility(R.id.ladder_image_layout, isChecked);

            if (!isChecked) {

                screen_ladder_one = null;
                screen_ladder_two = null;
                findViewById(R.id.ladder_image_one).setTag(null);
                findViewById(R.id.ladder_image_two).setTag(null);
                ((ImageView) findViewById(R.id.ladder_image_two)).setImageResource(R.drawable.ladder_sample2);
                ((ImageView) findViewById(R.id.ladder_image_one)).setImageResource(R.drawable.ladder_sample1);

            }
        });

        RadioGroup housing = (RadioGroup) findViewById(R.id.radioGroupHousing);
        housing.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton checkedRadioButton = group.findViewById(checkedId);
            // This puts the value (true/false) into the variable
            boolean isChecked = checkedRadioButton.isChecked();
            // If the radiobutton that has changed in check state is now checked...
            if (isChecked) {
                mHouseType = (String) checkedRadioButton.getTag();
                Log.d("mHouseType : " + mHouseType);
            }
        });

//        // 설치불가
//        RadioButton x1Complete = (RadioButton) findViewById(R.id.m_completedtX1);
//        // 설치불가
//        RadioButton x2Complete = (RadioButton) findViewById(R.id.m_completedtX2);
//        // 설치후반품
//        RadioButton x4Complete = (RadioButton) findViewById(R.id.m_completedtX4);
//        // 초도불량
//        RadioButton f2Complete = (RadioButton) findViewById(R.id.m_completedtF2);
//        RadioButton zComplete = (RadioButton) findViewById(R.id.m_completedtZ);

//        x1Complete.setOnCheckedChangeListener(cancelRadioBtnListener);
//        x2Complete.setOnCheckedChangeListener(cancelRadioBtnListener);
//        x4Complete.setOnCheckedChangeListener(cancelRadioBtnListener);
//        f2Complete.setOnCheckedChangeListener(cancelRadioBtnListener);
//        zComplete.setOnCheckedChangeListener(cancelRadioBtnListener);


        // 양중 사용 여부 라디오 버튼이다.
        RadioButton concent_y = (RadioButton) findViewById(R.id.concent_y);
        concent_y.setChecked(true);


        concent_y.setOnCheckedChangeListener((buttonView, isChecked) -> {
            setVisibility(R.id.ladder_image_layout, false);
            setChecked(R.id.ladder_n, true);
            setVisibility(R.id.concent_image_layout, isChecked);
            if (!isChecked) {
                screen_socket_one = null;
                screen_socket_two = null;
                screen_socket_three = null;

                findViewById(R.id.concent_image_one).setTag(null);
                findViewById(R.id.concent_image_two).setTag(null);
                findViewById(R.id.concent_image_three).setTag(null);
                ((ImageView) findViewById(R.id.concent_image_one)).setImageResource(R.drawable.yang1);
                ((ImageView) findViewById(R.id.concent_image_two)).setImageResource(R.drawable.yang2);
                ((ImageView) findViewById(R.id.concent_image_three)).setImageResource(R.drawable.yang3);
            } else {
                setVisibility(R.id.concent_image_layout, true);
                setVisibility(R.id.ladder_image_layout, false);
            }
        });


        // 사다리 결제 구분 카드/계좌/현금
        setSpinner(R.id.ladder_payment_type, R.array.ladder_payment_type);

        setVisibility(R.id.additional_price_layout, true);
        setVisibility(R.id.additional_memo_layout, true);

        if (Auth.d != null && Auth.d.resultData != null && Auth.d.resultData.PER_CD != 104) {
            ((EditText) findViewById(R.id.additional_ladder_price)).setText("20000");
        } else {
            ((EditText) findViewById(R.id.additional_ladder_price)).setText("10000");
        }

        // 추가 비용
        RadioGroup additionalRadioGroup = (RadioGroup) findViewById(R.id.additional_radio_group);
        additionalRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.additional_radio_add) {
                setVisibility(R.id.additional_price_layout, true);
                setVisibility(R.id.additional_memo_layout, true);
                if (Auth.d != null && Auth.d.resultData != null && Auth.d.resultData.PER_CD != 104) {
                    ((EditText) findViewById(R.id.additional_ladder_price)).setText("20000");
                } else {
                    ((EditText) findViewById(R.id.additional_ladder_price)).setText("10000");
                }

            } else if (checkedId == R.id.additional_radio_none) {
                setVisibility(R.id.additional_price_layout, false);
                setVisibility(R.id.additional_memo_layout, false);
                ((EditText) findViewById(R.id.additional_ladder_price)).setText("");

                findViewById(R.id.servicePicture1).setTag(null);
                findViewById(R.id.servicePicture2).setTag(null);
                ((ImageView) findViewById(R.id.servicePicture1)).setImageResource(R.drawable.service_sample1);
                ((ImageView) findViewById(R.id.servicePicture2)).setImageResource(R.drawable.service_sample2);
                ((EditText) findViewById(R.id.additional_ladder_price)).setText("");

            }
        });


        // 회수 서비스
        RadioGroup collectRadioGroup = (RadioGroup) findViewById(R.id.collect_radio_group);


        RadioButton colletRadio = (RadioButton) findViewById(R.id.collect_radio_add);
        colletRadio.setChecked(true);

        setVisibility(R.id.collect_price_layout, true);

        collectRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.collect_radio_add) {
                setVisibility(R.id.collect_price_layout, true);

            } else if (checkedId == R.id.collect_radio_none) {
                setVisibility(R.id.collect_price_layout, false);

                findViewById(R.id.collectPicture1).setTag(null);
                findViewById(R.id.collectPicture2).setTag(null);
                ((ImageView) findViewById(R.id.collectPicture1)).setImageResource(R.drawable.plus);
                ((ImageView) findViewById(R.id.collectPicture2)).setImageResource(R.drawable.plus);

            }
        });

        setOnClickListener(R.id.collectPicture1, onImageClickListener2);
        setOnClickListener(R.id.collectPicture2, onImageClickListener2);


        // 텍스트에 원을 추가한다.
        TextUtil.getInstance().setWon((EditText) findViewById(R.id.m_ladder_price));
        TextUtil.getInstance().setWon((EditText) findViewById(R.id.additional_ladder_price));

        if (findViewById(R.id.selectDeliveryman2) != null) {
            setOnClickListener(R.id.selectDeliveryman2, onSelDmanClickListener);
            setVisibility(R.id.selectDeliveryman2, View.INVISIBLE);
        }
        if (findViewById(R.id.selectDeliveryman3) != null) {
            setOnClickListener(R.id.selectDeliveryman3, onSelDmanClickListener3);
            setVisibility(R.id.selectDeliveryman3, View.INVISIBLE);
        }

        // 차량번호 변경
        View view = findViewById(R.id.btn_carnum);
        if (view != null) {
            setOnClickListener(R.id.btn_carnum, onSelCarClickListener);

            if (!isEmpty(PP.carNumber.get())) {
                setText(R.id.carnum, PP.carNumber.get());
            }
        }

        findViewById(R.id.btn1).performClick();

        if (findViewById(R.id.compcheck) != null) {
            setOnClickListener(R.id.compcheck, onCompcheckClickListener);
        }

        mGiftSpinnerFirst = (Spinner) findViewById(R.id.m_freegiftcheck1);
        mGiftSpinnerSecond = (Spinner) findViewById(R.id.m_freegiftcheck2);
        mGiftSpinnerFirst.setOnItemSelectedListener(onItemClickListener);
        mGiftSpinnerSecond.setOnItemSelectedListener(onItemClickListener);


        findViewById(R.id.buttonModifyAddress).setOnClickListener(v ->
                Net.async(new ModifyAddress(mResultData.SHIPPING_SEQ, ((EditText) findViewById(R.id.insaddr)).getText().toString()), new Net.OnNetResponse<ModifyAddress>() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                    @Override
                    public void onResponse(ModifyAddress response) {
                        if (response.data.resultCode == 200) {
                            reload();
                        } else {
                            Toast.makeText(mContext, response.data.resultMsg, Toast.LENGTH_SHORT).show();
                        }
                    }
                }));
//
    }


    AdapterView.OnItemSelectedListener onItemClickListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = (Spinner) parent;
            if (spinner.getId() == R.id.m_freegiftcheck1 && position != 0) {
                showDialog(((Spinner) findViewById(R.id.m_freegiftcheck1)).getSelectedItem().toString() + " 선택하였습니다.", "확인", null);
//                if (position == 2) {
//                    showGiftOneBarcodeView();
//                } else {
//                    dismissGiftOneBarcodeView();
//                }
            } else if (spinner.getId() == R.id.m_freegiftcheck2 && position != 0) {
                showDialog(((Spinner) findViewById(R.id.m_freegiftcheck2)).getSelectedItem().toString() + " 선택하였습니다.", "확인", null);
//                if (position == 2) {
//                    showGiftTwoBarcodeView();
//                } else {
//                    dismissGiftTwoBarcodeView();
//                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private DialogInterface.OnClickListener positiveListener2 = (dialog, which) -> {

        String json = PP.PHOTO.get();
        if (!TextUtils.isEmpty(json)) {
            try {
                JSONArray gson = new JSONArray(json);
                for (int i = 0; i < gson.length(); i++) {
                    String imagePath = gson.getJSONObject(i).getString(BaseConst.IMAGE_PATH);
                    int viewId = gson.getJSONObject(i).getInt(BaseConst.VIEW_ID);
                    ImageView imageView = (ImageView) findViewById(viewId);
                    if (imageView != null) {
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        imageView.setImageBitmap(bitmap);
                        imageView.setTag(new File(imagePath));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 기존에 저장된 임시 이미지를 다시 로드한다
     */
    private void addTemporarilyImage() {
        String json = PP.PHOTO.get();
        if (json.length() > 4) {
            showDialog("기존에 작성중이던 이미지가 있습니다. 다시 로드 하시겠습니까?", "네", positiveListener2, "아니오", null);
        }
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        Net.async(new ShippingMasterDetail(shipping_seq)).setOnNetResponse(onShippingDetailNetResponse);
        Net.async(new searchCodeListFreegift()).setOnNetResponse(onFreegiftNetResponse);

        /*if (NetConst.PRODUCT_TYPE_W.equals(NetConst.s_producttype)) { // 정수기이면
            Net.async(new searchCodeAdapter()).setOnNetResponse(onAdapterNetResponse);
            Net.async(new searchCodeJoli()).setOnNetResponse(onJoliNetResponse);
        }*/
    }

    /**
     * 사은품 리스트를 가져온다.
     */
    private void appSelectLatexCode() {
        Net.async(new appSelectLatexCode()).setOnNetResponse(onAppSelectLatexCodeListener);
    }

    @Override
    protected void onUpdateUI() {
        super.onUpdateUI();
    }

    protected Net.OnNetResponse<ShippingMasterDetail> onShippingDetailNetResponse;

    private boolean isLatex;


    // 사은품 주소 변경 이벤트
    private OnClickListener onChangGiftAddr = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!TextUtils.isEmpty(text(R.id.giftAddr))) {
                showProgress();
                Disposable subscribe = ServiceGenerator.createService(AppModifyShippingDivApi.class)
                        .updateGiftAddr(text(R.id.giftAddr), mBodyNo)
                        .subscribeOn(Schedulers.single())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(bfData -> {

                            hideProgress();
                            if (bfData.resultCode == 200) {
                                Toast.makeText(mContext, "수정 완료하였습니다..", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, "수정 실패하였습니다..", Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                Toast.makeText(mContext, "사은품 주소를 입력하세요.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private ArrayList<View> getmAddyViewList() {
        if (mAddyViewList == null) {
            return mAddyViewList = new ArrayList<>();
        }
        return mAddyViewList;
    }

    private void checkProductSerialValid(String serialCd, String itemCd) {
        Observable<BfData> productSerialValidDataObservable;

        productSerialValidDataObservable = ServiceGenerator.createService(WarehouseApi.class)
                .checkProductSerialType(serialCd, itemCd);


        productSerialValidDataObservable.subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<BfData>() {
                               @Override
                               public void onNext(BfData productSerialValidData) {
                                   if (productSerialValidData.resultCode != 200) {
                                       BFDialog.newInstance(mContext).showSimpleDialog(productSerialValidData.resultMsg);
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                                   OH.c().notifyObservers(OH.TYPE.SESSION_OUT);
                               }

                               @Override
                               public void onComplete() {

                               }
                           }
                );
    }


    /**
     * erp 데이터 맵에 저장 및 화면 출력 연결
     *
     * @param info
     * @param serialCd
     * @param resultCode
     */
    private void setProductSerialValidInfo(ProductSerialValidData.ResultData info, String
            serialCd, int resultCode) {
        View layoutViewGroup;
        info.toSlCd = "";
        info.makerCd = "";
        info.serialCd = serialCd;
        Log.d("TAG", "info : " + info.toString());

        switch (resultCode) {
            case RESULT_CODE_BARCODE_PRODUCT:
          /*      info.collectYn = "N";
                info.giftYn = "N";

                mCompleteProductMap.put(RESULT_CODE_BARCODE_PRODUCT, info);
                layoutViewGroup = findViewById(R.id.layoutCheckItemCode);
                setViewValidLayout(info, serialCd, layoutViewGroup);
*/
                setProductVaildView(info, serialCd, "N", "N", RESULT_CODE_BARCODE_PRODUCT, R.id.layoutCheckItemCode);
                break;
            case RESULT_CODE_BARCODE_PRODUCT_2:
            /*    info.collectYn = "N";
                info.giftYn = "N";
                mCompleteProductMap.put(RESULT_CODE_BARCODE_PRODUCT_2, info);
                layoutViewGroup = findViewById(R.id.layoutCheckItemCode2);
                setViewValidLayout(info, serialCd, layoutViewGroup);*/
                setProductVaildView(info, serialCd, "N", "N", RESULT_CODE_BARCODE_PRODUCT_2, R.id.layoutCheckItemCode2);
                break;
            case RESULT_CODE_BARCODE_RETURN_PRODUCT:
               /* info.collectYn = "Y";
                info.giftYn = "N";
                layoutViewGroup = findViewById(R.id.layoutCheckItemCode3);
                setViewValidLayout(info, serialCd, layoutViewGroup);

                mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT, info);*/

                setProductVaildView(info, serialCd, "N", "Y", RESULT_CODE_BARCODE_RETURN_PRODUCT, R.id.layoutCheckItemCode3);
                break;
            case RESULT_CODE_BARCODE_RETURN_PRODUCT_2:
                setProductVaildView(info, serialCd, "N", "Y", RESULT_CODE_BARCODE_RETURN_PRODUCT_2, R.id.layoutCheckItemCode4);
                break;

            case RESULT_CODE_BARCODE_GIFT_ITEM:
                setProductVaildView(info, serialCd, "Y", "N", RESULT_CODE_BARCODE_GIFT_ITEM, R.id.layoutCheckGiftCode1);
                break;

            case RESULT_CODE_BARCODE_GIFT_ITEM_2:
               /* layoutViewGroup = findViewById(R.id.layoutCheckGiftCode2);
                setViewValidLayout(info, serialCd, layoutViewGroup);
                info.giftYn = "Y";
                info.collectYn = "N";
                mCompleteProductMap.put(RESULT_CODE_BARCODE_GIFT_ITEM_2, info);*/

                setProductVaildView(info, serialCd, "Y", "N", RESULT_CODE_BARCODE_GIFT_ITEM_2, R.id.layoutCheckGiftCode2);
                break;

            case RESULT_CODE_BARCODE_GIFT_ITEM_3:
            /*    layoutViewGroup = findViewById(R.id.layoutCheckGiftCode3);
                setViewValidLayout(info, serialCd, layoutViewGroup);
                info.giftYn = "Y";
                info.collectYn = "N";
                mCompleteProductMap.put(RESULT_CODE_BARCODE_GIFT_ITEM_3, info);*/
                setProductVaildView(info, serialCd, "Y", "N", RESULT_CODE_BARCODE_GIFT_ITEM_3, R.id.layoutCheckGiftCode3);
                break;


            case RESULT_CODE_BARCODE_GIFT_ITEM_4:
                /*layoutViewGroup = findViewById(R.id.layoutCheckGiftCode4);
                setViewValidLayout(info, serialCd, layoutViewGroup);
                info.giftYn = "Y";
                info.collectYn = "N";
                mCompleteProductMap.put(RESULT_CODE_BARCODE_GIFT_ITEM_4, info);*/

                setProductVaildView(info, serialCd, "Y", "N", RESULT_CODE_BARCODE_GIFT_ITEM_4, R.id.layoutCheckGiftCode4);
                break;


            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM:
                setProductVaildView(info, serialCd, "Y", "Y", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM, R.id.layoutCheckReturnGiftCode1);
                break;

            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2:
                setProductVaildView(info, serialCd, "Y", "Y", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2, R.id.layoutReturnCheckGiftCode2);
                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3:
                setProductVaildView(info, serialCd, "Y", "Y", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3, R.id.layoutReturnCheckGiftCode3);
                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4:
                setProductVaildView(info, serialCd, "Y", "Y", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4, R.id.layoutReturnCheckGiftCode4);

            case RESULT_CODE_BARCODE_RETURN_PRODUCT_3:
                setProductVaildView(info, serialCd, "N", "Y", RESULT_CODE_BARCODE_RETURN_PRODUCT_3, R.id.layoutCheckItemCode5);
                break;
        }
    }

    private void setProductVaildView(ProductSerialValidData.ResultData info, String serialCd, String giftYn, String collectYn, int result_code_barcode_return_gift_item, int p) {
        View layoutViewGroup;
        info.giftYn = giftYn;
        info.collectYn = collectYn;
        mCompleteProductMap.put(result_code_barcode_return_gift_item, info);
        layoutViewGroup = findViewById(p);
        setViewValidLayout(info, serialCd, layoutViewGroup);
    }

    /**
     * isValid 기준 시리얼 키 색상 변경
     *
     * @param info     ResultData
     * @param serialCd serialCd
     * @param view     view
     */
    private void setViewValidLayout(ProductSerialValidData.ResultData info, String
            serialCd, View view) {
//        view.setTag(info);
        if (info.serialCd != null) {
            TextView itemNameTv = view.findViewById(R.id.tvItemName);
            TextView barcodeTv = view.findViewById(R.id.tvItemBarcode);
            itemNameTv.setText(info.itemNm);
            barcodeTv.setText(serialCd);
            if (info.isValid) {
                barcodeTv.setTextColor(Color.BLUE);
            } else {
                barcodeTv.setTextColor(Color.RED);
            }
        } else {
            TextView barcodeTv = view.findViewById(R.id.tvItemBarcode);
            barcodeTv.setText(serialCd);
            barcodeTv.setTextColor(Color.RED);
        }

        Log.d("info : " + info.toString());
    }

    private void getBarcode(Intent data, int code) {
        if (data.getStringExtra(CaptureActivity.RESULT.RESULT_SN) == null) {
            Toast.makeText(mContext, "바코드 다시 촬영 부탁드립니다.", Toast.LENGTH_LONG).show();
            return;
        }
        String strSn = data.getStringExtra(CaptureActivity.RESULT.RESULT_SN);
        ProductSerialValidData.ResultData outProductData;

        switch (code) {
            case RESULT_CODE_BARCODE_PRODUCT:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_PRODUCT);

                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_PRODUCT);
                }
                break;
            case RESULT_CODE_BARCODE_MANUFACTURER:
                setManufacturerBarcode(strSn, R.id.layoutManufacturerCode, R.id.tvBarcodeContent, RESULT_CODE_BARCODE_PRODUCT);
                break;
            case RESULT_CODE_BARCODE_PRODUCT_2:

                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_PRODUCT_2);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_PRODUCT_2);
                }
                break;
            case RESULT_CODE_BARCODE_MANUFACTURER_2:
                setManufacturerBarcode(strSn, R.id.layoutManufacturerCode2, R.id.tvBarcodeContent, RESULT_CODE_BARCODE_PRODUCT_2);
                break;
            case RESULT_CODE_BARCODE_RETURN_PRODUCT:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_PRODUCT);
                }
                break;
            case RESULT_CODE_BARCODE_RETURN_MANUFACTURER:
                setManufacturerBarcode(strSn, R.id.layoutManufacturerCode3, R.id.tvBarcodeContent, RESULT_CODE_BARCODE_RETURN_PRODUCT);
                break;
            case RESULT_CODE_BARCODE_RETURN_PRODUCT_2:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
                }
                break;

            case RESULT_CODE_BARCODE_RETURN_MANUFACTURER_2:
                setManufacturerBarcode(strSn, R.id.layoutManufacturerCode4, R.id.tvBarcodeContent, RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
                break;

            case RESULT_CODE_BARCODE_GIFT_ITEM:

                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_GIFT_ITEM);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_GIFT_ITEM);
                }
                break;
            case RESULT_CODE_BARCODE_GIFT_ITEM_2:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_GIFT_ITEM_2);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_GIFT_ITEM_2);
                }
                break;


            case RESULT_CODE_BARCODE_GIFT_ITEM_3:

                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_GIFT_ITEM_3);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_GIFT_ITEM_3);
                }
                break;
            case RESULT_CODE_BARCODE_GIFT_ITEM_4:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_GIFT_ITEM_4);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_GIFT_ITEM_4);
                }
                break;


            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
                if (outProductData != null) {
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
                }
                break;

            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2);
                }
                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3);
                }
                break;

            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4:
                outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4);
                if (outProductData != null) {
                    checkProductSerialValid(strSn, outProductData.itemCd);
                    outProductData.isValid = true;
                    setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4);
                }
                break;

        }
    }


    private void showDefaultItemCodeView() {
        if (mCompleteProductMap.get(RESULT_CODE_BARCODE_PRODUCT) != null) {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_PRODUCT, mCompleteProductMap.get(RESULT_CODE_BARCODE_PRODUCT));
        } else {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_PRODUCT, new ProductSerialValidData.ResultData());
        }
        showItemCodeView(R.id.layoutCheckItemCode, "제품 코드", RESULT_CODE_BARCODE_PRODUCT);
        showManufacturerCodeView(R.id.layoutManufacturerCode, "제조사 코드", RESULT_CODE_BARCODE_MANUFACTURER);
    }

    private void showDefaultTwoItemCodeView() {
        if (mCompleteProductMap.get(RESULT_CODE_BARCODE_PRODUCT_2) != null) {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_PRODUCT_2, mCompleteProductMap.get(RESULT_CODE_BARCODE_PRODUCT_2));
        } else {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_PRODUCT_2, new ProductSerialValidData.ResultData());
        }

        showItemCodeView(R.id.layoutCheckItemCode2, "제품2 코드", RESULT_CODE_BARCODE_PRODUCT_2);
        showManufacturerCodeView(R.id.layoutManufacturerCode2, "제조사 코드", RESULT_CODE_BARCODE_MANUFACTURER_2);
    }


    private void showReturnOneBarcodeView() {
        if (mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT) != null) {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT, mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT));
        } else {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT, new ProductSerialValidData.ResultData());
        }
        findViewById(R.id.layoutReturnReason).setVisibility(View.VISIBLE);
        findViewById(R.id.layoutReturnWarehouse).setVisibility(View.VISIBLE);

        if (mReturnSlCd.isEmpty())
            getWarehouseList();

        showItemCodeView(R.id.layoutCheckItemCode3, "회수 제품 코드", RESULT_CODE_BARCODE_RETURN_PRODUCT);
        showManufacturerCodeView(R.id.layoutManufacturerCode3, "회수 제조사 코드", RESULT_CODE_BARCODE_RETURN_MANUFACTURER);
    }

    private void showReturnTwoBarcodeView() {
       /* if (mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT_2) != null) {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT_2, mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT_2));
        } else {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT_2, new ProductSerialValidData.ResultData());
        }*/
        showItemCodeView(R.id.layoutCheckItemCode4, "회수2 제품 코드", RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
        showManufacturerCodeView(R.id.layoutManufacturerCode4, "회수2 제조사 코드", RESULT_CODE_BARCODE_RETURN_MANUFACTURER_2);
    }


    /**
     * 부분 맞교체 뷰 노출 d21
     */
    private void showReturnThreeBarcodeView() {

        if (mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT_3) != null) {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT_3, mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_PRODUCT_3));
        } else {
            mCompleteProductMap.put(RESULT_CODE_BARCODE_RETURN_PRODUCT_3, new ProductSerialValidData.ResultData());
        }


        findViewById(R.id.layoutReturnReason).setVisibility(View.VISIBLE);
        findViewById(R.id.layoutReturnWarehouse).setVisibility(View.VISIBLE);

        if (mReturnSlCd.isEmpty())
            getWarehouseList();

        showItemCodeView(R.id.layoutCheckItemCode5, "맞교체 코드", RESULT_CODE_BARCODE_RETURN_PRODUCT_3);

    }

    private void showGiftTwoBarcodeView() {
        showItemCodeView(R.id.layoutCheckGiftCode2, "사은품2 코드", RESULT_CODE_BARCODE_GIFT_ITEM_2);
    }

    private void showGiftOneBarcodeView() {
        showItemCodeView(R.id.layoutCheckGiftCode1, "사은품1 코드", RESULT_CODE_BARCODE_GIFT_ITEM);
    }


    private void showGiftThreeBarcodeView() {
        showItemCodeView(R.id.layoutCheckGiftCode3, "사은품3 코드", RESULT_CODE_BARCODE_GIFT_ITEM_3);
    }

    private void showGiftFourBarcodeView() {
        showItemCodeView(R.id.layoutCheckGiftCode4, "사은품4 코드", RESULT_CODE_BARCODE_GIFT_ITEM_4);
    }


    private void showReturnGiftTwoBarcodeView() {
        showItemCodeView(R.id.layoutReturnCheckGiftCode2, "회수 사은품2 코드", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2);
    }

    private void showReturnGiftOneBarcodeView() {
        showItemCodeView(R.id.layoutCheckReturnGiftCode1, "회수 사은품1 코드", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
    }

    private void showReturnGiftThreeBarcodeView() {
        showItemCodeView(R.id.layoutReturnCheckGiftCode3, "회수 사은품3 코드", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3);
    }

    private void showReturnGiftFourBarcodeView() {
        showItemCodeView(R.id.layoutReturnCheckGiftCode4, "회수 사은품4 코드", RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4);
    }


    private void dismissReturnBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_PRODUCT);
//        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
        findViewById(R.id.layoutCheckItemCode3).setVisibility(View.GONE);
        findViewById(R.id.layoutManufacturerCode3).setVisibility(View.GONE);

        findViewById(R.id.layoutReturnReason).setVisibility(View.GONE);
        findViewById(R.id.layoutReturnWarehouse).setVisibility(View.GONE);

        mReturnReason = "";
        mReturnSlCd = "";


        ((TextView) findViewById(R.id.layoutCheckItemCode3).findViewById(R.id.tvItemBarcode)).setText("");
        ((TextView) findViewById(R.id.layoutManufacturerCode3).findViewById(R.id.tvBarcodeContent)).setText("");
    }

    private void dismissReturnBarcodeView2() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
//        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
        findViewById(R.id.layoutCheckItemCode4).setVisibility(View.GONE);
        findViewById(R.id.layoutManufacturerCode4).setVisibility(View.GONE);

        ((TextView) findViewById(R.id.layoutCheckItemCode4).findViewById(R.id.tvItemBarcode)).setText("");
        ((TextView) findViewById(R.id.layoutManufacturerCode4).findViewById(R.id.tvBarcodeContent)).setText("");

    }

    private void dismissReturnBarcodeView3() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_PRODUCT_3);

        findViewById(R.id.layoutCheckItemCode5).setVisibility(View.GONE);
        findViewById(R.id.layoutCheckItemCode5).setVisibility(View.GONE);
        findViewById(R.id.layoutReturnReason).setVisibility(View.GONE);
        findViewById(R.id.layoutReturnWarehouse).setVisibility(View.GONE);

        mReturnReason = "";
        mReturnSlCd = "";

        ((TextView) findViewById(R.id.layoutCheckItemCode5).findViewById(R.id.tvItemBarcode)).setText("");


    }


    private void dismissDefaultItemCodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_PRODUCT);
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_PRODUCT_2);
        findViewById(R.id.layoutCheckItemCode).setVisibility(View.GONE);
        findViewById(R.id.layoutManufacturerCode).setVisibility(View.GONE);

        findViewById(R.id.layoutCheckItemCode2).setVisibility(View.GONE);
        findViewById(R.id.layoutManufacturerCode2).setVisibility(View.GONE);

        ((TextView) findViewById(R.id.layoutCheckItemCode).findViewById(R.id.tvItemBarcode)).setText("");
        ((TextView) findViewById(R.id.layoutManufacturerCode).findViewById(R.id.tvBarcodeContent)).setText("");
    }

    private void dismissDefaultTwoItemCodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_PRODUCT_2);
        findViewById(R.id.layoutCheckItemCode2).setVisibility(View.GONE);
        findViewById(R.id.layoutManufacturerCode2).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.layoutCheckItemCode).findViewById(R.id.tvItemBarcode)).setText("");
        ((TextView) findViewById(R.id.layoutManufacturerCode).findViewById(R.id.tvBarcodeContent)).setText("");
    }


    private void dismissGiftOneBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_GIFT_ITEM);
        findViewById(R.id.layoutCheckGiftCode1).setVisibility(View.GONE);
    }

    private void dismissGiftThreeBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_GIFT_ITEM_3);
        findViewById(R.id.layoutCheckGiftCode3).setVisibility(View.GONE);
    }

    private void dismissGiftFourBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_GIFT_ITEM_4);
        findViewById(R.id.layoutCheckGiftCode4).setVisibility(View.GONE);
    }

    private void dismissGiftTwoBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_GIFT_ITEM_2);
        findViewById(R.id.layoutCheckGiftCode2).setVisibility(View.GONE);
    }


    private void dismissReturnGiftOneBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
        findViewById(R.id.layoutCheckReturnGiftCode1).setVisibility(View.GONE);
    }

    private void dismissReturnGiftTwoBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2);
        findViewById(R.id.layoutReturnCheckGiftCode2).setVisibility(View.GONE);
    }

    private void dismissReturnGiftTreeBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3);
        findViewById(R.id.layoutReturnCheckGiftCode3).setVisibility(View.GONE);
    }

    private void dismissReturnGiftFourBarcodeView() {
        mCompleteProductMap.remove(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4);
        findViewById(R.id.layoutReturnCheckGiftCode4).setVisibility(View.GONE);
    }


    private void getWarehouseList() {
        Observer<WarehouseListData> observer = ServiceGenerator.createService(WarehouseApi.class)
                .selectWarehouseList("1000")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribeWith(new Observer<WarehouseListData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(WarehouseListData warehouseListData) {
                        ArrayList<String> arrSpinner = new ArrayList<String>();
                        arrSpinner.add("창고선택");
                        for (WarehouseListData.ResultData d : Objects.requireNonNull(warehouseListData.getResultDataList())) {
                            arrSpinner.add(d.getSlNm());
                        }


                        Spinner s = (Spinner) findViewById(R.id.spinnerWarehouse);
                        s.setTag(warehouseListData);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, arrSpinner);
                        s.setAdapter(adapter);

                        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    Toast.makeText(ShippingMasterDetailActivity.this, "창고를 선택하세요.", Toast.LENGTH_LONG).show();
                                } else {
                                    mReturnSlCd = warehouseListData.getResultDataList().get(position - 1).getSlCd();
                                    Log.d("TAG", "mReturnSlCd : " + mReturnSlCd);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void setManufacturerBarcode(String strSn, int viewGroup, int barcodeTextViewId,
                                        int key) {
        if (mCompleteProductMap.get(key) != null) {
            ProductSerialValidData.ResultData resultData = mCompleteProductMap.get(key);
            resultData.makerCd = strSn;
            mCompleteProductMap.put(key, resultData);
            ((TextView) (findViewById(viewGroup)).findViewById(barcodeTextViewId)).setText(strSn);
        } else {
            Toast.makeText(mContext, "제품 바코드 확인 부탁드립니다.", Toast.LENGTH_SHORT).show();
        }

    }


    private View showItemCodeView(int layoutId, String title, int resultCode) {
        View itemBarCodeView = findViewById(layoutId);

        itemBarCodeView.setVisibility(View.VISIBLE);
        ((TextView) (itemBarCodeView.findViewById(R.id.tvCheckCodeTitle))).setText(title);

        switch (resultCode) {
            case RESULT_CODE_BARCODE_GIFT_ITEM:
            case RESULT_CODE_BARCODE_GIFT_ITEM_2:
            case RESULT_CODE_BARCODE_GIFT_ITEM_3:
            case RESULT_CODE_BARCODE_GIFT_ITEM_4:
//                itemBarCodeView.findViewById(R.id.btnBarcode).setVisibility(View.GONE);
                showDialogInventoryList(resultCode, itemBarCodeView, true);
                break;
            case RESULT_CODE_BARCODE_PRODUCT:
            case RESULT_CODE_BARCODE_PRODUCT_2:
            case RESULT_CODE_BARCODE_RETURN_PRODUCT_3:

//                if (isOutDelivery()) {
//                    itemBarCodeView.findViewById(R.id.btnBarcode).setVisibility(View.GONE);
//                }
//                itemBarCodeView.findViewById(R.id.btnBarcode).setVisibility(View.GONE);
                showDialogInventoryList(resultCode, itemBarCodeView, false);
                break;
            case RESULT_CODE_BARCODE_RETURN_PRODUCT:
            case RESULT_CODE_BARCODE_RETURN_PRODUCT_2:
//                (itemBarCodeView.findViewById(R.id.btnBarcode)).setVisibility(View.GONE);
                showDialogReturnProduct(resultCode, itemBarCodeView);

                break;
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM:
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2:
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_3:
            case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_4:
                showDialogReturnGiftList(resultCode, itemBarCodeView);
                break;
        }


        (itemBarCodeView.findViewById(R.id.btnBarcode)).setOnClickListener(
                v -> {
                    Intent intent = new Intent(mContext, CaptureActivity.class);
                    startActivityForResult(intent, resultCode);
                }
        );
        return itemBarCodeView;
    }


    /**
     * 외주 본사 구분
     *
     * @return true 외주기사
     */
    private boolean isOutDelivery() {
        return Auth.d.resultData != null && Auth.d.resultData.PER_CD != PER_CD;
    }

    private void showDialogReturnProduct(int resultCode, View itemBarCodeView) {
        (itemBarCodeView.findViewById(R.id.tvItemName)).setOnClickListener(
                v -> {
                    String groupCode = "";
                    if (mResultData.PRODUCT_TYPE.equals("M")) {
                        groupCode = "A10000";
                    } else if (mResultData.PRODUCT_TYPE.equals("W")) {
                        groupCode = "W10000";
                    } else {
                        groupCode = "L10000";
                    }
                    ReturnProductDialogFragment.newInstance(groupCode, resultCode).show(getSupportFragmentManager(), "returnProduct");
                }
        );
    }

    private void showDialogInventoryList(int resultCode, View itemBarCodeView, boolean isGift) {

        (itemBarCodeView.findViewById(R.id.tvItemName)).setOnClickListener(
                v -> {
                    if (!isOutDelivery()) {
                        DeliveryInventoryDialogFragment deliveryInventoryDialogFragment;
                        if (isGift) {
                            // 사은품 리스트
                            deliveryInventoryDialogFragment = DeliveryInventoryDialogFragment.newInstance(DeliveryInventoryDialogFragment.DeliveryInventoryType.GIFT, resultCode);
                        } else {
                            //
                            deliveryInventoryDialogFragment = DeliveryInventoryDialogFragment.newInstance(DeliveryInventoryDialogFragment.DeliveryInventoryType.PRODUCT, resultCode);
                        }
                        deliveryInventoryDialogFragment.show(getSupportFragmentManager(), "stock");
                    } else {
                        // 외주 업체 제고 리스트
                        WarehouseStockListDialog warehouseStockListDialog = WarehouseStockListDialog.newInstance(isGift, resultCode);
                        warehouseStockListDialog.show(getSupportFragmentManager(), "stock");
                    }
                }
        );
    }


    private void showDialogReturnGiftList(int resultCode, View itemBarCodeView) {

        (itemBarCodeView.findViewById(R.id.tvItemName)).setOnClickListener(
                v -> {
                    GiftDialogFragment giftDialog;
                    giftDialog = GiftDialogFragment.newInstance(resultCode);
                    giftDialog.show(getSupportFragmentManager(), "giftDialog");
                }
        );
    }

    private View showManufacturerCodeView(int layoutId, String title, int resultCode) {
        View itemBarCodeView = findViewById(layoutId);
        itemBarCodeView.setVisibility(View.VISIBLE);
        ((TextView) (itemBarCodeView.findViewById(R.id.tvCheckCodeTitle))).setText(title);
        (itemBarCodeView.findViewById(R.id.btnManufacturerBarcode)).setOnClickListener(
                v -> {
                    Intent intent = new Intent(mContext, CaptureActivity.class);
                    startActivityForResult(intent, resultCode);
                }
        );
        return itemBarCodeView;
    }


    private String mBodyNo;

    {
        onShippingDetailNetResponse = new Net.OnNetResponse<ShippingMasterDetail>() {

            @SuppressLint("InflateParams")
            @Override
            public void onResponse(ShippingMasterDetail response) {
                hideProgress();

                resultData r = mResultData = response.data.resultData;

                final String shippingType = r.SHIPPING_TYPE;
                final String progressNo = r.PROGRESS_NO; // 진행상태


                showProductCodeView(shippingType, progressNo, r.PRODUCT_NAME, r.FREEGIFT_ONE, r.FREEGIFT_TWO);


                setText(R.id.deliveryman1, r.DELIVERYMAN_NM1);
                setText(R.id.deliveryman2, r.DELIVERYMAN_NM2);
                setText(R.id.deliveryman3, r.DELIVERYMAN_NM3);

                if (shippingType.equals(배송)) { // 배송
                } else if (shippingType.equals(AS)) {
//                    dismissDefaultItemCodeView();
                    setupAs();
                } else if (shippingType.equals(맞교)) {// 맞교체 인경우

                    setupTrade();
                } else if (shippingType.equals(계철)) {// 계약철회
//                    dismissDefaultItemCodeView();
                    setupCancelContract();
                } else if (shippingType.equals(이전) || shippingType.equals(수리)) { // 이전이면서 수리인경우
//                    dismissDefaultItemCodeView();
                    if (progressNo.equals(회수)) {
                        setupCollection();

                    } else if (progressNo.equals(이동)) {
                        setupMove(r);
                    }
                } // end of 이전 or 수리

                setupCommon(r);

                setupMove(r, shippingType);

                try {
                    setText(R.id.server_time, SDF.yyyymmdd__.format(SDF.yyyymmdd.parse(r.SERVER_TIME)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Log.d("r.imageone.substring(1) : " + r.imageone.substring(1));

                if ((shippingType.equals(이전) || shippingType.equals(수리)) && (progressNo.equals(회수) || progressNo.equals(이동))) { // 이전이면서 수리인경우
                    if (!TextUtils.isEmpty(r.backimageone.trim())) {
                        setImage(R.id.imageone, NetConst.host + r.backimageone.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.backimagetwo.trim())) {
                        setImage(R.id.imagetwo, NetConst.host + r.backimagetwo.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.backimagethree.trim())) {
                        setImage(R.id.imagethree, NetConst.host + r.backimagethree.substring(1));
                    }
                } else {
                    if (!TextUtils.isEmpty(r.imageone.trim())) {
                        setImage(R.id.imageone, NetConst.host + r.imageone.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.imagetwo.trim())) {
                        setImage(R.id.imagetwo, NetConst.host + r.imagetwo.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.imagethree.trim())) {
                        setImage(R.id.imagethree, NetConst.host + r.imagethree.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.imagefour.trim())) {
                        setImage(R.id.imagefour, NetConst.host + r.imagefour.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.imagefive.trim())) {
                        setImage(R.id.imagefive, NetConst.host + r.imagefive.substring(1));
                    }
                    if (!TextUtils.isEmpty(r.imagesix.trim())) {
                        setImage(R.id.imagesix, NetConst.host + r.imagesix.substring(1));
                    }
                }

                if (r.PROMISE != null) {
                    setChecked(r.PROMISE.equals("Y") ? R.id.promiseY : R.id.promiseN, true);
                }

                isLatex = r.PRODUCT_TYPE.equalsIgnoreCase("L");
                // 라텍스 이면 레이아웃과 라인을 보여준다
                setVisibility(R.id.freegift_l_line, isLatex);
                // 라텍스 이면 레이아웃과 라인을 보여준다
                setVisibility(R.id.freegift_l_layout, isLatex);

                // 라텍스인경우 사은품 정보를 가져온다.
                if (isLatex) appSelectLatexCode();

                reqOtherApi(r);

                addTemporarilyImage();
            }

            /**
             * 그냥 뭐 공통적으로 적용될것들에 대한 세팅~.
             *
             * @param r
             */
            private void setupCommon(resultData r) {
                r.PROMISE = r.PROMISE.replaceAll(" ", "");
                mBodyNo = r.BODY_NO;
                setText(R.id.body_num, r.BODY_NO);
                setText(R.id.cust_name, r.CUST_NAME);

                String symptom = "";
                if (!TextUtils.isEmpty(r.SYMPTOM)) {
                    symptom = "<br/> 증상 : " + r.SYMPTOM;
                }
                setText(R.id.company_check, Html.fromHtml(r.COMPANY_CHECK + symptom));
                if (!TextUtils.isEmpty(r.FREEGIFT_ONE.trim()) && !r.FREEGIFT_ONE.trim().equals("없음")) {


                    setText(R.id.freegift_one, r.FREEGIFT_ONE);
                    (findViewById(R.id.freegift_layout)).setVisibility(View.VISIBLE);
//                    (findViewById(R.id.giftAddrLayout)).setVisibility(View.VISIBLE);
                    ((TextView) (findViewById(R.id.freegift_one_title))).setText("사은품 " + r.FREEGIFT_ONE);
                    ((TextView) (findViewById(R.id.giftAddr))).setText(r.FREEGIFT_ADDR);

                    setOnClickListener(R.id.buttonModifyGiftAddress, onChangGiftAddr);

                    if (!TextUtils.isEmpty(r.FREEGIFT_TWO.trim()) && !r.FREEGIFT_TWO.trim().equals("없음")) {

                        setText(R.id.freegift_two, r.FREEGIFT_TWO);
                        ((TextView) (findViewById(R.id.freegift_two_title))).setText("사은품 " + r.FREEGIFT_TWO);
                    } else {
                        findViewById(R.id.freegift_two_title).setVisibility(View.GONE);
                        findViewById(R.id.m_freegiftcheck2).setVisibility(View.GONE);
                    }
                } else {
                    (findViewById(R.id.freegift_layout)).setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(r.FREEGIFT_TWO.trim()) && !r.FREEGIFT_TWO.trim().equals("없음")) {
                    (findViewById(R.id.freegift_layout)).setVisibility(View.VISIBLE);

                    setText(R.id.freegift_two, r.FREEGIFT_TWO);

                    ((TextView) (findViewById(R.id.giftAddr))).setText(r.FREEGIFT_ADDR);

                    setOnClickListener(R.id.buttonModifyGiftAddress, onChangGiftAddr);


                    ((TextView) (findViewById(R.id.freegift_two_title))).setText("사은품 " + r.FREEGIFT_TWO);
                } else {
                    findViewById(R.id.freegift_two_title).setVisibility(View.GONE);
                    findViewById(R.id.m_freegiftcheck2).setVisibility(View.GONE);
                }


                if (r.CUST_UPDATE != null && r.CUST_UPDATE.equals("AD")) {
                    findViewById(R.id.dueDateLayout).setBackgroundColor(Color.parseColor("#FAF4C0"));
                    findViewById(R.id.insaddrLayout).setBackgroundColor(Color.parseColor("#FAF4C0"));
                    findViewById(R.id.buttonAdCheck).setVisibility(View.VISIBLE);
                    findViewById(R.id.buttonAdCheck).setOnClickListener(v -> Net.async(new ModifyAddressConfirm(m_shippingseq), new Net.OnNetResponse<ModifyAddressConfirm>() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }

                        @Override
                        public void onResponse(ModifyAddressConfirm response) {
                            if (response.data.resultCode == 200) {
                                reload();
                            } else {
                                Toast.makeText(mContext, response.data.resultMsg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }));
                }
                setText(R.id.due_date, r.DUE_DATE);
                setText(R.id.hphone_no, r.HPHONE_NO);
                setText(R.id.insaddr, r.INSADDR);
                setText(R.id.product_name, r.PRODUCT_NAME);


                // 사다리 사용여부가 true이면 라디오버튼을 true로 한다.
                if (r.IS_LADDER) {
                    setChecked(R.id.ladder_y, true);

                    // 사다리 첨부 이미지1 적용
                    setImage(R.id.ladder_image_one, NetConst.host + r.ladder_image_one);
                    // 사다리 첨부 이미지2 적용
                    setImage(R.id.ladder_image_two, NetConst.host + r.ladder_image_two);
//                    setImage(R.id.ladder_image_three, NetConst.host + r.ladder_image_two);

                    // 사다리 이용금액을 넣는다.
                    setText(R.id.m_ladder_price, r.LADDER_PRICE);
                    setText(R.id.additional_ladder_price, r.ADDITIONAL_LADDER_PRICE);

//                    setText(R.id.m_extra_cost_memo, r.m_extra_cost_memo);


                    try {
                        // 1: 카드 2: 계좌 3: 현금
                        int position = r.LADDER_PAYMENT_TYPE;

                        AdapterView ladder_payment_type = (AdapterView) findViewById(R.id.ladder_payment_type);
                        if (position <= 3) {
                            ladder_payment_type.setSelection(position);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } // end of try-catch

                } else {
                    setChecked(R.id.ladder_n, true);
                }
                //양중 사용여부가 true이면 라디오버튼을 true로 한다.
                if (r.IS_SOCKET) {
                    setChecked(R.id.concent_y, true);

                    // 콘센트 이미지
                    setImage(R.id.concent_image_one, NetConst.host + r.socket_image_one);
                } else {
                    setChecked(R.id.concent_n, true);
                }

                setChecked(R.id.concent_y, true);

                setupReferName(r);

                setText(R.id.purchasing_office, r.PURCHASING_OFFICE);
                setText(R.id.qty, r.QTY);
                setText(R.id.receive_date, r.RECEIVE_DATE);
                setText(R.id.service_month, r.SERVICE_MONTH);
                setText(R.id.tel_no, r.TEL_NO);
                setText(R.id.m_productsn, r.PRODUCT_SN);

//                setVisibility(R.id.free_gift_warn, r.free_gift_warn);
            }

            /**
             * 이전
             *
             * @param r
             * @param shippingType
             */
            private void setupMove(resultData r, String shippingType) {
                if (shippingType.equals(이전)) {
                    // 이전이면서 설치일 경우 보이는 뷰가 다르다.
                    setText(R.id.고객명, "회수자"); // 라벨 변경
                    // 회수지역
                    setText(R.id.주소, "회수주소"); // 라벨 변경
                    setText(R.id.집전화, "회수자\n전화번호"); // 회수자 전화번호
                    setText(R.id.핸드폰, "회수자\n핸드폰번호"); // 회수자 핸드폰번호
                    setText(R.id.cust_name, r.AGREE_NAME); // 회수자
                    setText(R.id.hphone_no, r.HPHONE_NO2);
                    setText(R.id.tel_no, r.TEL_NO2);

                    setVisibility(R.id.form_회수_설치_1, true);
                    setVisibility(R.id.form_회수_설치_2, true);
                    setText(R.id.agree_name, r.CUST_NAME); // 설치자
                    setText(R.id.insaddr2, r.INSADDR2); // 설치주소
                    setText(R.id.tel_no2, r.TEL_NO); // 설치자 전화번호
                    setText(R.id.hphone_no2, r.HPHONE_NO); // 설치자 핸드폰번호
                    setVisibility(R.id.bigo_layout, true); // 설치비고
                    setVisibility(R.id.bigo_layout_line, true);
                }

            }

            private void reqOtherApi(resultData r) {
                Net.async(new searchCodeListDeliveryGb()).setOnNetResponse(onDeliveryGbNetResponse);
                Net.async(new searchCodeListArea()).setOnNetResponse(onLocationGbNetResponse);
                Net.async(new searchCodeListProductBonsa()).setOnNetResponse(onBonsaGbNetResponse);
                Net.async(new searchCodeListProductGb()).setOnNetResponse(onProductGbNetResponse);
                Net.async(new searchCodeListProductName()).setOnNetResponse(onProductNameNetResponse);
                Net.async(new selectDeliveryManList(r.DIV_STATUS)).setOnNetResponse(onManNetResponse);
            }

            /**
             * 이동
             *
             * @param r
             */
            private void setupMove(resultData r) {
                replaceTexT(R.id.btn2, "설치", "회수");

                ViewGroup viewGroup = (LinearLayout) findViewById(R.id.photo_layout);
                if (viewGroup != null) {
                    viewGroup.removeAllViewsInLayout();

                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.shipping_master_detail_body2_photo_1, null, false);
                    viewGroup.addView(view);

                    setOnClickListener(R.id.imageone, null);
                    setOnClickListener(R.id.imagetwo, null);
                    setOnClickListener(R.id.imagethree, null);
                    setOnClickListener(R.id.m_collection_sn, null);
                    findViewById(R.id.m_collection_sn).setClickable(false);
                    findViewById(R.id.m_collection_sn).setFocusable(false);
                    setText(R.id.m_collection_sn, r.BACK_SN);
                }

                setVisibility(R.id.sign_layout, false);
                setVisibility(R.id.sign_layout_line, false);
                setVisibility(R.id.modify, false);

                setVisibility(R.id.install_layout, false); // 설치일
                setVisibility(R.id.install_layout_line, false);
                setVisibility(R.id.install_layout2, false);// 설치여부
                setVisibility(R.id.install_layout2_line, false);
                setVisibility(R.id.bigo_layout, true); // 설치비고
                setVisibility(R.id.bigo_layout_line, true);
                setVisibility(R.id.productsn_layout, false); // 제품S/N
                setVisibility(R.id.productsn_layout_line, false);
                setVisibility(R.id._collection_sn_layout, true);
                setVisibility(R.id._collection_sn_line, true);
                setVisibility(R.id.freegift_layout, false);// 사은품

//                setVisibility(R.id.additional_ladder_price, true);// 사은품
//                TextUtil.getInstance().setWon((EditText) findViewById(R.id.additional_ladder_price));
//                m_extra_cost_memo = text(R.id.m_extra_cost_memo);

            }

            /**
             * 회수
             */
            private void setupCollection() {
                replaceTexT(R.id.btn2, "설치", "회수");

                // ㄱ. 회수확인서 : 고객서명, 사진배열 3장 (회수S/N, 회수, 회수첨부1), 회수S/N, 회수확인
                // ㄴ. 회수확인시 현재 진행상태 변경 팝업 (회수완료, 당일회수설치)
//                dismissDefaultItemCodeView();
//                showReturnBarcodeView();
//                ViewGroup viewGroup = (LinearLayout) findViewById(R.id.photo_layout);
              /*  if (viewGroup != null) {
                    viewGroup.removeAllViewsInLayout();

                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.shipping_master_detail_body2_photo_1, null, false);
                    viewGroup.addView(view);

                    setOnClickListener(R.id.imageone, onSn2ImageClickListener);
                    setOnClickListener(R.id.imagetwo, onImageClickListener2);
                    setOnClickListener(R.id.imagethree, onImageClickListener2);

                }*/

                setOnClickListener(R.id.imageone, onSn2ImageClickListener);
//                setOnClickListener(R.id.imagetwo, onImageClickListener2);
                setText(R.id.modify, "회수완료");

                setVisibility(R.id.install_layout, false); // 설치일
                setVisibility(R.id.install_layout_line, false);
                setVisibility(R.id.install_layout2, false);// 설치여부
                setVisibility(R.id.install_layout2_line, false);
                setVisibility(R.id.bigo_layout, true); // 설치비고
                setVisibility(R.id.bigo_layout_line, true);
                setVisibility(R.id.productsn_layout, false); // 제품S/N
                setVisibility(R.id.productsn_layout_line, false);
                setVisibility(R.id._collection_sn_layout, true);
                setVisibility(R.id._collection_sn_line, true);
                setVisibility(R.id.freegift_layout, false);// 사은품
            }

            /**
             * 계약철회
             */
            private void setupCancelContract() {
                String regularExpression = "설치";
                String replacement = "회수";

                replaceTexT(R.id.btn2, regularExpression, replacement);
                replaceTexT(R.id.textTwo, regularExpression, replacement);
                replaceTexT(R.id.install_day, regularExpression, replacement);
                replaceTexT(R.id.install_yn, regularExpression, replacement);
                replaceTexT(라디오_설치완료, regularExpression, replacement);
                replaceTexT(라디오_설치불가, regularExpression, replacement);
                replaceTexT(R.id.bigo, regularExpression, replacement);
            }

            /**
             * 맞교체
             */
            private void setupTrade() {
                ViewGroup viewGroup = (LinearLayout) findViewById(R.id.photo_layout);
                if (viewGroup != null) {
                    viewGroup.removeAllViewsInLayout();

                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.shipping_master_detail_body2_photo_d20, null, false);
                    viewGroup.addView(view);

                    setOnClickListener(R.id.imageone, onSnImageClickListener);
                    setOnClickListener(R.id.imagetwo, onImageClickListener2);
                    setOnClickListener(R.id.imagethree, onImageClickListener2);
                    setOnClickListener(R.id.imagefour, onSn2ImageClickListener);
                    setOnClickListener(R.id.imagefive, onImageClickListener2);
                    setOnClickListener(R.id.imagesix, onImageClickListener2);

                    setVisibility(R.id._collection_sn_layout, true);
                    setVisibility(R.id._collection_sn_line, true);

                }
            }


            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        };
    }

    /**
     * AS
     */
    private void setupAs() {
        setVisibility(R.id.layout_d70, true);

        if (mResultData.PAYMENT_TYPE != null) 입금유형(mResultData.PAYMENT_TYPE);

        setSpinner(R.id.spinnerCostType, R.array.umusang);
        if (mResultData.COSTNCOST.equals("1")) {
            setSelection(R.id.spinnerCostType, 1);
        }

        setText(R.id.editCostSum, mResultData.COST_AMOUNT);

        setSpinner(R.id.spinnerCostInputType, R.array.deposit);
        setSpinner(R.id.spinnerReceipt, R.array.phoneOrCard);

    }

    /**
     * 배송 타입별 erp 상품 입력 출력
     *
     * @param shippingType
     * @param progressNo
     * @param productName
     * @param giftOne
     * @param giftTwo
     */
    private void showProductCodeView(String shippingType, String progressNo, String
            productName, String giftOne, String giftTwo) {
        try {
            ReturnShippingType returnShippingType = ReturnShippingType.valueOf(shippingType);
            switch (returnShippingType) {
                /**
                 * 계약 철회
                 **/
                case D30:
                    dismissDefaultItemCodeView();
                    showReturnOneBarcodeView();
                    if (giftOne.contains("2EA")) {
                        showReturnGiftThreeBarcodeView(giftOne);
                    }

                    if (giftTwo.contains("2EA")) {
                        showReturnGiftFourBarcodeView(giftTwo);
                    }
                    showReturnGiftOneBarcodeView(giftOne);
                    showReturnGiftTwoBarcodeView(giftTwo);
                    if (productName.contains("+")) {
                        showReturnTwoBarcodeView();
                    }

                    break;
                /**
                 * 회수
                 * **/
                case P10: // 이전 요청

                    if (progressNo.trim().equals(회수)) {
                        showReturnOneBarcodeView();
//                        showReturnGiftOneBarcodeView(giftOne);
//                        showReturnGiftTwoBarcodeView(giftTwo);
                        dismissDefaultItemCodeView();

                        if (productName.contains("+")) {
                            showReturnTwoBarcodeView();
                        }
                    }
                    break;
                case D130:
                case D140:
                    showReturnOneBarcodeView();
                    dismissDefaultItemCodeView();

                    if (productName.contains("+")) {
                        showReturnTwoBarcodeView();
                    }
                    break;
                /**
                 * 설치
                 **/
                case D20: // 맞교체
                    showReturnOneBarcodeView();
                    showDefaultItemCodeView();


                    if (giftOne.contains("2EA")) {
                        showGiftThreeBarcodeView();
                        showReturnGiftThreeBarcodeView(giftOne);
                    }

                    if (giftTwo.contains("2EA")) {
                        showGiftFourBarcodeView();
                        showReturnGiftFourBarcodeView(giftTwo);
                    }
               /* if (giftOne.contains("2EA")) {
                    showReturnGiftThreeBarcodeView(giftOne);
                }

                if (giftTwo.contains("2EA")) {
                    showReturnGiftFourBarcodeView(giftTwo);
                }*/

                    showGiftOneCodeView(giftOne);
                    showGiftTwoCodeView(giftTwo);
                    showReturnGiftOneBarcodeView(giftOne);
                    showReturnGiftTwoBarcodeView(giftTwo);
                    if (productName.contains("+")) {
                        showReturnTwoBarcodeView();
                    }
                    break;
                case D10:
                case D40:
                case D50:

                    Log.d("TAG", "giftOne.contains : " + giftOne.contains("2EA"));

                    if (giftOne.contains("2EA")) {
                        showGiftThreeCodeView(giftOne);
                    }

                    if (giftTwo.contains("2EA")) {
                        showGiftFourCodeView(giftOne);
                    }


                    showDefaultItemCodeView();
                    showGiftOneCodeView(giftOne);
                    showGiftTwoCodeView(giftTwo);
                    if (productName.contains("+")) {
                        showDefaultTwoItemCodeView();
                    }
                    break;
                case D80: // 사은품
                    dismissDefaultItemCodeView();
                    showGiftOneBarcodeView();
                    if (giftOne.contains("2EA")) {
                        showGiftThreeCodeView(giftOne);
                    }
                    showGiftTwoCodeView(giftOne);
                    showGiftFourCodeView(giftTwo);

                    break;
                case D21:
                    showReturnThreeBarcodeView();
                    break;
                case SV10:
                case SV11:
                    setupAs();
                    break;
                case P20:
                case D60:
                case D70:
                case D100:
                case D120:
                case D150:
                case D90:
                    dismissDefaultItemCodeView();

                    dismissReturnBarcodeView();
                    dismissReturnBarcodeView2();
                    break;
                default:
                    dismissDefaultItemCodeView();
                    dismissReturnBarcodeView();
                    break;
            }
        } catch (IllegalArgumentException e) {
            dismissDefaultItemCodeView();
            dismissReturnBarcodeView();
        }

    }

    private void showGiftOneCodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showGiftOneBarcodeView();
        } else {
            dismissGiftOneBarcodeView();
        }
    }

    private void showGiftThreeCodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showGiftThreeBarcodeView();
        } else {
            dismissGiftThreeBarcodeView();
        }
    }

    private void showGiftFourCodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showGiftFourBarcodeView();
        } else {
            dismissGiftFourBarcodeView();
        }
    }

    private void showGiftTwoCodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showGiftTwoBarcodeView();
        } else {
            dismissGiftTwoBarcodeView();
        }
    }

    private void showReturnGiftOneBarcodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showReturnGiftOneBarcodeView();
        } else {
            dismissReturnGiftOneBarcodeView();
        }
    }

    private void showReturnGiftThreeBarcodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showReturnGiftThreeBarcodeView();
        } else {
            dismissReturnGiftTreeBarcodeView();
        }
    }

    private void showReturnGiftFourBarcodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showReturnGiftFourBarcodeView();
        } else {
            dismissReturnGiftFourBarcodeView();
        }
    }

    private void showReturnGiftTwoBarcodeView(String gift) {
        if (!gift.trim().isEmpty()) {
            showReturnGiftTwoBarcodeView();
        } else {
            dismissReturnGiftTwoBarcodeView();
        }
    }


//    private GalleryLoader mGalleryLoader;

    private ImageView mImageView;

    private OnClickListener onImageLadderClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mImageView = (ImageView) v;
            startCamera();

        }
    };


    private OnClickListener onImageClickListener2 = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mImageView = (ImageView) v;
            if (Build.VERSION.SDK_INT >= 24) {
                new AlertDialog.Builder(mContext).setItems(new String[]{"카메라", "앨범"}, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            startCamera();
                            break;
                        case 1:
                            showBottomPicker((ImageView) v, false);
                            break;
                    }
                }).show();
            } else {
                showBottomPicker((ImageView) v, true);
            }
        }
    };


    private File writeImage(String fileName) {

        File pathfile = Environment.getExternalStorageDirectory();


        String seq = mResultData.SHIPPING_SEQ != null ? mResultData.SHIPPING_SEQ : "M001";
        String bodyNo = mResultData.BODY_NO != null ? mResultData.BODY_NO : "M002";
        pathfile = new File(pathfile.getPath() + "/bodyfriend/" + seq + "_" + bodyNo);
        // pathfile = new File(pathfile.getPath() + "/bodyfreind/" + date + "/" + String.format("%s_%s", mResultData.CUST_NAME, SHIPPING_SEQ));
        Log.d("pathfile = " + pathfile.getPath());
        Log.d("storageDir = " + pathfile.getPath());
        if (!pathfile.exists())
            pathfile.mkdirs();

        pathfile = new File(pathfile.getPath() + "/" + fileName);

        return pathfile;
    }


    public static HashMap<Integer, ImageHolder> fileHashMap;

    private class ImageHolder {
        private Bitmap bitmap;
        private File file;
        private int viewId;
    }

    public void copy(File source, File destination) throws IOException {
        FileInputStream inStream = new FileInputStream(source);
        FileOutputStream outStream = new FileOutputStream(destination);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    private Net.OnNetResponse<appInsertLog> onLogNetResponse = new Net.OnNetResponse<appInsertLog>() {

        @Override
        public void onResponse(appInsertLog response) {

        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };


    @Override
    public boolean check() {
        RadioGroup housing = (RadioGroup) findViewById(R.id.radioGroupHousing);
        if (housing.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, "주거형태를 선택하세요..", Toast.LENGTH_SHORT).show();
            return true;
        }


        if (findViewById(R.id.layoutReturnWarehouse).getVisibility() == View.VISIBLE) {
            if (((Spinner) findViewById(R.id.spinnerWarehouse)).getSelectedItemPosition() == 0) {
                Toast.makeText(this, "회수 창고 코드 확인해주세요.", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (((EditText) findViewById(R.id.editReturnReason)).getText().toString().trim().isEmpty()) {
                Toast.makeText(this, "회수 사유 확인해주세요.", Toast.LENGTH_SHORT).show();
                return true;
            }
            mReturnReason = ((EditText) findViewById(R.id.editReturnReason)).getText().toString();
        }

        if (mResultData.SHIPPING_TYPE.equals(ReturnShippingType.SV10.name()) || mResultData.SHIPPING_TYPE.equals(ReturnShippingType.SV11.name())) {
            if (TextUtils.isEmpty(text(R.id.m_shippingps).trim())) {
                Toast.makeText(this, "설치 비고란에 분해 조립 정보를 입력하세요.", Toast.LENGTH_SHORT).show();

            }
        }


        Set<Integer> integers = mCompleteProductMap.keySet();
        for (Integer productItemIdx : integers) {
            ProductSerialValidData.ResultData resultData = mCompleteProductMap.get(productItemIdx);
            Log.d("ResultData : " + resultData.toString() + "\t productItemIdx : " + productItemIdx);
            if (!resultData.isValid) {
                Toast.makeText(this, "제품 코드 입력 확인해주세요.", Toast.LENGTH_SHORT).show();
                return true;
            }
        }


        if (isEmpty(PP.carNumber.get())) {
            showDialog("차량번호를 선택하여 주세요", "확인", positiveListener);
            return true;
        }
        //양중 이미지 체크
        if (isChecked(R.id.concent_y)) {
            File concent_image_one = (File) findViewById(R.id.concent_image_one).getTag();
            File concent_image_two = (File) findViewById(R.id.concent_image_two).getTag();
            File concent_image_three = (File) findViewById(R.id.concent_image_three).getTag();

            if (concent_image_one == null || concent_image_two == null || concent_image_three == null) {
                BFDialog.newInstance(mContext).showSimpleDialog("양중 이미지를 촬영하세요.");
                return true;
            }

        }

        // 회수 서비스
        if (isChecked(R.id.collect_radio_add)) {
            File screen_collect_one = (File) findViewById(R.id.collectPicture1).getTag();
            File screen_collect_two = (File) findViewById(R.id.collectPicture2).getTag();
            if (screen_collect_one == null || screen_collect_two == null) {
                toast(String.format("회수 이미지를 첨부하여 주세요."));
                return true;
            }
        }


        // 사다리를 사용했을경우에 결제 구분을 선택하지 않으면 알럿을 띄운다.
        if (isChecked(R.id.ladder_y)) {
            if (getSelectedItemPosition(R.id.ladder_payment_type) == 0) {
                BFDialog.newInstance(mContext).showSimpleDialog("사다리 결제구분을 선택해 주세요.");
                return true;
            }
            File ladderOne = (File) findViewById(R.id.ladder_image_one).getTag();
            File ladderTwo = (File) findViewById(R.id.ladder_image_two).getTag();
            File ladderThree = (File) findViewById(R.id.ladder_image_three).getTag();
            if (ladderOne == null) {
                BFDialog.newInstance(mContext).showSimpleDialog("수작업 불가구조 이미지를 촬영하세요.");
                return true;
            }
            if (ladderTwo == null) {
                BFDialog.newInstance(mContext).showSimpleDialog("사다리 사용인증 이미지를 촬영하세요.");
                return true;
            }
            if (ladderThree == null) {
                BFDialog.newInstance(mContext).showSimpleDialog("사다리 영수증 이미지를 촬영하세요.");
                return true;
            }
        }


        String progressNo = mResultData.PROGRESS_NO;
        String shippingType = mResultData.SHIPPING_TYPE;

        File screen1 = (File) findViewById(R.id.imageone).getTag();
        File screen2 = (File) findViewById(R.id.imagetwo).getTag();
        File screen3 = (File) findViewById(R.id.imagethree).getTag();
        File screen4 = (File) findViewById(R.id.imagefour).getTag();
        File screen5 = (File) findViewById(R.id.imagefive).getTag();
        File screen6 = (File) findViewById(R.id.imagesix).getTag();


        if (isChecked(R.id.additional_radio_add)) {
            servicePicture1 = (File) findViewById(R.id.servicePicture1).getTag();
            servicePicture2 = (File) findViewById(R.id.servicePicture2).getTag();
        } else {
            servicePicture1 = null;
            servicePicture2 = null;
        }

        if (isChecked(R.id.additional_radio_add)) {
            if (servicePicture1 == null || servicePicture2 == null) {
                toast("내림서비스 확인용 이미지 첨부하세요.");
                return true;
            }
        }


        if (shippingType.equals(맞교)) {
            if (!isLatex && screen1 == null) {
                toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textOne)));
                return true;
            }
            if (screen2 == null) {
                toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textTwo)));
                return true;
            }
//            if (screen3 == null) {
            // toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textThree)));
            // return true;
//            }
            if (screen4 == null) {
                toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textFour)));
                return true;
            }
            if (screen5 == null) {
                toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textFive)));
                return true;
            }
//            if (screen6 == null) {
            // toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textSix)));
            // return true;
//            }

            if (isEmpty(R.id.m_collection_sn)) { // 회수 sn
                toast("회수S/N을 입력하여주세요.");
                return true;
            }
        } else if (shippingType.equals(이전) || shippingType.equals(수리)) { // 이전이면서 수리인경우
            if (progressNo.equals(회수)) {
                if (!isLatex && screen1 == null) {
                    toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textOne)));
                    return true;
                }
                if (screen2 == null) {
                    toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textTwo)));
                    return true;
                }
//                if (screen3 == null) {
                // toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textThree)));
                // return true;
//                }

                if (isEmpty(R.id.m_collection_sn)) { // 회수 sn
                    toast("회수S/N을 입력하여주세요.");
                    return true;
                }

                if (screen_sign == null) {
                    toast("고객 서명을 해주시기 바랍니다.");
                    return true;
                }

                return false;
            }
            /*else if (progressNo.equals(이동)) {

            }*/
        }


        // 아래는 공통 적인 사항.
        if (!(isChecked(라디오_설치완료) || isChecked(R.id.m_completedtF1) || isChecked(라디오_초도불량)
                || isChecked(라디오_설치불가) || isChecked(라디오_수취거부) || isChecked(라디오_취소) || isChecked(라디오_설치_후_반품))) {
            toast("설치여부를 선택해주시기 바랍니다.");
            return true;
        }
        if ((mResultData.PROMISE.equals("N") || TextUtils.isEmpty(mResultData.PROMISE.trim()))) { // 약속 상태가 아닐때.
            toast("약속상태가 아닙니다.");
            return true;
        }

        if (isChecked(라디오_설치불가) || isChecked(라디오_수취거부) || isChecked(라디오_취소)) { // 설치 불가 이거나 수취 거부 이거나 취소 이면 아래의 조건들 보지 않음.
            return false;
        }


        if (isChecked(라디오_설치완료) || isChecked(R.id.m_completedtF1) || isChecked(라디오_초도불량)) {
            if (!isLatex && screen1 == null) {
                toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textOne)));
                return true;
            }
            if (screen2 == null) {
                toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textTwo)));
                return true;
            }
        }

        if (isChecked(라디오_설치완료) || isChecked(R.id.m_completedtF1)) {
            if (screen_sign == null) {
                toast("고객 서명을 해주시기 바랍니다.");
                return true;
            }
        }

        if (!isLatex && isEmpty(R.id.m_productsn)) {
            toast("제품S/N을 입력하여주세요.");
            return true;
        }

        if (NetConst.s_producttype.equals(NetConst.PRODUCT_TYPE_M)) {
            if (isEmpty(R.id.deliveryman2)) {
                toast("부사수를 지정하여 주세요.");
                return true;
            }
        }

        if (shippingType.equals(이전) || shippingType.equals(수리)) {
            if (progressNo.equals(설치)) {

            }
            if (progressNo.equals(회수)) {
                if (!isLatex && screen1 == null) {
                    toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textOne)));
                    return true;
                }
                if (screen2 == null) {
                    toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textTwo)));
                    return true;
                }
                // if (screen3 == null) {
                // toast(String.format("%s 이미지를 첨부하여 주세요.", text(R.id.textThree)));
                // return true;
                // }

                if (isEmpty(R.id.m_collection_sn)) { // 회수 sn
                    toast("회수S/N을 입력하여주세요.");
                    return true;
                }
            }
          /*  if (progressNo.equals(이동)) {

            }*/
        }


//        Log.d("((TextView) (findViewById(R.id.freegift_one))).getText().toString() : " + ((TextView) (findViewById(R.id.freegift_one))).getText().toString());

        if (!TextUtils.isEmpty(((TextView) (findViewById(R.id.freegift_one))).getText().toString().trim()) &&
                ((Spinner) findViewById(R.id.m_freegiftcheck1)).getSelectedItemPosition() == 0) {
            toast("사은품1을 선택하세요.");
            return true;
        }

        if (!TextUtils.isEmpty(((TextView) (findViewById(R.id.freegift_two))).getText().toString().trim()) &&
                ((Spinner) findViewById(R.id.m_freegiftcheck2)).getSelectedItemPosition() == 0) {
            toast("사은품2를 선택하세요.");
            return true;
        }

        return false;
    }

    private String mAdditional_ladder_price;
    private OnClickListener onModifyClickListener = new OnClickListener() {

        private DialogInterface.OnClickListener positiveListener = (dialog, which) -> reqModify();

        @Override
        public void onClick(View v) {
            if (check()) {
                return;
            }

            if (mResultData.SHIPPING_TYPE.equals(이전) && mResultData.PROGRESS_NO.equals(회수)) {
                // 이전이면서 회수이면 바로 완료등록을 물어본다.
                showDialog("완료 등록을 하시겠습니까?", "확인", positiveListener, "취소", null);
            } else if (isChecked(R.id.m_completedtZ)) {
                showDialog("완료 등록을 하시겠습니까?", "확인", positiveListener, "취소", null);
            } else {
                String str = "";
                if (isChecked(R.id.m_completedtF1)) {
                    str = text(R.id.m_completedtF1);
                }
                if (isChecked(R.id.m_completedtF2)) {
                    str = text(R.id.m_completedtF2);
//                    dismissDefaultItemCodeView();
                }
                if (isChecked(R.id.m_completedtX1)) {
//                    showReturnBarcodeView();
//                    dismissDefaultItemCodeView();
                    str = text(R.id.m_completedtX1);
                }
                if (isChecked(R.id.m_completedtX2)) {
//                    showReturnBarcodeView();
//                    dismissDefaultItemCodeView();
                    str = text(R.id.m_completedtX2);
                }
                if (isChecked(R.id.m_completedtX3)) {
//                    showReturnBarcodeView();
//                    dismissDefaultItemCodeView();
                    str = text(R.id.m_completedtX3);
                }
                if (isChecked(R.id.m_completedtX4)) {
//                    showReturnBarcodeView();
//                    dismissDefaultItemCodeView();
                    str = text(R.id.m_completedtX4);
                }

                showDialog(String.format("설치여부가 %s로 등록됩니다.", str), "확인", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showDialog("완료 등록을 하시겠습니까?", "확인", positiveListener, "취소", null);
                    }
                }, "취소", null);
            }
        }

        /**
         * 수정요청
         */

        private void reqModify() {
            showProgress();
            m_shippingseq = mResultData.SHIPPING_SEQ; // 상품번호
            m_promise = mResultData.PROMISE;
            m_shippingps = text(R.id.m_shippingps);

            if (mResultData.SHIPPING_TYPE.equals(AS)) {
                m_paymenttype = "0";//(입금유형, 1:카드, 2:현금, 3:계좌)

                final int selectedPosition = getSelectedItemPosition(R.id.spinnerCostInputType);
                if (selectedPosition == 0) {
                    // 입금유형
                } else if (selectedPosition == 1) {
                    // 카드
                    m_paymenttype = "1";
                } else if (selectedPosition == 2) {
                    // 계좌
                    m_paymenttype = "3";
                } else if (selectedPosition == 3) {
                    m_paymenttype = "2";
                }
                m_costamount = text(R.id.editCostSum);//(금액)
                m_cashreceipttype = String.valueOf(getSelectedItemPosition(R.id.spinnerReceipt)); //(현금영수증 요청정보, 1: 핸드폰번호, 2:현금영수증카드번호)

                m_cashreceiptcardnum = text(R.id.editReceipt); //(핸드폰번호/현금영수증카드번호)
                m_depositdetailhistory = text(R.id.editCostDetails);

                int umusang = getSelectedItemPosition(R.id.spinnerCostType);
                m_costncost = umusang == 1 ? "1" : "0";
            }


            m_completedt = "";
            try {
                m_completedt = SDF.yyyymmdd_1.format(SDF.yyyymmdd.parse(mResultData.SERVER_TIME));
            } catch (ParseException | java.text.ParseException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            // String m_completedt = text(R.id.server_time);


            m_complete = getRadioCode();

            m_productsn = text(R.id.m_productsn);

            screen1 = (File) findViewById(R.id.imageone).getTag();
            screen2 = (File) findViewById(R.id.imagetwo).getTag();
            screen3 = (File) findViewById(R.id.imagethree).getTag();
            screen4 = (File) findViewById(R.id.imagefour).getTag();
            screen5 = (File) findViewById(R.id.imagefive).getTag();
            screen6 = (File) findViewById(R.id.imagesix).getTag();

            servicePicture1 = (File) findViewById(R.id.servicePicture1).getTag();
            servicePicture2 = (File) findViewById(R.id.servicePicture2).getTag();


            screen_ladder_one = (File) findViewById(R.id.ladder_image_one).getTag();
            screen_ladder_two = (File) findViewById(R.id.ladder_image_two).getTag();
            screen_ladder_three = (File) findViewById(R.id.ladder_image_three).getTag();


            screen_collect_one = (File) findViewById(R.id.collectPicture1).getTag();
            screen_collect_two = (File) findViewById(R.id.collectPicture2).getTag();


            m_is_ladder = isChecked(R.id.ladder_y) ? "Y" : "N";
            if (isChecked(R.id.ladder_y)) {
                m_ladder_price = text(R.id.m_ladder_price).replaceAll(",", "");
            } else {
                m_ladder_price = "0";
            }

            mAdditional_ladder_price = text(R.id.additional_ladder_price).replaceAll(",", "");
            m_extra_cost_memo = text(R.id.m_extra_cost_memo);


            screen_socket_one = (File) findViewById(R.id.concent_image_one).getTag();
            screen_socket_two = (File) findViewById(R.id.concent_image_two).getTag();
            screen_socket_three = (File) findViewById(R.id.concent_image_three).getTag();
            m_is_socket = isChecked(R.id.concent_y) ? "Y" : "N";

            sign = getSignFile();

            m_freegiftcheck1 = "";
            m_freegiftcheck2 = "";
            if (!mFreegift.data.resultData.isEmpty()) {
                try {
                    m_freegiftcheck1 = mFreegift.data.resultData.get(mGiftSpinnerFirst.getSelectedItemPosition() - 1).DET_CD;
                    mGiftSpinnerSecond = (Spinner) findViewById(R.id.m_freegiftcheck2);
                    m_freegiftcheck2 = mFreegift.data.resultData.get(mGiftSpinnerSecond.getSelectedItemPosition() - 1).DET_CD;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            m_completenm = ""; // 설치여부명ee
            m_productname = ""; // 제품명
            m_productcode = ""; // 제품코드
            m_custname = ""; // 고객명

            if (isChecked(R.id.m_completedtF1) || isChecked(라디오_초도불량)) {
                screen5 = null;
                screen6 = null;
                if (isChecked(R.id.m_completedtF1)) { // 맞교체
                    m_completenm = text(R.id.m_completedtF1);
                } else if (isChecked(라디오_초도불량)) { // 초도불량
                    m_completenm = text(라디오_초도불량);
                }
                m_productname = mResultData.PRODUCT_NAME; // 제품명
                m_productcode = mResultData.PRODUCT_CODE; // 제품코드
                m_custname = mResultData.CUST_NAME; // 고객명
            }


            m_backsn = text(R.id.m_collection_sn);
            if (m_backsn.length() == 0) {
                m_backsn = null;
            }


            setOnClickListener(R.id.modify, null);

            String shippingType = mResultData.SHIPPING_TYPE;
            String progressNo = mResultData.PROGRESS_NO;

            ladder_payment_type = getSelectedItemPosition(R.id.ladder_payment_type);


            Set<Integer> integers = mCompleteProductMap.keySet();
//            if (!integers.isEmpty()) {
            JsonObject jsonObject = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            for (Integer productItemIdx : integers) {
                ProductSerialValidData.ResultData resultData = mCompleteProductMap.get(productItemIdx);

                UpdateProductSerialData updateProductSerialData = new UpdateProductSerialData();
                updateProductSerialData.giftYn = resultData.giftYn;
                updateProductSerialData.serialCd = resultData.serialCd;
                updateProductSerialData.makerCd = resultData.makerCd == null ? resultData.serialCd : resultData.makerCd;

                updateProductSerialData.collectYn = resultData.collectYn;
                updateProductSerialData.item = (resultData.item == null) ? resultData.itemCd : resultData.item;

                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                jsonArray.add(gson.toJson(updateProductSerialData));
            }
            jsonObject.addProperty("tSlCd", mReturnSlCd);
            jsonObject.addProperty("tReason", mReturnReason);
            jsonObject.add("itemList", jsonArray);
            mItemList = jsonObject.toString();
            Log.d("mItemList : " + mItemList);
           /* } else {
                mItemList = "";
                Log.d("mItemList : " + mItemList);
            }
*/

            // 이전이거나 수리이면서 회수인경우 따로 요청
            if ((shippingType.equals(이전) || shippingType.equals(수리)) && progressNo.equals(회수)) { // 이전이면서 수리인경우
                appModifyShippingDiv();
            } else {

                car_number = PP.carNumber.get();
                new AsyncTaskUpdateCarteImage().execute();
                Net.async(new appInsertLog(m_shippingseq, m_promise, m_shippingps, m_complete, m_completedt)).setOnNetResponse(onLogNetResponse);
            }
        }
    };


    String m_shippingseq;
    String m_promise;
    String m_shippingps;
    String m_complete;
    String m_completedt;
    String m_productsn;
    File screen_collect_one; //회수 이미지1
    File screen_collect_two; //회수 이미지2
    File screen1;
    File screen2;
    File screen3;
    File screen4;
    File screen5;
    File screen6;
    File sign;
    File screen_ladder_one; // 사다리 이미지 1
    File screen_ladder_two; // 사다리 이미지 2
    File screen_ladder_three; // 사다리 이미지 3

    File servicePicture1; // 내림서비스 이미지 1
    File servicePicture2; // 내림서비스 이미지 2

    File screen_socket_one; // 콘센트 이미지
    File screen_socket_two; // 콘센트 이미지
    File screen_socket_three; // 콘센트 이미지

    String m_is_ladder; // 사다리 이용여부
    String m_is_socket; // 콘센트 사용여부
    String m_ladder_price; // 사다리 이용 가격
    int ladder_payment_type; // 결제 구분

    String mItemList;
    String m_freegiftcheck1;
    String m_freegiftcheck2;
    String m_completenm;
    String m_productname;
    String m_productcode;
    String m_custname;
    String m_confirmps;
    String m_relationship;
    String m_relationshipnm;
    String m_backsn;
    String m_wateradaptor;
    String m_watercooking;
    String car_number;

    String m_costncost;//(유/무상, 1:유상, 0:무상)
    String m_cashreceipttype;//(현금영수증 요청정보, 1: 핸드폰번호, 2:현금영수증카드번호)
    String m_cashreceiptcardnum;//(핸드폰번호/현금영수증카드번호)
    String m_paymenttype;   //(입금유형, 1:카드, 2:현금, 3:계좌)
    String m_depositdetailhistory;  //(입금상세내역)
    String m_costamount; //(금액)

    private MultipartUploader multipartUploader;

    class AsyncTaskUpdateCarteImage extends AsyncTask<Void, Void, Void> {
        boolean isSuccess = false;

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String requestURL = NetConst.host + "mobile/api/appModifyShippingDiv.json";

                multipartUploader = new MultipartUploader(requestURL, "UTF-8", "Cookie", String.format("JSESSIONID=%s", Auth.d.resultData.SID));

                multipartUploader.addFormField("m_shippingseq", m_shippingseq); //1
                multipartUploader.addFormField("m_promise", m_promise); //2
                multipartUploader.addFormField("m_shippingps", m_shippingps); //3 설치비고
                multipartUploader.addFormField("m_complete", m_complete); //4
                multipartUploader.addFormField("m_completedt", m_completedt); //5


                multipartUploader.addFormField("m_productsn", m_productsn); //6

                multipartUploader.addFormField("m_freegiftcheck1", m_freegiftcheck1);
                multipartUploader.addFormField("m_freegiftcheck2", m_freegiftcheck2);

                multipartUploader.addFilePart("screen1", screen1);
                multipartUploader.addFilePart("screen2", screen2);//10
                multipartUploader.addFilePart("screen3", screen3);
                multipartUploader.addFilePart("screen4", screen4);
                multipartUploader.addFilePart("screen5", screen5);
                multipartUploader.addFilePart("screen6", screen6);
                multipartUploader.addFilePart("screen_sign", sign);

                multipartUploader.addFilePart("screen_ladder_one", screen_ladder_one); // 사다리 이미지 하나
                multipartUploader.addFilePart("screen_ladder_two", screen_ladder_two); // 사다리 이미지 둘
                multipartUploader.addFilePart("screen_ladder_three", screen_ladder_three); // 사다리 이미지 둘


                multipartUploader.addFilePart("screen_collect_one", screen_collect_one); // 회수 이미지1
                multipartUploader.addFilePart("screen_collect_two", screen_collect_two); // 회수 이미지2
                multipartUploader.addFormField("m_iscollect", isChecked(R.id.collect_radio_add) ? "Y" : "N"); // 회수 여부
                multipartUploader.addFormField("m_is_bodyproduct", isChecked(R.id.isBodyProduct) ? "Y" : "N"); // 회수 자사 구분


                multipartUploader.addFilePart("service_picture1", servicePicture1); // 내림서비스 1
                multipartUploader.addFilePart("service_picture2", servicePicture2); // 내림서비스 2

                multipartUploader.addFilePart("screen_socket_one", screen_socket_one); // 콘센트 이미지 20
                multipartUploader.addFilePart("screen_socket_two", screen_socket_two); // 콘센트 이미지
                multipartUploader.addFilePart("screen_socket_three", screen_socket_three); // 콘센트 이미지

                multipartUploader.addFormField("m_is_socket", m_is_socket); // 콘센트 사용여부

                multipartUploader.addFormField("m_is_ladder", m_is_ladder); // 사다리 사용 여부
                multipartUploader.addFormField("m_ladder_price", m_ladder_price); // 사다리 이용 가격
                multipartUploader.addFormField("m_extra_cost_memo", m_extra_cost_memo); // 사다리 이용 비고

                multipartUploader.addFormField("m_extra_cost ", mAdditional_ladder_price); // 사다리 이용 추가 가격

                multipartUploader.addFormField("ladder_payment_type", String.valueOf(ladder_payment_type)); // 사다리 구분

                multipartUploader.addFormField("m_completenm", m_completenm); // 설치여부명

                multipartUploader.addFormField("m_productname", m_productname); // 제품명
                multipartUploader.addFormField("m_productcode", m_productcode);  // 제품코드명
                multipartUploader.addFormField("m_custname", m_custname);
                multipartUploader.addFormField("m_confirmps", m_confirmps); //서명 비고
                multipartUploader.addFormField("m_relationship", m_relationship); //서명 관계
                multipartUploader.addFormField("m_relationshipnm", m_relationshipnm); //서명 관계 기타
                multipartUploader.addFormField("m_backsn", m_backsn);
                multipartUploader.addFormField("m_wateradaptor", m_wateradaptor);
                multipartUploader.addFormField("m_watercooking", m_watercooking);
                multipartUploader.addFormField("car_number", car_number);
                multipartUploader.addFormField("m_freegiftAddr", text(R.id.giftAddr)); // 사은품 주소

                multipartUploader.addFormField("m_costncost", m_costncost); //(유/무상, 1:유상, 0:무상)
                multipartUploader.addFormField("m_cashreceipttype", m_cashreceipttype); //(현금영수증 요청정보, 1: 핸드폰번호, 2:현금영수증카드번호)
                multipartUploader.addFormField("m_cashreceiptcardnum", m_cashreceiptcardnum); //(핸드폰번호/현금영수증카드번호)
                multipartUploader.addFormField("m_paymenttype", m_paymenttype); //(입금유형, 1:카드, 2:현금, 3:계좌)
                multipartUploader.addFormField("m_depositdetailhistory", m_depositdetailhistory); //(입금상세내역)
                multipartUploader.addFormField("m_costamount", m_costamount); //(금액)
                multipartUploader.addFormField("jsonData", mItemList); // rfid 체크
                multipartUploader.addFormField("m_houseType", mHouseType); // rfid 체크

                Log.d("jsonData :" + mItemList);
                Log.d("m_houseType :" + mHouseType);


                List<String> stringList = multipartUploader.finish();
                for (String str : stringList) {


                    JSONObject jsonObject = new JSONObject(str);

                    isSuccess = jsonObject.getInt("resultCode") != 0 || jsonObject.getInt("resultCode") != 309 || jsonObject.getInt("resultCode") != -1;
                    if (isSuccess) break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                isSuccess = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (isSuccess) {
                OH.c().notifyObservers(OH.TYPE.MODIFY_COMPLITE);
                setOnClickListener(R.id.modify, onModifyClickListener);
                hideProgress();

                Dialog d = showDialog("수정에 성공 하였습니다.", "확인", null);
                d.setOnDismissListener(dialog -> complete());

                deleteTmpImages();
            } else {
                if (!isFinishing()) {
                    showDialog("수정에 실패하였습니다.", "확인", null);
                }
            }

            hideProgress();
        }

    }

    private File getSignFile() {
        URI uri = null;
        try {
            if (screen_sign != null)
                uri = new URI(screen_sign);
        } catch (URISyntaxException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        File sign = null;
        if (uri != null) {
            sign = new File(uri.getPath());
        }
        return sign;
    }

    private void deleteTmpImg(File... files) {
        Log.d("deleteTmpImg >> ");
        for (File file : files) {
            if (file != null) {
                boolean exists = file.exists();
                Log.d("exists : " + exists);
                if (exists) {
                    file.delete();
                    Log.d("delete");
                }
            }
        }
    }

    private void deleteTmpImages() {
        deleteFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "GalleryLoader");
        deleteFile(FU.getRoot(), "photo");
        deleteTmpImg(getSignFile());

        fileHashMap.clear();
    }

    private void deleteFile(File folder, String contains) {

        File[] list = folder.listFiles();

        if (list == null)
            return;

        for (File file : list) {
            if (file.getName().contains(contains)) {
                // 파일명 또는 폴더명에 android가 들어가는지
                Log.e(String.format("del file : %s", file.getPath()));
                file.delete();
            }
        }
    }

    private void appModifyShippingDiv() {
        showProgress();

        m_shippingseq = mResultData.SHIPPING_SEQ;
        screen1 = (File) findViewById(R.id.imageone).getTag();
        screen2 = (File) findViewById(R.id.imagetwo).getTag();
        screen3 = (File) findViewById(R.id.imagethree).getTag();
        screen4 = (File) findViewById(R.id.imagefour).getTag();
        screen5 = (File) findViewById(R.id.imagefive).getTag();
        screen6 = (File) findViewById(R.id.imagesix).getTag();

        Net.async(new appInsertLog(String.format("mobile/api/appModifyShippingDiv.json : m_shippingseq = %s", m_shippingseq))).setOnNetResponse(onLogNetResponse);
        try {
            m_completedt = SDF.yyyymmdd_1.format(SDF.yyyymmdd.parse(mResultData.SERVER_TIME));
        } catch (ParseException | java.text.ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        BFMultiNet.newInstance(NetConst.host + "mobile/api/appModifyShippingDiv.json")
                .setFormField(
                        "m_shippingseq", m_shippingseq
                        , "m_productsn", text(R.id.m_collection_sn)
                        , "m_complete", "ZZ"
                        , "m_completedt", m_completedt
                        , "m_is_ladder", m_is_ladder
                        , "m_is_socket", m_is_socket
                        , "m_ladder_price", m_ladder_price
                        , "m_extra_cost_memo", m_extra_cost_memo
                        , "m_extra_cost", mAdditional_ladder_price
                        , "jsonData", mItemList
                        , "ladder_payment_type", String.valueOf(ladder_payment_type)
                )
                .setFilePart("screen1", screen1
                        , "screen2", screen2
                        , "screen3", screen3
                        , "screen4", screen4
                        , "screen5", screen5
                        , "screen6", screen6
                        , "screen_sign", getSignFile()
                        , "screen_ladder_one", screen_ladder_one
                        , "screen_ladder_two", screen_ladder_two
                        , "screen_ladder_three", screen_ladder_three
                        , "screen_socket_one", screen_socket_one

                )
                .setOnNetResultListener((isSuccess, resultMsg) -> {
                    Dialog d = showDialog(resultMsg, "확인", null);
                    d.setOnDismissListener(dialog -> {
                        if (isSuccess) {
                            complete();

                        }
                    });
                    deleteTmpImages();
                }).execute();
    }

    private Net.OnNetResponse<searchCodeShippingType> onShippingTypeCodeNetResponse = new Net.OnNetResponse<searchCodeShippingType>() {

        @Override
        public void onResponse(searchCodeShippingType response) {
            for (searchCodeShippingType.Data.resultData d : response.data.resultData) {
                if (mResultData.PROGRESS_NO.equals(d.DET_CD)) {
                    setText(R.id.gubun, String.format("%s - %s", text(R.id.gubun), d.DET_CD_NM));

                    break;
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    protected Net.OnNetResponse<searchCodeListDeliveryGb> onDeliveryGbNetResponse = new Net.OnNetResponse<searchCodeListDeliveryGb>() {

        @Override
        public void onResponse(searchCodeListDeliveryGb response) {
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            for (searchCodeListDeliveryGb.Data.resultData d : response.data.resultData) {
                if (mResultData.SHIPPING_TYPE.equals(d.DET_CD)) {
                    setText(R.id.gubun, d.DET_CD_NM);

                    if (mResultData.SHIPPING_TYPE.startsWith("P"))
                        Net.async(new searchCodeShippingType()).setOnNetResponse(onShippingTypeCodeNetResponse);

                    break;
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    protected Net.OnNetResponse<searchCodeListArea> onLocationGbNetResponse = new Net.OnNetResponse<searchCodeListArea>() {

        @Override
        public void onResponse(searchCodeListArea response) {

            setAreaText(response);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }

        /**
         * 회수이면서 설치인 경우에 회수 지역과, 설치 지역을 표기한다. 이때 사용한다.
         */
        private void setAreaText(searchCodeListArea response) {
            try {
                if (mResultData.SHIPPING_TYPE.equals(이전)) {
                    for (searchCodeListArea.Data.resultData d : response.data.resultData) {
                        if (mResultData.AREA.equals(d.DET_CD)) {
                            setText(R.id.area, d.DET_CD_NM); // 회수지역
                        }

                        if (mResultData.AREA2.equals(d.DET_CD)) {
                            setText(R.id.area2, d.DET_CD_NM); // 회수지역
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    protected Net.OnNetResponse<searchCodeListProductBonsa> onBonsaGbNetResponse = new Net.OnNetResponse<searchCodeListProductBonsa>() {

        @Override
        public void onResponse(searchCodeListProductBonsa response) {

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            for (searchCodeListProductBonsa.Data.resultData d : response.data.resultData) {
                if (d.DET_CD.equals(mResultData.DIV_STATUS)) {
                    setText(R.id.bonsa_det_cd_nm, d.DET_CD_NM);
                    break;
                }
            }

            // bonsa_det_cd_nm
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    protected Net.OnNetResponse<searchCodeListProductGb> onProductGbNetResponse = new Net.OnNetResponse<searchCodeListProductGb>() {

        @Override
        public void onResponse(searchCodeListProductGb response) {
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            for (searchCodeListProductGb.Data.resultData d : response.data.resultData) {
                if (d.DET_CD.equals(mResultData.PRODUCT_TYPE)) {
                    setText(R.id.comm_cd_nm, String.format("%s", d.COMM_CD_NM));
                    setText(R.id.det_cd_nm, d.DET_CD_NM);
                    break;
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    protected Net.OnNetResponse<searchCodeListProductName> onProductNameNetResponse = new Net.OnNetResponse<searchCodeListProductName>() {

        @Override
        public void onResponse(searchCodeListProductName response) {
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            for (searchCodeListProductName.Data.resultData d : response.data.resultData) {
                if (d.DET_CD.equals(mResultData.PRODUCT_CODE)) {
                    // setText(R.id.product_name, d.DET_CD_NM);
                    setText(R.id.product_name_type, String.format("%s", d.COMM_CD_NM));
                    break;
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    protected Net.OnNetResponse<selectDeliveryManList> onManNetResponse = new Net.OnNetResponse<selectDeliveryManList>() {

        @Override
        public void onResponse(selectDeliveryManList response) {
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }
//            setText(R.id.deliveryman2, "");
//            setText(R.id.deliveryman3, "");
            for (selectDeliveryManList.Data.resultData d : response.data.resultData) {

                if (d.ADMIN_ID.equals(mResultData.DELIVERYMAN1)) {
                    setText(R.id.deliveryman1, d.ADMIN_NM);
                }
                if (d.ADMIN_ID.equals(mResultData.DELIVERYMAN2)) {
                    setText(R.id.deliveryman2, d.ADMIN_NM);
                }

                if (d.ADMIN_ID.equals(mResultData.DELIVERYMAN3)) {
                    setText(R.id.deliveryman3, d.ADMIN_NM);
                }
            }

            if (findViewById(R.id.selectDeliveryman2) != null) {
                setVisibility(R.id.selectDeliveryman2, View.VISIBLE);
                String text = isEmpty(R.id.deliveryman2) ? "부사수 지정" : "부사수 변경";
                setText(R.id.selectDeliveryman2, text);
            }
            if (findViewById(R.id.selectDeliveryman3) != null) {
                setVisibility(R.id.selectDeliveryman3, View.VISIBLE);
                String text = isEmpty(R.id.deliveryman3) ? "부사수 지정" : "부사수 변경";
                setText(R.id.selectDeliveryman3, text);
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            // TODO Auto-generated method stub

        }
    };

    private OnClickListener onBtn1ClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            setVisibility(R.id.shipping_master_detail_body1, true);
            setVisibility(R.id.shipping_master_detail_body2, false);
            setSelected(R.id.btn1, true);
            setSelected(R.id.btn2, false);
            setTextColor(R.id.btn1, getResources().getColor(R.color.white));
            setTextColor(R.id.btn2, Color.parseColor("#393a3a"));
        }
    };
    private OnClickListener onBtn2ClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            setVisibility(R.id.shipping_master_detail_body1, false);
            setVisibility(R.id.shipping_master_detail_body2, true);
            setSelected(R.id.btn1, false);
            setSelected(R.id.btn2, true);
            setTextColor(R.id.btn1, Color.parseColor("#393a3a"));
            setTextColor(R.id.btn2, getResources().getColor(R.color.white));
        }
    };

    private ArrayList<RadioButton> radioGroup;

    /**
     * 설치 여부 선택 라디오 버튼들
     *
     * @param res
     */
    private void setRadioButton(int... res) {
        OnCheckedChangeListener listener = new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                String shippingType = mResultData.SHIPPING_TYPE;

                switch (buttonView.getId()) {
                    case R.id.m_completedtZ:
                    case R.id.m_completedtF1:

                        Log.d("설치완료");
                        showProductCodeView(mResultData.SHIPPING_TYPE, mResultData.PROGRESS_NO, mResultData.PRODUCT_NAME, mResultData.FREEGIFT_ONE, mResultData.FREEGIFT_TWO);
                        break;
                    case R.id.m_completedtX4: // 설치후 반품
                    case R.id.m_completedtF2: // 초도 불량
                    case R.id.m_completedtX2:  // 수취거부
                    case R.id.m_completedtX1:  // 설치불가
                        dismissReturnBarcodeView3();
                        dismissDefaultTwoItemCodeView();
                        dismissDefaultItemCodeView();
                        dismissGiftOneBarcodeView();
                        dismissGiftTwoBarcodeView();
                        showProductCodeView(ReturnShippingType.D30.name(), mResultData.PROGRESS_NO, mResultData.PRODUCT_NAME, mResultData.FREEGIFT_ONE, mResultData.FREEGIFT_TWO);
                        // 취소 처리
                        if (shippingType.equals(ReturnShippingType.D20.name()) || shippingType.equals(ReturnShippingType.SV10.name()) || shippingType.equals(ReturnShippingType.D90.name()) || shippingType.equals(ReturnShippingType.SV11.name())) {
                            dismissDefaultTwoItemCodeView();
                            dismissDefaultItemCodeView();
                            dismissReturnBarcodeView();
                            dismissReturnBarcodeView2();
                        }
                        break;
                }

                for (RadioButton radioButton : radioGroup) {
                    radioButton.setOnCheckedChangeListener(null);
                }
                for (RadioButton radioButton : radioGroup) {
                    if (!((radioButton == buttonView) && isChecked)) {
                        radioButton.setChecked(false);
                    }
                }

                for (RadioButton radioButton : radioGroup) {
                    radioButton.setOnCheckedChangeListener(this);
                }

                if (shippingType.equals(맞교)) {
                    return;
                }
                if (shippingType.equals(계철)) {
                    return;
                }

                String text;
                int visibility;
                String bigo;
                int id = buttonView.getId();
                if (id == 라디오_초도불량 || id == R.id.m_completedtF1) {
                    bigo = "증상";
                    text = isChecked ? "설치(전체)" : "설치";
                    visibility = isChecked ? View.INVISIBLE : View.VISIBLE;
                } else {
                    bigo = "설치비고";
                    text = "설치";
                    visibility = View.VISIBLE;
                }
                setText(R.id.bigo, bigo);
                setText(R.id.textTwo, text);
                setVisibility(R.id.imagefiveLayout, visibility); // 초도불량 선택시 첨부3과 첨부4는 안보이게
                setVisibility(R.id.imagesixLayout, visibility);// 초도불량 선택시 첨부3과 첨부4는 안보이게
            }
        };

        radioGroup = new ArrayList<>();
        RadioButton button;
        for (
                int r : res) {
            button = (RadioButton) findViewById(r);
            button.setOnCheckedChangeListener(listener);
            radioGroup.add(button);
        }

    }

    private String getRadioCode() {
        int key = 0;
        for (RadioButton b : radioGroup) {
            if (b.isChecked()) {
                break;
            }
            key++;
        }
//        Log.d("getRadioCode : " + key);
        String strKey;
        switch (key) {
            case 0:
                strKey = "ZZ";

                break;
            case 1:
                strKey = "F1";
                break;
            case 2:
                strKey = "F2";
                break;
            case 3:
                strKey = "X1";
                break;
            case 4:
                strKey = "X2";
                break;
            case 5:
                strKey = "X3";
                break;
            case 6:
                strKey = "X4";
                break;
            default:
                strKey = "";
                break;
        }

        return strKey;
    }

    private searchCodeListFreegift mFreegift;
    private Net.OnNetResponse<searchCodeListFreegift> onFreegiftNetResponse = new Net.OnNetResponse<searchCodeListFreegift>() {

        @Override
        public void onResponse(searchCodeListFreegift response) {
            mFreegift = response;

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            }

            ArrayList<String> arrSpinner = new ArrayList<String>();
            arrSpinner.add("--");
            for (searchCodeListFreegift.Data.resultData d : response.data.resultData) {
                arrSpinner.add(d.DET_CD_NM);
            }

            Spinner s = (Spinner) findViewById(R.id.m_freegiftcheck1);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, arrSpinner);
            s.setAdapter(adapter);
            s = (Spinner) findViewById(R.id.m_freegiftcheck2);
            s.setAdapter(adapter);

        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private OnClickListener onServiceClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            if (!isNotNull(mResultData)) {
                toast("서비스 접수에 필요한 데이터를 올바르게 얻어오지 못했습니다.\n종료 후 다시 시도해주세요.");
                return;
            }
        }
    };

    private OnClickListener onSign2ClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, SignActivity.class);
            if (mResultData.PROGRESS_NO != null && mResultData.PROGRESS_NO.equals(회수)) {
                intent.putExtra(SignActivity.EXTRA.PRODUCT_TYPE, "B");
            } else {
                intent.putExtra(SignActivity.EXTRA.PRODUCT_TYPE, mResultData.PRODUCT_TYPE);
            }

            FullSignActivity.setCustomerName(mResultData.CUST_NAME);

            startActivityForResult(intent, REQUEST_CODE_SIGN);
        }
    };

    private String screen_sign;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // 카메라 캡쳐
        if (requestCode == REQUEST_CAPTURE_IMAGE) {
            onCaptureImageResult(resultCode);
        }
        Log.d("requestCode : " + requestCode);


        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_SIGN:
                    resultSign(data);
                    break;
                case REQUEST_DELIVARYMAN:
                case REQUEST_DELIVARYMAN2:
                    reload();
                    break;
                case RESULT_CODE_BARCODE_1:
                case RESULT_CODE_BARCODE_2:
                    resultBarcode(requestCode, data);
                    break;
                case REQUEST_CHANGE_CAR:
                    resultCarnumber(data);
                    break;

                case RESULT_CODE_BARCODE_PRODUCT:
                    // 배송 제품 코드1
                    getBarcode(data, RESULT_CODE_BARCODE_PRODUCT);
                    break;
                case RESULT_CODE_BARCODE_MANUFACTURER:
                    // 배송 제조사 코드1
                    getBarcode(data, RESULT_CODE_BARCODE_MANUFACTURER);
                    break;
                case RESULT_CODE_BARCODE_PRODUCT_2:

                    // 배송 제품 코드2
                    getBarcode(data, RESULT_CODE_BARCODE_PRODUCT_2);
                    break;
                case RESULT_CODE_BARCODE_MANUFACTURER_2:
                    // 배송 제조사 코드1
                    getBarcode(data, RESULT_CODE_BARCODE_MANUFACTURER_2);
                    break;

                case RESULT_CODE_BARCODE_RETURN_PRODUCT:
                    // 회수 제품코드
                    getBarcode(data, RESULT_CODE_BARCODE_RETURN_PRODUCT);
                    break;

                case RESULT_CODE_BARCODE_RETURN_MANUFACTURER:
                    // 회수 제조사
                    getBarcode(data, RESULT_CODE_BARCODE_RETURN_MANUFACTURER);
                    break;
                case RESULT_CODE_BARCODE_RETURN_PRODUCT_2:
                    // 회수 제품코드
                    getBarcode(data, RESULT_CODE_BARCODE_RETURN_PRODUCT_2);
                    break;

                case RESULT_CODE_BARCODE_RETURN_MANUFACTURER_2:
                    // 회수2 제조사
                    getBarcode(data, RESULT_CODE_BARCODE_RETURN_MANUFACTURER_2);
                    break;


                case RESULT_CODE_BARCODE_GIFT_ITEM:
                    // 사은품 1 코드
                    getBarcode(data, RESULT_CODE_BARCODE_GIFT_ITEM);
                    break;
                case RESULT_CODE_BARCODE_GIFT_MANUFACTURER:
                    // 사은품 제조사 1코드
                    getBarcode(data, RESULT_CODE_BARCODE_GIFT_MANUFACTURER);
                    break;
                case RESULT_CODE_BARCODE_GIFT_ITEM_2:
                    // 사은품  2코드
                    getBarcode(data, RESULT_CODE_BARCODE_GIFT_ITEM_2);
                    break;
                case RESULT_CODE_BARCODE_GIFT_MANUFACTURER_2:
                    // 사은품 제조사 2코드
                    getBarcode(data, RESULT_CODE_BARCODE_GIFT_MANUFACTURER_2);
                    break;


                case RESULT_CODE_BARCODE_GIFT_ITEM_3:
                    // 사은품  2코드
                    getBarcode(data, RESULT_CODE_BARCODE_GIFT_ITEM_3);
                case RESULT_CODE_BARCODE_GIFT_ITEM_4:
                    // 사은품  2코드
                    getBarcode(data, RESULT_CODE_BARCODE_GIFT_ITEM_4);


                case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM:
                    // 회수 사은품  1코드
                    String strSn = data.getStringExtra(CaptureActivity.RESULT.RESULT_SN);
                    ProductSerialValidData.ResultData outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
                    View layoutViewGroup;
                    if (outProductData != null) {
                        outProductData.isValid = true;
                        setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
                        layoutViewGroup = findViewById(R.id.layoutCheckReturnGiftCode1);
                        setViewValidLayout(outProductData, strSn, layoutViewGroup);
                    }
                    break;

                case RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2:
                    // 회수 사은품  2코드
                    strSn = data.getStringExtra(CaptureActivity.RESULT.RESULT_SN);
                    outProductData = mCompleteProductMap.get(RESULT_CODE_BARCODE_RETURN_GIFT_ITEM_2);
                    if (outProductData != null) {
                        outProductData.isValid = true;
                        setProductSerialValidInfo(outProductData, strSn, RESULT_CODE_BARCODE_RETURN_GIFT_ITEM);
                        layoutViewGroup = findViewById(R.id.layoutReturnCheckGiftCode2);
                        setViewValidLayout(outProductData, strSn, layoutViewGroup);
                    }
                    break;
            }
        }
    }


    private void resultCarnumber(Intent data) {
        String carNumber = data.getStringExtra(SelectCarActivity.EXTRA.CAR_NUMBER);
        if (!isEmpty(carNumber)) {
            setText(R.id.carnum, carNumber);
            PP.carNumber.set(carNumber);
        }
    }

    private void resultBarcode(int requestCode, Intent data) {
        String strUri = data.getStringExtra(CaptureActivity.RESULT.RESULT_URI);
        String strSn = data.getStringExtra(CaptureActivity.RESULT.RESULT_SN);
        Log.d("strUri : ", strUri, ", strSn : ", strSn);
        if (strUri != null) {
            Uri uri = Uri.parse(data.getStringExtra(CaptureActivity.RESULT.RESULT_URI));
            String filePath = "file://" + uri.toString();
            Log.d("uri.toString()", filePath);
            setImage(mImageView, filePath);
            mImageView.setTag(new File(uri.toString()));

            String name = requestCode == RESULT_CODE_BARCODE_1 ? "imageone.jpg" : "imagetwo.jpg";
            File pathfile = writeImage(name);
            try {
                copy(new File(uri.toString()), pathfile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        int txtResId = R.id.m_productsn;
        if (requestCode == RESULT_CODE_BARCODE_1) {
            txtResId = R.id.m_productsn;
        } else if (requestCode == RESULT_CODE_BARCODE_2) {
            txtResId = R.id.m_collection_sn;
        }
        if (strSn != null) {
            setText(txtResId, strSn);
        }
    }

    private void resultSign(Intent data) {
        m_confirmps = data.getStringExtra(SignActivity.RESULT.M_CONFIRMPS);
        m_relationshipnm = data.getStringExtra(SignActivity.RESULT.M_RELATIONSHIPNM);
        m_relationship = data.getStringExtra(SignActivity.RESULT.M_RELATIONSHIP);
        screen_sign = data.getStringExtra(SignActivity.RESULT.SCREEN_SIGN);

        TextView sign = (TextView) findViewById(R.id.sign);
        sign.setBackgroundResource(R.drawable.shape_rect_gray);
        // sign.setTextColor(Color.BLACK);
        sign.setText("고객 서명이 완료 되었습니다.");
        setOnClickListener(R.id.sign, null);

        File pathfile = writeImage("sign.png");

        try {
            copy(new File(new URI(screen_sign)), pathfile);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * EXIF정보를 회전각도로 변환하는 메서드
     *
     * @param exifOrientation EXIF 회전각
     * @return 실제 각도
     */
    public int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    /**
     * 이미지를 회전시킵니다.
     *
     * @param bitmap  비트맵 이미지
     * @param degrees 회전 각도
     * @return 회전된 이미지
     */
    public Bitmap rotate(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != converted) {
                    bitmap.recycle();
                    bitmap = converted;
                }
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
                // 메모리가 부족하여 회전을 시키지 못할 경우 그냥 원본을 반환합니다.
            }
        }
        return bitmap;
    }

    /**
     * 부사수2 변경
     */
    private OnClickListener onSelDmanClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mResultData == null) {
                return;
            }
            Intent intent = new Intent(mContext, ShippingDeliverymanSelectActivity.class);
            intent.putExtra(ShippingDeliverymanSelectActivity.EXTRA.DUE_DATE, mResultData.DUE_DATE);
            intent.putExtra(ShippingDeliverymanSelectActivity.EXTRA.M_SHIPPINGSEQ, mResultData.SHIPPING_SEQ);
            intent.putExtra(ShippingDeliverymanSelectActivity.EXTRA.DELIVERY_MAN_INDEX, 2);
            startActivityForResult(intent, REQUEST_DELIVARYMAN);
        }
    };
    /**
     * 부사수3 변경
     */
    private OnClickListener onSelDmanClickListener3 = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mResultData == null) {
                return;
            }
            Intent intent = new Intent(mContext, ShippingDeliverymanSelectActivity.class);
            intent.putExtra(ShippingDeliverymanSelectActivity.EXTRA.DUE_DATE, mResultData.DUE_DATE);
            intent.putExtra(ShippingDeliverymanSelectActivity.EXTRA.M_SHIPPINGSEQ, mResultData.SHIPPING_SEQ);
            intent.putExtra(ShippingDeliverymanSelectActivity.EXTRA.DELIVERY_MAN_INDEX, 3);
            startActivityForResult(intent, REQUEST_DELIVARYMAN2);
        }
    };

    private OnClickListener onSnImageClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mImageView = (ImageView) v;

            if (Build.VERSION.SDK_INT >= 24) {
                new AlertDialog.Builder(mContext).setItems(new String[]{"카메라", "앨범", "바코드"}, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            startCamera();
                            break;
                        case 1:
                            showBottomPicker((ImageView) v, false);
                            break;
                        case 2:
                            Intent intent = new Intent(mContext, CaptureActivity.class);
                            startActivityForResult(intent, RESULT_CODE_BARCODE_1);
                            break;
                    }
                }).show();
            } else if (Build.VERSION.SDK_INT >= 14) {
                new AlertDialog.Builder(mContext).setItems(new String[]{"카메라&앨범", "바코드"}, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            showBottomPicker((ImageView) v, true);
                            break;
                        case 1:
                            Intent intent = new Intent(mContext, CaptureActivity.class);
                            startActivityForResult(intent, RESULT_CODE_BARCODE_1);
                            break;
                    }
                }).show();
            } else {
                showBottomPicker((ImageView) v, true);
            }
        }
    };

    private OnClickListener onSn2ImageClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mImageView = (ImageView) v;
            if (Build.VERSION.SDK_INT >= 24) {
                new AlertDialog.Builder(mContext).setItems(new String[]{"카메라", "앨범", "바코드"}, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            startCamera();
                            break;
                        case 1:
                            showBottomPicker((ImageView) v, false);
                            break;
                        case 2:
                            Intent intent = new Intent(mContext, CaptureActivity.class);
                            startActivityForResult(intent, RESULT_CODE_BARCODE_2);
                            break;
                    }
                }).show();
            } else if (Build.VERSION.SDK_INT >= 14) {
                new AlertDialog.Builder(mContext).setItems(new String[]{"카메라&앨범", "바코드"}, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            showBottomPicker((ImageView) v, true);
                            break;
                        case 1:
                            Intent intent = new Intent(mContext, CaptureActivity.class);
                            startActivityForResult(intent, RESULT_CODE_BARCODE_2);
                            break;
                    }
                }).show();
            } else {
                showBottomPicker((ImageView) v, true);
            }
        }
    };


    private void replaceTexT(int resId, String regularExpression, String replacement) {
        setText(resId, text(resId).replaceAll(regularExpression, replacement));
    }

    protected void complete() {
        mPhotoJsonArray = new JSONArray();
        PP.PHOTO.set("");
        OH.c().notifyObservers(OH.TYPE.MODIFY_SHIPPING_DETAIL);
        finish();
    }

    private Net.OnNetResponse<searchCodeAdapter> onAdapterNetResponse = new Net.OnNetResponse<searchCodeAdapter>() {

        @Override
        public void onResponse(searchCodeAdapter response) {
            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            setVisibility(R.id.adapter_layout, true);
            setVisibility(R.id.adapter_layout_line, true);

            ArrayList<String> strings = new ArrayList<>();

            for (searchCodeAdapter.Data.resultData d : response.data.resultData) {
                strings.add(d.DET_CD_NM);
            }

            setSpinner(R.id.water_adaptor, strings);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    private Net.OnNetResponse<searchCodeJoli> onJoliNetResponse = new Net.OnNetResponse<searchCodeJoli>() {

        @Override
        public void onResponse(searchCodeJoli response) {
            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            setVisibility(R.id.adapter_layout, true);
            setVisibility(R.id.adapter_layout_line, true);

            ArrayList<String> strings = new ArrayList<>();

            for (searchCodeJoli.Data.resultData d : response.data.resultData) {
                strings.add(d.DET_CD_NM);
            }

            setSpinner(R.id.jorisugi, strings);

        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    private OnClickListener onCompcheckClickListener = new OnClickListener() {

        private Net.OnNetResponse<UpdateShippnigCimpcheck> onNetResponse = new Net.OnNetResponse<UpdateShippnigCimpcheck>() {

            @Override
            public void onResponse(UpdateShippnigCimpcheck response) {
                reload();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        };

        @Override
        public void onClick(View v) {

            String m_compcheck = text(R.id.company_check);
            m_compcheck = m_compcheck.replace(mResultData.SYMPTOM, "");
            String m_compcheck1 = text(R.id.m_compcheck1);
            if (m_compcheck1.trim().length() == 0) {
                toast("비고추가 입력란에 추가비고를 입력해주세요.");
                return;
            }

            setText(R.id.m_compcheck1, "");
            hideKeyboard(v);
            Net.async(new UpdateShippnigCimpcheck(shipping_seq, mResultData.COMPANY_CHECK,
                    "(" + Auth.d.resultData.ADMIN_NM + " " + SDF.mmddhhmm_.format(System.currentTimeMillis()) + ")" + m_compcheck1)).setOnNetResponse(onNetResponse);
        }
    };

    private OnClickListener onSelCarClickListener = v -> selectCarNum();

    private void selectCarNum() {
        Intent intent = new Intent(mContext, SelectCarActivity.class);
        startActivityForResult(intent, REQUEST_CHANGE_CAR);
    }

    private DialogInterface.OnClickListener positiveListener = (dialog, which) -> selectCarNum();

    @Override
    public void onBackPressed() {
        finish();
    }

    protected void 입금유형(String payment_type) {
        final View v = findViewById(R.id.spinnerCostInputType);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView<?>))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        int position = 0;
        switch (payment_type) {
            case "1":
                position = 1;
                break;
            case "2":  // 현금
                position = 3;
                break;
            case "3":
                position = 2;
                break;
            default:
                position = 3;
                break;
        }

        try {
            int selectPosition = mResultData.CASH_RECEIPT_TYPE.equals("0") ? 0 : 1;

            Spinner spinner = (Spinner) findViewById(R.id.spinnerReceipt);
            spinner.setSelection(selectPosition);

            TextView textView = (TextView) findViewById(R.id.editReceipt);
            textView.setText(mResultData.CASH_RECEIPT_CARD_NUM);
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapterView.setSelection(position);
    }

    /**
     * 리퍼 이름이 고객에게 공개되면 안되므로 지문 아이콘을 눌러서 리퍼 이름이 나오도록 한다.
     */
    private void setupReferName(final resultData r) {
        if (!TextUtils.isEmpty(r.REFER_NAME.trim())) {
            View fingerprint = findViewById(R.id.fingerprint);
            fingerprint.setVisibility(View.VISIBLE);
            fingerprint.setOnTouchListener(touchListener);
        }
    }

    /**
     * 지문 아이콘의 터치이벤트
     */
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        private float downX, downY;

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getAction() == MotionEvent.ACTION_UP) {
                hideReferNamePopupWindow();
                return true;
            } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                downY = event.getRawY();
                downX = event.getRawX();
                showReferNamePopupWindow();
                return true;
            } else {
                if (Math.abs(Math.abs(event.getRawY()) - Math.abs(downY)) > 25) {
                    hideReferNamePopupWindow();
                }
                if (Math.abs(Math.abs(event.getRawX()) - Math.abs(downX)) > 25) {
                    hideReferNamePopupWindow();
                }
            }
            return true;
        }
    };

    /**
     * 팝업 윈도우를 내린다.
     */
    private void hideReferNamePopupWindow() {
        if (mReferNamePopupWindow != null && mReferNamePopupWindow.isShowing()) {
            mReferNamePopupWindow.dismiss();
            mReferNamePopupWindow = null;
        }
    }

    /**
     * 팝업 윈도우를 띄운다.
     */
    private void showReferNamePopupWindow() {
        hideReferNamePopupWindow();

        View form = getLayoutInflater().inflate(R.layout.refer_name_popup_window, null);
        TextView text = form.findViewById(R.id.textView1);
        text.setText(mResultData.REFER_NAME);
        mReferNamePopupWindow = new PopupWindow(form, RelativeLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // 누른 버튼의 상단에 나오도록 한다.
        mReferNamePopupWindow.showAsDropDown(findViewById(R.id.product_name), 0, (int) -Util.convertDpToPixel(52 * 1.4f, mContext));
    }

    /**
     * 사은품 리스트를 가져온다.
     */
    private Net.OnNetResponse<appSelectLatexCode> onAppSelectLatexCodeListener = new Net.OnNetResponse<appSelectLatexCode>() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(appSelectLatexCode response) {

            try {
                // 데이터를 가져올때 예외가 발생하면 그냥 리턴시켜 버린다.
                mAppSelectLatexCode = response.data.resultData;
            } catch (Exception e) {
                return;
            }

            ArrayList<String> arrSpinner = new ArrayList<>();
            for (appSelectLatexCode.Data.resultData d : mAppSelectLatexCode)
                arrSpinner.add(d.PRODUCT_NAME);

            // 스피너에 라텍스 사은품을 넣는다.
            Spinner s = (Spinner) findViewById(R.id.freegift_l);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item, arrSpinner);
            s.setAdapter(adapter);

            // 라텍스 베개 저장눌렀을때 이벤트 리스너
            setOnClickListener(R.id.freegift_l_save, v -> {
                // 선택한 사은품을 서버에 저장한다.
                appUpdateLFreegift();
            });

            // 받은 이미 선택된 데이터를 스피너에 선택한다
            selectSelectedLatex();
        }

        /**
         * 받은 이미 선택된 데이터를 스피너에 선택한다
         */
        private void selectSelectedLatex() {
            // 라텍스 사은품 스피너
            Spinner s = (Spinner) findViewById(R.id.freegift_l);
            // /mobile/api/appShippingMasterDetail.json에서 받은 L_FREEGIFT 값을 선택한다.
            try {
                for (int i = 0; i < mAppSelectLatexCode.size(); i++) {
                    appSelectLatexCode.Data.resultData d = mAppSelectLatexCode.get(i);
                    if (d.PRODUCT_CODE.equals(mResultData.L_FREEGIFT)) {
                        s.setSelection(i);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 선택한 사은품을 서버에 저장한다.
     */
    private void appUpdateLFreegift() {
        // this.shipping_seq가 없을수도 있을까??
        String shipping_seq = this.shipping_seq;
        String l_freegift = "";

        Spinner s = (Spinner) findViewById(R.id.freegift_l);
        try {
            // l_freegift에 사은품 코드를 넣는다. 과정에서 익셉션이 발생하면 더 이상 진행하지 않는다.
            l_freegift = mAppSelectLatexCode.get(s.getSelectedItemPosition()).PRODUCT_CODE;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        // 선택한 사은품을 저장해보자
        Net.async(new appUpdateLFreegift(shipping_seq, l_freegift)).setOnNetResponse(onAppUpdateLFreegiftListener);
    }

    /**
     * 선택한 사은품을 서버에 저장한다.
     */
    private Net.OnNetResponse<appUpdateLFreegift> onAppUpdateLFreegiftListener = new Net.OnNetResponse<appUpdateLFreegift>() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(appUpdateLFreegift response) {
            String resultMsg;
            try {
                resultMsg = response.data.resultMsg;
            } catch (Exception e) {
                return;
            }
            BFDialog.newInstance(mContext).showSimpleDialog(resultMsg);
        }
    };

    /**
     * 하단 갤러리 보여줌
     *
     * @param v
     */
    private void showBottomPicker(ImageView v, boolean showCameraTile) {
        mImageView = v;
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(ShippingMasterDetailActivity.this)
                        .setOnImageSelectedListener(uri -> operateImage(uri))
                        .showCameraTile(showCameraTile)
                        .create();

                bottomSheetDialogFragment.show(getSupportFragmentManager());
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(ShippingMasterDetailActivity.this, "아래의 권한이 거부되었습니다." + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("해당 기능을 이용하려면 아래의 권한이 필요합니다.\n\n다시 설정하시려면 [설정] > [권한]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    /**
     * 받아온 이미지를 가공해서 전송하기위한 메소드이다.
     */
    private void operateImage(Uri data) {
        Log.d("ShippingMasterDetailActivity > operateImage > data : " + data.toString());


        try {
            File pathfile = writeImage(getImageName());

            FileOutputStream os = new FileOutputStream(pathfile);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), data);
            if (bitmap == null) {
                Toast.makeText(mContext, "이미지를 다시 선택하세요.", Toast.LENGTH_SHORT).show();
                return;
            }
            File craetTime = new File(Objects.requireNonNull(data.getPath()));
            Date lastModDate = new Date(craetTime.lastModified());

            ExifInterface exif = new ExifInterface(data.getPath());
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = exifOrientationToDegrees(exifOrientation);
            final int maxSize = 640;
            final float bitmapHeight = bitmap.getHeight();
            final float bitmapWidth = bitmap.getWidth();

            float newHeight; // 1000
            float newWidth; // 500
            Log.d("bitmapWidth  : " + bitmapWidth);
            if (bitmapHeight > bitmapWidth) {
                newHeight = maxSize;
                newWidth = (newHeight / bitmapHeight * bitmapWidth);
            } else {
                newWidth = maxSize;
                newHeight = (newWidth / bitmapWidth * bitmapHeight);
            }

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
            bitmap = rotate(bitmap, exifDegree);

            if (System.currentTimeMillis() - lastModDate.getTime() < INTERVAL_TIME) {
                bitmap = applyWaterMarkEffect(bitmap, SDF.mmddhhmm_.format(lastModDate.getTime()), 100, 100, Color.RED, 80, 24, false);
            }

            bitmap.compress(CompressFormat.JPEG, 80, os);
            os.close();
            mImageView.setImageBitmap(bitmap);
            mImageView.setTag(pathfile);
            ImageHolder holder = new ImageHolder();
            holder.bitmap = bitmap;
            holder.file = pathfile;
            fileHashMap.put(mImageView.getId(), holder);
            saveImageToJson(holder.file.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String getImageName() {
        String name = null;
        if (mImageView.getId() == R.id.imageone)
            name = "imageone.jpg";
        else if (mImageView.getId() == R.id.imagetwo)
            name = "imagetwo.jpg";
        else if (mImageView.getId() == R.id.imagethree)
            name = "imagethree.jpg";
        else if (mImageView.getId() == R.id.imagefour)
            name = "imagefour.jpg";
        else if (mImageView.getId() == R.id.imagefive)
            name = "imagefive.jpg";
        else if (mImageView.getId() == R.id.imagesix)
            name = "imagesix.jpg";
        else if (mImageView.getId() == R.id.ladder_image_one)
            name = "ladder_image_one.jpg";
        else if (mImageView.getId() == R.id.ladder_image_two)
            name = "ladder_image_two.jpg";
        else if (mImageView.getId() == R.id.ladder_image_three)
            name = "ladder_image_three.jpg";
        else if (mImageView.getId() == R.id.concent_image_one)
            name = "concent_image_one.jpg";
        else if (mImageView.getId() == R.id.concent_image_two)
            name = "concent_image_two.jpg";
        else if (mImageView.getId() == R.id.concent_image_three)
            name = "concent_image_three.jpg";
        else if (mImageView.getId() == R.id.servicePicture1)
            name = "service_picture1.jpg";
        else if (mImageView.getId() == R.id.servicePicture2)
            name = "service_picture2.jpg";

        else if (mImageView.getId() == R.id.collectPicture1)
            name = "collectPicture1.jpg";
        else if (mImageView.getId() == R.id.collectPicture2)
            name = "collectPicture2.jpg";
        return name;
    }

    private void startCamera() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        String name = getImageName();
        mPickImageFile = new File(dir, name);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", mPickImageFile));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);
        } else {
            // 카메라를 사용할 수 없는 기기 입니다.
        }
    }

    /**
     * 캡쳐 사진에 대한 리절트
     */
    private void onCaptureImageResult(int resultCode) {
        if (resultCode == Activity.RESULT_OK && mPickImageFile != null) {
//            Point size = ImageUtils.testImageSize(mPickImageFile.getAbsolutePath());
            FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", mPickImageFile);
            saveImageToTargetFile(mPickImageFile.getAbsolutePath());
        }
    }

    /**
     * 이미지를 파일로 저장한다.
     */
    private void saveImageToTargetFile(String filePath) {
        Log.d("saveImageToTargetFile ~~");
        Bitmap bitmap = ImageUtils.scale(filePath, 640, 320);

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String imgDate_Time = exif.getAttribute(ExifInterface.TAG_DATETIME);

        try {
            if (imgDate_Time != null && System.currentTimeMillis() - SDF.yyyymmddhhmmss_3.parse(imgDate_Time) < 10000) {
//                Log.d("currentTimeMillis - imgDate_Time : " + (System.currentTimeMillis() - SDF.yyyymmddhhmmss_3.parse(imgDate_Time)));
                bitmap = applyWaterMarkEffect(bitmap, SDF.mmddhhmm_.format(System.currentTimeMillis()), 100, 100, Color.RED, 80, 24, false);
            }
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        bitmap = ImageUtils.rotateBitmap(filePath, bitmap);
        mImageView.setImageBitmap(bitmap);
        mImageView.setTag(mPickImageFile);

        saveImageToJson(filePath);
        // 새로 얻어온 파일의 사이즈가 너무나 크다 비트맵 크기만큼 변환하자
        saveBitmapToFileCache(bitmap, mPickImageFile.getAbsolutePath());
    }

    private void saveImageToJson(String filePath) {
        JSONObject listData = new JSONObject();
        try {
            listData.put(BaseConst.VIEW_ID, mImageView.getId());
            listData.put(BaseConst.IMAGE_PATH, filePath);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mPhotoJsonArray.put(listData);
    }


    /**
     * 워터 마크 생성
     *
     * @param src
     * @param watermark
     * @param x
     * @param y
     * @param color
     * @param alpha
     * @param size
     * @param underline
     * @return
     */
    public Bitmap applyWaterMarkEffect(Bitmap src, String watermark, int x, int y, int color,
                                       int alpha, int size, boolean underline) {
        Log.d("applyWaterMarkEffect ~~");
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Paint paint = new Paint();
        paint.setColor(color);
//        paint.setAlpha(alpha);
        paint.setTextSize(size);

        paint.setAntiAlias(true);
        paint.setUnderlineText(underline);
        canvas.drawText(watermark, x, y, paint);

        return result;
    }


    /**
     * 비트맵을 파일로 저장
     */
    private void saveBitmapToFileCache(Bitmap bitmap, String strFilePath) {

        File fileCacheItem = new File(strFilePath);

        Log.d("fileCacheItem.length() " + fileCacheItem.length());
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(CompressFormat.JPEG, 80, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}