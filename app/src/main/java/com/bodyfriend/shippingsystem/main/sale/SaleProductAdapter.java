package com.bodyfriend.shippingsystem.main.sale;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFDialog;
import com.bodyfriend.shippingsystem.databinding.ItemBizSaleBinding;

import java.util.ArrayList;

public class SaleProductAdapter extends RecyclerView.Adapter<SaleProductAdapter.SaleProductHolder> implements SaleProductEvent {


    private ArrayList<SelectBizSalesData.ResultData.List> salesData;
    private ItemBizSaleBinding inflate;

    @Override
    public SaleProductAdapter.SaleProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        inflate = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_biz_sale, parent, false);
        inflate.setEvent(this);
        return new SaleProductHolder(inflate);
    }

    @Override
    public void onBindViewHolder(SaleProductAdapter.SaleProductHolder holder, int position) {
        SelectBizSalesData.ResultData.List data = salesData.get(position);
//        if ( position % 2 == 0) {
//            holder.mbinding.itemConLayout.setBackgroundColor(Color.WHITE);
//        } else {
//            holder.mbinding.itemConLayout.setBackgroundColor(Color.parseColor("#e0e0e0"));
//        }
        holder.mbinding.setViewData(data);
    }

    @Override
    public int getItemCount() {
        return salesData != null ? salesData.size() : 0;
    }


    public void clearData() {
        if (salesData != null) {
            final int itemCount = salesData.size();
            salesData.clear();
            // 변경 갱신
            notifyItemRangeRemoved(0, itemCount);
        }
    }


    public void setBenefitDataList(ArrayList<SelectBizSalesData.ResultData.List> benefitDataList) {
        clearData();
        if (benefitDataList != null && benefitDataList.size() > 0) {
            this.salesData = benefitDataList;
            // 변경 갱신
//            notifyItemRangeInserted(benefitDataList.size() - 1, benefitDataList.size());
            notifyDataSetChanged();
        }
    }

    @Override
    public void onEventClicked(SelectBizSalesData.ResultData.List list) {
        Context context = inflate.getRoot().getContext();
        BFDialog bfDialog = BFDialog.newInstance(context);
        bfDialog.showSimpleDialog(list.RECEIVE_PS);
    }

    public class SaleProductHolder extends RecyclerView.ViewHolder {
        public ItemBizSaleBinding mbinding;

        SaleProductHolder(ItemBizSaleBinding itemView) {
            super(itemView.getRoot());
            mbinding = itemView;
        }
    }
}
