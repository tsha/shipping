package com.bodyfriend.shippingsystem.main.delivery_order.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator
import com.bodyfriend.shippingsystem.base.util.OH
import com.bodyfriend.shippingsystem.main.delivery_order.net.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import util.SoundSearcher


class WarehouseViewModel : ViewModel() {
    private val _warehouseList: MutableLiveData<MutableList<WarehouseListData.ResultData>> = MutableLiveData()
    val warehouseList: LiveData<MutableList<WarehouseListData.ResultData>> get() = _warehouseList

    private val _returnProductDataList: MutableLiveData<MutableList<ReturnProductData.ResultDatum>> = MutableLiveData()
    val returnProductDataList: LiveData<MutableList<ReturnProductData.ResultDatum>> get() = _returnProductDataList

    private val _giftListData: MutableLiveData<MutableList<GiftListData.ResultData>> = MutableLiveData()
    val giftListData: LiveData<MutableList<GiftListData.ResultData>> get() = _giftListData
    var warehouseData: MutableList<WarehouseListData.ResultData> = arrayListOf()
    var returnProductList: MutableList<ReturnProductData.ResultDatum> = arrayListOf()

    var isSessionOut: MutableLiveData<Boolean> = MutableLiveData()

//    lateinit var createBarcodeData: List<CreatedBarcodeData.ResultDatum>

    //    val _createBarcodeData: MutableLiveData<MutableList<CreatedBarcodeData.ResultDatum>> get() = MutableLiveData()
    val createBarcodeData: MutableLiveData<DeliverManStockData.ResultDatum> = MutableLiveData()

    fun getWarehouseList(groupCd: String) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectWarehouseList(groupCd)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<WarehouseListData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: WarehouseListData) {
                        Log.d("TAG", "getWarehouseList")
                        t.resultDataList?.let {
                            Log.d("TAG", "getWarehouseList")
                            if (!it.isNullOrEmpty()) {
                                _warehouseList.postValue(it as MutableList<WarehouseListData.ResultData>?)
                                warehouseData = it
                            }
                        }

                    }

                    override fun onError(e: Throwable) {
                        isSessionOut.postValue(true)
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }

                })

    }

    fun getReturnProductList(itemGroup: String) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectReturnItemList(itemGroup)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ReturnProductData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: ReturnProductData) {
                        Log.d("TAG", "getWarehouseList")
                        t.resultData.let {
                            Log.d("TAG", "getWarehouseList")
                            if (!it.isNullOrEmpty()) {
                                _returnProductDataList.postValue(it as MutableList<ReturnProductData.ResultDatum>?)
                                returnProductList = it
                            }
                        }

                    }

                    override fun onError(e: Throwable) {
                        isSessionOut.postValue(true)
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }

                })

    }

    fun createBarcode(resultDatum: ReturnProductData.ResultDatum) {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .createBarcode(resultDatum.itemCd)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CreatedBarcodeData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: CreatedBarcodeData) {
                        Log.d("TAG", "getWarehouseList")
                        t.resultData.let {
                            Log.d("TAG", "getWarehouseList")
                            t.resultData?.tempLot.let {
                                val stockData: DeliverManStockData.ResultDatum? = it?.let { it1 ->
                                    DeliverManStockData.ResultDatum(itemCd = resultDatum.itemCd,
                                            serialCd = it1, itemNm = resultDatum.itemNm, isValid = true)
                                }
                                createBarcodeData.postValue(stockData)

                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        isSessionOut.postValue(true)
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }
                })

    }

    fun selectGiftList() {
        ServiceGenerator.createService(WarehouseApi::class.java)
                .selectGiftList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<GiftListData> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: GiftListData) {
                        t.resultData.let {

                            _giftListData.postValue(t.resultData as MutableList<GiftListData.ResultData>?)
                        }
                    }

                    override fun onError(e: Throwable) {
                        isSessionOut.postValue(true)
                        OH.c().notifyObservers(OH.TYPE.SESSION_OUT)
                    }
                })

    }


    fun onReturnProductTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        checkReturnProductSearch(s.toString())
    }


    /**
     * 검색 창에 텍스트가 입력될때에 리스트를 재구성한다.
     */
    private fun checkReturnProductSearch(search: String) {
        val newDatas: ArrayList<ReturnProductData.ResultDatum>

        if (search.isEmpty()) {
            /*   warehouseData = warehouseList.value as List<WarehouseListData.ResultData>
               newDatas = warehouseData as ArrayList<WarehouseListData.ResultData>*/
            _returnProductDataList.postValue(returnProductList as MutableList<ReturnProductData.ResultDatum>?)
        } else {
            newDatas = ArrayList()
            var value: String
            val value1 = returnProductDataList.value
            if (value1 != null) {
                for (data in returnProductList) {
                    value = data.itemNm
                    if (SoundSearcher.matchString(value, search)) {
                        newDatas.add(data)
                    }
                }
                _returnProductDataList.postValue(newDatas as MutableList<ReturnProductData.ResultDatum>?)
            }
        }
    }


    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        checkSearch(s.toString())
    }

    /**
     * 검색 창에 텍스트가 입력될때에 리스트를 재구성한다.
     */
    private fun checkSearch(search: String) {
        val newDatas: ArrayList<WarehouseListData.ResultData> = ArrayList()

        if (search.isEmpty()) {
            /*   warehouseData = warehouseList.value as List<WarehouseListData.ResultData>
               newDatas = warehouseData as ArrayList<WarehouseListData.ResultData>*/
            _warehouseList.postValue(warehouseData as MutableList<WarehouseListData.ResultData>?)
        } else {

            var value: String
            val value1 = warehouseList.value
            if (value1 != null) {
                for (data in warehouseData) {
                    value = data.slNm.toString()
                    if (SoundSearcher.matchString(value, search)) {
                        newDatas.add(data)
                    }
                }
//                if (newDatas.isNotEmpty())
                _warehouseList.postValue(newDatas as MutableList<WarehouseListData.ResultData>?)
            }
        }
    }

}
