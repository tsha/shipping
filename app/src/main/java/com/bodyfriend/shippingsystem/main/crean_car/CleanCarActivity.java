package com.bodyfriend.shippingsystem.main.crean_car;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.BFDialog;
import com.bodyfriend.shippingsystem.base.BFMultiNet;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.databinding.ActivityCleanCarBinding;
import com.bodyfriend.shippingsystem.main.crean_car.utils.CameraUtil;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.kerrel.imageresizer_lib.ImageResizer;

import java.io.File;
import java.util.ArrayList;

import gun0912.tedbottompicker.TedBottomPicker;


public class CleanCarActivity extends BFActivity {


    private ActivityCleanCarBinding mBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_clean_car);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mBinding.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        CleanCarViewModel cleanCarViewModel = new CleanCarViewModel();
        mBinding.setViewModel(cleanCarViewModel);
//        setupRecyclerView();

    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

//        setupToolbar();
        setupButtons();
    }

    /**
     * 버튼 셋업
     */
    private void setupButtons() {
        setOnClickListener(R.id.registration, onRegistrationClickListener);
        mBinding.screen1.setOnClickListener(onImageClickListener);
        mBinding.screen2.setOnClickListener(onImageClickListener);
        mBinding.screen3.setOnClickListener(onImageClickListener);
        mBinding.screen4.setOnClickListener(onImageClickListener);
        mBinding.screen5.setOnClickListener(onImageClickListener);
        mBinding.screen6.setOnClickListener(onImageClickListener);
    }

    /* */

    /**
     * 툴바를 세팅한다
     *//*
    private void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("세차 사진 등록");
        mToolbar.setTitleTextColor(Color.BLACK);

        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView textView = getActionBarTextView();
        if (textView != null) {
            setFont(textView, BFFont.BODY_M); // 바디프랜드 폰트를 삽입한다.
        }
    }*/
    @Override
    public void onBackPressed() {
        finish();
    }

    /* *//**
     * 툴바의 font를 지정하는 뷰를 가져온다.
     * 리플렉팅으로...
     *
     * @return
     *//*
    private TextView getActionBarTextView() {
        TextView titleTextView = null;

        try {
            Field f = mToolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(mToolbar);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
        return titleTextView;
    }*/

    /**
     * 등록 클릭
     */
    private View.OnClickListener onRegistrationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (
                    mBinding.screen1.getTag() == null
                            && mBinding.screen2.getTag() == null
                            && mBinding.screen3.getTag() == null
                            && mBinding.screen4.getTag() == null
                            && mBinding.screen5.getTag() == null
                            && mBinding.screen6.getTag() == null
                    ) {
                Snackbar.make(findViewById(R.id.activity_crean_car), "사진을 찍어주세요.", Snackbar.LENGTH_LONG).show();
                return;
            }

            BFMultiNet.newInstance(NetConst.host + "stats/admin/insertCarStatus.json")

                    .setFilePart("screen1", mBinding.screen1.getTag()
                            , "screen2", mBinding.screen2.getTag()
                            , "screen3", mBinding.screen3.getTag()
                            , "screen4", mBinding.screen4.getTag()
                            , "screen5", mBinding.screen5.getTag()
                            , "screen6", mBinding.screen6.getTag()
                    )
                    .setOnNetResultListener((isSuccess, resultMsg) -> {
                        if (isSuccess) {
                            BFDialog.newInstance(mContext).showDialog(resultMsg, "확인", positiveListener);
                        } else {
                            BFDialog.newInstance(mContext).showSimpleDialog(resultMsg);
                        }
                    }).execute();

        }

        private DialogInterface.OnClickListener positiveListener = (dialog, which) -> finish();
    };


    /**
     * 이미지 클릭하면 카메라를 띄운다.
     */
    private View.OnClickListener onImageClickListener = new View.OnClickListener() {

        private ImageView mView;

        @Override
        public void onClick(View v) {
//            mView = (ImageView) v;
//            CameraUtil.getInstance().startCamera(CleanCarActivity.this, String.valueOf(v.getId()) + ".jpg", onCameraListener);


            PermissionListener permissionlistener = new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(mContext)
                            .setOnImageSelectedListener(uri -> {
                                Bitmap resizedBitmap = resizeImage(uri);
                                saveToFile(resizedBitmap, v);
                                loadImage(resizedBitmap, (ImageView) v);
                            })
                            .create();
                    tedBottomPicker.show(getSupportFragmentManager());
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
//                    showSnackbar("권한 설정이 거부되었습니다.");
                }
            };

            TedPermission.with(getApplicationContext())
                    .setPermissionListener(permissionlistener)
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                    .check();

        }

        public CameraUtil.OnCameraListener onCameraListener = new CameraUtil.OnCameraListener() {
            @Override
            public void onCreatedImage(Bitmap bitmap, File file) {
                mView.setImageBitmap(bitmap);
                mView.setTag(file);
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
                BFDialog.newInstance(mContext).showSimpleDialog(String.format("%s\n%s", "사진 생성에 실패하였습니다.", e.getMessage()));
            }
        };
    };


    /**
     * 이미지를 리사이즈 한다.
     * 통신때 용량조절을 위해
     * 서버의 사진 디비에 부하를 줄이기 위해
     *
     * @param uri
     */
    private Bitmap resizeImage(Uri uri) {
        File file = new File(uri.getPath());
        return ImageResizer.resize(file, 320, 640);
    }

    /**
     * 이미지 로드
     */
    private void loadImage(Bitmap resizedBitmap, ImageView v) {
        v.setImageBitmap(resizedBitmap);
    }

    /**
     * 파일을 저장한다.
     */
    private void saveToFile(Bitmap resizedBitmap, View v) {
        String fileName = null;
        if (v == mBinding.screen1) fileName = "clean1.png"; // 시리얼
        else if (v == mBinding.screen2) fileName = "clean2.png"; // 전체사진
        else if (v == mBinding.screen3) fileName = "clean3.png"; // 첨부1
        else if (v == mBinding.screen4) fileName = "clean4.png"; // 첨부2
        else if (v == mBinding.screen5) fileName = "clean5.png"; // 첨부2
        else if (v == mBinding.screen6) fileName = "clean6.png"; // 첨부2

        if (fileName != null) {
            File file = createFile(fileName);
            ImageResizer.saveToFile(resizedBitmap, file);
            v.setTag(file);
        }
    }

    /**
     * 인자로 들어온 이름의 파일을 만든다.
     */
    private File createFile(String fileName) {
        File pathfile = Environment.getExternalStorageDirectory();

//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
//        String date = simpleDateFormat.format(System.currentTimeMillis());
        pathfile = new File(pathfile.getPath() + "/bodyfriend/shipping/");

        if (!pathfile.exists()) pathfile.mkdirs();

        pathfile = new File(pathfile.getPath() + "/" + fileName);
        return pathfile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        CameraUtil.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    protected void setFont(TextView view, String font) {
        @SuppressWarnings("unused")
        Typeface typeface = Typeface.createFromAsset(getAssets(), font);
        view.setTypeface(typeface);
    }
}
