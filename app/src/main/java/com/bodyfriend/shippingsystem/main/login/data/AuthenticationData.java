package com.bodyfriend.shippingsystem.main.login.data;

public class AuthenticationData {
    public data data;

    public class data {
        public String msgid;
        public String authCode;

        @Override
        public String toString() {
            return "data{" +
                    "msgId='" + msgid + '\'' +
                    ", authCode='" + authCode + '\'' +
                    '}';
        }
    }

    public status status;

    public class status {
        public String code;
        public String message;

        @Override
        public String toString() {
            return "status{" +
                    "code='" + code + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AuthenticationData{" +
                "data=" + data +
                ", status=" + status +
                '}';
    }
}
