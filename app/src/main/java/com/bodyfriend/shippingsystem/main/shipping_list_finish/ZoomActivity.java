package com.bodyfriend.shippingsystem.main.shipping_list_finish;

import image.ZoomableImageView;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.image.AUIL;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ZoomActivity extends BFActivity {

    public static class EXTRA {
        public static final String IMAGE_URI = "IMAGE_URI";
    }

    private String imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoom_activity);
    }


    @Override
    protected void onParseExtra() {
        super.onParseExtra();

        final Intent intent = getIntent();
        imgUri = intent.getStringExtra(EXTRA.IMAGE_URI);
        Log.d("imgUri : " + imgUri);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setImage((ImageView) findViewById(R.id.zoom), imgUri);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private ImageLoadingListener imageLodingListener = new ImageLoadingListener() {

        @Override
        public void onLoadingStarted(String imageUri, View view) {

        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            imageLoadingFail();
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            ZoomableImageView zoom = (ZoomableImageView) findViewById(R.id.zoom);
            zoom.setImageBitmap(loadedImage);
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {
            imageLoadingFail();
        }
    };

    private void imageLoadingFail() {
        toast("이미지 로딩에 실패하였습니다.");
        finish();
    }
}
