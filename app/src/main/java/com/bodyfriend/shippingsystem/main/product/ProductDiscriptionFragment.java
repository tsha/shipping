package com.bodyfriend.shippingsystem.main.product;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.PdfDownloader;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.product.net.appProductList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 이주영 on 2016-08-08.
 */
public class ProductDiscriptionFragment extends BFFragment2 {

    protected ListView mList;
    private List<appProductList.Data.resultData.list> mData;
    private MainActivity.MainActivityControll mainActivityControll;

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.product_discription_list_fragment2, container, false);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        mainActivityControll.changeTitle(R.layout.title_layout_product_list);

        mList = (ListView) findViewById(R.id.list);
        mList.setEmptyView(findViewById(R.id.emptyView));


        findViewById(R.id.latexInfo).setOnClickListener(onClickListener);

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            PdfDownloader.newInstance(mContext).downloadPdf("https://www.bfservice.co.kr/images/pdf/latex_as_guide.pdf");
        }
    };

    @Override
    protected void onLoad() {
        super.onLoad();

        reqProductList();

    }

    private void reqProductList() {
        Net.async(new appProductList(NetConst.s_producttype, "")).setOnNetResponse(onProductListNetResponse);
    }

    private Net.OnNetResponse<appProductList> onProductListNetResponse = new Net.OnNetResponse<appProductList>() {

        @Override
        public void onResponse(appProductList response) {

            mData = response.data.resultData.list;
            ProductListAdapter listAdapter = new ProductListAdapter(mContext, response.data.resultData.list);
            mList.setAdapter(listAdapter);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    public class ProductListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<appProductList.Data.resultData.list> mItemList;

        public ProductListAdapter(Context context, List<appProductList.Data.resultData.list> list) {
            this.mItemList = (ArrayList<appProductList.Data.resultData.list>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            final appProductList.Data.resultData.list item = mData.get(position);
            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.app_product_list_item2, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.chair = (ImageView) convertView.findViewById(R.id.chair);
                viewHolder.product_name = (TextView) convertView.findViewById(R.id.product_name);
                viewHolder.feature = (TextView) convertView.findViewById(R.id.feature);

                convertView.setTag(viewHolder);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.product_name.setText(item.PRODUCT_NAME);
            viewHolder.feature.setText(item.PRODUCT_SPEC);
            int chairImage = getChairImage(item.PRODUCT_NAME);
            if (chairImage != 0) viewHolder.chair.setImageResource(chairImage);


            convertView.setOnClickListener(new View.OnClickListener() {

                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mContext, ProductDescriptionDetailActivity.class);
                    intent.putExtra(ProductDescriptionDetailActivity.EXTRA.PRODUCTCODE, item.PRODUCT_CODE);
                    startActivity(intent);

                }
            });

            return convertView;
        }

        public class ViewHolder {
            public ImageView chair; // 제품 이미지
            public TextView product_name; // 제품명
            public TextView feature; // 제품명
        }

        private int getChairImage(String pName) {
            int resId = 0;
            if (pName.contains("아이로보")) {
                resId = R.drawable.chair_irobo;
            } else if (pName.contains("팬텀")) {
                if (pName.contains("블랙")) {
                    resId = R.drawable.chair_pantom_black;
                } else if (pName.contains("브라운")) {
                    resId = R.drawable.chair_pantom_brown;
                } else if (pName.contains("레드")) {
                    resId = R.drawable.chair_pantom_red;
                } else {
                    resId = R.drawable.chair_pantom_black;
                }
            } else if (pName.contains("파라오")) {
                resId = R.drawable.chair_parao;
                if (pName.contains("S")) {
                    resId = R.drawable.chair_parao_s;
                }
            } else if (pName.contains("프레지던트")) {
                resId = R.drawable.chair_president;
                if (pName.contains("플러스")) {
                    resId = R.drawable.chair_president_plus;
                }
            } else if (pName.contains("렉스")) {
                resId = R.drawable.chair_rexl;
                if (pName.contains("블랙")) {
                    resId = R.drawable.chair_rexl_black;
                }
            } else if (pName.contains("레지나")) {
                resId = R.drawable.chair_regina;
            }
            return resId;
        }
    }
}
