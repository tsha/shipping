package com.bodyfriend.shippingsystem.main.shipping_list;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appSelectDeliveryMan2List;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appSelectDeliveryMan2List.Data.resultData;
import com.bodyfriend.shippingsystem.main.shipping_list.net.modifyDeliveryMan2;

import java.util.ArrayList;
import java.util.List;

import util.SoundSearcher;

/**
 * 부사수 선택 팝업
 */
public class ShippingDeliverymanSelectActivity extends BFActivity {

    private int mDeliveryManIndex = 2;

    public static class EXTRA {
        public final static String DUE_DATE = "DUE_DATE";
        final static String M_SHIPPINGSEQ = "M_SHIPPINGSEQ";
        final static String DELIVERY_MAN_INDEX = "DELIVERY_MAN_INDEX";
    }

    private String dueDate;
    private String mShippingSeq;
    private ListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_delivaryman);
    }

    @Override
    protected void onParseExtra() {
        super.onParseExtra();
        dueDate = getIntent().getStringExtra(EXTRA.DUE_DATE);
        mShippingSeq = getIntent().getStringExtra(EXTRA.M_SHIPPINGSEQ);
        mDeliveryManIndex = getIntent().getIntExtra(EXTRA.DELIVERY_MAN_INDEX, 2);
        if (mShippingSeq == null) {
            mShippingSeq = "";
        }
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        mList = (ListView) findViewById(R.id.list);
        mList.setEmptyView(findViewById(R.id.emptyView));
        setFinish(R.id.close);

        EditText searchEdit = (EditText) findViewById(R.id.searchEdit);
        searchEdit.addTextChangedListener(watcher);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        showProgress();
        Net.async(new appSelectDeliveryMan2List(dueDate)).setOnNetResponse(onNetResponse);
    }


    public List<resultData> datas;
    private NoticeListAdapter mListAdapter;
    private Net.OnNetResponse<appSelectDeliveryMan2List> onNetResponse = new Net.OnNetResponse<appSelectDeliveryMan2List>() {

        @Override
        public void onResponse(appSelectDeliveryMan2List response) {
            hideProgress();
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            datas = response.data.resultData;
            datas.add(0, new resultData("", "미등록"));

            if (mListAdapter == null) {
                mListAdapter = new NoticeListAdapter(mContext, datas);
                mList.setAdapter(mListAdapter);
            } else {
                mListAdapter.setItemList(datas);
                mListAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    public class NoticeListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<resultData> mItemList;

        public void setItemList(List<resultData> list) {
            this.mItemList = (ArrayList<resultData>) list;
        }

        NoticeListAdapter(Context context, List<resultData> list) {
            this.mItemList = (ArrayList<resultData>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            final resultData item = mItemList.get(position);
            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.select_dman_list_item, parent, false);

                viewHolder = new ViewHolder(convertView, item);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.dman_name.setText(item.ADMIN_NM);
            convertView.setOnClickListener(v -> {
                showProgress();
                final String m_deliveryman2 = item.ADMIN_ID;
                final String m_duedate = dueDate.trim().equals("") ? null : dueDate;
                final String m_shippingseq = mShippingSeq;
                Net.async(new modifyDeliveryMan2(m_deliveryman2, m_duedate, m_shippingseq, mDeliveryManIndex)).setOnNetResponse(onModifyDeliveryMan2NetResponse);

            });
            return convertView;
        }

        private Net.OnNetResponse<modifyDeliveryMan2> onModifyDeliveryMan2NetResponse = new Net.OnNetResponse<modifyDeliveryMan2>() {

            private android.content.DialogInterface.OnClickListener positiveListener = (dialog, which) -> {
                setResult(RESULT_OK);
                finish();
            };

            @Override
            public void onResponse(modifyDeliveryMan2 response) {
                showDialog(response.data.resultMsg, "확인", positiveListener);
                hideProgress();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
            }
        };

        public class ViewHolder {
            public resultData mItem;
            public TextView dman_name;

            public ViewHolder(View convertView, final resultData item) {
                mItem = item;
                dman_name = convertView.findViewById(R.id.dman_name);
                convertView.setTag(this);
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            checkSearch();
        }

    };

    /**
     * 검색 창에 텍스트가 입력될때에 리스트를 재구성한다.
     */
    private void checkSearch() {

        String search = text(R.id.searchEdit);

        List<resultData> newDatas;

        if (search.length() == 0) {
            newDatas = datas;
        } else {
            newDatas = new ArrayList<>();
            String value;

            for (resultData data : datas) {
                value = data.ADMIN_NM;
                if (SoundSearcher.matchString(value, search)) {
                    newDatas.add(data);
                }
            }
        }

        mListAdapter.setItemList(newDatas);
        mListAdapter.notifyDataSetChanged();
    }
}
