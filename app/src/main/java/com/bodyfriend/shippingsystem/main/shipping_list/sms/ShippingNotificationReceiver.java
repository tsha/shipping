package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.main.shipping_list.db.SmsDBHelper;

import java.util.ArrayList;

/**
 * Created by 이주영 on 2016-09-01.
 */
public class ShippingNotificationReceiver extends BroadcastReceiver {
    private SmsDBHelper mSmsDBHelper;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(":: onReceive");
        mSmsDBHelper = new SmsDBHelper(context);

        try {
            ArrayList<SmsModel> smsModels = mSmsDBHelper.select(Integer.parseInt(intent.getAction()));
            mSmsDBHelper.delete(Integer.parseInt(intent.getAction()));
            ShippingSmsManager.getInstance(context).startSmsActivity(smsModels);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
