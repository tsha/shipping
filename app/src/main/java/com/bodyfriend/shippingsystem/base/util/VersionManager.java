package com.bodyfriend.shippingsystem.base.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.gc.android.market.api.MarketSession;
import com.gc.android.market.api.MarketSession.Callback;
import com.gc.android.market.api.model.Market;
import com.gc.android.market.api.model.Market.AppsRequest;
import com.gc.android.market.api.model.Market.ResponseContext;

/**
 * 버전 확인 관리자 클래스<br/>
 * [변경이력]<br/>
 * @author neon2231@cruxware.net
 * @version 1.0.0
 * @since 1.0.0
 */
@SuppressLint("HandlerLeak")
public class VersionManager {
	
	/**
	 * 버전 로드 리스너 인터페이스<br/>
	 * [변경이력]<br/>
	 * @author neon2231@cruxware.net
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	public interface OnVersionListener {
		public void onLoadSuccess(String version);
		public void onLoadFailed();
	}
	
	/**
	 * 핸들러<br/>
	 * @since 1.0.0
	 */
	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:	if (listener != null) listener.onLoadSuccess((String)msg.obj);	break;
			case 1:	if (listener != null) listener.onLoadFailed();					break;
			default:break;
			}
 		}
		
	};
	
	/**
	 * 컨텍스트<br/>
	 * @since 1.0.0
	 */
	private static Context context;
	
	/**
	 * 싱글톤 인스턴스<br/>
	 * @since 1.0.0
	 */
	private static VersionManager instance;
	
	/**
	 * 버전 로드 리스너<br/>
	 * @since 1.0.0
	 */
	private OnVersionListener listener;

	private String id;

	private String pwd;

	private String packName;
	
	/**
	 * 생성자
	 * @since 1.0.0
	 */
	private VersionManager() {}
	
	/**
	 * 싱글톤 인스턴스를 얻는다.<br/>
	 * @param c
	 * @return
	 * @since 1.0.0
	 */
	public static VersionManager getInstance(Context c) {
		if (instance == null) {
			context = c;
			instance = new VersionManager();
		}
		return instance;
	}
	
	/**
	 * 마켓에 등록된 최신 버전을 로드한다.
	 * <br/>
	 * @param id
	 * @param pwd
	 * @param package_name
	 * @since 1.0.0
	 */
	public void loadMarketVersion(final String id, final String pwd, final String package_name, OnVersionListener listener) {
		this.listener = listener;
		this.id = id;
		this.pwd = pwd;
		this.packName = package_name;
		
		MarketAsync async = new MarketAsync();
		async.execute("");
	}
	
	public class MarketAsync extends AsyncTask<Object, Object, Object>{

		private int result;
		private String version;
		
		@Override
		protected Void doInBackground(Object... params) {
			
			try {
				MarketSession session = new MarketSession();
				session.login(id, pwd);
				session.getContext().setAndroidId(getAndroidId(context));

				String query = packName;
				AppsRequest appsRequest = AppsRequest.newBuilder()
						.setQuery(query)
						.setStartIndex(0).setEntriesCount(10)
						.setWithExtendedInfo(true)
						.build();
				session.append(appsRequest, new Callback<Market.AppsResponse>() {

					@Override
					public void onResult(ResponseContext arg0, Market.AppsResponse arg1) {
						if (arg1 != null & arg1.getApp(0) != null & arg1.getApp(0).getVersion() != null) {
							result = 0;
							version = arg1.getApp(0).getVersion();
							return;
						}
						result = 1;
					}
				});
				session.flush();
			} catch (Exception e) {
				e.printStackTrace();
				result = 1;
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			
			switch (this.result) {
			case 0:	if (listener != null) listener.onLoadSuccess(version);	break;
			case 1:	if (listener != null) listener.onLoadFailed();					break;
			default:break;
			}
		}
		
	}
	
	/**
	 * 클라이언트의 버전을 얻는다.<br/>
	 * @return
	 * @since 1.0.0
	 */
	public String getClientVersion() {
		String versionName = "";  
	    try {  
	        PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);  
	        versionName = info.versionName;  
	    } catch (NameNotFoundException e) {  
	    }  
	    return versionName;
	}
	
	/**
	 * 안드로이드 아이디를 얻는다.<br/>
	 * @param context
	 * @return
	 * @since 1.0.0
	 */
	public String getAndroidId(Context context) {
    	final Uri uri = Uri.parse("content://com.google.android.gsf.gservices");
    	final String ID_KEY = "android_id";
        String[] params = { ID_KEY };
        Cursor c = context.getContentResolver().query(uri, null, null, params, null);
        if (!c.moveToFirst() || c.getColumnCount() < 2)
            return null;
     
        try {
            return Long.toHexString(Long.parseLong(c.getString(1)));
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
