package com.bodyfriend.shippingsystem.base.net;

import android.support.annotation.NonNull;
import android.util.Log;

import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.login.Auth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Taeseok on 2018-01-16.
 */

public class ServiceGenerator {

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(NetConst.host)
                    .client(getRequestHeader())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();
    public static String JSESSIONID = "";


    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public static <S> S changeApiBaseUrl(Class<S> serviceClass, String newApiBaseUrl) {

        builder = new Retrofit.Builder()
                .client(getRequestHeader())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())

//                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(new Persister(new AnnotationStrategy())))
                .baseUrl(newApiBaseUrl);

        return builder.build().create(serviceClass);
    }


    @NonNull
    private static Response addHeader(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;

        try {
            newRequest = request.newBuilder()
                    .header("Cookie", String.format("JSESSIONID=%s", Auth.getSid()))
                    .method(request.method(), request.body())
                    .build();
            JSESSIONID = Auth.getSid();
        } catch (Exception exception) {
            Log.d("addHeader", "Error");
            exception.printStackTrace();
            return chain.proceed(request);
        }
        okhttp3.Response proceed = chain.proceed(newRequest);
        if (proceed.code() == 500) {
            return proceed;
        }

        Log.e("addHeader", "c : " + proceed.code());


        return proceed;
    }


    private static class ResponseInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            // We obtain the response using chain.proceed() API. This invokes the request and return the response
            Response response = chain.proceed(chain.request());
            try {
                JSONObject jsonObject = new JSONObject();
                if (response.code() == 200) {
                    jsonObject.put("code", 200);
                    jsonObject.put("status", "OK");
                    jsonObject.put("message", new JSONObject(response.body().string()));
                } else {
                    jsonObject.put("code", 404);
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", new JSONObject(response.body().string()));
                }
                MediaType contentType = response.body().contentType();
                ResponseBody body = ResponseBody.create(contentType, jsonObject.toString());
                return response.newBuilder().body(body).build();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    private static OkHttpClient getRequestHeader() {

        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
                .build();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            return new OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
//                    .addInterceptor(new ResponseInterceptor())
                    .addInterceptor(ServiceGenerator::addHeader)
                    .addInterceptor(interceptor)
//                    .connectionSpecs(Collections.singletonList(spec))
                    .build();
        } else {
            return new OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(ServiceGenerator::addHeader)
//                    .connectionSpecs(Collections.singletonList(spec))
                    .build();
        }
    }
}
