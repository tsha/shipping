package com.bodyfriend.shippingsystem.base;

/**
 * Created by 이주영 on 2016-06-28.
 *
 * 리퀘스트 코드들을 관리한다.
 */
public class RequestCodes {

    public static final int FULLSIGN = 1001;
//    public static final int LOGIN = 1002;
    public static final int NOTICE = 1003;
}
