package com.bodyfriend.shippingsystem.base.util;

import java.util.Calendar;

/**
 * Created by 이주영 on 2017-01-04.
 */

public class CalendarManager {
    public static CalendarManager newInstance() {
        return new CalendarManager();
    }


    public boolean isToday(Calendar compareDay) {
        Calendar calendar = Calendar.getInstance();
        return SDF.yyyymmdd.format(calendar.getTime()).equals(SDF.yyyymmdd.format(compareDay.getTime()));
    }
}