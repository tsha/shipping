package com.bodyfriend.shippingsystem.base.common;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.IdRes;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.TextView.OnEditorActionListener;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bodyfriend.shippingsystem.base.log.Log;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author djrain
 */

public abstract class BaseFragment extends Fragment {

    private void setFinish(int resid) {
        final View v = findViewById(resid);
        if (v == null) {
            throw new IllegalArgumentException("뭐야이건");
        }

        v.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private WeakReference<Activity> mActivity;
    private Window mWindow;
    protected Context mContext;
    private Resources mResources;
    private View mView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Activity activity = getActivity();
        mActivity = new WeakReference<Activity>(activity);
        mContext = activity;
        mResources = mContext.getResources();
        mWindow = activity.getWindow();
        mView = view;

        if (SCREEN)
            keepScreenOn();
    }

    public boolean isOnCreated() {
        return mView != null;
    }

    @Override
    public void onStop() {
        super.onStop();
        cancelProgressAll();
//		clearDlgs();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<Integer> value = new ArrayList<Integer>();
        final ArrayList<View> views = mManagedTagView;
        for (View view : views) {
            String view_name = mResources.getResourceName(view.getId());
            final Object obj = view.getTag();
            if (obj instanceof Bundle) {
                Bundle bundle = (Bundle) obj;
                Set<String> keyset = bundle.keySet();
                for (String key : keyset) {
                    outState.putString(view_name + key, bundle.getString(key));
                }
                value.add(view.getId());
            }
        }
        outState.putIntegerArrayList("managedViewIds", value);
        mManagedTagView.clear();
        // Log.l(outState);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState == null)
            return;
        ArrayList<Integer> resids = savedInstanceState.getIntegerArrayList("managedViewIds");
        Set<String> keyset = savedInstanceState.keySet();

        for (Integer resid : resids) {
            String view_name = mResources.getResourceName(resid);
            for (String key : keyset) {
                Bundle bundle = null;
                if (key.startsWith(view_name)) {
                    String value = savedInstanceState.getString(key);
                    String bundleKey = key.substring(view_name.length());
                    bundle = new Bundle();
                    bundle.putString(bundleKey, value);
                }
                findViewById(resid).setTag(bundle);
            }
            mManagedTagView.add(findViewById(resid));
        }
    }

    //activity/////////////////////////
    public void finish() {
        final Activity activity = mActivity.get();
        if (activity == null)
            throw new NullPointerException("!!Activity is Null");
        activity.finish();
    }

    public Intent getIntent() {
        final Activity activity = mActivity.get();
        if (activity == null)
            throw new NullPointerException("!!Activity is Null");
        return activity.getIntent();
    }

    public BaseActivity getBaseActivity() {
        final Activity activity = mActivity.get();
        if (activity == null)
            throw new NullPointerException("!!Activity is Null");
        return (BaseActivity) activity;
    }

    //View/////////////////////////
    public View findViewById(int resid) {
        if (mView == null)
            throw new NullPointerException("!!findViewById 할수 없는상황 잠시후가능");
        return mView.findViewById(resid);
    }

    public View findViewWithTag(Object tag) {
        if (mView == null)
            throw new NullPointerException("!!findViewById 할수 없는상황 잠시후가능");

        return mView.findViewWithTag(tag);
    }

    //window/////////////////////////////////////////////////////////////////////////////////////////
    public static boolean SCREEN = false;

    public void screenBrightnessMax() {
        WindowManager.LayoutParams lp = mWindow.getAttributes();
        lp.screenBrightness = 100 / 100.0f;
        mWindow.setAttributes(lp);
    }

    public void keepScreenOn() {
        mWindow.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void keepScreenOff() {
        mWindow.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    protected void setFragmentVisible(Fragment fr, boolean b) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (b == true)
            ft.show(fr);
        else
            ft.hide(fr);
        ft.commit();
    }

    protected void setFragmentVisible(int resid_fragment, boolean b) {
        setFragmentVisible(getFragmentManager().findFragmentById(resid_fragment), b);
    }

    public void fragmentReplace(int resid_unique_container, Class<?> fragment) {
        try {
            getFragmentManager().beginTransaction().replace(resid_unique_container, (Fragment) fragment.newInstance()).commit();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected Fragment getItemAtPosition(int pager_resid, int position) {
        final View v = findViewById(pager_resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof ViewPager))
            throw new IllegalArgumentException("ViewPager 불가능 컨트롤임");

        return getFragmentManager().findFragmentByTag("android:switcher:" + pager_resid + ":" + position);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    protected ArrayList<Dialog> mDialogs = new ArrayList<Dialog>();

    protected void clearDlgs() {
        final ArrayList<Dialog> dlgs = mDialogs;

        while (dlgs.size() > 0) {
            Dialog alertDialog = dlgs.remove(0);
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
                alertDialog = null;
            }
        }
    }

    protected ProgressDialog mProgressDlg;

    protected void cancelProgressAll() {
        if (mProgressDlg != null) {
            removeProgressDialog();
        }
    }

    protected void createProgressDialog() {
//		 style="?android:attr/progressBarStyleLarge"
        mProgressDlg = new ProgressDialog(mContext, android.R.attr.progressBarStyleLarge);
    }

    protected int mProgressDlgRef = 0;

    protected void removeProgressDialog() {
        mProgressDlgRef = 0;
        if (mProgressDlg != null)
            mProgressDlg.cancel();
    }

    public Dialog showDialogFinish(Object message, Object positiveButtonText, DialogInterface.OnClickListener positiveListener) {
        Dialog dlg = getDialog(null, null, message, null, positiveButtonText, positiveListener, null, null, null, null);

        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });

        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });

        dlg.show();
        return dlg;
    }

    public void showProgress() {
        if (mProgressDlg != null && mProgressDlg.isShowing()) {
            mProgressDlgRef++;
            return;
        }
        createProgressDialog();
        mProgressDlg.show();
    }

    public void hideProgress() {
        if (mProgressDlg != null && mProgressDlg.isShowing())
            mProgressDlgRef--;

        if (mProgressDlgRef <= 0) {
            removeProgressDialog();
        }
    }

    public Dialog showDatePicker(OnDateSetListener onDateSetListener, int year, int monthOfYear, int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, onDateSetListener, year, monthOfYear, dayOfMonth);
        datePickerDialog.updateDate(year, monthOfYear, dayOfMonth);
        datePickerDialog.show();
        mDialogs.add(datePickerDialog);
        return datePickerDialog;
    }

    public static class OnDateClickListener implements OnClickListener, OnDateSetListener {

        private TextView display;
        private long maxDate = -1;
        private long minDate = -1;
        private Calendar current;
        private String format;

        @Override
        public void onClick(View v) {
            final Calendar cal = current;
            int year = cal.get(Calendar.YEAR);
            int monthOfYear = cal.get(Calendar.MONTH);
            int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), this, year, monthOfYear, dayOfMonth);

            DatePicker datePicker = datePickerDialog.getDatePicker();
            if (minDate != -1)
                datePicker.setMinDate(minDate);
            if (maxDate != -1)
                datePicker.setMaxDate(maxDate);

            datePickerDialog.updateDate(year, monthOfYear, dayOfMonth);
            datePickerDialog.setTitle("");
            datePickerDialog.show();
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            final Calendar cal = current;
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear);
            cal.set(Calendar.DATE, dayOfMonth);
            display.setText(new SimpleDateFormat(format, Locale.getDefault()).format(cal.getTime()));
        }

        public Calendar getCalendar() {
            return current;
        }

        public long getMilliseconds() {
            return current.getTimeInMillis();
        }

        public OnDateClickListener(TextView display, String format, Calendar current, long minDate, long maxDate) {
            this.display = display;
            this.maxDate = maxDate;
            this.minDate = minDate;
            this.current = current;
            this.format = format;
        }

        public OnDateClickListener(TextView display, String format, Calendar current) {
            this.display = display;
            this.current = current;
            this.format = format;
        }

        public static class Builder {
            TextView display = null;
            String format = "yyyy.MM.dd";
            Calendar current;
            long maxDate = Long.MAX_VALUE;
            long minDate = Long.MIN_VALUE;

            public Builder setDisplay(TextView display) {
                if (display == null)
                    throw new NullPointerException("!!display must not null");
                this.display = display;
                return this;
            }

            public Builder setMaxDate(long maxDate) {
                this.maxDate = maxDate;
                return this;
            }

            public Builder setMinDate(long minDate) {
                this.minDate = minDate;
                return this;
            }

            public Builder setCurrent(long current) {
                this.current = Calendar.getInstance();
                this.current.setTimeInMillis(current);
                return this;
            }

            public Builder setCurrent(Calendar current) {
                this.current = current;
                return this;
            }

            public Builder setFormat(String format) {
                this.format = format;
                return this;
            }

            public OnDateClickListener create() {
                return new OnDateClickListener(display, format, current, minDate, maxDate);
            }
        }

    }

    public Dialog showTimePicker(OnTimeSetListener onTimeSetListener, int hourOfDay, int minutOfHour) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(mContext, onTimeSetListener, hourOfDay, minutOfHour, false);
        timePickerDialog.updateTime(hourOfDay, minutOfHour);
        timePickerDialog.show();
        mDialogs.add(timePickerDialog);
        return timePickerDialog;
    }

    public class OnTimeClickListener implements OnClickListener, OnTimeSetListener {

        View display;
        String mTemplate = "HH:mm";
        Calendar mCalendar;

        public OnTimeClickListener(TextView display, long milliseconds) {
            this.display = display;
            this.mCalendar = Calendar.getInstance();
            mCalendar.setTimeInMillis(milliseconds);
        }

        public OnTimeClickListener(TextView display, Calendar calendar) {
            this.display = display;
            mCalendar = calendar;
            mTemplate = "HH:mm";
        }

        public OnTimeClickListener(int displayResid, Calendar calendar) {
            this(displayResid, calendar, "HH:mm");
        }

        public OnTimeClickListener(int displayResid, long milliseconds) {
            display = findViewById(displayResid);
            this.mCalendar = Calendar.getInstance();
            mCalendar.setTimeInMillis(milliseconds);
        }

        public OnTimeClickListener(int displayResid, Calendar calendar, String template) {
            display = findViewById(displayResid);
            mCalendar = calendar;
            mTemplate = template;
        }

        @Override
        public void onClick(View v) {
            showTimePicker(this, mCalendar.get(Calendar.HOUR_OF_DAY), mCalendar.get(Calendar.MINUTE));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar cal = mCalendar;
            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cal.set(Calendar.MINUTE, minute);
            setText(display, new SimpleDateFormat(mTemplate, Locale.getDefault()).format(cal.getTime()));
        }

        public Calendar getCalendar() {
            return mCalendar;
        }

    }

    public Dialog showDialog(View v) {
        Dialog dlg = new Dialog(mContext);
        dlg.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlg.setContentView(v);
        dlg.show();
        mDialogs.add(dlg);

        return dlg;
    }

    public Dialog showDialog(Object message, Object positiveButtonText, DialogInterface.OnClickListener positiveListener) {
        return showDialog(null, null, message, null, positiveButtonText, positiveListener, null, null, null, null);
    }

    public Dialog showDialog(Object message,//
                             Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener) {
        return showDialog(null, null, message, null, positiveButtonText, positiveListener, null, null, negativeButtonText, negativeListener);
    }

    public Dialog showDialog(Object message,//
                             Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object neutralButtonText, DialogInterface.OnClickListener neutralListener//
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener//
    ) {
        return showDialog(null, null, message, null, positiveButtonText, positiveListener, neutralButtonText, neutralListener, negativeButtonText, negativeListener);
    }

    public Dialog showDialog(Object title, Object icon, Object message, View view//
            , Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object neutralButtonText, DialogInterface.OnClickListener neutralListener //
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener//
    ) {
        final Dialog dlg = getDialog(title, icon, message, view, positiveButtonText, positiveListener, neutralButtonText, neutralListener, negativeButtonText, negativeListener);
        dlg.show();
        return dlg;
    }

    public Dialog getDialog(Object title, Object icon, Object message, View view//
            , Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object neutralButtonText, DialogInterface.OnClickListener neutralListener //
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener//
    ) {

        Context context = mContext;

        final Builder builder = new Builder(context);
        if (title != null)
            builder.setTitle(title instanceof String ? (String) title : mResources.getString((Integer) title));
        if (message != null)
            builder.setMessage(message instanceof String ? (String) message : mResources.getString((Integer) message));
        if (icon != null)
            builder.setIcon(icon instanceof Drawable ? (Drawable) icon : mResources.getDrawable((Integer) icon));
        if (view != null)
            builder.setView(view);
        if (positiveButtonText != null)
            builder.setPositiveButton(positiveButtonText instanceof String ? (String) positiveButtonText : mResources.getString((Integer) positiveButtonText), positiveListener);
        if (negativeButtonText != null)
            builder.setNegativeButton(negativeButtonText instanceof String ? (String) negativeButtonText : mResources.getString((Integer) negativeButtonText), negativeListener);
        if (neutralButtonText != null)
            builder.setNeutralButton(neutralButtonText instanceof String ? (String) neutralButtonText : mResources.getString((Integer) neutralButtonText), neutralListener);

        final AlertDialog dlg = builder.create();

        mDialogs.add(dlg);

        return dlg;
    }

    public Dialog showTDialog(View view) {
        Dialog dlg = new Dialog(mContext);
        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlg.setContentView(view);
        mDialogs.add(dlg);
        dlg.show();
        return dlg;
    }

    public void showList(int resid, Collection<? extends CharSequence> collection, DialogInterface.OnClickListener onItemclickedListener) {
        if (collection == null)
            throw new IllegalArgumentException("뭐야이건");
        showList(resid, collection.toArray(new CharSequence[collection.size()]), onItemclickedListener);
    }

    public void showList(int resid, final int array_resid, final DialogInterface.OnClickListener onItemclickedListener) {
        showList(resid, mResources.getTextArray(array_resid), onItemclickedListener);
    }

    public void showList(int resid, final CharSequence[] array, final DialogInterface.OnClickListener onItemclickedListener) {
        setOnClickListener(resid, new OnClickListener() {
            @Override
            public void onClick(View v) {
                showList(array, onItemclickedListener);
            }
        });
    }

    public Dialog showList(int array_resid, DialogInterface.OnClickListener onItemclickedListener) {
        return showList(mResources.getTextArray(array_resid), onItemclickedListener);
    }

    public Dialog showList(CharSequence[] items, DialogInterface.OnClickListener onItemclickedListener) {
        Dialog dlg = new Builder(mContext).setItems(items, onItemclickedListener).create();
        dlg.show();
        mDialogs.add(dlg);
        return dlg;
    }

    public Dialog showList(ListAdapter adapter, DialogInterface.OnClickListener onItemclickedListener) {

        Dialog dlg = new Builder(mContext).setAdapter(adapter, onItemclickedListener).create();

        dlg.show();
        mDialogs.add(dlg);
        return dlg;
    }

    protected Intent getIntent(Class<?> cls, Object... extras) {
        if (cls == null)
            return null;

        if (extras.length % 2 == 1)
            throw new UnsupportedOperationException("!!extras must pair");

        final Context context = mContext;
        final Intent intent = new Intent(context, cls);

        int N = extras.length / 2;
        for (int i = 0; i < N; i++) {
            final String key = (String) extras[i * 2];
            final Object value = extras[i * 2 + 1];
            if (!(key instanceof String)) {
                Log.l("!key is null skep \n" + key + "=" + value);
                continue;
            }
            if (value == null) {
                Log.l("!value is null skep \n" + key + "=" + value);
                continue;
            }

            if (value instanceof String)
                intent.putExtra(key, (String) value);
            else if (value instanceof Integer)
                intent.putExtra(key, (Integer) value);
            else
                new UnsupportedOperationException("!!extras must String or int or Integer\n" + key + "=" + value).printStackTrace();
        }
        return intent;
    }

    protected void setIntent(@IdRes int resid, Intent intent) {
        setIntent(findViewById(resid), -1, intent);
    }

    protected void setIntent(View v, Intent intent) {
        setIntent(v, -1, intent);
    }

    protected void setIntent(@IdRes int resid, int requestCode, Intent intent) {
        setIntent(findViewById(resid), requestCode, intent);
    }

    protected void setIntent(@IdRes int resid, int requestCode, Class<?> cls) {
        setIntent(findViewById(resid), requestCode, getIntent(cls));
    }

    protected void setIntent(@IdRes int resid, Class<?> cls) {
        setIntent(findViewById(resid), -1, getIntent(cls));
    }

    protected void setIntent(View v, Class<?> cls) {
        setIntent(v, -1, getIntent(cls));
    }

    protected void setIntent(View v, final int requestCode, final Intent intent) {
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (intent == null) {
            v.setOnClickListener(null);
        } else {
            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startActivityForResult(intent, requestCode);
                    } catch (ActivityNotFoundException e) {
                        Log.l(e);
                    }
                }
            });
        }
    }

    protected void setIntentFinish(@IdRes int resid, Class<?> cls) {
        setIntentFinish(findViewById(resid), getIntent(cls));
    }

    protected void setIntentFinish(View v, Class<?> cls) {
        setIntentFinish(v, getIntent(cls));
    }

    protected void setIntentFinish(@IdRes int resid, Intent intent) {
        setIntentFinish(findViewById(resid), intent);
    }

    protected void setIntentFinish(View v, final Intent intent) {
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (intent == null)
            throw new IllegalArgumentException("!! intent is not null");

        v.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivityForResult(intent, -1);
                    finish();
                } catch (ActivityNotFoundException e) {
                    Log.l(e);
                }
            }
        });
    }

    protected void setApplication(int resid, final String packagename) {
        setApplication(findViewById(resid), mContext.getPackageManager().getLaunchIntentForPackage(packagename), new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packagename)), null, null);
    }

    protected void setApplication(int resid, final Intent start, final Intent market) {
        setApplication(findViewById(resid), start, market, null, null);
    }

    protected void setApplication(int resid, final Intent start, final Intent market, final String move_message, final String install_message) {
        setApplication(findViewById(resid), start, market, move_message, install_message);
    }

    protected void setApplication(View v, final Intent start, final Intent market, final String move_message, final String install_message) {
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        v.setOnClickListener(new OnClickListener() {

            //이동한다는 메시지
            final DialogInterface.OnClickListener move_message_positiveListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startApplication();
                }
            };

            //설치한다는 메시지
            final DialogInterface.OnClickListener install_message_positiveListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startApplication();
                }
            };

            public void startApplication() {
                try {
                    startActivityForResult(start, -1);
                } catch (Exception e) {
                    if (install_message == null || install_message.length() <= 0) {
                        try {
                            startActivityForResult(market, -1);
                        } catch (ActivityNotFoundException e1) {
                            Log.l(e1);
                        }
                    } else {
                        showDialog(install_message, "예", install_message_positiveListener, "아니오", null);
                    }
                }
            }

            @Override
            public void onClick(View v) {
                if (install_message == null || install_message.length() <= 0) {
                    startApplication();
                } else {
                    showDialog(move_message, "예", move_message_positiveListener);
                }
            }
        });
    }

    protected void toastL(Object foramt, Object... args) {
        String text;
        if (foramt instanceof Integer)
            text = mContext.getString((Integer) foramt, args);
        else if (foramt instanceof String)
            text = String.format((String) foramt, args);
        else
            text = foramt.toString();

        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    public void toast(Object foramt, Object... args) {
        String text;
        if (foramt instanceof Integer)
            text = mContext.getString((Integer) foramt, args);
        else if (foramt instanceof String)
            text = String.format((String) foramt, args);
        else
            text = foramt.toString();

        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }

    protected void toastLC(Object foramt, Object... args) {
        String text;
        if (foramt instanceof Integer)
            text = mContext.getString((Integer) foramt, args);
        else if (foramt instanceof String)
            text = String.format((String) foramt, args);
        else
            text = foramt.toString();

        Toast toast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    protected void toastC(Object foramt, Object... args) {
        String text;
        if (foramt instanceof Integer)
            text = mContext.getString((Integer) foramt, args);
        else if (foramt instanceof String)
            text = String.format((String) foramt, args);
        else
            text = foramt.toString();

        Toast toast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private Toast _toast;

    protected void toastCI(Object foramt, Object... args) {
        if (_toast != null)
            _toast.cancel();

        String text;
        if (foramt instanceof Integer)
            text = mContext.getString((Integer) foramt, args);
        else if (foramt instanceof String)
            text = String.format((String) foramt, args);
        else
            text = foramt.toString();

        _toast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        _toast.setGravity(Gravity.CENTER, 0, 0);
        _toast.show();
    }

    protected void toggleCheck(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof Checkable))
            throw new IllegalArgumentException("Checkable 불가능 컨트롤임");
        final Checkable checkbutton = (Checkable) v;
        checkbutton.setChecked(!isChecked(resid));

    }

    protected boolean isChecked(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof Checkable))
            throw new IllegalArgumentException("Checkable 불가능 컨트롤임");

        final Checkable checkbutton = (Checkable) v;
        return checkbutton.isChecked();

    }

    protected void toggleSelected(View v) {

        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        v.setSelected(!v.isSelected());

    }

    protected void toggleSelected(int resid) {
        toggleSelected(findViewById(resid));
    }

    protected boolean isSelected(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        return v.isSelected();
    }

    protected void setRadioCheckIndex(int resid, int index) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof RadioGroup))
            throw new IllegalArgumentException("RadioGroup 불가능 컨트롤임");

        final RadioGroup radioGroup = (RadioGroup) v;
        final int N = radioGroup.getChildCount();
        int cruuent_index = 0;
        for (int i = 0; i < N; i++) {
            final View radioButton = radioGroup.getChildAt(i);
            if (radioButton instanceof RadioButton && radioButton.getId() > 0) {
                if (cruuent_index == index)
                    radioGroup.check(radioButton.getId());
                cruuent_index++;
            }
        }
    }

    protected void setRadioCheck(int resid, int checkeditemresid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof RadioGroup))
            throw new IllegalArgumentException("RadioGroup 불가능 컨트롤임");

        final RadioGroup radioGroup = (RadioGroup) v;
        radioGroup.check(checkeditemresid);
    }

    protected int getCheckedRadioButtonId(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof RadioGroup))
            throw new IllegalArgumentException("RadioGroup 불가능 컨트롤임");

        final RadioGroup radioGroup = (RadioGroup) v;
        return radioGroup.getCheckedRadioButtonId();
    }

    protected <T> T getCheckedRadioButtonId(int resid, SparseArray<T> map) {
        return (T) map.get(getCheckedRadioButtonId(resid));
    }

    protected void setImageBitmap(int resid, Bitmap bitmap) {
        setImageDrawable(resid, new BitmapDrawable(mResources, bitmap));
    }

    protected void setImageResource(int resid, int drawableid) {
        setImageDrawable(resid, mResources.getDrawable(drawableid));
    }

    protected void setImageDrawable(int resid, Drawable drawable) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof ImageView))
            throw new IllegalArgumentException("ImageView 불가능 컨트롤임");

        final ImageView imageView = (ImageView) v;

        imageView.setImageDrawable(drawable);
    }

    protected void setText(View v, CharSequence text) {
        setText(v, text, BufferType.NORMAL);
    }

    protected void setText(int resid, String format, Object... args) {
        setText(resid, String.format(format, args));
    }

    protected void setText(int resid, int resid_string) {
        setText(resid, mResources.getString(resid_string));
    }

    protected void setText(int resid, CharSequence text) {
        setText(resid, text, BufferType.NORMAL);
    }

    protected void setText(int resid, SpannableString text) {
        setText(resid, text, BufferType.SPANNABLE);
    }

    protected void setText(int resid, CharSequence text, BufferType bufferType) {
        setText(findViewById(resid), text, bufferType);
    }

    protected void setText(View v, CharSequence text, BufferType bufferType) {
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        tv.setText(text, bufferType);
    }

    protected void setHint(int resid, String text) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        tv.setHint(text);
    }

    protected CharSequence getHint(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        return tv.getHint();
    }

    protected void setTextColor(int resid, int color) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        tv.setTextColor(color);
    }

    protected void setHintTextColor(int resid, int color) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        tv.setHintTextColor(color);
    }

    protected String text(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;
        return tv.getText().toString();
    }

    protected void setTextUnderLine(int resid, String regularExpression) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;
        SpannableString s;
        if (tv.getText() instanceof SpannableString)
            s = (SpannableString) tv.getText();
        else
            s = new SpannableString(tv.getText().toString());

        Pattern pattern = Pattern.compile(regularExpression);
        Matcher m = pattern.matcher(s);
        while (m.find()) {
            if (s.getSpans(m.start(), m.end(), AbsoluteSizeSpan.class).length <= 0) {
                s.setSpan(new UnderlineSpan(), m.start(), m.end(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        tv.setText(s, BufferType.SPANNABLE);
    }

    protected void setTextSize(int resid, String regularExpression, int font_size) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;
        SpannableString s;
        if (tv.getText() instanceof SpannableString)
            s = (SpannableString) tv.getText();
        else
            s = new SpannableString(tv.getText().toString());

        Pattern pattern = Pattern.compile(regularExpression);
        Matcher m = pattern.matcher(s);
        while (m.find()) {
            if (s.getSpans(m.start(), m.end(), AbsoluteSizeSpan.class).length <= 0) {
                s.setSpan(new AbsoluteSizeSpan(font_size), m.start(), m.end(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        tv.setText(s, BufferType.SPANNABLE);
    }

    protected void setTextColor(int resid, String regularExpression, int font_color) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;
        SpannableString s;
        if (tv.getText() instanceof SpannableString)
            s = (SpannableString) tv.getText();
        else
            s = new SpannableString(tv.getText().toString());

        Pattern pattern = Pattern.compile(regularExpression);
        Matcher m = pattern.matcher(s);
        while (m.find()) {
            if (s.getSpans(m.start(), m.end(), ForegroundColorSpan.class).length <= 0) {
                s.setSpan(new ForegroundColorSpan(font_color), m.start(), m.end(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        tv.setText(s, BufferType.SPANNABLE);
    }

    protected String hint(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        return tv.getHint().toString();
    }

    protected void addTextChangedListener(int resid, TextWatcher watcher) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        tv.addTextChangedListener(watcher);
    }

    protected void setOnEditorActionListener(int resid_edittext, final int resid) {
        final View v = findViewById(resid_edittext);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof EditText))
            throw new IllegalArgumentException("애디트 불가능 컨트롤임");

        ((EditText) v).setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                View vv = findViewById(resid);
                if (vv == null)
                    throw new IllegalArgumentException("뭐야이건");

                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                    case EditorInfo.IME_ACTION_GO:
                    case EditorInfo.IME_ACTION_NONE:
                    case EditorInfo.IME_ACTION_SEARCH:
                    case EditorInfo.IME_ACTION_SEND:
                    case EditorInfo.IME_ACTION_UNSPECIFIED:
                        vv.performClick();
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    protected long getLong(int resid) {
        try {
            return (long) Long.parseLong(text(resid));
        } catch (NumberFormatException e) {
        }
        return 0L;
    }

    protected void setLong(int resid, long l) {
        setText(resid, "" + l);
    }

    protected int getInt(int resid) {
        try {
            return (int) Integer.parseInt(text(resid).replaceAll("[^\\d.+-]", ""));
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    protected void setInt(int resid, int l) {
        setText(resid, "" + l);
    }

    protected void setTag(int resid, Object tag) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setTag(tag);
    }

    protected void setTag(int resid, int key, Object tag) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setTag(key, tag);
    }

    protected Object getTag(int resid, int key) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        return v.getTag(key);
    }

    protected Object getTag(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        return v.getTag();
    }

    @SuppressWarnings("unchecked")
    protected <T> T getTag(int resid, T defaultValue) {
        try {
            return (T) getTag(resid);
        } catch (ClassCastException e) {
            Log.l(e);
            return defaultValue;
        }
    }

    protected boolean getTagBoolean(int resid) {
        return (Boolean) getTag(resid);
    }

    protected String getTagString(int resid) {
        return (String) getTag(resid);
    }

    protected int getTagInt(int resid) {
        return (Integer) getTag(resid);
    }

    // Tag에 값을 번들에 보관하는 방법
    private ArrayList<View> mManagedTagView = new ArrayList<View>();

    protected void addManageTag(View v) {
        mManagedTagView.add(v);
    }

    protected void removeManageTag(View v) {
        mManagedTagView.remove(v);
    }

    protected String getTagBS(int resid, String key) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        Object obj = v.getTag();
        if (obj == null)
            return null;
        // throw new IllegalArgumentException("뭐야 또 이건");

        if (!(obj instanceof Bundle))
            return null;
        // throw new IllegalArgumentException("Bundle 불가능 Tag임");

        Bundle bundle = (Bundle) obj;
        if (!bundle.containsKey(key)) {
            return null;
            // throw new IllegalArgumentException("다 좋은대 Bundle에 없어!");
        }
        return bundle.getString(key);
    }

    protected int getTagBI(int resid, String key) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        Object obj = v.getTag();
        if (obj == null)
            return 0;
        // throw new IllegalArgumentException("뭐야 또 이건");

        if (!(obj instanceof Bundle))
            return 0;
        // throw new IllegalArgumentException("Bundle 불가능 Tag임");

        Bundle bundle = (Bundle) obj;
        if (!bundle.containsKey(key)) {
            return 0;
            // throw new IllegalArgumentException("다 좋은대 Bundle에 없어!");
        }
        return bundle.getInt(key);
    }

    protected boolean isEmpty(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        return tv.getText().toString().trim().length() <= 0;
    }

    protected boolean isVisibility(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        return v.getVisibility() == View.VISIBLE;
    }

    protected int getVisibility(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        return v.getVisibility();
    }

    protected void setVisibility(View v, int visibility) {
        if (v != null)
            v.setVisibility(visibility);
    }

    protected void setVisibility(View v, boolean visibility) {
        setVisibility(v, visibility ? View.VISIBLE : View.GONE);
    }

    protected void setVisibility(int resid, int visibility) {
        final View v = findViewById(resid);
        setVisibility(v, visibility);
    }

    protected void setVisibility(int resid, boolean visibility) {
        setVisibility(resid, visibility ? View.VISIBLE : View.GONE);
    }

    protected void setVisibility_INVISIBLE(int resid, boolean visibility) {
        setVisibility(resid, visibility ? View.VISIBLE : View.INVISIBLE);
    }

    protected Animation startAnimation(int resid_view, int resid_anim) {
        return startAnimation(resid_view, AnimationUtils.loadAnimation(mContext, resid_anim));
    }

    protected Animation startAnimation(int resid_view, Animation animation) {
        final View v = findViewById(resid_view);
        if (v == null) {
            throw new IllegalArgumentException("뭐야이건");
        }
        v.startAnimation(animation);
        return animation;
    }

    protected void setEnabled(int resid, boolean b) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setEnabled(b);
    }

    protected void toggleGone(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setVisibility(View.GONE - v.getVisibility());
    }

    protected void setOnClickListener(int resid, OnClickListener onClickListener) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setOnClickListener(onClickListener);
    }

    protected void performClick(int resid) {
        final View v = findViewById(resid);
        if (v == null) {
            throw new IllegalArgumentException("뭐야이건");
        }
        v.performClick();
    }

    protected void setOnLongClickListener(int resid, OnLongClickListener onLongClickListener) {
        final View v = findViewById(resid);
        if (v == null) {
            // Log.l("뭐야이건");
            return;
        }
        if (!(v instanceof View))
            throw new IllegalArgumentException("View가아님");
        v.setOnLongClickListener(onLongClickListener);
    }

    protected void setLongIntent(int resid, final Class<?> cls) {
        setLongIntent(resid, new Intent(mContext, cls));
    }

    protected void setLongIntent(int resid, final Intent intent) {
        setLongIntent(resid, intent, -1);
    }

    protected void setLongIntent(int resid, final Intent intent, final int requestCode) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        v.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startActivityForResult(intent, requestCode);
                return true;
            }

            ;
        });
    }

    protected void setChecked(int resid, boolean checked) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof Checkable))
            throw new IllegalArgumentException("Checkable 이 아님");
        ((Checkable) v).setChecked(checked);
    }

    protected void setOnCheckedChangeListener(int resid, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof CompoundButton))
            throw new IllegalArgumentException("CompoundButton 이 아님");
        ((CompoundButton) v).setOnCheckedChangeListener(onCheckedChangeListener);
    }

    protected void setOnCheckedChangeListener(int resid, RadioGroup.OnCheckedChangeListener onCheckedChangeListener) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof RadioGroup))
            throw new IllegalArgumentException("RadioGroup 이 아님");
        ((RadioGroup) v).setOnCheckedChangeListener(onCheckedChangeListener);
    }

    protected void setOnItemSelectedListener(int resid, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        adapterView.setOnItemSelectedListener(onItemSelectedListener);

    }

    protected void setOnItemClickListener(int resid, AdapterView.OnItemClickListener onItemClickListener) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;
        adapterView.setOnItemClickListener(onItemClickListener);
    }

    protected <T> void setSpinner(int resid, T[] object) {
        setSpinner(resid, Arrays.asList(object));
    }

    protected void setSpinner(int resid, CharSequence[] strings) {
        setSpinner(resid, Arrays.asList(strings));
    }

    protected void setSpinner(int resid, int textArrayResId) {
        CharSequence[] strings = mResources.getTextArray(textArrayResId);
        setSpinner(resid, Arrays.asList(strings));
    }

    protected <T> void setSpinner(int resid, List<T> objects) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof Spinner))
            throw new IllegalArgumentException("Spinner 불가능 컨트롤임");

        final Spinner spinner = (Spinner) v;

        ArrayAdapter<T> adapter = new ArrayAdapter<T>(mContext, android.R.layout.simple_spinner_item, android.R.id.text1, objects);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public int getSelectedItemPosition(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView<?>))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        return adapterView.getSelectedItemPosition();
    }

    public Object getSelectedItem(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView<?>))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        return adapterView.getSelectedItem();
    }

    public void setSelection(int resid, int position) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView<?>))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        adapterView.setSelection(position);
    }

    public void setSelection(View v, int position) {
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView<?>))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        adapterView.setSelection(position);
    }

    public void setSelectionItem(int resid, String text) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof AdapterView<?>))
            throw new IllegalArgumentException("AdapterView 불가능 컨트롤임");

        final AdapterView<?> adapterView = (AdapterView<?>) v;

        if (text != null) {
            for (int i = 0; i < adapterView.getCount(); i++) {
                if (text.equals(adapterView.getItemAtPosition(i).toString())) {
                    adapterView.setSelection(i);
                    break;
                }
            }
        }

    }

    protected void setSelected(int resid, boolean selected) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setSelected(selected);
    }

    protected int getColor(int resid) {
        try {
            return mResources.getColor(resid);
        } catch (NotFoundException e) {
            return 0x00ffffff;
        }
    }

    protected void requestFocus(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.requestFocus();
    }

    protected int getInt(Uri uri_item, String single_projection) {
        Cursor cursor = mContext.getContentResolver().query(uri_item, new String[]{single_projection}, null, null, null);
        if (cursor.getCount() != 1)
            return 0;
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    protected String getString(Uri uri_item, String single_projection) {
        Cursor cursor = mContext.getContentResolver().query(uri_item, new String[]{single_projection}, null, null, null);
        if (cursor.getCount() != 1)
            return "";
        cursor.moveToFirst();
        String result = cursor.getString(0);
        cursor.close();
        return result;
    }

    protected static final int REQ_RECOGNIZER = 1975;

    /**
     * result get
     * <pre>
     * ArrayList&lt;String> mResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS, key);
     * </pre>
     */
    protected void startRecognizer(int resid) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        v.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mContext.getPackageName()); //호출한 패키지
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR"); //음성인식 언어 설정
                    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "검색어를 말하세요.");
                    startActivityForResult(intent, REQ_RECOGNIZER);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            ;
        });
    }

    protected OnGroupClickListener onNothingGroupClickListener = new OnGroupClickListener() {
        @Override
        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            return true;
        }
    };

}