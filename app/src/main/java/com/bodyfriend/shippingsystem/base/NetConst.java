package com.bodyfriend.shippingsystem.base;


import com.bodyfriend.shippingsystem.BuildConfig;

public class NetConst {
    public static final String GOOGEL_GAS_SCRIPT_URL = "https://script.google.com/macros/s/";
    public static final String PROMOTION_NOTICE = "https://www.bfservice.co.kr/ShippingMgt/ShippingTeamMgt/promotionNotice.view";
    public static String host;

    public static String tmpSsionId = "";
    public static boolean makePojo;
    public static final String HOST_SALE = "https://svc.bfservice.co.kr/";

    /**
     * 테스트 서버
     */

    private static final String yjy_local = "http://172.30.40.37:8082/";
    private static final String yjy = "http://192.168.1.88:8081/";
    private static final String ERP_TEST_1 = "https://bfs1.bfservice.co.kr/";
//    private static final String ERP_TEST_1 = "http://121.138.34.94/";
    public static final String HOST_TMAP = "https://api2.sktelecom.com/";



    /**
     * 운영서버
     */
//    public static final String HOST_REAL = "https://www.bfservice.co.kr/";
    public static final String HOST_REAL = ERP_TEST_1;
//    public static final String HOST_REAL = JSG; // 바로 접속


    public static final String HOST_TEST = yjy;

    // 카카오톡 인증
    public static String URL_AUTH = "http://121.138.34.14/";


    static {
        host = BuildConfig.DEBUG ? HOST_TEST : HOST_REAL;
    }

    /**
     * host를 변경한다.
     *
     * @param host
     */
    public static void setHost(String host) {
        NetConst.host = host;
    }

    /**
     * 제품코드 : 안마의자
     */
    public static final String PRODUCT_TYPE_M = "M";

    /**
     * 제품코드 : 정수기
     */
    public static final String PRODUCT_TYPE_W = "W";

    /**
     * 제품 코드
     */
    public static String s_producttype = PRODUCT_TYPE_M;

    public static String KEY_SPEC = "bfservicekey!@12";
    public static String HEADER_SECRET_KEY = "27e924340df23acc1776b1d07f9cd304356a4814";

    /**
     * 운영서버로 연결되었는가?
     *
     * @return true : 운영서버 or not : 테스트서버
     */
    public static boolean isReal() {
        return host.equals(HOST_REAL);
    }
}