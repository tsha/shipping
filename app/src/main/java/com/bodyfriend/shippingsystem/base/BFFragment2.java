package com.bodyfriend.shippingsystem.base;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.image.AUIL;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.CC;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

public class BFFragment2 extends Fragment implements Observer {

    protected Context mContext;
    private Resources mResources;
    // refresh
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    private Handler mHandler;

    private final int WHAT_SHOW_PROGRESS = 0;
    private final int WHAT_HIDE_PROGRESS = 1;

    private Handler.Callback mCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_SHOW_PROGRESS:
                    showProgress();
                    break;
                case WHAT_HIDE_PROGRESS:
                    hideProgress();
                    break;
            }
            return false;
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OH.c().addObserver(this);

        // ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        // setGlobalFont(root);
        mHandler = new Handler(mCallback);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        OH.c().deleteObserver(this);
    }

    private Class<?>[] mReloadType;

    protected void registerReloadObserver(Class<?>... types) {
        mReloadType = types;
    }

    @Override
    public void update(Observable observable, Object data) {
        if (OH.c() != observable)
            return;

        if (data instanceof OH.TYPE) {
            OH.TYPE type = (OH.TYPE) data;
            switch (type) {
                case EXIT:
                    Auth.d = null;
                    break;
                case LOGIN:
                    if (!(this instanceof CC.ILOGIN))
                        reload();
                    break;
                case SHOW_PROGRESS:
                    mHandler.sendEmptyMessage(WHAT_SHOW_PROGRESS);
                    break;
                case HIDE_PROGRESS:
                    mHandler.sendEmptyMessage(WHAT_HIDE_PROGRESS);
                    break;
                default:
                    break;
            }
        }

        if (mReloadType == null)
            return;

        if (!(data instanceof BFEnty))
            return;

        for (Class<?> reloadType : mReloadType) {
            if (reloadType.isInstance(data)) {
                Log.l(getClass().getSimpleName() + " reload ", data.getClass().getSimpleName());
                reload();
                return;
            }
        }
    }

    /**
     * 종료
     */
    protected void exit() {
        OH.c().notifyObservers(OH.TYPE.EXIT);
    }

    protected boolean isLoading = false;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Activity activity = getActivity();
        mContext = activity;
        mResources = mContext.getResources();

        debugSet();
        parseExtra();
        loadOnce();
        reload();
        updateUI();
    }

    private void debugSet() {
    }

    /**
     * Extra 파싱
     */
    final public void parseExtra() {
        try {
            onParseExtra();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 인스턴스등을 선언하는 부분
     */
    final public void loadOnce() {
        // Log.l("loadOnce", this);
        onLoadOnce();
    }

    /**
     * 데이터를 다시 로드하는 부분
     */
    final public void reload() {
        Log.l("BFFragment2 reload", this);
        onReload();
    }

    final public void clear() {
        try {
            onClear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 데이터 로드
     */
    final public void load() {
        try {
            onLoad();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * UI에 출력하는 부분
     */
    final public void updateUI() {
        try {
            onUpdateUI();
        } catch (Exception e) {
        }
    }

    protected void onParseExtra() {
    }

    protected void onLoadOnce() {
        setSwipeRefresh();
    }

    protected void onReload() {
        clear();
        load();
    }

    protected void onClear() {
    }

    protected void onLoad() {
    }

    protected void onUpdateUI() {
    }

    public boolean check() {
        return true;
    }

    protected static void setImage(ImageView imageview, String image, DisplayImageOptions options) {
        ImageLoader.getInstance().displayImage(image, imageview, options);
    }

    protected static void setImage(ImageView imageview, String image) {
        ImageLoader.getInstance().displayImage(image, imageview, AUIL.options);
    }


    protected void show() {

    }

    protected boolean isNotNull(Object... objects) {
        for (Object object : objects) {
            if (object == null) {
                return false;
            }
        }
        return true;
    }

    protected final void hideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected final boolean isEmpty(Object text) {
        if (text == null)
            return true;
        if (text instanceof String) {
            return ((String) text).trim().length() == 0;
        }
        return false;
    }


    @SuppressLint("NewApi")
    protected View findViewById(int id) {
        return Objects.requireNonNull(getView()).findViewById(id);
    }

    protected void setSpinner(int resid, CharSequence[] strings) {
        setSpinner(resid, Arrays.asList(strings));
    }

    protected <T> void setSpinner(int resid, List<T> objects) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof Spinner))
            throw new IllegalArgumentException("Spinner 불가능 컨트롤임");

        final Spinner spinner = (Spinner) v;

        ArrayAdapter<T> adapter = new ArrayAdapter<T>(getContext(), android.R.layout.simple_spinner_item, android.R.id.text1, objects);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    protected void setOnClickListener(int resid, View.OnClickListener onClickListener) {
        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");
        v.setOnClickListener(onClickListener);
    }

    protected String text(int resid) {

        final View v = findViewById(resid);
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;
        return tv.getText().toString();
    }

    protected void setText(int resid, CharSequence text) {
        setText(resid, text, TextView.BufferType.NORMAL);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void setText(int resid, CharSequence text, TextView.BufferType bufferType) {
        setText(findViewById(resid), text, bufferType);
    }

    protected void setText(View v, CharSequence text, TextView.BufferType bufferType) {
        if (v == null)
            throw new IllegalArgumentException("뭐야이건");

        if (!(v instanceof TextView))
            throw new IllegalArgumentException("텍스트 불가능 컨트롤임");

        final TextView tv = (TextView) v;

        tv.setText(text, bufferType);
    }


    public Dialog showDialog(View v, String msg, String button, DialogInterface.OnClickListener positiveListener) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(v);
        builder.setMessage(msg);
        builder.setPositiveButton(button, positiveListener);

        Dialog dlg = builder.create();
        dlg.show();

        mDialogs.add(dlg);

        return dlg;
    }

    public Dialog showDialog(View v) {
        Dialog dlg = new Dialog(mContext);
        dlg.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dlg.setContentView(v);
        dlg.show();
        mDialogs.add(dlg);

        return dlg;
    }

    public Dialog showDialog(Object message, Object positiveButtonText, DialogInterface.OnClickListener positiveListener) {
        return showDialog(null, null, message, null, positiveButtonText, positiveListener, null, null, null, null);
    }

    public Dialog showDialog(Object message,//
                             Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener) {
        return showDialog(null, null, message, null, positiveButtonText, positiveListener, null, null, negativeButtonText, negativeListener);
    }

    public Dialog showDialog(Object message,//
                             Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object neutralButtonText, DialogInterface.OnClickListener neutralListener//
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener//
    ) {
        return showDialog(null, null, message, null, positiveButtonText, positiveListener, neutralButtonText, neutralListener, negativeButtonText, negativeListener);
    }

    public Dialog showDialog(Object title, Object icon, Object message, View view//
            , Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object neutralButtonText, DialogInterface.OnClickListener neutralListener //
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener//
    ) {
        final Dialog dlg = getDialog(title, icon, message, view, positiveButtonText, positiveListener, neutralButtonText, neutralListener, negativeButtonText, negativeListener);
        dlg.show();
        return dlg;
    }

    public Dialog getDialog(Object title, Object icon, Object message, View view//
            , Object positiveButtonText, DialogInterface.OnClickListener positiveListener//
            , Object neutralButtonText, DialogInterface.OnClickListener neutralListener //
            , Object negativeButtonText, DialogInterface.OnClickListener negativeListener//
    ) {

        Context context = mContext;

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null)
            builder.setTitle(title instanceof String ? (String) title : mResources.getString((Integer) title));
        if (message != null)
            builder.setMessage(message instanceof String ? (String) message : mResources.getString((Integer) message));
        if (icon != null)
            builder.setIcon(icon instanceof Drawable ? (Drawable) icon : mResources.getDrawable((Integer) icon));
        if (view != null)
            builder.setView(view);
        if (positiveButtonText != null)
            builder.setPositiveButton(positiveButtonText instanceof String ? (String) positiveButtonText : mResources.getString((Integer) positiveButtonText), positiveListener);
        if (negativeButtonText != null)
            builder.setNegativeButton(negativeButtonText instanceof String ? (String) negativeButtonText : mResources.getString((Integer) negativeButtonText), negativeListener);
        if (neutralButtonText != null)
            builder.setNeutralButton(neutralButtonText instanceof String ? (String) neutralButtonText : mResources.getString((Integer) neutralButtonText), neutralListener);

        final AlertDialog dlg = builder.create();

        mDialogs.add(dlg);

        return dlg;
    }

    protected ArrayList<Dialog> mDialogs = new ArrayList<Dialog>();

    /**
     * SwipeRefreshLayout가 존재하면 리프레쉬가 가능하도록 설정
     */
    protected void setSwipeRefresh() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeColors(0xfffdd96a, 0xff4ec9b1, 0xff5cd2ff);
            mSwipeRefreshLayout.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    reload();
                }
            });
        }
    }

    public void showProgress() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
    }

    public void hideProgress() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    protected final void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public Dialog showTimePicker(TimePickerDialog.OnTimeSetListener onTimeSetListener, int hourOfDay, int minutOfHour) {
        TimePickerDialog timePickerDialog;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePickerDialog = new TimePickerDialog(mContext, R.style.TimePicker, onTimeSetListener, hourOfDay, minutOfHour, false);
        } else {
            timePickerDialog = new TimePickerDialog(mContext, onTimeSetListener, hourOfDay, minutOfHour, false);
        }

        timePickerDialog.updateTime(hourOfDay, minutOfHour);
        timePickerDialog.show();
        mDialogs.add(timePickerDialog);
        return timePickerDialog;
    }

    public Dialog showDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener, Calendar calendar) {
        return showDatePicker(onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }


    public Dialog showDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener, int year, int monthOfYear, int dayOfMonth) {
        DatePickerDialog datePickerDialog;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            datePickerDialog = new DatePickerDialog(mContext, R.style.TimePicker, onDateSetListener, year, monthOfYear, dayOfMonth);
        } else {
            datePickerDialog = new DatePickerDialog(mContext, onDateSetListener, year, monthOfYear, dayOfMonth);
        }

        datePickerDialog.updateDate(year, monthOfYear, dayOfMonth);
        datePickerDialog.show();
        mDialogs.add(datePickerDialog);
        return datePickerDialog;
    }

    protected void setVisibility(View v, int visibility) {
        if (v != null)
            v.setVisibility(visibility);
    }

    protected void setVisibility(View v, boolean visibility) {
        setVisibility(v, visibility ? View.VISIBLE : View.GONE);
    }

    protected void setVisibility(int resid, int visibility) {
        final View v = findViewById(resid);
        setVisibility(v, visibility);
    }

    protected void setVisibility(int resid, boolean visibility) {
        setVisibility(resid, visibility ? View.VISIBLE : View.GONE);
    }

    public void toast(Object foramt, Object... args) {
        String text;
        if (foramt instanceof Integer)
            text = mContext.getString((Integer) foramt, args);
        else if (foramt instanceof String)
            text = String.format((String) foramt, args);
        else
            text = foramt.toString();

        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }
}
